/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:09:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_id` int NOT NULL COMMENT 'ID của loại setting',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'title của setting',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'giá trị của setting',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT 'miêu tả',
  `status` enum('active','inactive') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT 'active' COMMENT ' active: đang hoạt động, inactive: đã ngừng hoạt động',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_TYPE_ID` (`type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
BEGIN;
INSERT INTO `settings` VALUES (1, 0, 'setting', 'setting', NULL, 'active', '2022-06-13 16:37:21', 1, '2022-06-13 16:37:21', 1);
INSERT INTO `settings` VALUES (2, 1, 'role', 'Role', NULL, 'active', '2022-06-13 16:35:52', 1, '2022-06-19 02:43:09', 1);
INSERT INTO `settings` VALUES (7, 2, 'admin', 'Admin', '', 'active', '2022-06-13 16:31:34', 1, '2022-08-24 12:00:46', 1);
INSERT INTO `settings` VALUES (8, 2, 'author', 'Author', NULL, 'active', '2022-06-13 16:33:01', 1, '2022-08-20 12:57:38', 1);
INSERT INTO `settings` VALUES (9, 2, 'trainer', 'Trainer', NULL, 'active', '2022-06-13 16:31:34', 1, '2022-06-18 19:28:09', 1);
INSERT INTO `settings` VALUES (10, 2, 'student', 'Student', NULL, 'active', '2022-06-13 16:33:01', 1, '2022-08-25 03:29:47', 1);
INSERT INTO `settings` VALUES (11, 69, 'Complexity ', 'Complexity ', NULL, 'active', '2022-08-20 09:04:06', 1, '2022-08-20 09:04:06', 1);
INSERT INTO `settings` VALUES (12, 69, 'Quality ', 'Quality ', NULL, 'active', '2022-08-20 09:04:06', 1, '2022-08-20 19:59:18', 1);
INSERT INTO `settings` VALUES (13, 1, 'Subject evaluation weight', 'Subject evaluation weight', NULL, 'active', '2022-06-13 16:40:34', 1, '2022-08-17 17:52:42', 1);
INSERT INTO `settings` VALUES (68, 70, 'Function status', 'function status', NULL, 'active', '2022-08-13 13:57:59', 1, '2022-08-16 17:37:56', 1);
INSERT INTO `settings` VALUES (69, 1, 'Subject Setting', 'Subject Setting', NULL, 'active', '2022-08-13 13:57:59', 1, '2022-08-13 13:57:59', 1);
INSERT INTO `settings` VALUES (70, 1, 'Class Setting', 'Class Setting', NULL, 'active', '2022-08-13 13:57:59', 1, '2022-08-16 17:37:56', 1);
INSERT INTO `settings` VALUES (71, 70, 'Label ', 'Label ', NULL, 'active', '2022-08-13 13:57:59', 1, '2022-08-16 18:02:18', 1);
INSERT INTO `settings` VALUES (76, 70, 'Function Type', 'Function Type', NULL, 'active', '2022-08-20 09:04:06', 1, '2022-08-20 09:04:06', 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
