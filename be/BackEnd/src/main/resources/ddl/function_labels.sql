/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:08:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for function_labels
-- ----------------------------
DROP TABLE IF EXISTS `function_labels`;
CREATE TABLE `function_labels` (
  `id` int NOT NULL AUTO_INCREMENT,
  `function_id` int NOT NULL,
  `label_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
