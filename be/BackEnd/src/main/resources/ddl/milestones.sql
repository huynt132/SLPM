/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:09:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for milestones
-- ----------------------------
DROP TABLE IF EXISTS `milestones`;
CREATE TABLE `milestones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `iteration_id` int NOT NULL,
  `class_id` int NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT 'miêu tả',
  `from` date DEFAULT NULL COMMENT 'Ngày bắt đầu',
  `to` date DEFAULT NULL COMMENT 'Ngày kết thúc',
  `status` enum('open','closed','cancelled') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT 'open' COMMENT 'open: mở, closed: đã đóng, cancelled: đã hủy',
  `gitlab_id` int DEFAULT NULL COMMENT 'id của milestone lưu trên gitlab',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_ITERATION_ID` (`iteration_id`) USING BTREE,
  KEY `IDX_CLASSS_ID` (`class_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
