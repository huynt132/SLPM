/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:09:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for project_milestones
-- ----------------------------
DROP TABLE IF EXISTS `project_milestones`;
CREATE TABLE `project_milestones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NOT NULL COMMENT 'khóa ngoại đến bảng projects',
  `milestone_id` int NOT NULL COMMENT 'khóa ngoại đến bảng milestones',
  `gitlab_milestone_id` int NOT NULL COMMENT 'milestone id trên gitlab của project này',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UNIQUE` (`project_id`,`milestone_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
