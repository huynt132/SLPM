/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:08:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for iteration_evaluations
-- ----------------------------
DROP TABLE IF EXISTS `iteration_evaluations`;
CREATE TABLE `iteration_evaluations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `milestone_id` int NOT NULL,
  `class_user_id` int NOT NULL COMMENT 'Khóa ngoại đến bảng class_users mang ý nghĩa điẻm iteration thuộc về học sinh của lớp nào, nhóm nào',
  `bonus` decimal(4,2) DEFAULT NULL COMMENT 'điểm bonus của học sinh',
  `grade` decimal(4,2) DEFAULT NULL COMMENT 'điểm trên thang điểm 10 của iterations được tự động input data vào từ bảng memeber_evaluation',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_MILESTONE_ID` (`milestone_id`) USING BTREE,
  KEY `IDX_CLASS_USER_ID` (`class_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=468 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
