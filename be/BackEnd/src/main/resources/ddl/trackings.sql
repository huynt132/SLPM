/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:10:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for trackings
-- ----------------------------
DROP TABLE IF EXISTS `trackings`;
CREATE TABLE `trackings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `milestone_id` int NOT NULL,
  `function_id` int NOT NULL,
  `assigner_id` int NOT NULL COMMENT 'Người gán người xử lý',
  `assignee_id` int NOT NULL COMMENT 'Người xử lý',
  `status` enum('pending','committed','submitted','evaluated','rejected') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT 'pending',
  `note` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_time` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_FUNCTION_ID` (`function_id`) USING BTREE,
  KEY `IDX_MILESTONE_ID` (`milestone_id`) USING BTREE,
  KEY `IDX_ASSIGNEE_ID` (`assignee_id`) USING BTREE,
  KEY `IDX_ASSIGNER_ID` (`assigner_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
