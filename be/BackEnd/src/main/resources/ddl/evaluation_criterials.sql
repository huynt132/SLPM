/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:08:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for evaluation_criterials
-- ----------------------------
DROP TABLE IF EXISTS `evaluation_criterials`;
CREATE TABLE `evaluation_criterials` (
  `id` int NOT NULL AUTO_INCREMENT,
  `iteration_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tên của tiêu chí đánh giá',
  `evaluation_weight` decimal(3,2) NOT NULL COMMENT 'hệ số tính điẻm (Ví dụ: 10% = 0,1, 50% = 0.5)',
  `guide` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT 'miêu tả',
  `max_loc` smallint unsigned DEFAULT NULL COMMENT 'Tổng số điểm tối đa có thể đạt được',
  `type` enum('team','demo','individual') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'Loại tiêu chí đánh giá: team thì sẽ đẩy về team_evaluations, demo thì sẽ đẩy về member_evaluations còn lại thì trainer sẽ edit trực tiếp',
  `status` enum('active','inactive') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT 'active',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_ITERATION_ID` (`iteration_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
