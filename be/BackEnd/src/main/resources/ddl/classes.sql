/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:08:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(50) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL COMMENT 'Mã lớp học',
  `trainer_id` int DEFAULT NULL,
  `subject_id` int NOT NULL COMMENT 'id môn học',
  `year` smallint NOT NULL COMMENT 'năm học',
  `term` enum('spring','summer','fall') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'Kỳ học',
  `gitlab_group_id` int DEFAULT NULL,
  `gitlab_group_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','closed','cancelled') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT 'active' COMMENT ' active: đang hoạt động,closed: đã hết thời gian lớp diễn ra, cancelled: đã hủy',
  `block5_class` tinyint(1) DEFAULT '0' COMMENT '0: không phải là lớp thuộc bl5, 1: là lớp thuộc bl5 của kỳ',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_TRAINER_ID` (`trainer_id`) USING BTREE,
  KEY `IDX_SUBJECT_ID` (`subject_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
