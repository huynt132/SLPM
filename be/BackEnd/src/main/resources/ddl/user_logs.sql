/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:10:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_logs
-- ----------------------------
DROP TABLE IF EXISTS `user_logs`;
CREATE TABLE `user_logs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `group_type` varchar(20) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'Nhóm tính năng',
  `action` varchar(40) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'Tên action thao tác',
  `desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'Miêu tả',
  `ref_id` int DEFAULT NULL COMMENT 'Id bản ghi liên quan đến log',
  `ref_column_name` varchar(40) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL COMMENT 'Tên cột thao tác tương ứng',
  `ref_table_name` varchar(40) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL COMMENT 'Tên bảng thao tác tương ứng',
  `old_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'Giá trị bản ghi cũ',
  `new_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'Giá trị bản ghi mới',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'Người tạo log',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ID` (`ref_id`),
  KEY `IDX_COLUMN` (`ref_column_name`),
  KEY `IDX_TABLE` (`ref_table_name`),
  KEY `IDX_ACTION` (`action`)
) ENGINE=InnoDB AUTO_INCREMENT=368 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
