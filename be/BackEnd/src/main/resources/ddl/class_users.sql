/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:08:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for class_users
-- ----------------------------
DROP TABLE IF EXISTS `class_users`;
CREATE TABLE `class_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class_id` int NOT NULL COMMENT 'id của lớp học mà học viên tham gia',
  `project_id` int NOT NULL COMMENT 'id của nhóm mà học viên tham gia',
  `user_id` int NOT NULL COMMENT 'id của học viên trong lớp học',
  `project_leader` tinyint(1) DEFAULT '0' COMMENT '0: không phải là leader, 1: là leader của nhóm',
  `dropout_date` date DEFAULT NULL COMMENT 'Ngày học viên nghỉ ngang',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ghi chú',
  `ongoing_eval` decimal(4,2) DEFAULT '0.00',
  `final_eval` decimal(4,2) DEFAULT '0.00',
  `final_topic_eval` decimal(4,2) DEFAULT '0.00' COMMENT 'Điểm tổng kết',
  `status` enum('active','inactive') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT 'active' COMMENT ' active: đang hoạt động, inactive: ngừng hoạt động',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_CLASS_ID` (`class_id`) USING BTREE,
  KEY `IDX_TEAM_ID` (`project_id`) USING BTREE,
  KEY `IDX_USER_ID` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
