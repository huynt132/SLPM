/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:08:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for loc_evaluations
-- ----------------------------
DROP TABLE IF EXISTS `loc_evaluations`;
CREATE TABLE `loc_evaluations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `milestone_id` int DEFAULT NULL,
  `function_id` int DEFAULT NULL,
  `complexity_id` int DEFAULT NULL COMMENT 'số loc nhận đực dựa theo độ khó của function, Khóa ngoại đến bảng subject_settings với các bản ghi của type là quality',
  `quality_id` int DEFAULT NULL COMMENT 'hệ số nhận điểm, Khóa ngoại đến bảng subject_settings với các bản ghi của type là quality',
  `converted_loc` smallint DEFAULT NULL COMMENT 'Tổng số loc',
  `is_late_submit` tinyint(1) DEFAULT '0' COMMENT 'submit muộn',
  `comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_milestone_id` int DEFAULT NULL,
  `new_complexity_id` int DEFAULT NULL,
  `new_quality_id` int DEFAULT NULL,
  `new_converted_loc` int DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_COMPLEXITY_ID` (`complexity_id`) USING BTREE,
  KEY `IDX_QUALITY_ID` (`quality_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
