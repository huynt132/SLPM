/*
 Navicat Premium Data Transfer

 Source Server         : SLPM
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 34.142.228.186:3306
 Source Schema         : SLPM

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 30/08/2022 10:08:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for functions
-- ----------------------------
DROP TABLE IF EXISTS `functions`;
CREATE TABLE `functions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `feature_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tên chức năng',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT 'miêu tả',
  `priority` int DEFAULT '0' COMMENT 'thứ tự ưu tiên',
  `status` enum('pending','committed','submitted','evaluated','rejected') CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT 'pending',
  `type_id` int DEFAULT NULL COMMENT 'id của function type lưu trong bảng class setting',
  `status_id` int DEFAULT NULL COMMENT 'id của status lưu trong bảng class setting',
  `gitlab_id` int DEFAULT NULL COMMENT 'id của functions lưu trên gitlab',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL COMMENT 'id người tạo',
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_FEATURE_ID` (`feature_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
