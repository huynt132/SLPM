package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class SubjectConfigDTO {
    private Integer subjectId;
    private String subjectCode;
    private String subjectName;
    private Integer authorId;
    private String subjectStatus;

    private Integer iterationId;
    private BigDecimal iterationWeight;
    private Integer iterationIsOngoing;
    private String iterationStatus;
    private String iterationName;

    private Integer criteriaId;
    private BigDecimal criteriaWeight;
//    private Integer criteriaIsTeamEvaluation;
    private String criteriaStatus;
    private String criteriaType;

    private Integer subjectSettingId;
    private Integer subjectSettingTypeId;
    private String subjectSettingValue;

    public SubjectConfigDTO(Integer subjectId, String subjectCode, String subjectName, Integer authorId,
                            String subjectStatus, Integer iterationId, BigDecimal iterationWeight,
                            Integer iterationIsOngoing, String iterationStatus, Integer criteriaId,
                            BigDecimal criteriaWeight, String criteriaType, String criteriaStatus,
                            Integer subjectSettingId, Integer subjectSettingTypeId, String subjectSettingValue,
                            String iterationName) {
        this.subjectId = subjectId;
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
        this.authorId = authorId;
        this.subjectStatus = subjectStatus;
        this.iterationId = iterationId;
        this.iterationWeight = iterationWeight;
        this.iterationIsOngoing = iterationIsOngoing;
        this.iterationStatus = iterationStatus;
        this.criteriaId = criteriaId;
        this.criteriaWeight = criteriaWeight;
        this.criteriaType = criteriaType;
        this.criteriaStatus = criteriaStatus;
        this.subjectSettingId = subjectSettingId;
        this.subjectSettingTypeId = subjectSettingTypeId;
        this.subjectSettingValue = subjectSettingValue;
        this.iterationName = iterationName;
    }
}
