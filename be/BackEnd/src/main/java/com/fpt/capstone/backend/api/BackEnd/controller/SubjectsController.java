package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingOptionDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectsListDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.service.impl.SubjectsServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("subject")
@CrossOrigin(origins = "*", maxAge = 3600)
//@PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
public class SubjectsController {
    @Autowired
    private SubjectsServiceImpl subjectsService;

    @Autowired
    private ModelMapper modelMapper;

    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @PostMapping("/add")
    public ResponseEntity<?> addSubject(@RequestBody SubjectInputDTO subjectInputDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add subject successfully");
            response.setData(subjectsService.addSubjects(subjectInputDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add subject fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @GetMapping("/delete")
    public ResponseEntity<?> deleteSubject(@RequestParam(name = "id") String id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Delete subject successfully");
            subjectsService.deleteSubjects(Integer.parseInt(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Delete subject fail " + "Message:" + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getList")
    public ResponseEntity<?> getListSubject() {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get subject list successfully");
            response.setData(subjectsService.showSubjectsList());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get subject list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/findById")
    public ResponseEntity<?> findSubjectByID(@RequestParam(name = "id") int id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get subject successfully");
            response.setData(subjectsService.findById(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get subject fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @PostMapping("/edit")
    public ResponseEntity<?> editSubject(@RequestBody SubjectInputDTO subjectInputDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update subject successfully");
            response.setData(subjectsService.updateSubject(subjectInputDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update subject fail," + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    //    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @GetMapping("/getAll")
    public ResponseEntity<?> getSettingActive() {
        ApiResponse response = new ApiResponse();
        try {
            List<SubjectsListDTO> subjects = subjectsService.listBy();
            response.setSuccess(true);
            response.setMessage("Get subject list successfully");
            response.setData(subjects);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get subject list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/search")
    public ResponseEntity<?> findSettingBy(
            @RequestParam(value = "subjectCode", required = false) String key_code,
            @RequestParam(value = "subjectName", required = false) String key_name,
            @RequestParam(value = "authorId", required = false) List<Integer> authorId,
            @RequestParam(value = "status", required = false) String key_status,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            Page<SubjectsDTO> subjects = subjectsService.searchBy(key_code, key_name, authorId, key_status, page, per_page, response);
            List<SubjectsDTO> subjectsDTOS = subjects.getContent();

            response.setSuccess(true);
            response.setMessage("Get subject list successfully");
            response.setData(subjectsDTOS);
            response.setTotal(subjects.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get subject list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getSubjectSetting")
    public ResponseEntity<?> getSubjectSetting(
            @RequestParam(value = "classId", required = false) Integer classId,
            @RequestParam(value = "subjectId", required = false) Integer subjectId,
            @RequestParam(value = "type", required = false) String type) {
        ApiResponse response = new ApiResponse();
        try {
            List<ClassSettingOptionDTO> subjects = subjectsService.getSubjectSetting(subjectId, type, classId);
            response.setSuccess(true);
            response.setMessage("Get subject list successfully");
            response.setData(subjects);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get subject list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
