package com.fpt.capstone.backend.api.BackEnd.dto.loc_evaluations;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocEvaluationsDTO {
    private Integer id;
    private Integer milestoneId;
    private Integer functionId;
    private String functionName;
    private Integer featureId;
    private String featureName;
    private Integer complexityId;
    private String complexityTitle;
    private String complexityValue;
    private Integer qualityId;
    private String qualityTitle;
    private String qualityValue;
    private String status;
    private Integer convertedLoc;
    private Byte isLateSubmit;
    private String comment;
    private Integer newMilestoneId;
    private Integer newComplexityId;
    private Integer newQualityId;
    private Integer newConvertedLoc;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date created;
    private Integer createdBy;
    private String createdByName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date modified;
    private Integer modifiedBy;
    private String modifiedByName;

    public LocEvaluationsDTO(Integer id, Integer milestoneId, Integer functionId, String functionName, Integer featureId,
                             String featureName, Integer complexityId, String complexityTitle, String complexityValue,
                             Integer qualityId, String qualityTitle, String qualityValue, Integer convertedLoc,
                             Byte isLateSubmit, String comment, Integer newMilestoneId, Integer newComplexityId,
                             Integer newQualityId, Integer newConvertedLoc, String status) {
        this.id = id;
        this.milestoneId = milestoneId;
        this.functionId = functionId;
        this.functionName = functionName;
        this.featureId = featureId;
        this.featureName = featureName;
        this.complexityId = complexityId;
        this.complexityTitle = complexityTitle;
        this.complexityValue = complexityValue;
        this.qualityId = qualityId;
        this.qualityTitle = qualityTitle;
        this.qualityValue = qualityValue;
        this.convertedLoc = convertedLoc;
        this.isLateSubmit = isLateSubmit;
        this.comment = comment;
        this.newMilestoneId = newMilestoneId;
        this.newComplexityId = newComplexityId;
        this.newQualityId = newQualityId;
        this.newConvertedLoc = newConvertedLoc;
    }
}
