package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.class_users.ClassUserInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.class_users.ClassUsersDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ClassUserResponse;
import com.fpt.capstone.backend.api.BackEnd.service.ClassUserService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("classUser")
@CrossOrigin(origins = "*", maxAge = 3600)
//@PreAuthorize("hasAuthority('Admin')")
public class ClassUserController {
    @Autowired
    private ClassUserService classUserService;

    @Autowired
    private PermissionService permissionService;

    @PostMapping("/add")
    public ResponseEntity<?> addClassUser(@RequestBody ClassUserInputDTO classUserInput) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add class user successfully");
            response.setData(classUserService.add(classUserInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add class user fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editClassUser(@RequestBody ClassUserInputDTO classUserInput) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Edit class user successfully");
            response.setData(classUserService.update(classUserInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Edit class user fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/detail")
    public ResponseEntity<?> getDetailClasses(@RequestParam(name = "id") int id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get detail class user successfully");
            response.setData(classUserService.showDetail(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get detail class user fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findClasses(
            @RequestParam(value = "getExcel", defaultValue = "false", required = false) String getExcel,
            @RequestParam(value = "classId", required = false) List<Integer> classId,
            @RequestParam(value = "projectId", required = false) List<Integer> projectId,
            @RequestParam(value = "userId", required = false) List<Integer> userId,
            @RequestParam(value = "projectLead", required = false) Integer projectLead,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam("page") int page, @RequestParam("limit") int limit) {

        ClassUserResponse response = new ClassUserResponse();
        try {
            if (classId == null) {
                List<Integer> authoritySubjectIds = permissionService.getClassAuthor(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurClass(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }

                classId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurClass(classId.get(0));
            Page<ClassUsersDTO> classesUsers = classUserService.searchBy(classId, projectId, userId, projectLead, status, page, limit);
            List<ClassUsersDTO> classesUserDTOs = classesUsers.getContent();
            if (getExcel.equals("true")) {
                Page<ClassUsersDTO> classesUsersExcel = classUserService.searchBy(classId, projectId, userId, projectLead, status, page, (int) classesUsers.getTotalElements());
                List<ClassUsersDTO> classesUserDTOExcel = classesUsersExcel.getContent();
                response.setSuccess(true);
                response.setMessage("Export function data successfully");
                response.setData(classUserService.exportClassUser(classesUserDTOExcel));
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
            response.setSuccess(true);
            response.setMessage("Get class user list successfully");
            response.setData(classesUserDTOs);
            response.setTotal(classesUsers.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(limit);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get class user list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/import")
    public ResponseEntity<?> importExcel(@RequestParam("file") MultipartFile fileStream) {
        ApiResponse response = new ApiResponse();
        try {
            return classUserService.importClassUser(fileStream);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Import excel fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
