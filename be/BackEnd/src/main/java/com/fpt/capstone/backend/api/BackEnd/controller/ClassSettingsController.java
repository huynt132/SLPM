package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingOptionDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.service.ClassSettingsService;
import com.fpt.capstone.backend.api.BackEnd.service.SettingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("classSetting")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ClassSettingsController {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private SettingService settingService;
    @Autowired
    private ClassSettingsService classSettingsService;

    @PostMapping("/add")
    public ResponseEntity<?> addClasses(@RequestBody ClassSettingsDTO classSettingsDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add class setting successfully");
            response.setData(classSettingsService.add(classSettingsDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add class setting fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editSubject(@RequestBody ClassSettingsDTO classSettingsDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update class setting successfully");
            response.setData(classSettingsService.update(classSettingsDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update class setting fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findClasses(
            @RequestParam(value = "classId", required = false) List<Integer> classId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "titleOrValue", required = false) String titleOrValue,
            @RequestParam("page") int page, @RequestParam("limit") int limit) {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            Page<ClassSettingsDTO> classSettingsDTOPage = classSettingsService.searchBy(classId, status, titleOrValue, page, limit);
            List<ClassSettingsDTO> classesDTOS = Arrays.asList(modelMapper.map(classSettingsDTOPage.getContent(), ClassSettingsDTO[].class));
            response.setSuccess(true);
            response.setMessage("Get class list successfully");
            response.setData(classesDTOS);
            response.setTotal(classSettingsDTOPage.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(limit);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get class list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/getOptions")
    public ResponseEntity<?> getOptions(
            @RequestParam(value = "type") String type,
            @RequestParam(value = "classId") Integer classId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get options successfully");
            List<ClassSettingOptionDTO> options = classSettingsService.getOptions(type, classId);
            response.setData(options);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get options fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getListSetting")
    public ResponseEntity<?> getListClassSetting() {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get setting detail successfully");
            response.setData(settingService.getSettingList());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get setting detail fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


}
