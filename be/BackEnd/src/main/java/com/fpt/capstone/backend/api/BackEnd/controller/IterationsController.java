package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationsInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.IterationResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.service.IterationsService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("iterations")
@CrossOrigin(origins = "*", maxAge = 3600)
//@PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
public class IterationsController {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IterationsService iterationsService;
    @Autowired
    private PermissionService permissionService;

    @PostMapping("/add")
    public ResponseEntity<?> addIteration(@RequestBody IterationsInputDTO iterationsInputDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add iteration successfully");
            response.setData(iterationsService.addIterations(iterationsInputDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add iteration fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/inactive")
    public ResponseEntity<?> deleteIteration(@RequestParam(name = "id") String id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update iteration status successfully");
            response.setData(iterationsService.deleteIterations(Integer.parseInt(id)));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update iteration status fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getList")
    public ResponseEntity<?> showListIterations() {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            response.setSuccess(true);
            response.setMessage("Get iteration list successfully");
            response.setData(iterationsService.showIterationList());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get iteration list fail:" + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getIterations")
    public ResponseEntity<?> showListSearchIterations(
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "classId", required = false) Integer classId,
            @RequestParam(value = "subjectId", required = false) Integer subjectId
    ) {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            response.setSuccess(true);
            response.setMessage("Get iteration list successfully");
            response.setData(iterationsService.listIteration(status, subjectId, classId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get iteration list fail:" + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/findById")
    public ResponseEntity<?> findIterationsByID(@RequestParam(name = "id") String id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get iteration successfully");
            response.setData(iterationsService.getIterationDetail(Integer.parseInt(id)));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get iteration fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findIterationsByTile(
            @RequestParam(value = "subjectId", required = false) List<Integer> subjectId,
            @RequestParam(value = "manageId", required = false) List<Integer> manageId,
            @RequestParam(value = "iterationName", required = false) String iterationName,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam("page") int page, @RequestParam("limit") int limit) {
        IterationResponse response = new IterationResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get iteration list successfully");
            // Không truyền subject thì sẽ tự động lấy subject đầu tiên theo auth
            if (subjectId == null) {
                List<Integer> authoritySubjectIds = permissionService.getSubjectAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurSubject(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }

                subjectId = Arrays.asList(authoritySubjectIds.get(0));
            }

            response.setCurSubject(subjectId.get(0));

            Page<IterationsDTO> iterations = iterationsService.listBy(subjectId, iterationName, manageId, status, page, limit);
            List<IterationsDTO> iterationsDTOS = Arrays.asList(modelMapper.map(iterations.getContent(), IterationsDTO[].class));

            if (iterationsDTOS.size() > 0) {
                response.setTotalWeight(iterationsService.getTotalWeightInSubject(iterationsDTOS.get(0).getSubjectId()));
            }
            response.setData(iterationsDTOS);
            response.setTotal(iterations.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(limit);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get iteration list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editIterations(@RequestBody IterationsInputDTO iterationsInputDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update iteration successfully");
            response.setData(iterationsService.updateIterations(iterationsInputDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update iteration fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
