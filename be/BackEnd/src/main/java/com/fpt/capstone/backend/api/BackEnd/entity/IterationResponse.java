package com.fpt.capstone.backend.api.BackEnd.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class IterationResponse {
    private boolean success;
    private String message;
    private Object data;
    private Integer curSubject;
    private Integer perPages;
    private Integer currentPage;
    private Long total;
    private BigDecimal totalWeight;
}
