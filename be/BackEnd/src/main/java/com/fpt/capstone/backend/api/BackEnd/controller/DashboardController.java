package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.MilestoneResponse;
import com.fpt.capstone.backend.api.BackEnd.service.DashboardService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("dashboard")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DashboardController {
    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private PermissionService permissionService;
    @GetMapping("/getClassStatistic")
    public ResponseEntity<?> getClassStatistic(
            @RequestParam(name = "classId", required = false) Integer classId,
            @RequestParam(name = "milestoneId", required = false) List<Integer> milestoneId)
    {
        MilestoneResponse response = new MilestoneResponse();

        try {
            response.setSuccess(true);
            response.setMessage("Get class statistic successfully");
            if (classId == null) {
                List<Integer> authorityClassIds = permissionService.getClassAuthor(null);
                if (authorityClassIds == null || authorityClassIds.isEmpty() || authorityClassIds.get(0) == -1) return new ResponseEntity<>(response, HttpStatus.OK);
                classId = authorityClassIds.get(0);
            }
            response.setCurClassId(classId);

            response.setData(dashboardService.getClassStatistic(classId, milestoneId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get class statistic fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getTeamStatistic")
    public ResponseEntity<?> getTeamStatistic(
            @RequestParam(name = "projectId", required = false) Integer projectId,
            @RequestParam(name = "milestoneId", required = false) List<Integer> milestoneId)
    {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get team statistic successfully");
            response.setData(dashboardService.getTeamStatistic(projectId, milestoneId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get team statistic fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
