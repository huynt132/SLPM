package com.fpt.capstone.backend.api.BackEnd.repository;


import com.fpt.capstone.backend.api.BackEnd.dto.EvaluationCriteriaDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.EvaluationCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface EvaluationCriteriaRepository extends JpaRepository<EvaluationCriteria, Integer> {
    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.EvaluationCriteriaDTO(e.id,e.iterationId,i.name,s.id, " +
            "e.evaluationWeight,e.maxLoc,e.status,e.created,e.created_by,e.modified,e.modified_by," +
            "u1.fullName,u2.fullName, e.name, e.guide, e.type)" +
            " FROM EvaluationCriteria e   " +
            " join Users u1 on u1.id= e.created_by " +
            " join Iterations i on i.id=e.iterationId " +
            " join Subjects s on s.id=i.subjectId " +
            " join Users u2 on u2.id=e.modified_by " +
            " WHERE (COALESCE(:subjectId) is null or s.id IN :subjectId)" +
            " AND (COALESCE(:iterationId) is null or i.id IN :iterationId)" +
            " AND (COALESCE(:authorId) is null or s.authorId IN :authorId)" +
            " AND (COALESCE(:status) is null or e.status IN :status)" +
            " AND (COALESCE(:type) is null or e.type IN :type)" +
            " AND (COALESCE(:authorityCriteriaIds) is null or e.id IN :authorityCriteriaIds)" +
            " order by i.id")
    Page<EvaluationCriteriaDTO> search(@Param("subjectId") List<Integer> subjectId,
                                       @Param("iterationId") List<Integer> iterationId,
                                       @Param("authorId") List<Integer> authorId,
                                       @Param("status") String status,
                                       @Param("type") String type,
                                       Pageable pageable,
                                       @Param("authorityCriteriaIds") List<Integer> authorityCriteriaIds);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.EvaluationCriteriaDTO(e.id,e.iterationId,i.name,s.id, " +
            "e.evaluationWeight,e.maxLoc,e.status,e.created,e.created_by,e.modified,e.modified_by," +
            "u1.fullName,u2.fullName, e.name, e.guide, e.type)" +
            " FROM EvaluationCriteria e   " +
            " LEFT join Users u1 on u1.id= e.created_by " +
            " join Iterations i on i.id=e.iterationId" +
            " join Subjects s on s.id=i.subjectId " +
            " LEFT join Users u2 on u2.id=e.modified_by " +
            "  WHERE e.id =:id")
    EvaluationCriteriaDTO getEvaluationCriteriaDetail(int id);

    @Query("SELECT sum(ec.evaluationWeight) FROM EvaluationCriteria ec " +
            "WHERE ec.iterationId =:iterationId AND ec.status = 'active' " +
            "AND (COALESCE(:evaluationCriteria) is null or ec.id <> :evaluationCriteria)")
    BigDecimal getTotalEvaluationWeight(Integer iterationId, Integer evaluationCriteria);

    @Query("select ec.id from EvaluationCriteria  ec " +
            "left join Iterations i on i.id = ec.iterationId " +
            "left join Classes c on c.subjectId =i.subjectId" +
            " where c.trainerId=:trainerId")
    List<Integer> getEvaluationTrainer(Integer trainerId);
    @Query("select ec.id from EvaluationCriteria  ec")
    List<Integer> getEvaluationAdmin();

    @Query("Select ec.id from EvaluationCriteria ec where " +
            "ec.type = 'demo' " +
            "and (COALESCE(:id) is null or ec.id <> :id) " +
            "and (COALESCE(:iterationId) is null or ec.iterationId = :iterationId) " +
            "and ec.status = 'active'")
    List<Integer>getLocEvalCriteria(@Param("id") Integer id, @Param("iterationId") Integer iterationId);
}
