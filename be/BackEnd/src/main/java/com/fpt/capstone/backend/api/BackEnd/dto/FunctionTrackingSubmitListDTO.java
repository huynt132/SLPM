package com.fpt.capstone.backend.api.BackEnd.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Data
@NoArgsConstructor
public class FunctionTrackingSubmitListDTO {
    private Integer functionId;
    private String functionName;
    private Integer featureId;
    private String featureName;
    private String status;
    private Integer assigneeId;
    private String assignee;
    private String assigneeName;
    private String rollNumber;
    @JsonIgnore
    private String iterationName;

    private Integer projectId;

    private String projectName;

    private Integer key;


    public FunctionTrackingSubmitListDTO(Integer functionId, String functionName, Integer featureId, String featureName, String status, Integer assigneeId, String assigneeName, String rollNumber, String iterationName, Integer projectId, String projectName) {
        this.functionId = functionId;
        this.functionName = functionName;
        this.featureId = featureId;
        this.featureName = featureName;
        this.status = status;
        this.assigneeId = assigneeId;
        this.assigneeName = assigneeName;
        this.rollNumber = rollNumber;
        this.iterationName = iterationName;
        this.projectId = projectId;
        this.projectName = projectName;
        this.assignee = formatUserString(assigneeName);
        this.key=functionId;
    }

    private Boolean chosen = false;
    public static String removeAccent(String s) {

        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
         return pattern.matcher(temp).replaceAll("").replace('đ','d').replace('Đ','D');
    }
    public String formatUserString(String fullName) {
        String[] parts = fullName.split("@");
        return parts[0];

    }
}
