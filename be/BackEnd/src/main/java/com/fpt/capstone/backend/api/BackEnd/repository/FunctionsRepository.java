package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.entity.Functions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface FunctionsRepository extends JpaRepository<Functions, Integer> {

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionsDTO(fc.id,p.id,p.code,p.name,ft.id,ft.name,fc.name," +
            "fc.description,fc.priority,fc.typeId,cs1.title,fc.statusId,cs.title,fc.status,fc.gitLabId,fc.created,fc.created_by,fc.modified,fc.modified_by,u2.email,u2.rollNumber,u3.email,u3.rollNumber)" +
            " FROM Functions fc   " +
            " join Features ft on ft.id=fc.featureId " +
            " join Projects p on p.id=ft.projectId " +
            " join Users u2 on u2.id= fc.created_by " +
            " join Users u3 on u3.id=fc.modified_by " +
            " left join ClassSettings cs on ( fc.statusId=cs.id ) " +
            " left join ClassSettings cs1 on ( fc.typeId=cs1.id )" +
            "  WHERE fc.id =:id")
    FunctionsDTO getFunctionDetail(int id);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionsDTO(f.id, t.assigneeId, f.name, " +
            "f.description, t.note, le.convertedLoc, le.comment, t.status, le.complexityId, le.qualityId, " +
            "ss1.value, ss2.value, u.rollNumber, u.fullName, i.name, p.name, p.classId, le.id, t.id, m.id, p.id, c.code) " +
            "FROM Functions f " +
            "JOIN Trackings t on (f.id = t.functionId) " +
            "LEFT JOIN LocEvaluations le on (le.milestoneId = t.milestoneId AND le.functionId = t.functionId) " +
            "LEFT JOIN SubjectSettings ss1 on ss1.id = le.complexityId " +
            "LEFT JOIN SubjectSettings ss2 on ss2.id = le.qualityId " +
            "join Features ft on ft.id=f.featureId " +
            "JOIN Projects p on p.id = ft.projectId " +
            "JOIN Classes c on c.id = p.classId " +
            "JOIN Users u on u.id = t.assigneeId " +
            "JOIN Milestones m on m.id = t.milestoneId " +
            " join Iterations i on i.id= m.iterationId " +
            "Where f.id = :functionId " +
            "AND p.status = 'active' " +
            "AND f.status = 'submitted'"
    )
    FunctionsDTO getFunctionEvaluate(Integer functionId);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionsDTO(f.id, t.assigneeId, f.name, " +
            "f.description, t.note, le.convertedLoc, le.comment, t.status, le.complexityId, le.qualityId, " +
            "u.rollNumber, u.fullName, i.name, p.name, p.classId, le.id, t.id, m.id, p.id, c.code, le.newComplexityId, " +
            "le.newQualityId, le.newMilestoneId, le.newConvertedLoc) FROM Functions f " +
            "JOIN Trackings t on (f.id = t.functionId) " +
            "JOIN LocEvaluations le on (le.milestoneId = t.milestoneId AND le.functionId = t.functionId) " +
            " join Features ft on ft.id=f.featureId " +
            "JOIN Projects p on p.id = ft.projectId " +
            "JOIN Classes c on c.id = p.classId " +
            "JOIN Users u on u.id = t.assigneeId " +
            "JOIN Milestones m on m.id = t.milestoneId " +
            " join Iterations i on i.id= m.iterationId " +
            "Where f.id = :functionId " +
            "AND p.status = 'active' "
    )
    FunctionsDTO getNewLocFunction(Integer functionId);

    //Vì count theo member nên lấy status theo tracking (được gán cho member đáy) thay vì status của function
    @Query("SELECT  new com.fpt.capstone.backend.api.BackEnd.dto.FunctionCountDTO(u.id, u.email, u.rollNumber, t.status " +
            ", count (fc.id)) FROM Users u " +
            "JOIN ClassUsers cu on (cu.userId = u.id and cu.projectId = :projectId) " +
            "LEFT JOIN Features ft on (ft.projectId = cu.projectId) " +
            "LEFT JOIN Functions fc on (ft.id = fc.featureId) " +
            "LEFT JOIN Trackings t on (t.functionId = fc.id and u.id = t.assigneeId) " +
            "where (COALESCE(:projectId) is null or cu.projectId in :projectId) " +
            "AND (COALESCE(:milestoneId) is null or t.milestoneId in :milestoneId) " +
            "GROUP BY t.assigneeId, fc.status")
    List<FunctionCountDTO> getFunctionCount(Integer projectId, List<Integer> milestoneId);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionCountDTO(u.id, u.email, u.rollNumber) " +
            "FROM Users u " +
            "JOIN ClassUsers cu on (cu.userId = u.id and cu.projectId = :projectId)")
    List<FunctionCountDTO> getStudentByClass(Integer projectId);

    Functions findByName(String name);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionsDTO(fc.id,p.id,p.code,p.name,ft.id,ft.name,fc.name," +
            "fc.description,fc.priority,fc.typeId,cs1.title,fc.statusId,cs.title,fc.status,fc.gitLabId,fc.created,fc.created_by," +
            "fc.modified,fc.modified_by,u2.email,u2.rollNumber,u3.email,u3.rollNumber)" +
            " FROM Functions fc   " +
            " join Features ft on ft.id=fc.featureId " +
            " join Projects p on p.id=ft.projectId " +
            " join Users u2 on u2.id= fc.created_by " +
            " join Users u3 on u3.id=fc.modified_by " +
            " left join ClassSettings cs on ( fc.statusId=cs.id ) " +
            " left join ClassSettings cs1 on ( fc.typeId=cs1.id )" +
            " WHERE (COALESCE(:projectId) is null or p.id IN :projectId)" +
            " AND (COALESCE(:featureId) is null or ft.id IN :featureId)  " +
            " AND (:name is null or fc.name LIKE %:name%) " +
            " AND (:status is null or fc.status = :status ) " +
            " AND (COALESCE(:authorityFunctionIds) is null or fc.id in :authorityFunctionIds)" +
            " order by p.id")
    Page<FunctionsDTO> search(@Param("projectId") List<Integer> projectId,
                              @Param("featureId") List<Integer> featureId,
                              @Param("name") String name,
                              @Param("status") String status,
                              Pageable pageable,
                              @Param("authorityFunctionIds") List<Integer> authorityFunctionIds
    );

    @Query("SELECT count(fc.id) FROM Functions fc " +
            " WHERE fc.featureId = :featureId " +
            " AND fc.name = :name ")
    Integer searchByNameOnFeature(@Param("featureId") Integer featureId, @Param("name") String name);

    @Query(" SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionListDTO(fc.id,fc.name)" +
            "FROM Functions fc" +
            " where fc.status='active'")
    List<FunctionListDTO> getLabelList();

    @Query(value = "SELECT COLUMN_TYPE FROM information_schema.columns WHERE table_name = :table and COLUMN_NAME = :field", nativeQuery = true)
    String showColumns(@Param("table") String table, @Param("field") String field);

    @Query(" SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionListTrackingDTO(fc.id,CONCAT(fc.name,'-',ft.name,'(',fc.priority,')'),p.id) " +
            "FROM Functions fc " +
            "LEFT JOIN Trackings t on (t.functionId = fc.id)" +
            " join Features ft on ft.id=fc.featureId " +
            " join Projects p on p.id=ft.projectId " +
            " join ClassUsers cu on cu.projectId=p.id" +
            " where cu.userId =:userId " +
            " and t.id is null" +//chỉ lấy các function chưa có tracking
            " AND fc.status = :functionStatus " +
            " AND (COALESCE(:projectId) is null or p.id = :projectId) " +
            "AND (COALESCE(:featureId) is null or ft.id IN :featureId) ")
    List<FunctionListTrackingDTO> getFunctionTracking(@Param("userId") Integer userId, @Param("projectId") Integer projectId, @Param("featureId") List<Integer> featureId, @Param("functionStatus") String functionStatus);

    @Query(" SELECT new com.fpt.capstone.backend.api.BackEnd.dto.FunctionTrackingSubmitListDTO(fc.id,fc.name,ft.id,ft.name,fc.status, " +
            " u.id,u.email,u.rollNumber,i.name,p.id,p.name ) " +
            " FROM Functions fc " +
            " left join Features ft on ft.id=fc.featureId " +
            " left join Projects p on p.id = ft.projectId  " +
            " left join Trackings t on t.functionId=fc.id " +
            " join Users  u on u.id = t.assigneeId " +
            " join Milestones m on m.id = t.milestoneId" +
            " join Iterations i on i.id=m.iterationId" +
            " where (fc.status='pending' or fc.status='committed') " +
            " AND  p.status='active'" +
            " AND (COALESCE(:projectId) is null or p.id IN :projectId) " +
            " AND (COALESCE(:milestoneId) is null or t.milestoneId IN :milestoneId) " +
            " group by fc.id")
    List<FunctionTrackingSubmitListDTO> showListFunctionInTracking(@Param("projectId") List<Integer> projectId, @Param("milestoneId") List<Integer> milestoneId);

    @Modifying
    @Query("UPDATE Functions set status='pending' where id=:functionId")
    void updateFunctionStatus(@Param("functionId") Integer functionId);

    @Modifying
    @Query("UPDATE Functions set status=:status where id=:functionId")
    void updateFunctionStatusToSubmit(@Param("status") String status,@Param("functionId") Integer functionId);

    @Query("select distinct ft.projectId from Functions fc " +
            " join Features ft on ft.id=fc.featureId " +
            "where fc.id=:functionId")
    Integer findProjectId(Integer functionId);

    @Query(" select f.id from Functions f " +
            "join Features ft on f.id = f.featureId " +
            "join Projects p on p.id = ft.projectId " +
            "join Classes c on (c.id = p.classId AND (COALESCE(:trainerId) is null or c.trainerId IN :trainerId)) " +
            "join Subjects s on (s.id = c.subjectId AND (COALESCE(:authorId) is null or s.authorId IN :authorId)) " +
            "join ClassUsers cu on (cu.projectId = p.id AND c.id = cu.classId AND (COALESCE(:studentId) is null or cu.userId IN :studentId))")
    List<Integer> getFunctionAuthority(Integer studentId, Integer authorId, Integer trainerId);

    @Modifying
    @Query("UPDATE Functions f SET f.status = :newStatus WHERE f.id = :functionId ")
    void evaluateFunction(@Param("functionId") Integer functionId,
                          @Param("newStatus") String newStatus);

    @Modifying
    @Query("UPDATE Functions set status = :status where id=:functionId")
    void updateFunctionStatus(@Param("functionId") Integer functionId, @Param("status") String status);

    @Query("select id from Functions where id=:id")
    Integer getByFunctionId(Integer id);

    @Query("select fl.labelId from FunctionLabels fl " +
            " join Functions f on f.id=fl.functionId " +
            " where f.id=:functionId ")
    List<Integer> getFunctionLabel(Integer functionId);

    @Query("SELECT  new com.fpt.capstone.backend.api.BackEnd.dto.FunctionSubmitStatusCountDTO(p.id, p.code,fc.status " +
            ", count (fc.id)) FROM Projects p " +
            "JOIN Classes c on (c.id = p.classId) " +
            "LEFT JOIN Features ft on (ft.projectId = p.id) " +
            "LEFT JOIN Functions fc on (ft.id = fc.featureId)" +
            "LEFT join Trackings t on t.functionId=fc.id " +
            "where c.id = :classId  " +
            "AND (COALESCE(:milestoneId) is null or t.milestoneId in :milestoneId) " +
            "GROUP BY p.id, fc.status")
    List<FunctionSubmitStatusCountDTO> getFunctionStatusCount(Integer classId, List<Integer> milestoneId);

    @Query("select id from Functions ")
    List<Integer> getFunctionAdmin();

    @Query("select new com.fpt.capstone.backend.api.BackEnd.dto.FunctionCountDTO(count(f), sum(le.convertedLoc))  From Functions f " +
            "join Trackings t on (t.functionId = f.id) " +
            "left join LocEvaluations le on (le.functionId = f.id and le.milestoneId = t.milestoneId) " +
            "where t.assigneeId = :assigneeId and t.milestoneId = :milestoneId")
    FunctionCountDTO getTotalFunctionWithLoc(@Param("assigneeId") Integer assigneeId, @Param("milestoneId") Integer milestoneId);

    @Query("SELECT f.gitLabId from Functions f where  f.id=:functionId")
    Integer findGitId(Integer functionId);
}
