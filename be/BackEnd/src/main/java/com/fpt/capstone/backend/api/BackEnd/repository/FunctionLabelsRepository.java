package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.entity.FunctionLabels;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface FunctionLabelsRepository extends JpaRepository<FunctionLabels, Integer> {
    @Query("SELECT fl.labelId from FunctionLabels fl where fl.functionId=:functionId")
    List<Integer> getListLabel(Integer functionId);

    @Modifying
    @Query("delete from FunctionLabels fl where fl.functionId=:functionId and fl.labelId in (:labelId)")
    void deleteFunctionLabel(Integer functionId, List<Integer> labelId);
}
