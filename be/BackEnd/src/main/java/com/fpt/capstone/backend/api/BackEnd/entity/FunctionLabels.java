package com.fpt.capstone.backend.api.BackEnd.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "function_labels")
@Data
public class FunctionLabels {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "function_id")
    private Integer functionId;

    @Column(name = "label_id")
    private Integer labelId;
}
