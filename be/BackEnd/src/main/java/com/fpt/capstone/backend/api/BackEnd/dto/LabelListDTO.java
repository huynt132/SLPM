package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LabelListDTO {
    private Integer value;
    private String label;
    private String color;

    public LabelListDTO(Integer value, String color, String label) {
        this.value = value;
        this.color = color;
        this.label = label;
    }
}
