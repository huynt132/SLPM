package com.fpt.capstone.backend.api.BackEnd.entity;

import lombok.Data;

@Data
public class GitLabUser {
    private Integer id;
    private String username;
    private String name;
    private String state;
    private String avatar_url;
    private String web_url;
}
