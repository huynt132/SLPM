package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.UserRegisterDTO;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

//Transactional(rollbackFor = {Exception.class})
public interface MailService {
    void sendMailVerifycode(UserRegisterDTO userRegisterDTO, String code) throws MessagingException, UnsupportedEncodingException;

    void sendMailInvite(String userName, String pass) throws MessagingException, UnsupportedEncodingException;

    void sendPassChange(String email, String randomPass)throws MessagingException, UnsupportedEncodingException;
}
