package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingOptionDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.LabelListDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ClassSettings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface ClassSettingsRepository extends JpaRepository<ClassSettings, Integer> {
    ClassSettings findByTitleAndAndClassId(String title, Integer classId);

    ClassSettings findByValueAndTypeIdAndClassId(String value, Integer typeId, Integer classId);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingsDTO(cs.id,cs.classId,c.code,cs.typeId,s.title ," +
            "cs.title,cs.value,cs.description,cs.gitLabId,cs.status,cs.created,cs.created_by,cs.modified,cs.modified_by,u1.email,u2.email ) " +
            " FROM ClassSettings cs   " +
            " join Settings s on s.id= cs.typeId " +
            " join Classes c on c.id=cs.classId " +
            " LEFT join Users u1 on u1.id= cs.created_by " +
            " LEFT join Users u2 on u2.id=cs.modified_by " +
            " WHERE cs.id =?1 ")
    ClassSettingsDTO getClassesDetail(Integer id);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingsDTO(cs.id,cs.classId,c.code,cs.typeId,s.title, " +
            "cs.title,cs.value,cs.description,cs.gitLabId,cs.status,cs.created,cs.created_by,cs.modified,cs.modified_by,u1.email,u2.email ) " +
            " FROM ClassSettings cs " +
            " join Settings s on s.id= cs.typeId " +
            " join Classes c on c.id=cs.classId " +
            " LEFT join Users u1 on u1.id= cs.created_by " +
            " LEFT join Users u2 on u2.id=cs.modified_by " +
            " WHERE (COALESCE(:classId) is null or c.id IN :classId)  " +
            " and (:status is null or cs.status = :status )" +
            " AND (COALESCE(:authorityClassSettingsIds) is null or cs.id in :authorityClassSettingsIds) " +
            " AND (COALESCE(:titleOrValue) is null or cs.value like %:titleOrValue% or cs.title like %:titleOrValue%) ")
    Page<ClassSettingsDTO> search(@Param("classId") List<Integer> classId,
                                  @Param("status") String status,
                                  @Param("titleOrValue") String titleOrValue,
                                  Pageable pageable,
                                  @Param("authorityClassSettingsIds") List<Integer> authorityClassSettingsIds);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingOptionDTO(cs.id, cs.title, cs.value) " +
            "FROM ClassSettings cs   " +
            "WHERE (cs.typeId = :typeId)  " +
            "AND (COALESCE(:status) is null or cs.status IN :status) " +
            "and cs.status='active' " +
            "AND cs.classId = :classId")
    List<ClassSettingOptionDTO> getOptions(Integer typeId, String status, Integer classId);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingOptionDTO(cs.id, cs.title, cs.value) " +
            "FROM ClassSettings cs   " +
            "left join Classes  c on cs.classId=c.id " +
            "left join Projects p on c.id=p.classId " +
            "WHERE (cs.typeId = :typeId)  " +
            "AND (COALESCE(:status) is null or cs.status IN :status) " +
            "AND p.id = :projectId")
    List<ClassSettingOptionDTO> getOptionProject(Integer typeId, String status, Integer projectId);

    List<ClassSettings> findAllByClassIdAndStatusAndTypeIdIn(Integer classId, String status, List<Integer> typeId);

    @Query("select cs.id from ClassSettings  cs" +
            " join Classes c on (c.id=cs.classId and c.trainerId=:id)")
    List<Integer> getClassSettingTrainer(Integer id);

    @Query("select cs.id from ClassSettings  cs" +
            " join Classes c on c.id=cs.classId" +
            " join Subjects s on (s.id=c.subjectId and s.authorId=:id)")
    List<Integer> getClassSettingAuthor(Integer id);

    @Query("select cs.id from ClassSettings cs" +
            " join Classes c on c.id=cs.classId" +
            " join ClassUsers cu on ( cu.classId = cs.classId and cu.userId = :id )")
    List<Integer> getClassSettingStudent(Integer id);

    @Query("select new com.fpt.capstone.backend.api.BackEnd.dto.LabelListDTO(cs.id,cs.value,cs.title) " +
            "from ClassSettings cs" +
            " where cs.classId=:classId" +
            " and cs.typeId=71 " +
            " and cs.status='active'")
    List<LabelListDTO> getLabelList(Integer classId);

    @Query("select cl.title from ClassSettings  cl where cl.typeId=71 and cl.id IN :id")
    List<String> getListTitleLabel(List<Integer> id);

    @Query("select cs.id from ClassSettings  cs")
    List<Integer> getClassSettingAdmin();
}
