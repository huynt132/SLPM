package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FunctionSubmitStatusCountDTO {
    private Integer projectId;
    private String projectCode;
    private String status;
    private Long statusCount;
    private Integer total;
    private Integer totalPending;
    private Integer totalCommitted;
    private Integer totalSubmitted;
    private Integer totalRejected;
    private Integer totalEvaluated;

    public FunctionSubmitStatusCountDTO(Integer projectId, String projectCode) {
        this.projectId = projectId;
        this.projectCode = projectCode;
    }

    public FunctionSubmitStatusCountDTO(Integer projectId, String projectCode, String status, Long statusCount) {
        this.projectId = projectId;
        this.projectCode = projectCode;
        this.status=status;
        this.statusCount = statusCount;
    }
}
