package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
//Transactional(rollbackFor = {Exception.class})
@Service
public interface SubmitService {
    /**
     * Api lấy danh sách thông tin submit milestones
     *
     * @param projectId
     * @param milestoneId
     * @return
     */
    List<MilestoneSubmitDTO> getMilestoneSubmits(Integer classId, List<Integer> projectId, Integer milestoneId) throws Exception;

    /**
     * Hàm lấy điểm tổng của đánh giá team dựa theo list projectId và milestone truyền vào
     *
     * @param projectId
     * @param milestoneId
     * @return
     */
    HashMap<Integer, MilestoneSubmitDTO> getTeamGrade(List<Integer> projectId, Integer milestoneId, HashMap<Integer, MilestoneSubmitDTO> result);

    /**
     * Hãm dùng để update hoặc create team evalutation
     *
     * @param teamEvaluationsDTO
     * @return
     */
    TeamEvaluationsDTO editTeamEvaluation(TeamEvaluationsDTO teamEvaluationsDTO) throws Exception;

    /**
     * Hãm dùng để update hoặc create điểm individual (có type là other)
     *
     * @param teamEvaluationsDTO
     * @return
     */
    MemberEvaluationsDTO editIndividualEvaluation(MemberEvaluationsDTO teamEvaluationsDTO) throws Exception;

    /**
     * Hãm dùng để lấy data của loc evaluation
     *
     * @param
     * @return
     */
    FunctionsDTO getFunctionEvaluate(Integer functionId) throws Exception;

    /**
     * Hãm dùng để lấy data của loc evaluation cho new loc evaluate
     *
     * @param
     * @return
     */
    FunctionsDTO getNewLocFunction(Integer functionId) throws Exception;

    /**
     * Hãm dùng để lấy thêm mới hoặc cập nhật điểm đánh giác loc của function
     *
     * @param
     * @return
     */
    FunctionsDTO evaluateFunction(FunctionsDTO function) throws Exception;

    void updateStatus(Integer projectId, Integer milestoneId);

    /**
     * Hàm dùng để đếm số lượng function được gán cho các học sinh dựa theo submit status và milestone id
     * @param projectId
     * @param milestoneId
     * @return
     */
    List<FunctionCountDTO> getFunctionCount(Integer projectId, List<Integer> milestoneId);

    List<FunctionSubmitStatusCountDTO> getFunctionStatusCount(Integer classId,List<Integer> milestoneId);
}
