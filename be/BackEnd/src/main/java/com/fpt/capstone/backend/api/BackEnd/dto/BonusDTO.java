package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BonusDTO {
    private BigDecimal totalWeight;
    private Integer totalLoc;
    private Integer totalFunctions;
    private BigDecimal bonus;
    private BigDecimal grade;
}
