package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class DashboardDatumDTO {
    private String label;
    private String backgroundColor;
    private List<BigDecimal> data;

    public DashboardDatumDTO(String label, String backgroundColor, List<BigDecimal> data) {
        this.label = label;
        this.backgroundColor = backgroundColor;
        this.data = data;
    }
}

