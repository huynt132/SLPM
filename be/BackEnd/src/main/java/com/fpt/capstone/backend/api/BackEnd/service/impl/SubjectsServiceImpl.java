package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectsListDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.entity.RoleID;
import com.fpt.capstone.backend.api.BackEnd.entity.Subjects;
import com.fpt.capstone.backend.api.BackEnd.entity.Users;
import com.fpt.capstone.backend.api.BackEnd.repository.SubjectsRepository;
import com.fpt.capstone.backend.api.BackEnd.service.SubjectsService;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsStatus;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SubjectsServiceImpl implements SubjectsService {

    @Autowired
    private SubjectsRepository subjectsRepository;

    @Autowired
    private Validate validate = new Validate();

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    EntityManager entityManager;

    @Autowired
    private UserService userService;

    @Autowired
    private PermissionService permissionService;

    @Override
    public SubjectsDTO addSubjects(SubjectInputDTO subjectInputDTO) throws Exception {
        validate.validateSubject(subjectInputDTO);
        subjectInputDTO.setCode(subjectInputDTO.getCode().trim());
        subjectInputDTO.setName(subjectInputDTO.getName().trim());
        if (subjectInputDTO.getDescription() != null) {
            subjectInputDTO.setDescription(subjectInputDTO.getDescription().trim());
        }
        List<Subjects> validateExistSubject = subjectsRepository.getAllByCodeOrName(subjectInputDTO.getCode(), subjectInputDTO.getName());
        for (Subjects subject : validateExistSubject) {
            if (subject.getName().equals(subjectInputDTO.getName())) {
                throw new Exception("subjects name already exist!");
            }
            if (subject.getCode().equals(subjectInputDTO.getCode())) {
                throw new Exception("subjects code already exist!");
            }
        }

        Subjects subjects = modelMapper.map(subjectInputDTO, Subjects.class);

        subjects = subjectsRepository.save(subjects);
        return subjectsRepository.getSubjectDetail(subjects.getId());
    }


    @Override
    public void deleteSubjects(int id) {
        Subjects subjects = subjectsRepository.getById(id);
        subjects.setStatus(ConstantsStatus.inactive.toString());
        subjectsRepository.save(subjects);
    }

    public SubjectsDTO findById(int id) {
        return subjectsRepository.getSubjectDetail(id);
    }

    @Override
    public List<SubjectsDTO> showSubjectsList() {
        List<Subjects> subjects = subjectsRepository.findAll();
        List<SubjectsDTO> subjectsDTOS = subjects.stream()
                .map(subject -> modelMapper.map(subject, SubjectsDTO.class))
                .collect(Collectors.toList());
        return subjectsDTOS;
    }

    @Override
    public SubjectsDTO updateSubject(SubjectInputDTO subjectInputDTO) throws Exception {
        if (ObjectUtils.isEmpty(subjectInputDTO.getId())) {
            throw new Exception("subject id cannot be empty!");
        }
        if (!subjectInputDTO.getId().matches(ConstantsRegex.NUMBER_PATTERN.toString())) {
            throw new Exception("subject id must be integer and bigger than 0!");
        }
        if (subjectsRepository.getSubjectDetail(Integer.valueOf(subjectInputDTO.getId())) == null) {
            throw new Exception("subject not exist!");
        }
        validate.validateSubject(subjectInputDTO);
        subjectInputDTO.setCode(subjectInputDTO.getCode().trim());
        subjectInputDTO.setName(subjectInputDTO.getName().trim());
        if (subjectInputDTO.getDescription() != null)
            subjectInputDTO.setDescription(subjectInputDTO.getDescription().trim());

        Subjects subjects = subjectsRepository.getById(Integer.valueOf(subjectInputDTO.getId()));
        if (!subjects.getName().equals(subjectInputDTO.getName())) {
            if (subjectsRepository.findBySubjectName(subjectInputDTO.getName()) > 0)
                throw new Exception("subjects name already exist!");
        }
        if (!subjects.getCode().equals(subjectInputDTO.getCode())) {
            if (subjectsRepository.findBySubjectCode(subjectInputDTO.getCode()) > 0)
                throw new Exception("subjects code already exist!");
        }
        subjects = modelMapper.map(subjectInputDTO, Subjects.class);

        subjects = subjectsRepository.save(subjects);
        return subjectsRepository.getSubjectDetail(subjects.getId());
    }

    @Override
    public List<SubjectsListDTO> listBy() throws Exception {
        //   Pageable pageable = PageRequest.of(page - 1, per_page);
        List<Integer> authoritySubjectIds = permissionService.getSubjectAuthority(null);
        List<SubjectsListDTO> subjects = subjectsRepository.search(authoritySubjectIds);
        //List<SubjectsListDTO> subjects = subjectsRepository.search();
        // Page<SubjectsDTO> subjectsDTOS = subjects.map(subjects1 -> modelMapper.map(subjects1, SubjectsDTO.class));
        return subjects;
    }


    @Override
    public Page<SubjectsDTO> searchBy(String code, String name, List<Integer> authorId, String status, int page, int per_page, ResponsePaggingObject respone) throws Exception {
        Users user = userService.getUserLogin();
        List<Integer> authoritySubjectIds = permissionService.getSubjectAuthority(null);
        Pageable pageable = PageRequest.of(page - 1, per_page);

        if (Objects.equals(user.getSettings().getId(), RoleID.ADMIN) || Objects.equals(user.getSettings().getId(), RoleID.AUTHOR)) {
            respone.getPermission().setAdd(Boolean.TRUE);
            respone.getPermission().setUpdate(Boolean.TRUE);
        }
        return subjectsRepository.searchBy(code, name, authorId, status, pageable, authoritySubjectIds);

    }

    /**
     * Hàm dùng để check xem môn học đã được config đầy đủ các data bắt buộc hay chưa, đã được phép tạo lớp học hay chưa
     *
     * @param subjectId
     * @return
     */
    public boolean checkSubjectInitCondition(Integer subjectId) throws Exception {
        List<SubjectConfigDTO> subjectConfigs = subjectsRepository.getSubjectConfigs(subjectId, Arrays.asList(ModelStatus.SETTING_EVALUATE_TYPE));
        HashMap<Integer, IterationsDTO> iterations = new HashMap<>();
        HashMap<Integer, SubjectConfigDTO> checkTotalIsOngoingIte = new HashMap<>();

        Boolean hasComplexity = false;
        Boolean hasQuality = false;

        for (SubjectConfigDTO subjectConfig : subjectConfigs) {
            //Mỗi môn học đều phải có config cho quality và complexity
            if (subjectConfig.getSubjectSettingTypeId() != null) {
                if (Integer.valueOf(subjectConfig.getSubjectSettingTypeId()) == ModelStatus.QUALITY_SETTING_TYPE_ID)
                    hasQuality = true;
                if (Integer.valueOf(subjectConfig.getSubjectSettingTypeId()) == ModelStatus.COMPLEXITY_SETTING_TYPE_ID)
                    hasComplexity = true;
            }

            if (subjectConfig.getIterationId() != null) {
                //Mỗi môn học chỉ được phép có tối đa 1 iteration không phải là iteration tiến trình
                if (subjectConfig.getIterationIsOngoing() != ModelStatus.IS_ONGOING) {
                    checkTotalIsOngoingIte.putIfAbsent(subjectConfig.getIterationId(), subjectConfig);
                    if (checkTotalIsOngoingIte.size() > 1) throw new Exception("each subject is only allowed to have a maximum of 1 iteration not an ongoing iteration!");
                }


                iterations.putIfAbsent(subjectConfig.getIterationId(), new IterationsDTO(
                        subjectConfig.getIterationId(), subjectConfig.getIterationWeight(),
                        subjectConfig.getIterationIsOngoing(), subjectConfig.getIterationStatus(),
                        subjectConfig.getIterationName()));
            }

            if (subjectConfig.getCriteriaId() != null) {
                iterations.get(subjectConfig.getIterationId()).getEvaluationCriteria().putIfAbsent(subjectConfig.getCriteriaId(), new EvaluationCriteriaDTO(
                        subjectConfig.getCriteriaId(), subjectConfig.getCriteriaWeight(),
                        subjectConfig.getCriteriaType(), subjectConfig.getCriteriaStatus()));
            }
        }

        if (!hasComplexity) throw new Exception("subject have not config complexity value in subject setting yet!");
        if (!hasQuality) throw new Exception("subject have not config quality value in subject setting yet!");

        // Check tổng iteration weight đã bằng 1 hay chưa
        BigDecimal totalIterationWeight = new BigDecimal(0);
        for (IterationsDTO iterationDTO : iterations.values()) {
            // Check xem tất cả iteration đã có đủ config hay chưa
            if (iterationDTO.getEvaluationCriteria().isEmpty()) {
                throw new Exception("this subject has not yet configured evaluation criteria for iteration " + iterationDTO.getName() + "!");
            }

            totalIterationWeight = totalIterationWeight.add(iterationDTO.getEvaluationWeight());

            // Check tổng trọng số của từng iteration 1 đã đủ 100 hay chưa
            BigDecimal totalIteEvalWeight = new BigDecimal(0);
            for (EvaluationCriteriaDTO evalCriteria : iterationDTO.getEvaluationCriteria().values()) {
                totalIteEvalWeight = totalIteEvalWeight.add(evalCriteria.getEvaluationWeight());
            }

            if (totalIteEvalWeight.compareTo(BigDecimal.valueOf(1)) != 0) {
                throw new Exception("total evaluation criteria weight of iteration " + iterationDTO.getName() + " not equal to 100%!");
            }
        }

        if (totalIterationWeight.compareTo(BigDecimal.valueOf(1)) != 0) {
            throw new Exception("subject config for total weighted of iterations not equal to 100%");
        }

        return true;
    }

    @Override
    public List<ClassSettingOptionDTO> getSubjectSetting(Integer subjectId, String type, Integer classId) {
        Integer typeId = ModelStatus.SETTING_TYPE_MAPPER.get(type);
        if (typeId == null) return null;
        return subjectsRepository.getComplexity(subjectId, typeId, classId);
    }
}
