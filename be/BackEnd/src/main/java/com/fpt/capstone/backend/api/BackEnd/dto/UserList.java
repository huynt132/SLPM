package com.fpt.capstone.backend.api.BackEnd.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.List;

@Data
@NoArgsConstructor
public class UserList {
    private Integer value;
    private String label;
    @JsonIgnore
    private String userName;
    @JsonIgnore
    private String rollNumber;

    public UserList(Integer value, String userName, String rollNumber) {
        this.value = value;
        this.userName = userName;
        this.rollNumber = rollNumber;
        this.label = formatUserString(userName);
    }



    public String formatUserString(String fullName) {
        String[] parts = fullName.split("@");
        return parts[0];
    }
}
