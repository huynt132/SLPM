package com.fpt.capstone.backend.api.BackEnd.dto.class_users;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
@Data
@NoArgsConstructor
public class ClassUsersDTO {
    private Integer id;
    private Integer classId;
    private String classCode;
    private Integer projectId;
    private String projectCode;
    private String projectName;
    private Integer userId;
    private String userEmail;

    private String rollNumber;
    private String fullName;
    private Integer projectLeader;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date dropoutDate;
    private String note;
    private BigDecimal ongoingEval;
    private BigDecimal finalEval;
    private BigDecimal finalTopicEval;
    private String status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date created;
    private Integer createdBy;
    private String createdByUser;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date modified;
    private Integer modifiedBy;
    private String modifiedByUser;

    public ClassUsersDTO(Integer id, Integer classId, String classCode, Integer projectId, String projectCode,
                         String projectName, Integer userId, String userEmail, String fullName, Integer projectLeader, Date dropoutDate,
                         String note, BigDecimal ongoingEval, BigDecimal finalEval, BigDecimal finalTopicEval,
                         String status, Date created, Integer createdBy, String createdByUser, Date modified,
                         Integer modifiedBy, String modifiedByUser, String rollNumber)
    {
        this.id = id;
        this.classId = classId;
        this.classCode = classCode;
        this.projectId = projectId;
        this.projectCode = projectCode;
        this.projectName = projectName;
        this.userId = userId;
        this.userEmail = userEmail;
        this.fullName = fullName;
        this.projectLeader = projectLeader;
        this.dropoutDate = dropoutDate;
        this.note = note;
        this.ongoingEval = ongoingEval;
        this.finalEval = finalEval;
        this.finalTopicEval = finalTopicEval;
        this.status = status;
        this.created = created;
        this.createdBy = createdBy;
        this.createdByUser = createdByUser;
        this.modified = modified;
        this.modifiedBy = modifiedBy;
        this.modifiedByUser = modifiedByUser;
        this.rollNumber = rollNumber;
    }

    public ClassUsersDTO(Integer classId, Integer userId) {
        this.classId = classId;
        this.userId = userId;
    }

    public ClassUsersDTO(Integer id, Integer classId, Integer projectId, Integer userId) {
        this.id = id;
        this.classId = classId;
        this.projectId = projectId;
        this.userId = userId;
    }
}
