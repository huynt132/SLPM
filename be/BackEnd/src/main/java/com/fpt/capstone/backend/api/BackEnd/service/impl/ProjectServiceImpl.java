package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.capstone.backend.api.BackEnd.dto.MilestonesSyncDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ProjectGitIdDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ProjectsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ProjectsListDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.Classes;
import com.fpt.capstone.backend.api.BackEnd.entity.ProjectMilestones;
import com.fpt.capstone.backend.api.BackEnd.entity.Projects;
import com.fpt.capstone.backend.api.BackEnd.entity.Users;
import com.fpt.capstone.backend.api.BackEnd.repository.*;
import com.fpt.capstone.backend.api.BackEnd.service.*;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.jobrunr.scheduling.BackgroundJob;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ClassesRepository classesRepository;

    @Autowired
    private ClassesService classesService;
    @Autowired
    private EvaluationJobService evaluationJobService;

    @Autowired
    private TeamEvaluationsRepository teamEvaluationsRepository;

    @Autowired
    private SubmitsRepository submitsRepository;

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private UserService userService;

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private Validate validate = new Validate();
    @Autowired
    private MilestonesRepository milestonesRepository;
    @Autowired
    private ProjectMilestonesRespository projectMilestonesRespository;

    @Override
    public ProjectsDTO addProject(ProjectsDTO projectsDTO) throws Exception {
        try {
            validate.validateProject(projectsDTO);
            Projects projectExist = projectRepository.findByCodeAndClassId(projectsDTO.getCode(), projectsDTO.getClassId());
            if (projectExist != null) {
                if (projectsDTO.getCode().equals(projectExist.getCode()))
                    throw new Exception("project code " + projectsDTO.getCode() + " already exist, please choose another code");
                if (projectsDTO.getName().equals(projectExist.getName()))
                    throw new Exception("project name " + projectsDTO.getName() + "  already exist, please choose another code");
            }

            //Check lớp đã hoàn thành config, được phép thêm học sinh, team vào
            if (!classesService.checkClassInitCondition(projectsDTO.getClassId()))
                throw new Exception(" This class have not finished configured yet!");

            Users auth = userService.getUserLogin();

            Projects projects = modelMapper.map(projectsDTO, Projects.class);
            ProjectsDTO projectDTOSync = syncProjectToGit(projectsDTO);
            projects.setGitLabProjectId(projectDTOSync.getGitLabProjectId());
            projects.setGitlabUrl(projectDTOSync.getGitlabUrl());
            projects = projectRepository.save(projects);

            Projects finalProjects = projects;
            BackgroundJob.enqueue(() -> evaluationJobService.createTeamEvaluation(finalProjects, auth));

            //syncMilestonGitLab(projects.getClassId(), projects.getGitLabProjectId(), projects.getId());
            return getProjectDetail(projects.getId());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }


    }

    public List<MilestonesSyncDTO> listMilestoneSync(Integer classId) {
        List<MilestonesSyncDTO> milestonesDTOS = milestonesRepository.getMilestonesSync(classId);
        return milestonesDTOS;
    }

    public void syncMilestonGitLab(Integer classId, Integer projectGitId, Integer projectId) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            List<MilestonesSyncDTO> listMilestoneSync = listMilestoneSync(classId);
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("Update you gitlab token in Update profile");
            }

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(token);

            //Lay ra nhung thang co trong project.getMilestones() maf khong co trong setGitLabMilestones
            //Thi ban nhung thang do len git
            List<ProjectMilestones> projectMilestonesList = new ArrayList<>();

            listMilestoneSync.forEach(milestonesSyncDTO -> {
                if (milestonesSyncDTO.getGitLabMilestoneId() != null) {
                    String url = "https://gitlab.com/api/v4/projects/" + projectGitId + "/milestones";
                    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
                    map.add("title", milestonesSyncDTO.getTitle());
                    if (milestonesSyncDTO.getDescription() != null)
                        map.add("description", milestonesSyncDTO.getDescription());
                    if (milestonesSyncDTO.getTo() != null) map.add("due_date", milestonesSyncDTO.getTo().toString());
                    if (milestonesSyncDTO.getFrom() != null)
                        map.add("start_date", milestonesSyncDTO.getFrom().toString());

                    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
                    ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        JsonNode root = mapper.readTree(response.getBody());
                        JsonNode idNode = root.path("id");
                        Integer milestoneGitId = idNode.asInt();
                        //add to project_milestone
                        ProjectMilestones projectMilestones = new ProjectMilestones();
                        projectMilestones.setProjectId(projectId);
                        projectMilestones.setMilestoneId(milestonesSyncDTO.getMilestoneId());
                        projectMilestones.setGitlabMilestoneId(milestoneGitId);
                        projectMilestonesList.add(projectMilestones);
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            projectMilestonesRespository.saveAll(projectMilestonesList);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void checkStatus(HttpStatus httpStatus) throws Exception {
        if (httpStatus == HttpStatus.NOT_FOUND) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.BAD_REQUEST) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.UNAUTHORIZED) {
            throw new Exception("please check your Gitlab token");
        }
        if (httpStatus == HttpStatus.FORBIDDEN) {
            throw new Exception("please check your Gitlab token's permission");
        }
    }
    private ProjectsDTO syncProjectToGit(ProjectsDTO projectsDTO) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            Classes classes = classesRepository.getById(Integer.valueOf(projectsDTO.getClassId()));
            Integer gitSubGroupId = classes.getGitLabId();
            if (gitSubGroupId==null){
                throw new Exception(" please update group git id of thi class!");
            }
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            ProjectsDTO projectsDTOSync = new ProjectsDTO();
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/projects/";
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("namespace_id", String.valueOf(gitSubGroupId));
            map.add("name", projectsDTO.getCode());
            map.add("path", projectsDTO.getCode());
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode idNode = root.path("id");
            JsonNode webUrlNode = root.path("web_url");
            Integer id = idNode.asInt();
            String webUrl = webUrlNode.asText();
            projectsDTOSync.setGitLabProjectId(id);
            projectsDTOSync.setGitlabUrl(webUrl);
            return projectsDTOSync;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private ProjectsDTO syncUpdateProjectToGit(ProjectsDTO projectsDTO) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            ProjectsDTO projectsDTOSync = new ProjectsDTO();
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/projects/" + projectsDTO.getGitLabProjectId();
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("name", projectsDTO.getCode());
            map.add("path", projectsDTO.getCode());
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode idNode = root.path("id");
            JsonNode webUrlNode = root.path("web_url");
            Integer id = idNode.asInt();
            String webUrl = webUrlNode.asText();
            projectsDTOSync.setGitLabProjectId(id);
            projectsDTOSync.setGitlabUrl(webUrl);
            return projectsDTOSync;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public ProjectsDTO deleteaddProject(int id) {
        return null;
    }

    @Override
    public List<ProjectsDTO> showProject() {
        return null;
    }

    @Override
    public ProjectsDTO updateProject(ProjectsDTO projectsDTO) throws Exception {
        validate.validateProject(projectsDTO);
        Projects projects = projectRepository.getById(projectsDTO.getId());


        if (!projects.getName().equals(projectsDTO.getName())) {
            if (projectRepository.findByName(projectsDTO.getName()) != null)
                throw new Exception("project name already exist!");
        }
        if (!projects.getCode().equals(projectsDTO.getCode())) {
            if (projectRepository.findByCode(projectsDTO.getCode()) != null)
                throw new Exception("project Code already exist!");
            ProjectsDTO projectDTOSync = syncUpdateProjectToGit(projectsDTO);
            projectsDTO.setGitlabUrl(projectDTOSync.getGitlabUrl());

        }
        projects = modelMapper.map(projectsDTO, Projects.class);


        //projects.setClasses(classesRepository.getById(Integer.valueOf(projectsDTO.getClassId())));
        projectRepository.save(projects);
        projects = entityManager.find(Projects.class, projects.getId());
        return getProjectDetail(projects.getId());
    }

    @Override
    public ProjectsDTO findById(int id) throws Exception {
        return null;
    }

    @Override
    public Page<ProjectsDTO> listBy(List<Integer> id, String projectName, String status, int page, int per_page) throws Exception {
        //check quyen
        List<Integer> authorityProjectIds = permissionService.getProjectAuthority(null);

        if (ObjectUtils.isEmpty(status)) status = null;
        if (!ObjectUtils.isEmpty(status) && !status.matches(ConstantsRegex.STATUS_PATTERN.toString())) {
            throw new Exception("input status Empty to search all or active/inactive!");
        }
        return projectRepository.search(id, projectName, status, PageRequest.of(page - 1, per_page), authorityProjectIds);
    }

    @Override
    public ProjectsDTO getProjectDetail(int id) {
        return projectRepository.getProjectDetail(id);
    }

    @Override
    public List<ProjectsListDTO> showProjectList(List<Integer> subjectId, List<Integer> classId, String status) throws Exception {
        if (ObjectUtils.isEmpty(status)) {
            status = null;
        }
        List<Integer> userId = new ArrayList<>();
        List<Integer> authorityProjectIds = permissionService.getProjectAuthority(null);
        return projectRepository.getLabelList(subjectId, classId, userId, status, authorityProjectIds);
    }

    @Override
    public List<ProjectGitIdDTO> showProjectGitId(List<Integer> classId, List<Integer> projectId) {
//        return projectRepository.getGitProjectId(classId, projectId);
        return null;
    }

    @Override
    public List<ProjectGitIdDTO> showProjectGitIdById(List<Integer> projectId) {
        return null;
    }

    /**
     * @param userId id user
     * @return first available project Id
     */
    @Override
    public List<ProjectsDTO> getFirstProject(Integer userId) {
        return projectRepository.getFirstProject(userId, PageRequest.of(0, 1));
    }

    @Override
    public List<ProjectsListDTO> showProjectListInFeatue(List<Integer> featureId) throws Exception {
        List<Integer> authorityProjectIds = permissionService.getProjectAuthority(null);
        return projectRepository.ProjectListInFeatue(featureId, authorityProjectIds);
    }
}
