package com.fpt.capstone.backend.api.BackEnd.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassUserResponse {
    private boolean success;
    private String message;
    private Object data;
    private Integer curClass;
    private Integer perPages;
    private Integer currentPage;
    private Long total;
}
