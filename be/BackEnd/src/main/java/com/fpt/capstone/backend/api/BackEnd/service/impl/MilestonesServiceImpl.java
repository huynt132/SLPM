package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.capstone.backend.api.BackEnd.dto.MilestonesListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.MilestonesSyncDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestoneInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.Classes;
import com.fpt.capstone.backend.api.BackEnd.entity.Iterations;
import com.fpt.capstone.backend.api.BackEnd.entity.Milestones;
import com.fpt.capstone.backend.api.BackEnd.entity.Users;
import com.fpt.capstone.backend.api.BackEnd.repository.ClassesRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.IterationsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.MilestonesRepository;
import com.fpt.capstone.backend.api.BackEnd.service.*;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class MilestonesServiceImpl implements MilestonesService {

    @Autowired
    private ClassesRepository classesRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private MilestonesRepository milestonesRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Validate validate;
    @Autowired
    private UserService userService;
    @Autowired
    private PermissionService permissionService;

    @Override
    public MilestonesDTO add(MilestoneInputDTO milestoneInput) throws Exception {
        validate.validateMilestone(milestoneInput);
//        if (milestonesRepository.getMilestonesByTitle(milestoneInput.getTitle()) != null) {
//            throw new Exception("title already exist!");
//        }
        if (milestonesRepository.countByClassIdAndIterationId(
                Integer.valueOf(milestoneInput.getClassId()),
                Integer.valueOf(milestoneInput.getIterationId())) > 0) {
            throw new Exception("this class already configured milestone for this iteration!");
        }
        milestoneInput.setFrom(validate.convertDDMMYYtoYYMMDD(milestoneInput.getFrom()));
        milestoneInput.setTo(validate.convertDDMMYYtoYYMMDD(milestoneInput.getTo()));
        Milestones milestones = modelMapper.map(milestoneInput, Milestones.class);
        milestones.setGitLabId(syncGitLab(milestoneInput));
        milestonesRepository.save(milestones);
        milestones = entityManager.find(Milestones.class, milestones.getId());
        return showDetail(milestones.getId());
    }

    @Override
    public MilestonesDTO showDetail(int id) {
        return milestonesRepository.getMilestonesDetail(id);
    }

    @Override
    public MilestonesDTO edit(MilestoneInputDTO milestoneInput) throws Exception {
        if (ObjectUtils.isEmpty(milestoneInput.getId())) {
            throw new Exception("milestone id cannot be empty!");
        }
        if (milestonesRepository.getById(Integer.valueOf(milestoneInput.getId())) == null) {
            throw new Exception("milestone not found!");
        }
        validate.validateMilestone(milestoneInput);
        milestoneInput.setFrom(validate.convertDDMMYYtoYYMMDD(milestoneInput.getFrom()));
        milestoneInput.setTo(validate.convertDDMMYYtoYYMMDD(milestoneInput.getTo()));
        Milestones milestones = milestonesRepository.getById(Integer.valueOf(milestoneInput.getId()));
//        if (!milestones.getTitle().equals(milestoneInput.getTitle())
//                && milestonesRepository.getMilestonesByTitle(milestoneInput.getTitle()) != null) {
//            throw new Exception("title already exist!");
//        }
        if (milestones.getClassId() != Integer.valueOf(milestoneInput.getClassId())
                || milestones.getIterationId() != Integer.valueOf(milestoneInput.getIterationId())) {
            if (milestonesRepository.countByClassIdAndIterationId(
                    Integer.valueOf(milestoneInput.getClassId()),
                    Integer.valueOf(milestoneInput.getIterationId())) > 0) {
                throw new Exception("iteration already exist in this Class!");
            }
        }
        milestones = modelMapper.map(milestoneInput, Milestones.class);
        syncGitLabUpdateMilestone(milestoneInput);
        milestonesRepository.save(milestones);
        milestones = entityManager.find(Milestones.class, milestones.getId());
        return showDetail(milestones.getId());
    }

    @Autowired
    private ClassesService classesService;

    @Override
    public Page<MilestonesDTO> listBy(List<Integer> iterationId, List<Integer> classId, List<Integer> trainerId,
                                      String title, String status, int page, int limit) throws Exception {
        Pageable pageable = PageRequest.of(page - 1, limit);
        List<Integer> authoritySubjectIds = permissionService.getMilestoneAuthority(null);
        if (ObjectUtils.isEmpty(classId)) {
            List<ClassesListDTO> listClass = classesService.showListClass(null, null);
            if (listClass.size() == 0) return null;
            Integer firstClass = listClass.get(0).getValue();
            return milestonesRepository.search(iterationId, Collections.singletonList(firstClass), trainerId, title, status, pageable, authoritySubjectIds);
        }
        return milestonesRepository.search(iterationId, classId, trainerId, title, status, pageable, authoritySubjectIds);

    }

    @Override
    public List<MilestonesListDTO> listMilestoneByClass(List<Integer> classId, List<Integer> projectId) throws Exception {
        List<Integer> authorityMilestoneIds = permissionService.getMilestoneAuthority(null);
        List<MilestonesListDTO> milestonesDTOS = milestonesRepository.getMilestonesByClass(classId, projectId, authorityMilestoneIds);
        return milestonesDTOS;
    }

    @Override
    public List<MilestonesSyncDTO> listMilestoneSync(Integer classId) {
        List<MilestonesSyncDTO> milestonesDTOS = milestonesRepository.getMilestonesSync(classId);
        return milestonesDTOS;
    }

    @Autowired
    ProjectService projectService;

    @Autowired
    private IterationsRepository iterationsRepository;

    @Override
    public Integer syncGitLab(MilestoneInputDTO milestoneInput) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            Optional<Classes> classes = classesRepository.findById(Integer.valueOf(milestoneInput.getClassId()));
            Integer classGitId = classes.get().getGitLabId();
            if (classGitId == null) {
                throw new Exception("this class does not have git lab Id");
            }
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(token);
            Iterations iterations = iterationsRepository.getById(Integer.valueOf(milestoneInput.getIterationId()));
            //Lay ra nhung thang co trong project.getMilestones() maf khong co trong setGitLabMilestones
            //Thi ban nhung thang do len git
            String url = "https://gitlab.com/api/v4/groups/" + classGitId + "/milestones";
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("title", iterations.getName());
            if (milestoneInput.getDescription() != null) map.add("description", milestoneInput.getDescription());
            if (milestoneInput.getTo() != null) map.add("due_date", milestoneInput.getTo());
            if (milestoneInput.getFrom() != null) map.add("start_date", milestoneInput.getFrom());

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode idNode = root.path("id");
            Integer id = idNode.asInt();
            return id;
        } catch (Exception e) {
            return null;
        }
    }

    public void checkStatus(HttpStatus httpStatus) throws Exception {
        if (httpStatus == HttpStatus.NOT_FOUND) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.BAD_REQUEST) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.UNAUTHORIZED) {
            throw new Exception("please check your Gitlab token");
        }
        if (httpStatus == HttpStatus.FORBIDDEN) {
            throw new Exception("please check your Gitlab token's permission");
        }
    }

    public void syncGitLabUpdateMilestone(MilestoneInputDTO milestoneInput) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            Milestones milestones = milestonesRepository.getById(Integer.valueOf(milestoneInput.getId()));
            Optional<Classes> classes = classesRepository.findById(Integer.valueOf(milestoneInput.getClassId()));
            Integer classGitId = classes.get().getGitLabId();
            if (classGitId == null) {
                throw new Exception("This class does not have git lab Id");
            }
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(token);
            Iterations iterations = iterationsRepository.getById(Integer.valueOf(milestoneInput.getIterationId()));
            //Lay ra nhung thang co trong project.getMilestones() maf khong co trong setGitLabMilestones
            //Thi ban nhung thang do len git
            String url = "https://gitlab.com/api/v4/groups/" + classGitId + "/milestones/" + milestones.getGitLabId();
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            if (!milestoneInput.getIterationId().equals(milestones.getIterationId())) {
                map.add("title", iterations.getName());
            }
            if (milestoneInput.getDescription() != null && milestones.getDescription() != null && !milestoneInput.getDescription().equals(milestones.getDescription())) {
                if (milestoneInput.getDescription() != null) map.add("description", milestoneInput.getDescription());
            }
            if (!milestoneInput.getTo().equals(milestones.getTo())) {
                if (milestoneInput.getTo() != null) map.add("due_date", milestoneInput.getTo());
            }
            if (!milestoneInput.getFrom().equals(milestones.getFrom())) {
                if (milestoneInput.getFrom() != null) map.add("start_date", milestoneInput.getFrom());
            }
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List<String> getGitLabMilestones(Integer gitlabId, String token) throws Exception {
        if (gitlabId == null) return null;

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBearerAuth(token);
        List<String> list = new ArrayList<>();
        String url = "https://gitlab.com/api/v4/projects/" + gitlabId + "/milestones";
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
        String body = response.getBody();
        JsonArray jsObject = new Gson().fromJson(body, JsonArray.class);
        jsObject.forEach(milestone -> {
            JsonObject jobj = new Gson().fromJson(milestone, JsonObject.class);
            list.add(jobj.get("title").toString());
        });
        return list;
    }
}
