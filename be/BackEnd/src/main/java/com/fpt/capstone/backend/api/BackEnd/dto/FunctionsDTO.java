package com.fpt.capstone.backend.api.BackEnd.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@Component
public class FunctionsDTO {
    private Integer id;
    private Integer projectId;
    private String projectCode;
    private String projectName;
    private Integer featureId;
    private String featureName;
    private String complexity;
    private Integer complexityId;
    private Integer qualityId;
    private String complexityValue;
    private String qualityValue;
    private Integer milestoneId;
    private Integer convertedLoc;
    private Integer newComplexityId;
    private Integer newQualityId;
    private Integer newMilestoneId;
    private Integer newConvertedLoc;
    private Integer trackingId;
    private Integer assigneeId;
    private String assigneeRollNumber;
    private String assigneeFullName;
    private String milestoneName;
    private String name;
    private String description;
    private String trackingNote;
    private Integer locEvaluationId;
    private Integer classId;
    private String className;
    private String evaluationComment;
    private Integer priority;
    private Integer typeId;
    private String typeName;
    private Integer statusId;
    private String statusName;
    private String status;
    private Integer gitLabId;

    private List<UpdateLogDTO> updateLog;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date created;
    private Integer createdBy;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date modified;
    private Integer modifiedBy;
    private String createdByUser;
    @JsonIgnore
    private String createdByEmail;
    @JsonIgnore
    private String createdByRollNumber;
    private String modifiedByUser;
    @JsonIgnore
    private String modifiedByEmail;
    @JsonIgnore
    private String modifiedByRollNumber;

    private List<Integer> labels;

    // DTO for newLoc
    public FunctionsDTO(
            Integer id, Integer assigneeId, String name, String description, String trackingNote,
            Integer convertedLoc, String evaluationComment, String status, Integer complexityId,
            Integer qualityId, String assigneeRollNumber, String assigneeFullName, String milestoneName,
            String projectName, Integer classId, Integer locEvaluationId, Integer trackingId, Integer milestoneId,
            Integer projectId, String className, Integer newComplexityId, Integer newQualityId, Integer newMilestoneId,
            Integer newConvertedLoc) {
        this.id = id;
        this.assigneeId = assigneeId;
        this.name = name;
        this.description = description;
        this.trackingNote = trackingNote;
        this.convertedLoc = convertedLoc;
        this.evaluationComment = evaluationComment;
        this.status = status;
        this.complexityId = complexityId;
        this.qualityId = qualityId;
        this.assigneeRollNumber = assigneeRollNumber;
        this.assigneeFullName = assigneeFullName;
        this.milestoneName = milestoneName;
        this.projectName = projectName;
        this.classId = classId;
        this.locEvaluationId = locEvaluationId;
        this.trackingId = trackingId;
        this.milestoneId = milestoneId;
        this.projectId = projectId;
        this.className = className;
        this.newComplexityId = newComplexityId;
        this.newQualityId = newQualityId;
        this.newMilestoneId = newMilestoneId;
        this.newConvertedLoc = newConvertedLoc;
    }

    public FunctionsDTO(
            Integer id, Integer assigneeId, String name, String description, String trackingNote,
            Integer convertedLoc, String evaluationComment, String status, Integer complexityId,
            Integer qualityId, String complexityValue, String qualityValue, String assigneeRollNumber, String assigneeFullName, String milestoneName,
            String projectName, Integer classId, Integer locEvaluationId, Integer trackingId, Integer milestoneId,
            Integer projectId, String className) {
        this.id = id;
        this.assigneeId = assigneeId;
        this.name = name;
        this.description = description;
        this.trackingNote = trackingNote;
        this.convertedLoc = convertedLoc;
        this.evaluationComment = evaluationComment;
        this.status = status;
        this.complexityId = complexityId;
        this.qualityId = qualityId;
        this.assigneeRollNumber = assigneeRollNumber;
        this.assigneeFullName = assigneeFullName;
        this.milestoneName = milestoneName;
        this.projectName = projectName;
        this.classId = classId;
        this.locEvaluationId = locEvaluationId;
        this.trackingId = trackingId;
        this.milestoneId = milestoneId;
        this.projectId = projectId;
        this.className = className;
        this.complexityValue = complexityValue;
        this.qualityValue = qualityValue;
    }

    public FunctionsDTO(Integer id, Integer projectId, String projectCode, String projectName, Integer featureId,
                        String featureName, String name, String description, Integer priority, Integer typeId,
                        String typeName, Integer statusId, String statusName, String status,Integer gitLabId, Date created,
                        Integer createdBy, Date modified, Integer modifiedBy, String createdByEmail,
                        String createdByRollNumber, String modifiedByEmail, String modifiedByRollNumber) {

        this.id = id;
        this.projectId = projectId;
        this.projectCode = projectCode;
        this.projectName = projectName;
        this.featureId = featureId;
        this.featureName = featureName;
        this.name = name;
        this.description = description;
        this.priority = priority;
        this.typeId = typeId;
        this.typeName = typeName;
        this.statusId = statusId;
        this.statusName = statusName;
        this.status = status;
        this.gitLabId=gitLabId;
        this.created = created;
        this.createdBy = createdBy;
        this.modified = modified;
        this.modifiedBy = modifiedBy;
        this.createdByEmail = createdByEmail;
        this.createdByRollNumber = createdByRollNumber;
        this.modifiedByEmail = modifiedByEmail;
        this.modifiedByRollNumber = modifiedByRollNumber;
        this.createdByUser = formatUserString(createdByEmail);
        this.modifiedByUser = formatUserString(modifiedByEmail);

    }

    public String formatUserString(String fullName) {
        String[] parts = fullName.split("@");
        return parts[0];
    }

}
