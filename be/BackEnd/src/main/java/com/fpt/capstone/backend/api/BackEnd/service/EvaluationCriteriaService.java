package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.EvaluationCriteriaDTO;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
//Transactional(rollbackFor = {Exception.class})
public interface EvaluationCriteriaService {
    EvaluationCriteriaDTO addCriteria(EvaluationCriteriaDTO evaluationCriteriaDTO) throws Exception;

    EvaluationCriteriaDTO deleteCriteria(int id);

    List<EvaluationCriteriaDTO> showCriteria();

    EvaluationCriteriaDTO updateCriteria(EvaluationCriteriaDTO evaluationCriteriaDTO) throws Exception;

    EvaluationCriteriaDTO findById(int id) throws Exception;

    Page<EvaluationCriteriaDTO> listBy(List<Integer> subjectId, List<Integer> id, List<Integer> authorId, String status, String type, int page, int per_page) throws Exception;

    public EvaluationCriteriaDTO getCriteriaDetail(int id);

    public List<EvaluationCriteriaDTO> showCriteriaList();
}
