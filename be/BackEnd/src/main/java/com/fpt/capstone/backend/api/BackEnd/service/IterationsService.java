package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.IterationListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationsInputDTO;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
//Transactional(rollbackFor = {Exception.class})
public interface IterationsService {
    IterationsDTO addIterations(IterationsInputDTO iterationsInputDTO) throws Exception;

    IterationsDTO deleteIterations(int id) throws Exception;

    List<IterationsDTO> showIterationsList();

    IterationsDTO updateIterations(IterationsInputDTO iterationsInputDTO) throws Exception;

    Page<IterationsDTO> listBy(List<Integer> id, String name, List<Integer> manageId, String status, int page, int per_page) throws Exception;

    public IterationsDTO getIterationDetail(int id) throws Exception;

    public List<IterationsDTO> showIterationList();

    public List<IterationListDTO> listIteration(String status, Integer subjectId, Integer classId) throws Exception;

    Page<IterationsDTO>  listIterationEvalCriteria() throws Exception;

    BigDecimal getTotalWeightInSubject(Integer subjectId);
}
