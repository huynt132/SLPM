package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.FunctionResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.Users;
import com.fpt.capstone.backend.api.BackEnd.service.*;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("function")
@CrossOrigin(origins = "*", maxAge = 3600)
//@PreAuthorize("hasAuthority('Admin')||hasAuthority('Trainer')||hasAuthority('Teacher')||hasAuthority('Student')")
public class FunctionController {
    @Autowired
    FunctionsService functionsService;
    @Autowired
    ProjectService projectService;
    @Autowired
    UserService userService;
    @Autowired
    private TrackingsService trackingsService;
    @Autowired
    private ClassSettingsService classSettingsService;
    @Autowired
    private PermissionService permissionService;

    @PostMapping("/add")
    public ResponseEntity<?> addFunction(@RequestBody FunctionsDTO functionsDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add function successfully");
            response.setData(functionsService.addFunction(functionsDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add function fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editFunction(@RequestBody FunctionsDTO functionsDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setData(functionsService.updateFunction(functionsDTO));
            response.setMessage("Update function successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update function fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findFunction(
            @RequestParam(value = "getExcel", defaultValue = "false", required = false) String getExcel,
            @RequestParam(value = "projectId", required = false) List<Integer> projectId,
            @RequestParam(value = "featureId", required = false) List<Integer> featureId,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        FunctionResponse response = new FunctionResponse();
        try {
            if (projectId == null) {
                List<Integer> authoritySubjectIds = permissionService.getProjectAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurProjectId(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                projectId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurProjectId(projectId.get(0));
            Page<FunctionsDTO> functionsDTOPage = functionsService.listBy(projectId, featureId, name, status, page, per_page);
            List<FunctionsDTO> functionsDTOS = functionsDTOPage.getContent();

            if (getExcel.equals("true")) {
                Page<FunctionsDTO> functionsDTOPageExcel = functionsService.listBy(projectId, featureId, name, status, page, (int) functionsDTOPage.getTotalElements());
                List<FunctionsDTO> functionsDTOSExcel = functionsDTOPageExcel.getContent();
                response.setSuccess(true);
                response.setMessage("Export function data successfully");
                response.setData(functionsService.exportFunctions(functionsDTOSExcel));
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
            if (functionsDTOS.size() > 0) {
                response.setCurProjectId(functionsDTOS.get(0).getProjectId());
                for (FunctionsDTO key : functionsDTOS) {
                    key.setLabels(functionsService.listLabelFunction(key.getId()));
                }
            }
            response.setSuccess(true);
            response.setMessage("Get function list successfully");
            response.setData(functionsDTOS);
            response.setTotal(functionsDTOPage.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/import")
    public ResponseEntity<?> importExcel(@RequestParam("file") MultipartFile fileStream) {
        ApiResponse response = new ApiResponse();
        try {
            return functionsService.importFunctions(fileStream);
        } catch (IOException e) {
            response.setSuccess(false);
            response.setMessage("Import excel fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getList")
    public ResponseEntity<?> getList() {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get function list successfully");
            response.setData(functionsService.showFeatureList());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getFunctionTracking")
    public ResponseEntity<?> getFunctionTracking(@RequestParam(value = "featureId", required = false) List<Integer> featureId,
                                                 @RequestParam(value = "projectId", required = false) Integer projectId) {
        ApiResponse<Map<String, Object>> response = new ApiResponse();
        try {
            Map<String, Object> result = new HashMap<>();
            response.setSuccess(true);
            response.setMessage("Get function list successfully");
            Users users = userService.getUserLogin();
            if (projectId == null) {
                List<ProjectsDTO> project = projectService.getFirstProject(users.getId());
                if (project.isEmpty()) throw new Exception(" User have not join any project yet!");
                projectId = project.get(0).getId();
            }
            result.put("curProjectId", projectId);
            List<FunctionListTrackingDTO> list = functionsService.showTrackingFeature(projectId, featureId, users, ModelStatus.STATUS_PENDING);
            result.put("functionTracking", list);
            response.setData(result);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getListFunction")
    public ResponseEntity<?> getListFunction(@RequestParam(value = "milestoneId", required = false) List<Integer> milestoneId,
                                             @RequestParam(value = "projectId", required = false) List<Integer> projectId) {
        ApiResponse<Map<String, Object>> response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get function list successfully");
            List<FunctionTrackingSubmitListDTO> list = functionsService.showListFunctionInTraking(projectId, milestoneId);
            Map<String, Object> result = new HashMap<>();
            result.put("functions", list);
            if (list.size() > 0) {
                result.put("iterationName", list.get(0).getIterationName());
                result.put("projectName", list.get(0).getProjectName());
            }
            response.setData(result);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/addLog")
    public ResponseEntity<?> addLogUpdate(@RequestBody AddUpdateLogDTO updateLogDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add tracking successfully");
            response.setData(trackingsService.addLogUpdate(updateLogDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add tracking fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateLog")
    public ResponseEntity<?> updateLogUpdate(@RequestBody AddUpdateLogDTO updateLogDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add tracking successfully");
            response.setData(trackingsService.updateLogUpdate(updateLogDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add tracking fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getListTypeFunction")
    public ResponseEntity<?> getListType(@RequestParam(value = "projectId", required = false) Integer projectId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get function list successfully");
            List<ClassSettingOptionDTO> list = classSettingsService.getFunctionTypeOption(projectId);
            response.setData(list);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getListStatusFunction")
    public ResponseEntity<?> getListStatus(@RequestParam(value = "projectId", required = false) Integer projectId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get function list successfully");
            List<ClassSettingOptionDTO> list = classSettingsService.getFunctionStatusOption(projectId);
            response.setData(list);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getProjectLabel")
    public ResponseEntity<?> getProjectLabel(@RequestParam(value = "projectId") Integer projectId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get label successfully");
            response.setData(classSettingsService.getListLabel(projectId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get label detail fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
