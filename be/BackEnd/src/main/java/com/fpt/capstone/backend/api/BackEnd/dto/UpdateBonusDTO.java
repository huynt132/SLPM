package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class UpdateBonusDTO {
    private Integer studentId;
    private Integer milestoneId;
    private  BigDecimal bonus;
    private BigDecimal demoWeight;
    private BigDecimal demoGrade;
}
