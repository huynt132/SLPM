package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.IndividualEvalDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.MemberEvaluationsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.MemberEvaluations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface MemberEvaluationRepository extends JpaRepository<MemberEvaluations, Integer> {

    @Query("select new com.fpt.capstone.backend.api.BackEnd.dto.IndividualEvalDTO(me.id, me.grade, ie.id, ie.grade, " +
            "i.evaluationWeight, ec.id, ec.evaluationWeight, cu.id, cu.ongoingEval, cu.finalTopicEval, i.isOngoing, cu.finalEval) from MemberEvaluations me " +
            "join EvaluationCriteria ec on (ec.id = me.criteriaId) " +
            "join IterationEvaluations ie on (ie.id = me.evaluationId) " +
            "join ClassUsers cu on (cu.id = ie.classUserId) " +
            "join Iterations i on (i.id = ec.iterationId) " +
            "where me.id = :memberEvalId")
    IndividualEvalDTO getIndividualEval(@Param("memberEvalId") Integer memberEvalId);

    @Modifying
    @Query("update MemberEvaluations me set me.grade = :grade where me.id = :id")
    void updateMemberGrade(@Param("id") Integer id,@Param("grade") BigDecimal grade);

    @Query("select new com.fpt.capstone.backend.api.BackEnd.dto.MemberEvaluationsDTO(me.id, cu.projectId, me.grade, cu.userId) " +
            "from MemberEvaluations me " +
            "join IterationEvaluations ie on (ie.id = me.evaluationId) " +
            "join ClassUsers cu on (cu.id = ie.classUserId and cu.projectId in :projectId)" +
            "and (COALESCE(:milestoneId) is null or ie.milestoneId in :milestoneId)")
    List<MemberEvaluationsDTO> getMemberGradeStatistic(@Param("projectId") List<Integer> projectId,
                                                       @Param("milestoneId") List<Integer> milestoneId);
}
