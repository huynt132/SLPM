package com.fpt.capstone.backend.api.BackEnd.dto.setting;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class SettingTypeDTO {
    private Integer id;
    private String title;
    private String value;

}
