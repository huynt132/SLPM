package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class MemberEvaluationsDTO {
    private Integer id;
    private Integer evaluationId;
    private Integer studentId;
    private Integer criteriaId;
    private String criteriaName;
    private String criteriaType;
    private Integer convertedLoc;
    private Integer projectId;
    private BigDecimal grade;
    private String note;
    private Boolean canEdit;
    private String submitStatus;
    private BigDecimal criteriaWeight;

    public MemberEvaluationsDTO(Integer id, Integer projectId, BigDecimal grade, Integer studentId) {
        this.id = id;
        this.projectId = projectId;
        this.grade = grade;
        this.studentId = studentId;
    }

    public MemberEvaluationsDTO(Integer id, Integer evaluationId, Integer criteriaId, Integer convertedLoc, BigDecimal grade, String note) {
        this.id = id;
        this.evaluationId = evaluationId;
        this.criteriaId = criteriaId;
        this.convertedLoc = convertedLoc;
        this.grade = grade;
        this.note = note;
    }

    public MemberEvaluationsDTO(Integer id, Integer evaluationId, Integer criteriaId, BigDecimal grade, String note,
                                BigDecimal criteriaWeight, String criteriaName, String submitStatus, String criteriaType) {
        this.id = id;
        this.evaluationId = evaluationId;
        this.criteriaId = criteriaId;
        this.grade = grade;
        this.note = note;
        this.criteriaWeight = criteriaWeight;
        this.criteriaName = criteriaName;
        this.submitStatus = submitStatus;
        this.criteriaType = criteriaType;
    }
}
