package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.RoleID;
import com.fpt.capstone.backend.api.BackEnd.entity.Users;
import com.fpt.capstone.backend.api.BackEnd.repository.*;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
public class PermissionService {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private FunctionsRepository functionsRepository;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private MilestonesRepository milestonesRepository;
    @Autowired
    private ClassSettingsRepository classSettingsRepository;
    @Autowired
    private ClassesRepository classesRepository;
    @Autowired
    private IterationsRepository iterationsRepository;
    @Autowired
    private SubjectSettingsRepository subjectSettingsRepository;
    @Autowired
    private EvaluationCriteriaRepository evaluationCriteriaRepository;

    public Boolean hasTeamEvaluationPermission(Integer projectId) {
        Users auth = userService.getUserLogin();
        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) return true;

        return false;
    }

    /**
     * Hàm dùng để lấy danh sách các projectId mà user được thao tác
     *
     * @param auth Users -> mặc định sẽ lấy auth nếu truyền vào null
     * @return
     */
    public List<Integer> getProjectAuthority(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();

        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = projectRepository.projectAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = projectRepository.projectAuthority(null, auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = projectRepository.projectAuthority(auth.getId(), null);
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = projectRepository.projectAuthorityStudent(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        throw new Exception("you do not have permission to access this data!");
    }

    /**
     * Hàm dùng để lấy danh sách các functionId mà user được thao tác
     *
     * @param auth Users -> mặc định sẽ lấy auth nếu truyền vào null
     * @return
     */
    public List<Integer> getFunctionAuthority(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = functionsRepository.getFunctionAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = functionsRepository.getFunctionAuthority(null, auth.getId(), null);
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = functionsRepository.getFunctionAuthority(null, null, auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = functionsRepository.getFunctionAuthority(auth.getId(), null, null);
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        throw new Exception("you do not have permission to access this data!");
    }

    public List<Integer> getSubjectAuthority(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = subjectsRepository.getSubjectAuthorityRoleAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = subjectsRepository.getSubjectAuthorityRoleAuthor(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = subjectsRepository.getSubjectAuthorityTrainerId(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = subjectsRepository.getSubjectStudent(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        throw new Exception("you do not have permission to access this data!");
    }

    public List<Integer> getMilestoneAuthority(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = milestonesRepository.getMilestoneAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = milestonesRepository.getMilestoneAuthor(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = milestonesRepository.getMilestoneTrainer(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = milestonesRepository.getMilestoneStudent(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        throw new Exception("you do not have permission to access this data!");
    }

    public List<Integer> getClassSetting(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = classSettingsRepository.getClassSettingAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = classSettingsRepository.getClassSettingTrainer(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = classSettingsRepository.getClassSettingAuthor(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = classSettingsRepository.getClassSettingStudent(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }

        throw new Exception("you do not have permission to access this data!");
    }

    public List<Integer> getClassAuthor(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = classesRepository.getClassAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = classesRepository.getClassTrainer(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = classesRepository.getClassAuthor(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = classesRepository.getClassStudent(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        throw new Exception("you do not have permission to access this data!");
    }

    public List<Integer> getIteration(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = iterationsRepository.getIterationAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = iterationsRepository.getIterationTrainer(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = iterationsRepository.getIterationAuthor(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = iterationsRepository.getIterationStudent(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        throw new Exception("you do not have permission to access this data!");
    }

    public List<Integer> getSubjectSetting(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = subjectSettingsRepository.getSubjectSettingsAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = subjectSettingsRepository.getSubjectSettingsTrainer(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            authority = subjectSettingsRepository.getSubjectSettingsAuthor(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            authority = subjectSettingsRepository.getSubjectSettingsStudent(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        throw new Exception("you do not have permission to access this data!");
    }

    public List<Integer> geEvalCrietia(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();
        List<Integer> authority;

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            authority = evaluationCriteriaRepository.getEvaluationAdmin();
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            authority = evaluationCriteriaRepository.getEvaluationTrainer(auth.getId());
            return authority.isEmpty() ? new ArrayList<Integer>() {{
                add(-1);
            }} : authority;
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
//            authority = evaluationCriteriaRepository.getEvaluationAuthor(auth.getId());
//            return authority.isEmpty() ? new ArrayList<Integer>() {{
//                add(-1);
//            }} : authority;
            return null;
        }
        throw new Exception("you do not have permission to access this data!");
    }

    /**
     * Lấy ra current class và milestone theo auth
     * @param auth
     * @return
     * @throws Exception
     */
    public List<MilestonesDTO> getClassMilestoneAuthority(Users auth) throws Exception {
        if (auth == null) auth = userService.getUserLogin();

        if (Objects.equals(auth.getSettings().getId(), RoleID.ADMIN)) {
            return milestonesRepository.getClassMilestoneAuthority(null, null, null);
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.AUTHOR)) {
            return milestonesRepository.getClassMilestoneAuthority(null, auth.getId(), null);
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.TRAINER)) {
            return milestonesRepository.getClassMilestoneAuthority(auth.getId(), null, null);
        }
        if (Objects.equals(auth.getSettings().getId(), RoleID.STUDENT)) {
            return milestonesRepository.getClassMilestoneAuthority(null, null, auth.getId());
        }

        throw new Exception("you do not have permission to access this data!");
    }
}
