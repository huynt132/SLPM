package com.fpt.capstone.backend.api.BackEnd.entity;

import lombok.Data;

@Data
public class EvalCriteriaResponse {
    private boolean success;
    private Integer perPages;
    private Integer currentPage;
    private Integer curIterationId;
    private Integer curSubjectId;
    private Long total;
    private String message;
    private Object data;
}
