package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.IterationListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ListIterationEvalDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.Iterations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface IterationsRepository extends JpaRepository<Iterations, Integer> {
    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO(p.id,p.subjectId,p.name,s.name,p.evaluationWeight," +
            "p.status,p.description,p.isOngoing,p.created,p.created_by,p.modified,p.modified_by,u1.fullName,u2.fullName) FROM Iterations p   " +
            " LEFT join Users u1 on u1.id= p.created_by " +
            " join Subjects s on s.id=p.subjectId " +
            " LEFT join Users u2 on u2.id=p.modified_by " +
            " WHERE (:iterationName is null or p.name LIKE %:iterationName%) " +
            " AND (COALESCE(:subjectId) is null or s.id IN :subjectId) " +
            " AND (COALESCE(:authorId) is null or s.authorId IN :authorId) " +
            " AND ( :status is null or p.status like :status% )" +
            " AND (COALESCE(:authorityIterationIds) is null or p.id in :authorityIterationIds) " +
            " group by p.id" +
            " order by s.id")
    Page<IterationsDTO> search(@Param("subjectId") List<Integer> subjectId,
                               @Param("iterationName") String iterationName,
                               @Param("authorId") List<Integer> authorId,
                               @Param("status") String status,
                               Pageable pageable,
                               @Param("authorityIterationIds") List<Integer> authorityIterationIds
    );

    @Query("SELECT p.id FROM Iterations p WHERE p.name like ?1 and p.subjectId = ?2")
    Integer findByIterationsNameAndSubjectId(String keyName, Integer subjectId);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO(p.id,p.subjectId,p.name,s.name,p.evaluationWeight," +
            "p.status,p.description,p.isOngoing,p.created,p.created_by,p.modified,p.modified_by,u1.fullName,u2.fullName) FROM Iterations p   " +
            " LEFT join Users u1 on u1.id= p.created_by " +
            " join Subjects s on s.id=p.subjectId" +
            " LEFT join Users u2 on u2.id=p.modified_by " +
            "  WHERE p.id =:id")
    IterationsDTO getIterationsDetail(int id);

    @Query("select distinct i.subjectId from Iterations i")
    List<Integer> getAllSubjectIdInIteration();

    @Query(" SELECT new com.fpt.capstone.backend.api.BackEnd.dto.IterationListDTO(i.id,i.name)" +
            " FROM Iterations i " +
            "JOIN Subjects s on s.id = i.subjectId " +
            "LEFT JOIN Classes c on c.subjectId = s.id " +
            "where (COALESCE(:status) is null or i.status = :status) " +
            "AND (COALESCE(:authorityIterationIds) is null or i.id in :authorityIterationIds) " +
            "AND (COALESCE(:subjectId) is null or i.subjectId = :subjectId) " +
            "AND (COALESCE(:classId) is null or c.id = :classId) " +
            "GROUP BY i.id")
    List<IterationListDTO> showAll(@Param("status") String status,
                                   @Param("authorityIterationIds") List<Integer> authorityIterationIds,
                                   @Param("subjectId") Integer subjectId,
                                   @Param("classId") Integer classId);


    @Query(" SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ListIterationEvalDTO(i.name, i.id, m.id)" +
            " FROM Iterations i" +
            " JOIN Milestones  m on( m.iterationId=i.id and m.classId= :classId)")
    List<ListIterationEvalDTO> findIterationInClass(@Param("classId") Integer classId);

    @Query("SELECT i.evaluationWeight from Iterations i " +
            "where i.subjectId=:subjectId " +
            "and i.status = 'active' " +
            "and (COALESCE(:iterationId) is null or i.id <> :iterationId)")
    List<BigDecimal> getEvaluationWeight(@Param("subjectId") Integer subjectId, @Param("iterationId") Integer iterationId);

    @Query("SELECT i.id from Iterations  i  " +
            "left join Classes c on c.subjectId=i.subjectId " +
            "where c.trainerId=:id " +
            "group by i.id " +
            "order by i.id")
    List<Integer> getIterationTrainer(Integer id);

    @Query("SELECT i.id from Iterations  i  " +
            "left join Subjects s on s.id=i.subjectId " +
            "where s.authorId=:id " +
            "group by i.id " +
            "order by i.id")
    List<Integer> getIterationAuthor(Integer id);

    @Query("SELECT i.id from Iterations  i  " +
            "left join Classes c on c.subjectId=i.subjectId " +
            "left join ClassUsers  cu on cu.classId=c.id " +
            "left join Users  u on u.id=cu.userId " +
            "where u.id=:id " +
            "group by i.id " +
            "order by i.id")
    List<Integer> getIterationStudent(Integer id);

    @Query(" select new com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO(i.id,i.subjectId) from EvaluationCriteria ev " +
            " left join Iterations i on ev.iterationId=i.id" +
            " where (COALESCE(:authorityIterationIds) is null or i.id  in :authorityIterationIds)" +
            " group by i.id " +
            " order by i.id ")
    Page<IterationsDTO> getIterationInCriteria(List<Integer> authorityIterationIds, Pageable pageable);

    @Query(" select i.evaluationWeight from Iterations i " +
            " where i.subjectId=:subjectId " +
            " and i.status='active' ")
    List<BigDecimal> getListWeight(Integer subjectId);


    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO(i.id, i.evaluationWeight) from Iterations i " +
            "JOIN Classes c on (c.subjectId = i.subjectId) " +
            "JOIN Projects p on (p.classId = c.id and p.id = :projectId) " +
            "Where i.status = :status and i.isOngoing = :isOngoing")
    IterationsDTO findFinalEvalWeight(@Param("projectId") Integer projectId,
                                      @Param("isOngoing") Integer isOngoing,
                                      @Param("status") String status);

    @Query("SELECT i.id from Iterations  i ")
    List<Integer> getIterationAdmin();
}
