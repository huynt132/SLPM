package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.entity.*;
import com.fpt.capstone.backend.api.BackEnd.repository.*;
import com.fpt.capstone.backend.api.BackEnd.service.*;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FunctionsServiceImpl implements FunctionsService {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ClassSettingsService classSettingsService;
    @Autowired
    private FeaturesRepository featuresRepository;
    @Autowired
    private FunctionsRepository functionsRepository;
    @Autowired
    private Validate validate;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private FeaturesService featuresService;
    @Autowired
    private FunctionLabelsRepository functionLabelsRepository;
    @Autowired
    private ClassSettingsRepository classSettingsRepository;
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public FunctionsDTO addFunction(FunctionsDTO functionsDTO) throws Exception {
        validate.validateFunction(functionsDTO);

        //feature
        String featureName = functionsDTO.getFeatureName();
        Features features = featuresRepository.findByNameAndProjectId(featureName, functionsDTO.getProjectId());
        if (features == null) {
            FeaturesDTO featuresDTO = new FeaturesDTO();
            featuresDTO.setProjectId(functionsDTO.getProjectId());
            featuresDTO.setName(featureName);
            featuresDTO.setStatus(ModelStatus.STATUS_ACTIVE);
            validate.validateFeature(featuresDTO);
            Features features2 = modelMapper.map(featuresDTO, Features.class);
            features2 = featuresRepository.save(features2);
            functionsDTO.setFeatureId(features2.getId());
        } else {
            functionsDTO.setFeatureId(features.getId());
        }
        if (functionsRepository.searchByNameOnFeature(functionsDTO.getFeatureId(), functionsDTO.getName()) > 0) {
            throw new Exception("name of this function is already exist in this feature!");
        }
        //get list label
        List<Integer> listLabel = functionsDTO.getLabels();
        List<String> listLabelTitle = classSettingsRepository.getListTitleLabel(listLabel);
        String labelTitle = "";
        if (listLabelTitle.size() > 0) {
            for (String key : listLabelTitle) {
                labelTitle += (labelTitle.isEmpty() ? "" : ", ") + key;
            }
        }

        //get git project Id

        Integer gitProjectId = projectRepository.getById(functionsDTO.getProjectId()).getGitLabProjectId();

        // save and sync
        Functions functions = modelMapper.map(functionsDTO, Functions.class);
        //comment sync
        functions.setGitLabId(syncIssue(gitProjectId, functionsDTO.getName(), labelTitle));
        functions.setStatus(ModelStatus.STATUS_PENDING); // default tạo mới function sẽ có status = pending
        functions = functionsRepository.save(functions);
        List<FunctionLabels> functionLabelsList = new ArrayList<>();


        if (listLabel.size() > 0) {
            for (Integer key : listLabel) {
                FunctionLabels functionLabels = new FunctionLabels();
                functionLabels.setFunctionId(functions.getId());
                functionLabels.setLabelId(key);
                functionLabelsList.add(functionLabels);
            }

        }

        functionLabelsRepository.saveAll(functionLabelsList);
        return getFunctionDetail(functions.getId());
    }

    private Integer syncIssue(Integer gitProjectId, String name, String listLabel) throws Exception {

        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/projects/" + gitProjectId + "/issues";

            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();

            map.add("title", name);
            map.add("labels", listLabel);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode idNode = root.path("iid");
            Integer id = idNode.asInt();
            return id;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    public void checkStatus(HttpStatus httpStatus) throws Exception {
        if (httpStatus == HttpStatus.NOT_FOUND) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.BAD_REQUEST) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.UNAUTHORIZED) {
            throw new Exception("please check your Gitlab token");
        }
        if (httpStatus == HttpStatus.FORBIDDEN) {
            throw new Exception("please check your Gitlab token's permission");
        }
    }
    @Override
    public FunctionsDTO deleteFunction(int id) {
        return null;
    }

    @Override
    public List<FunctionsDTO> showFunction() {
        return null;
    }

    @Override
    public FunctionsDTO updateFunction(FunctionsDTO functionsDTO) throws Exception {
        validate.validateFunction(functionsDTO);
        if (functionsRepository.getByFunctionId(functionsDTO.getId()) == null) {
            throw new Exception("Function is not exist");
        }

        Functions functions = functionsRepository.getById(functionsDTO.getId());

        //feature
        String featureName = functionsDTO.getFeatureName();
        Features features = featuresRepository.findByNameAndProjectId(featureName, functionsDTO.getProjectId());
        if (features == null) {
            FeaturesDTO featuresDTO = new FeaturesDTO();
            featuresDTO.setProjectId(functionsDTO.getProjectId());
            featuresDTO.setName(featureName);
            featuresDTO.setStatus(ModelStatus.STATUS_ACTIVE);
            validate.validateFeature(featuresDTO);
            Features features2 = modelMapper.map(featuresDTO, Features.class);
            features2 = featuresRepository.save(features2);
            functionsDTO.setFeatureId(features2.getId());
        } else {
            functionsDTO.setFeatureId(features.getId());
        }
        if (!functions.getName().equals(functionsDTO.getName())) {
            if (functionsRepository.searchByNameOnFeature(functionsDTO.getFeatureId(), functionsDTO.getName().toLowerCase()) > 0) {
                throw new Exception("name of this function is already exist in this feature!");
            }
        }
        //get list label
        List<Integer> listLabel = functionsDTO.getLabels();
        List<String> listLabelTitle = classSettingsRepository.getListTitleLabel(listLabel);
        String labelTitle = "";
        if (listLabelTitle.size() > 0) {
            for (String key : listLabelTitle) {
                labelTitle += (labelTitle.isEmpty() ? "" : ", ") + key;
            }
        }
        //get git project Id
        Integer gitProjectId = projectRepository.getById(functionsDTO.getProjectId()).getGitLabProjectId();
        //sync git lab
        Integer functionGitId = functions.getGitLabId();
        //comment sync
        syncIssueUpdate(gitProjectId, functionGitId, functionsDTO.getName(), labelTitle);

        List<Integer> oldLabel = listLabel(functions.getId());      //1,2,3,4
        List<Integer> newLabel = functionsDTO.getLabels();//1,2,5,6
        // List<Integer> listCloneNew = new ArrayList<Integer>(newLabel);
        List<Integer> listCloneOld = new ArrayList<Integer>(oldLabel);

        List<FunctionLabels> functionLabelsList = new ArrayList<>();
        if (!oldLabel.equals(newLabel)) {
            for (Integer key : newLabel) {
                FunctionLabels functionLabels = new FunctionLabels();
                functionLabels.setFunctionId(functions.getId());
                functionLabels.setLabelId(key);
                functionLabelsList.add(functionLabels);
                if (oldLabel.contains(key)) {
                    functionLabelsList.remove(functionLabels);
                    listCloneOld.remove(key);
                }
            }
            if (listCloneOld.size() > 0 || listCloneOld != null) {
                functionLabelsRepository.deleteFunctionLabel(functions.getId(), listCloneOld);
            }
            if (functionLabelsList.size() > 0 || functionLabelsList != null) {
                functionLabelsRepository.saveAll(functionLabelsList);
            }
        }

        functions = modelMapper.map(functionsDTO, Functions.class);
        functionsRepository.save(functions);
        functions = entityManager.find(Functions.class, functions.getId());
        return getFunctionDetail(functions.getId());
    }

    private void syncIssueUpdate(Integer gitProjectId, Integer functionGitId, String name, String labelTitle) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/projects/" + gitProjectId + "/issues/" + functionGitId;
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("title", name);
            map.add("labels", labelTitle);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response=  restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private List<Integer> listLabel(Integer functionId) {
        return functionLabelsRepository.getListLabel(functionId);
    }

    @Override
    public FunctionsDTO findById(int id) throws Exception {
        return null;
    }

    @Override
    public Page<FunctionsDTO> listBy(List<Integer> projectId, List<Integer> featureId, String name, String status, int page, int limit) throws Exception {
        // check quyen
//        List<Integer> authorityFunctionIds =  permissionService.getFunctionAuthority(null);
//        return functionsRepository.search(projectID, featureId,  name, status, PageRequest.of(page - 1, limit), authorityFunctionIds);
        Pageable pageable = PageRequest.of(page - 1, limit);
        if (ObjectUtils.isEmpty(projectId)) {
            projectId = new ArrayList<>();
            List<ProjectsListDTO> listProject = projectService.showProjectListInFeatue(featureId);
            if (listProject.size() == 0) {
                return functionsRepository.search(Collections.singletonList(0), featureId, name, status, pageable, null);
            } else {
                Integer firstProjectId = listProject.get(0).getValue();
                projectId.add(firstProjectId);
                return functionsRepository.search(projectId, featureId, name, status, pageable, null);
            }
        } else {
            return functionsRepository.search(projectId, featureId, name, status, pageable, null);
        }
    }

    @Override
    public FunctionsDTO getFunctionDetail(int id) {
        return functionsRepository.getFunctionDetail(id);
    }

    @Override
    public List<FunctionsDTO> showFunctionList() {
        return null;
    }

    @Override
    public List<ColumnTypeDTO> showColumns(String table, String field) {
        List<String> result = new ArrayList<>();
        List<ColumnTypeDTO> columnTypeDTOS = new ArrayList<>();
        String value = functionsRepository.showColumns(table, field);
        value = value.substring(value.indexOf('(') + 2, value.length() - 2);
        result.addAll(Arrays.asList(value.split("','")));
        result.forEach(r -> {
            columnTypeDTOS.add(new ColumnTypeDTO(r, r));
        });
        return columnTypeDTOS;
    }

    @Override
    public List<Map> toListOfMap(List<FunctionsDTO> functions) {
        List<Map> result = new ArrayList<>();
        functions.forEach(function -> {
            result.add(toMap(function));
        });
        return result;
    }

    @Override
    public Map<String, String> toMap(FunctionsDTO function) {
        Map<String, String> result = new HashMap<>();
        result.put("id", function.getId().toString());
        result.put("projectCode", function.getProjectCode());
        result.put("projectName", function.getProjectName());
        result.put("featureName", function.getFeatureName());
        result.put("name", function.getName());
        result.put("description", function.getDescription());
        result.put("priority", function.getPriority().toString());
        result.put("status", function.getStatus());
        result.put("statusName", function.getStatusName());
        result.put("typeName", function.getTypeName());
        result.put("created", function.getCreated().toString());
        result.put("modified", function.getModified().toString());
        result.put("createdByUser", function.getCreatedByUser());
        result.put("modifiedByUser", function.getModifiedByUser());
        return result;
    }

    public LinkedHashMap<String, String> generateFunctionExcelHeader() {

        LinkedHashMap<String, String> excelFunctionHeader = new LinkedHashMap<>();
        excelFunctionHeader.put("id", "Id");
        excelFunctionHeader.put("projectName", "Topic name");
        excelFunctionHeader.put("projectCode", "Project code");
        excelFunctionHeader.put("featureName", "Feature name");
        excelFunctionHeader.put("name", "Function name");
        excelFunctionHeader.put("description", "Description");
        excelFunctionHeader.put("priority", "Priority");
        excelFunctionHeader.put("status", "Submit status");
        excelFunctionHeader.put("statusName", "Status");
        excelFunctionHeader.put("typeName", "Function type");
        excelFunctionHeader.put("created", "Created");
        excelFunctionHeader.put("modified", "Modified");
        excelFunctionHeader.put("createdByUser", "Created by");
        excelFunctionHeader.put("modifiedByUser", "Modified by");
        return excelFunctionHeader;
    }

    public LinkedHashMap<String, String> configFunctionExcelHeader() {

        LinkedHashMap<String, String> excelFunctionHeader = new LinkedHashMap<>();
        excelFunctionHeader.put("projectId", "Project id");
        excelFunctionHeader.put("featureName", "Feature name");
        excelFunctionHeader.put("name", "Function name");
        excelFunctionHeader.put("description", "Description");
        excelFunctionHeader.put("priority", "Priority");
        excelFunctionHeader.put("statusId", "Status id");
        excelFunctionHeader.put("typeId", "Function type id");
        return excelFunctionHeader;
    }

    @Autowired
    private ExcelService excelService;
    @Autowired
    private ClassUserRepository classUserRepository;

    @Override
    public ResponseEntity<?> importFunctions(MultipartFile fileStream) throws IOException {
        try {
            Users users = userService.getUserLogin();
            Integer userId = users.getId();
            File file = convertMultiPartToFile(fileStream);
            List<Functions> functions = new ArrayList<Functions>();
            Integer headerIndex = 0;
            Boolean hasErr = false;
            Workbook workbook = new XSSFWorkbook(file);
            // Integer gitProjectId=projectRepository.getById(projectId).getGitLabProjectId();
            Sheet sheet = workbook.getSheetAt(0);
            Row headerRow = sheet.getRow(0);
            LinkedHashMap<String, String> excelFunctionHeader = configFunctionExcelHeader();
            Set<String> keySet = excelFunctionHeader.keySet();

            List<String> listKeys = new ArrayList<String>(keySet);
            String key = "";


            for (Integer i = 0; i < excelFunctionHeader.size(); i++) {
                key = listKeys.get(i);
                if (!(String.valueOf(headerRow.getCell(i))).equals(excelFunctionHeader.get(key))) {
                    throw new IOException("Wrong header at row 1, column " + CellReference.convertNumToColString(i));
                }
            }

            // Tạo cell mới ở header row = 0, cell = totalHeaderCell + 1 có tên là Result log
            excelService.createRow(headerRow, workbook, excelFunctionHeader.size(), "Error log");
            Iterator<Row> rows = sheet.rowIterator();
            Projects projects = new Projects();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();
                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                int i = -1;
                Iterator<Cell> cellsInRow = currentRow.cellIterator();
                Functions function = new Functions();
                function.setStatus(ModelStatus.STATUS_PENDING);
                int cellIdx = 0;
                String resultLog = "";
                short lastCellNum = currentRow.getLastCellNum();
                while (cellsInRow.hasNext()) {
                    i++;
                    Cell currentCell = currentRow.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                    if (i == lastCellNum) {
                        break;
                    }
                    if (currentCell == null || currentCell.getCellType().equals(CellType.BLANK)) {
                        currentCell.setCellValue("");
                    }


                    switch (cellIdx) {
                        case 0:
                            if (!currentCell.getCellType().equals(CellType.NUMERIC)) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Project id must be number";
                                break;
                            }
                            if (currentCell.getCellType().equals(CellType.BLANK)) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Project id cannot empty";
                                break;
                            }
                            projects = projectRepository.getById(Integer.valueOf((int) currentCell.getNumericCellValue()));
                            if (projects == null) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "projects does not exist";
                                break;
                            } else {
                                if (classUserRepository.findByProjectIdAndUserId(projects.getId(), userId) == null) {
                                    resultLog += (resultLog.isEmpty() ? "" : ", ") + "You are not in this project";
                                    break;
                                }
                            }

                            //check user in project

                            break;
                        case 1:
                            if (!currentCell.getCellType().equals(CellType.STRING)) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Feature name must be character";
                                break;
                            }
                            if (currentCell.getStringCellValue().trim().equals("")) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Feature name cannot empty";
                                break;
                            }
                            if (!currentCell.getStringCellValue().trim().matches(String.valueOf(ConstantsRegex.NAME_PATTERN))) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Feature name must be character";
                                break;
                            }
                            Features feature = featuresRepository.findByNameAndProjectId(currentCell.getStringCellValue().trim(), projects.getId());
                            if (feature == null) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Feature does not exist";
                                break;
                            }
                            function.setFeatureId(feature.getId());
                            break;
                        case 2:
                            if (!currentCell.getCellType().equals(CellType.STRING)) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Function name must be character";
                                break;
                            }
                            if (currentCell.getStringCellValue().trim().equals("")) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Function name cannot empty";
                                break;
                            }
                            if (!currentCell.getStringCellValue().trim().matches(String.valueOf(ConstantsRegex.NAME_PATTERN))) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Function name must be character";
                                break;
                            }
                            if (functionsRepository.searchByNameOnFeature(function.getFeatureId(), currentCell.getStringCellValue().trim()) > 0) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "This function is already exist in this feature";
                                break;
                            }
                            function.setName(currentCell.getStringCellValue());
                            break;
                        case 3:
                            if (!currentCell.getStringCellValue().trim().equals("")) {
                                if (!currentCell.getCellType().equals(CellType.STRING)) {
                                    resultLog += (resultLog.isEmpty() ? "" : ", ") + "Description must be character";
                                    break;
                                }
                            }
                            function.setDescription(currentCell.getStringCellValue());
                            break;
                        case 4:
                            if (!currentCell.getCellType().equals(CellType.NUMERIC)) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Priority is positive number!";
                                break;
                            } else if (currentCell.getCellType().equals(CellType.NUMERIC)) {
                                if (currentCell.getNumericCellValue() <= 0) {
                                    resultLog += (resultLog.isEmpty() ? "" : ", ") + "Priority is positive number!";
                                    break;
                                } else {
                                    function.setPriority((int) currentCell.getNumericCellValue());
                                    break;
                                }
                            } else {
                                function.setPriority(null);
                                break;
                            }
                        case 5:
                            if (!currentCell.getCellType().equals(CellType.NUMERIC)) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Status id is positive number!";
                                break;
                            } else {
                                if (currentCell.getNumericCellValue() <= 0) {
                                    resultLog += (resultLog.isEmpty() ? "" : ", ") + "Status id is positive number!";
                                    break;
                                }
                                List<ClassSettingOptionDTO> list = classSettingsService.getFunctionStatusOption(projects.getId());
                                List<Integer> listStatus = new ArrayList<>();
                                for (ClassSettingOptionDTO typeId : list) {
                                    listStatus.add(typeId.getValue());
                                }
                                if (!listStatus.contains(Integer.valueOf((int) currentCell.getNumericCellValue()))) {
                                    resultLog += (resultLog.isEmpty() ? "" : ", ") + "Status id is not exist!";
                                    break;
                                }
                                function.setStatusId((int) currentCell.getNumericCellValue());
                                break;
                            }
                        case 6:
                            if (!currentCell.getCellType().equals(CellType.NUMERIC)) {
                                resultLog += (resultLog.isEmpty() ? "" : ", ") + "Type id is positive number!";
                                break;
                            } else {
                                if (currentCell.getNumericCellValue() <= 0) {
                                    resultLog += (resultLog.isEmpty() ? "" : ", ") + "Type id is positive number!";
                                    break;
                                }
                                List<ClassSettingOptionDTO> list = classSettingsService.getFunctionTypeOption(projects.getId());
                                List<Integer> listStatus = new ArrayList<>();
                                for (ClassSettingOptionDTO typeId : list) {
                                    listStatus.add(typeId.getValue());
                                }
                                if (!listStatus.contains(Integer.valueOf((int) currentCell.getNumericCellValue()))) {
                                    resultLog += (resultLog.isEmpty() ? "" : ", ") + "Type id is not exist!";
                                    break;
                                }
                                function.setTypeId((int) currentCell.getNumericCellValue());
                                break;
                            }
                        default:
                            break;
                    }
                    cellIdx++;
                }
                if (!resultLog.isEmpty()) {
                    hasErr = true;
                    //ghi them zo shell
                    Cell logShell = currentRow.createCell(cellIdx);
                    logShell.setCellValue(resultLog);
                    sheet.setColumnWidth(headerIndex, 5000);
                    continue;
                }
                if (projects != null) {
                    Integer gitProjectId = projects.getGitLabProjectId();
                    function.setGitLabId(syncIssue(gitProjectId, function.getName(), ""));
                    functions.add(function);
                }
            }


            if (!hasErr) {
                functionsRepository.saveAll(functions);
                workbook.close();
                return ResponseEntity.status(200).body(
                        ApiResponse.builder()
                                .success(true)
                                .message("Import excel successfully").build()
                );
            } else {
                String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());

                File currDir = new File(String.valueOf(this.getClass().getResourceAsStream("/ExportFile/function/.")));
                String path = currDir.getAbsolutePath();
                String fileLocation = path.substring(0, path.length() - 1) + timeStamp + "-import-function-error-log.xlsx";
                FileOutputStream outputStream = new FileOutputStream(fileLocation);
                workbook.write(outputStream);
                workbook.close();
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        ApiResponse.builder()
                                .success(false)
                                .message("Import excel error").data(excelService.uploadExcelToGS(fileLocation, "funciton")).build()
                );
            }
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } catch (InvalidFormatException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<FunctionListDTO> showFeatureList() {
        List<FunctionListDTO> featuresListDTOS = functionsRepository.getLabelList();
        return featuresListDTOS;
    }

    @Autowired
    UserService userService;

    @Override
    public List<FunctionListTrackingDTO> showTrackingFeature(Integer projectId, List<Integer> featureName, Users users, String functionStatus) {
        if (Objects.equals(users.getSettings().getId(), RoleID.STUDENT)) {
            List<FunctionListTrackingDTO> featuresListDTOS = functionsRepository.getFunctionTracking(users.getId(), projectId, featureName, functionStatus);
            if (featuresListDTOS.size()==0) return null;
            return featuresListDTOS;
        }
        return null;
    }

    @Override
    public List<FunctionTrackingSubmitListDTO> showListFunctionInTraking(List<Integer> projectId, List<Integer> milestoneId) {
        return functionsRepository.showListFunctionInTracking(projectId, milestoneId);
    }


    private File convertMultiPartToFile(MultipartFile fileStream) throws IOException {
        if (fileStream.getSize() > 20000000) {
            throw new IOException("Update has not been successfully uploaded. Requires less than 10 MB size");
        }
        File convFile = new File(fileStream.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(fileStream.getBytes());
        fos.close();
        System.out.println("convert " + fileStream.getOriginalFilename() + " to " + convFile.getName());
        return convFile;
    }


    @Override
    public String exportFunctions(List<FunctionsDTO> functions) throws IOException {
        LinkedHashMap<String, String> excelFunctionHeader = generateFunctionExcelHeader();
        List<Map> functionMap = toListOfMap(functions);
        return excelService.exportExcel(excelFunctionHeader, functionMap, "function");
    }

    @Override
    public Integer getProjectByFunction(Integer functionId) {
        return functionsRepository.findProjectId(functionId);
    }

    @Override
    public List<Integer> listLabelFunction(Integer functionId) {
        return functionsRepository.getFunctionLabel(functionId);
    }
}