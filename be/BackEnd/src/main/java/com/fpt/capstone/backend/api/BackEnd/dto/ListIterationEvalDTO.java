package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListIterationEvalDTO {
    private String iterationName;
    private Integer milestoneId;

    private String key;
    private Integer id;

    public ListIterationEvalDTO(String iterationName, Integer id, Integer milestoneId) {
        this.iterationName = iterationName;
        this.id = id;
        this.key="iterationGrade-"+id;
        this.milestoneId = milestoneId;
    }
}
