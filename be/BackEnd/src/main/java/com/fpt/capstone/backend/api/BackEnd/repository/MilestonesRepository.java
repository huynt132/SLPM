package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.Milestones;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface MilestonesRepository extends JpaRepository<Milestones, Integer> {
    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO(" +
            "m.id,m.iterationId,i.name,m.classId,c.code,i.name,m.gitLabId,m.description,m.from,m.to,m.status," +
            "m.created,m.created_by,u1.fullName,m.modified,m.modified_by,u2.fullName) " +
            " FROM Milestones m   " +
            " join Iterations i on i.id= m.iterationId " +
            " join Classes c on c.id= m.classId " +
            " LEFT join Users u1 on u1.id= m.created_by " +
            " LEFT join Users u2 on u2.id=m.modified_by " +
            " WHERE m.id =?1")
    MilestonesDTO getMilestonesDetail(int id);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO(" +
            "m.id,m.iterationId,i.name,m.classId,c.code,i.name,m.gitLabId,m.description,m.from,m.to,m.status," +
            "m.created,m.created_by,u1.fullName,m.modified,m.modified_by,u2.fullName) " +
            " FROM Milestones m   " +
            " left join Iterations i on i.id= m.iterationId " +
            " left join Classes c on c.id= m.classId " +
            " left join Users u1 on u1.id= m.created_by " +
            " left join Users u2 on u2.id=m.modified_by " +
            " WHERE (COALESCE(:iterationId) is null or i.id IN :iterationId)" +
            " AND (COALESCE(:classId) is null or c.id IN :classId) " +
            " AND (COALESCE(:trainerId) is null or c.trainerId IN :trainerId)" +
            " AND (:title is null or i.name LIKE %:title%)" +
            " AND (i.status = 'active')" +
            " AND (:status is null or m.status like :status% )" +
            " AND (COALESCE(:authorityMilestoneIds) is null or m.id in :authorityMilestoneIds)" +
            " order by m.from")
    Page<MilestonesDTO> search(@Param("iterationId") List<Integer> iterationId,
                               @Param("classId") List<Integer> classId,
                               @Param("trainerId") List<Integer> trainerId,
                               @Param("title") String title,
                               @Param("status") String status,
                               Pageable pageable,
                               @Param("authorityMilestoneIds") List<Integer> authorityMilestoneIds);

    //Milestones getMilestonesByTitle(String title);

    Integer countByClassIdAndIterationId(int ClassID, int IterationID);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.MilestonesListDTO(" +
            "m.id,i.name) " +
            " FROM Milestones m   " +
            " left join Iterations i on i.id= m.iterationId " +
            " left join Classes c on c.id= m.classId " +
            " left join Projects p on p.classId=c.id" +
            " left join Users u1 on u1.id= m.created_by " +
            " left join Users u2 on u2.id=m.modified_by " +
            " WHERE (COALESCE(:authorityMilestoneIds) is null or m.id in :authorityMilestoneIds)" +
            " AND(COALESCE(:classId) is null or c.id IN :classId) " +
            " AND (COALESCE(:projectId) is null or p.id IN :projectId)" +
            " group by m.id")
    List<MilestonesListDTO> getMilestonesByClass(@Param("classId") List<Integer> classId,
                                                 @Param("projectId") List<Integer> projectId,
                                                 @Param("authorityMilestoneIds") List<Integer> authorityMilestoneIds);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.MilestonesSyncDTO(" +
            " m.id,i.name,m.description,m.classId,m.gitLabId,m.from,m.to ) " +
            " FROM Milestones m  " +
            " left join Iterations i on i.id= m.iterationId " +
            " join Classes c on  m.classId = c.id " +
            " WHERE c.id=:classId AND " +
            " m.status='open'" +
            " group by m.id")
    List<MilestonesSyncDTO> getMilestonesSync(Integer classId);

    @Query("Select new com.fpt.capstone.backend.api.BackEnd.dto.MilestoneSubmitDTO(" +
            "m.id, p.id, s.status, p.name, t.status, f.id, t.milestoneId, s.packageFileLink) FROM Milestones m " +
            "Join Classes c on m.classId = c.id " +
            "Join Projects p on (p.classId = c.id and p.status = 'active') " +
            "JOIN Submits s on (s.milestoneId = m.id and s.projectId = p.id) " +
            "LEFT join Features ft on ft.projectId = p.id " +
            "LEFT JOIN Functions f on (f.featureId = ft.id)" +
            "LEFT JOIN Trackings t on (t.functionId = f.id AND m.id = :milestoneId ) " +
            "WHERE (COALESCE(:classId) is null or c.id = :classId ) " +
            "AND (COALESCE(:projectId) is null or p.id in :projectId ) " +
            "AND s.milestoneId = :milestoneId " +
            "AND m.status <> 'cancelled' " +
            "GROUP BY p.id, f.id")
    List<MilestoneSubmitDTO> getMilestoneSubmits(Integer classId, List<Integer> projectId, Integer milestoneId);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.TeamEvaluationsDTO(te.id, ec.name, ec.id, te.milestoneId, " +
            "p.id, te.grade, ec.evaluationWeight, te.comment, i.id) FROM EvaluationCriteria ec " +
            "JOIN Iterations i on i.id = ec.iterationId " +
            "JOIN Classes  c on c.subjectId = i.subjectId " +
            "JOIN Projects p on p.classId = c.id " +
            "JOIN Milestones m on (i.id = m.iterationId and m.id = :milestoneId)" +
            "LEFT JOIN TeamEvaluations te on (ec.id = te.criteriaId and te.projectId = p.id and ec.status = 'active') " +
            "Where COALESCE(:projectId) is null or p.id in :projectId " +
            "AND ec.type = 'team' " +
            "AND ec.status = 'active'" +
            "group by p.id, m.id, ec.id")
    List<TeamEvaluationsDTO> getMilestoneTeamEval(List<Integer> projectId, Integer milestoneId);

    @Query("Select m.id from Milestones m " +
            "left join Classes c on c.id=m.classId " +
            "left join Subjects s on s.id=c.subjectId " +
            "where s.authorId=:id" +
            " group by m.id")
    List<Integer> getMilestoneAuthor(Integer id);

    @Query(" Select m.id from Milestones m " +
            " left join Classes c on c.id=m.classId " +
            " where c.trainerId=:id" +
            " group by m.id")
    List<Integer> getMilestoneTrainer(Integer id);

    @Query(" Select m.id from Milestones m " +
            " left join Classes c on c.id=m.classId " +
            " left join ClassUsers cu on c.id=cu.classId" +
            " where cu.userId=:id" +
            " group by m.id")
    List<Integer> getMilestoneStudent(Integer id);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.MilestoneCriteriaDTO(m.id, m.iterationId, ec.id," +
            " ec.type, ec.evaluationWeight) FROM Milestones m " +
            "JOIN EvaluationCriteria ec on (ec.iterationId = m.iterationId and ec.status = 'active') " +
            "WHERE m.classId = :classId and m.status <> 'cancelled' ")
    List<MilestoneCriteriaDTO>getIterationCriteria(@Param("classId") Integer classId);

    @Query("select m.id from Milestones m where m.classId=:classId order by m.id")
    List<String> getListMilestoneClassId(Integer classId);

    @Query("select m.id from Milestones m")
    List<Integer> getMilestoneAdmin();

    //    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO() " +
//            " FROM Milestones m " +
//            " join EvaluationCriterias ec on (ec.iterationId = m.iterationId and ec.status = 'active' and ec.teamEvaluation = 0) " +
//            " WHERE m.id = :milestoneId")
//    MilestonesDTO getMilestoneEval(@Param("milestoneId") Integer milestoneId);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO(m.id, c.id) from Milestones m " +
            "JOIN Classes c on (c.id = m.classId and (COALESCE(:trainerId) is null or c.trainerId = :trainerId)) " +
            "JOIN Subjects s on (s.id = c.subjectId and (COALESCE(:authorId) is null or s.authorId = :authorId)) " +
            "JOIN ClassUsers cu on (cu.classId = c.id and (COALESCE(:studentId) is null or cu.userId = :studentId))")
    List<MilestonesDTO> getClassMilestoneAuthority(@Param("trainerId") Integer trainerId,
                                                   @Param("authorId") Integer authorId,
                                                   @Param("studentId") Integer studentId
    );

    @Query("Select new com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO(m.id, s.status, m.from, m.to) " +
            "from Milestones m " +
            "join Projects p on (p.id = :projectId and p.classId = m.classId) " +
            "join Submits s on (s.milestoneId = m.id)" +
            "where m.status <> 'cancelled' " +
            "order by m.to desc")
    List<MilestonesDTO> getLastMilestone (@Param("projectId") Integer projectId, Pageable pageable);

    @Query("Select new com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO(m.id, i.id, c.id, i.isOngoing) " +
            "from Milestones m " +
            "join Classes c on c.id = m.classId " +
            "join Iterations i on i.id = m.iterationId " +
            "where m.id = :milestoneId")
    MilestonesDTO getMilestonesDetail(@Param("milestoneId") Integer milestoneId);

    @Query("Select m.id " +
            "from Milestones m " +
            "join Iterations i on i.id = m.iterationId " +
            "join EvaluationCriteria ec on (ec.iterationId = i.id and ec.type = 'demo') " +
            "where m.id = :milestoneId")
    List<Integer> checkLocCriteria(@Param("milestoneId") Integer milestoneId);
}
