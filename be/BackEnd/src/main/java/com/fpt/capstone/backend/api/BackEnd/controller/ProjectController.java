package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.ProjectsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.repository.MilestonesRepository;
import com.fpt.capstone.backend.api.BackEnd.service.EvaluationJobService;
import com.fpt.capstone.backend.api.BackEnd.service.MilestonesService;
import com.fpt.capstone.backend.api.BackEnd.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("project")
@CrossOrigin(origins = "*", maxAge = 3600)

public class ProjectController {
    @Autowired
    ProjectService projectService;

    @Autowired
    EvaluationJobService evaluationJobService;

    @Autowired
    MilestonesRepository milestonesRepository;
//    @Autowired
//    private JobScheduler jobScheduler;

    @PostMapping("/add")
    public ResponseEntity<?> addProject(@RequestBody ProjectsDTO projectsDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add project successfully");
            response.setData(projectService.addProject(projectsDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add project fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findProject(
            @RequestParam(value = "classId", required = false) List<Integer> classId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            Page<ProjectsDTO> projectsDTOS = projectService.listBy(classId, name, status, page, per_page);
            List<ProjectsDTO> projectsDTOS1 = projectsDTOS.getContent();
            response.setSuccess(true);
            response.setMessage("Get project list successfully");
            response.setData(projectsDTOS1);
            response.setTotal(projectsDTOS.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get project list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editProject(@RequestBody ProjectsDTO projectsDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update project successfully");
            response.setData(projectService.updateProject(projectsDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update project fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getList")
    public ResponseEntity<?> getListClass(
            @RequestParam(value = "subjectId", required = false) List<Integer> subjectId,
            @RequestParam(value = "classId", required = false) List<Integer> classId,
            @RequestParam(value = "status", required = false) String status) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get project list successfully");
            response.setData(projectService.showProjectList(subjectId, classId, status));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get project list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

//    @GetMapping("/getListGitProjectId")
//    public ResponseEntity<?> getListGitProjectId(
//            @RequestParam(value = "classId", required = false) List<Integer> classId,
//            @RequestParam(value = "projectId", required = false) List<Integer> projectId) {
//        ApiResponse response = new ApiResponse();
//        try {
//            response.setSuccess(true);
//            response.setMessage("Get project id list successfully");
//            response.setData(projectService.showProjectGitId(classId, projectId));
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            response.setSuccess(false);
//            response.setMessage("Get project id list fail, " + e.getMessage());
//            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//        }
//    }

//    @GetMapping("/test")
//    public ResponseEntity<?> test() {
//        ApiResponse response = new ApiResponse();
//        try {
//            response.setSuccess(true);
//            response.setMessage("Get project id list successfully");
//            JobId jobId = BackgroundJob.enqueue(() -> System.out.println("easy"));
//            JobId jobId1 = BackgroundJob.enqueue(() -> evaluationJobService.testJobs());
////            response.setData(jobId);
////            jobScheduler.enqueue(() -> evaluationJobService.testJobs());
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            response.setSuccess(false);
//            response.setMessage("Get project id list fail, " + e.getMessage());
//            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//        }
//    }

    @Autowired
    MilestonesService milestoneService;

    //37484324
    // "glpat-6TfhZJ_XRNChhHD7gum7"

}
