package com.fpt.capstone.backend.api.BackEnd.controller;


import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.IterationResponse;
import com.fpt.capstone.backend.api.BackEnd.service.SubjectSettingsService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("subjectSettings")
@CrossOrigin(origins = "*", maxAge = 3600)

public class SubjectSettingsController {
    @Autowired
    private SubjectSettingsService subjectSettingsService;
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private ModelMapper modelMapper;
    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @PostMapping("/add")
    public ResponseEntity<?> addSubjectSetting(@RequestBody SubjectSettingInputDTO subjectSettingInputDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add subject setting successfully");
            response.setData(subjectSettingsService.addSubjectSetting(subjectSettingInputDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add subject setting fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findIterationsByTile(@RequestParam(value = "settingTypeId", required = false) List<Integer> settingTypeId,
                                                  @RequestParam(value = "subjectId", required = false) List<Integer> subjectId,
                                                  @RequestParam(value = "settingTitle", required = false) String key_title,
                                                  @RequestParam(value = "settingValue", required = false) String key_value,
                                                  @RequestParam(value = "status", required = false) String status,
                                                  @RequestParam("page") int page,
                                                  @RequestParam("limit") int limit) {
        IterationResponse response = new IterationResponse();
        try {
            if (subjectId == null) {
                List<Integer> authoritySubjectIds = permissionService.getSubjectAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurSubject(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }

                subjectId = Arrays.asList(authoritySubjectIds.get(0));
            }

            response.setCurSubject(subjectId.get(0));

            Page<SubjectSettingsDTO> subjectSettingsDTOS = subjectSettingsService
                    .searchBy(settingTypeId, subjectId, key_title, key_value, status, page, limit);
            List<SubjectSettingsDTO> subjectSettingsDTO = subjectSettingsDTOS.getContent();
            response.setSuccess(true);

            response.setMessage("Get subject setting list successfully");
            response.setData(subjectSettingsDTO);
            response.setTotal(subjectSettingsDTOS.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(limit);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get list subject setting fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/delete")
    public ResponseEntity<?> deleteSetting(@RequestParam("id") String id) {
        ApiResponse response = new ApiResponse();
        try {
            if (!id.matches(ConstantsRegex.NUMBER_PATTERN.toString())) {
                throw new Exception("setting id delete must be integer");
            }
            response.setSuccess(true);
            response.setMessage("delete subject setting successfully");
            response.setData(subjectSettingsService.deleteSubjectSetting(Integer.parseInt(id)));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Delete subject setting fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/findById")
    public ResponseEntity<?> getSettingDetail(@RequestParam("id") int id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get subject settings detail successfully");
            response.setData(subjectSettingsService.getSubjectSettingDetail(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get subject settings detail fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @PostMapping("/edit")
    public ResponseEntity<?> editSubject(@RequestBody SubjectSettingInputDTO subjectSettingInputDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update subject setting successfully");
            response.setData(subjectSettingsService.updateSubjectSetting(subjectSettingInputDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update subject setting fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getType")
    public ResponseEntity<?> getType() {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get subject setting type successfully");
            response.setData(subjectSettingsService.getSubjectSettingType());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get subject setting type fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
