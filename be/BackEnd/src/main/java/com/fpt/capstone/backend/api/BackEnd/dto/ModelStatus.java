package com.fpt.capstone.backend.api.BackEnd.dto;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ModelStatus {
    public final static String STATUS_PENDING = "pending";
    public final static String STATUS_COMMITTED = "committed";
    public final static String STATUS_SUBMITTED = "submitted";
    public final static String STATUS_EVALUATED = "evaluated";
    public final static String STATUS_REJECTED = "rejected";
    public static final String[] SUBMIT_STATUS_HAS_TEAM_EVAL = new String[]{
            STATUS_SUBMITTED,
            STATUS_EVALUATED
    };

    public static final Map<String, String> NEXT_STATUS_STEP = new HashMap<String, String>() {{
        put(STATUS_PENDING, STATUS_COMMITTED);
        put(STATUS_COMMITTED, STATUS_SUBMITTED);
        put(STATUS_SUBMITTED, STATUS_EVALUATED);
    }};

    public final static String STATUS_ACTIVE = "active";
    public final static String STATUS_INACTIVE = "inactive";

    public final static Integer IS_ONGOING = 1;
    public final static Integer IS_PROJECT_LEADER = 1;

    public final static Integer GIT_LAB_DEV = 30;
    public final static Integer GIT_LAB_LEADER = 40;
    public final static Integer GIT_LAB_TEACHER = 50;
    public final static Integer COMPLEXITY_SETTING_TYPE_ID = 11;
    public final static Integer QUALITY_SETTING_TYPE_ID = 12;

    // các base setting type trong bảng settings
    public final static Integer SETTING_FUNCTION_TYPE_ID = 76;
    public final static Integer SETTING_LABEL_TYPE_ID = 71;
    public final static Integer SETTING_FUNCTION_STATUS_ID = 68;
    // end các base setting type trong bảng settings

    public final static Integer ONGOING_WEIGHT_SETTING_TYPE_ID = 18;
    public final static Integer PRES_WEIGHT_SETTING_TYPE_ID = 19;
    public final static String COMPLEXITY_TITLE = "complexity";
    public final static String QUALITY_TITLE = "quality";

    public final static Map<String, Integer> SETTING_TYPE_MAPPER = Stream.of(new Object[][] {
            { COMPLEXITY_TITLE, COMPLEXITY_SETTING_TYPE_ID },
            { QUALITY_TITLE, QUALITY_SETTING_TYPE_ID },
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (Integer) data[1]));

    // Milestone
    public final static String MILESTONE_STATUS_CANCELLED = "cancelled";
    public final static Integer IS_TEAM_EVALUATION = 1;

    public final static String SUBMIT_STATUS_PENDING = "pending";

    // Evaluation criteria
    public final static String CRITERIA_TYPE_TEAM = "team";
    public final static String CRITERIA_TYPE_DEMO = "demo";
    public final static String CRITERIA_TYPE_OTHER = "individual";
    public static final String[] CRITERIA_TYPE_INDIVIDUAL = new String[]{
            CRITERIA_TYPE_DEMO,
            CRITERIA_TYPE_OTHER
    };

    // Setting ID
    public final static Integer Setting_Complex_ID = 11;
    public final static Integer Setting_QUALITY_ID = 12;

    public static final Integer[] SETTING_EVALUATE_TYPE = new Integer[]{
            Setting_Complex_ID,
            Setting_QUALITY_ID
    };

    // role
    public static final String ROLE_ADMIN = "admin";
    public static final String ROLE_AUTHOR = "author";
    public static final String ROLE_TRAINER = "trainer";
    public static final String ROLE_STUDENT = "student";

    public static final Integer ROLE_ADMIN_ID = 7;
    public static final Integer ROLE_AUTHOR_ID = 8;
    public static final Integer ROLE_TRAINER_ID = 9;
    public static final Integer ROLE_STUDENT_ID = 10;

    public final static Map<String, Integer> SETTING_ROLE_MAPPER = Stream.of(new Object[][] {
            { ROLE_ADMIN, ROLE_ADMIN_ID },
            { ROLE_AUTHOR, ROLE_AUTHOR_ID },
            { ROLE_TRAINER, ROLE_TRAINER_ID },
            { ROLE_STUDENT, ROLE_STUDENT_ID },
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (Integer) data[1]));

    // key for dashboard
    public final static String LABEL_TOTAL = "Total";
    public final static String LABEL_TEAM_BASED = "Team-based";
    public final static String LABEL_INDIVIDUAL_BASED = "Individual-based";

    public final static String BACKGROUND_LABEL_TOTAL = "#82C272";
    public final static String BACKGROUND_LABEL_TEAM_BASED = "#00A88F";
    public final static String BACKGROUND_LABEL_INDIVIDUAL_BASED = "#0087AC";

    //const empty array
    public final static Integer[] EMPTY_ARRAY = {};
}
