package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.service.SubmitService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("submits")
@CrossOrigin(origins = "*", maxAge = 3600)
public class SubmitsController {
    @Autowired
    private SubmitService submitService;
    @Autowired
    private PermissionService permissionService;

    @GetMapping("/getMilestoneSubmits")
    public ResponseEntity<?> getMilestoneSubmits(
            @RequestParam(value = "classId", required = false) Integer classId,
            @RequestParam(value = "projectId", required = false) List<Integer> projectId,
            @RequestParam(value = "milestoneId", required = false) Integer milestoneId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get milestone submit successfully");
            //getCurr
            HashMap<String, Object> data = new HashMap<>();
            //get Cur
            if (classId == null || milestoneId == null) {
                List<MilestonesDTO> curFilter = permissionService.getClassMilestoneAuthority(null);
                if (curFilter == null || curFilter.isEmpty()) {
                    data.put("curClassId", null);
                    data.put("curMilestoneId", null);
                    data.put("milestoneSubmit", null);
                    response.setData(data);

                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                classId = curFilter.get(0).getClassId();
                milestoneId = curFilter.get(0).getId();
            }
            List<MilestoneSubmitDTO> milestoneSubmits = submitService.getMilestoneSubmits(classId, projectId, milestoneId);
            data.put("milestoneSubmits", milestoneSubmits);
            data.put("curClassId", classId);
            data.put("curMilestoneId", milestoneId);

            response.setData(data);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get milestone submit details fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/editTeamEval")
    public ResponseEntity<?> editTeamEval(@RequestBody TeamEvaluationsDTO teamEvaluationsDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update team evaluation successfully");
            response.setData(submitService.editTeamEvaluation(teamEvaluationsDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update team evaluation fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/editIndividualEvaluation")
    public ResponseEntity<?> editIndividualEvaluation(@RequestBody MemberEvaluationsDTO memberEvaluationsDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update individual evaluation successfully");
            response.setData(submitService.editIndividualEvaluation(memberEvaluationsDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update individual evaluation fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getFunctionEvaluate")
    public ResponseEntity<?> getFunctionEvaluate(
            @RequestParam(value = "functionId") Integer functionId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get function evaluation information successfully");
            FunctionsDTO functionsDTO = submitService.getFunctionEvaluate(functionId);
            response.setData(functionsDTO);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function evaluation information fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getNewLocFunction")
    public ResponseEntity<?> getNewLocFunction(
            @RequestParam(value = "functionId") Integer functionId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get function evaluation information successfully");
            FunctionsDTO functionsDTO = submitService.getNewLocFunction(functionId);
            response.setData(functionsDTO);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get function evaluation information fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getFunctionCount")
    public ResponseEntity<?> getFunctionCount(
            @RequestParam(value = "projectId", required = false) Integer projectId,
            @RequestParam(value = "milestoneId", required = false) List<Integer> milestoneId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get tracking detail successfully");
            List<FunctionCountDTO> count = submitService.getFunctionCount(projectId, milestoneId);
            response.setData(count);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get milestone details fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getFunctionStatusCount")
    public ResponseEntity<?> getFunctionStatusCount(
            @RequestParam(value = "classId", required = false) Integer classId,
            @RequestParam(value = "milestoneId", required = false) List<Integer> milestoneId) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get tracking detail successfully");
            List<FunctionSubmitStatusCountDTO> count = submitService.getFunctionStatusCount(classId, milestoneId);
            response.setData(count);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get milestone details fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

//    @GetMapping("/getGradeDashboard")
//    public ResponseEntity<?> getGradeDashboard(
//            @RequestParam(value = "projectId") Integer projectId,
//            @RequestParam(value = "milestoneId") List<Integer> milestoneId) throws Exception {
//        ApiResponse response = new ApiResponse();
//        try {
//            response.setSuccess(true);
//            response.setMessage("Get tracking detail successfully");
//            List<FunctionCountDTO> count = submitService.getFunctionCount(projectId, milestoneId);
//            response.setData(count);
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            response.setSuccess(false);
//            response.setMessage("Get milestone details fail, " + e.getMessage());
//            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//        }
//    }

    @PostMapping("/updateStatus")
    public ResponseEntity<?> updateStatus(@RequestBody MilestoneSubmitDTO data) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update status successfully");
            submitService.updateStatus(data.getProjectId(), data.getMilestoneId());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update status fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/locEvaluate")
    public ResponseEntity<?> locEvaluate(
            @RequestBody FunctionsDTO functionsDTO) {
        ApiResponse response = new ApiResponse();
        try {
            submitService.evaluateFunction(functionsDTO);
            response.setSuccess(true);
            response.setMessage("Evaluate for function " + functionsDTO.getName() + " successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Evaluate function fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
