package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.Data;

@Data
public class UpdateTrackingDTO {
   private Integer trackingId;
   private Integer assigneeId;
   private Integer functionId;
   private Integer projectId;
}
