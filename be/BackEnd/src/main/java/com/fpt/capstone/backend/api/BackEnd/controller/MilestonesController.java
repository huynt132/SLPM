package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestoneInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.MilestoneResponse;
import com.fpt.capstone.backend.api.BackEnd.service.MilestonesService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("milestones")
@CrossOrigin(origins = "*", maxAge = 3600)

public class MilestonesController {
    @Autowired
    private MilestonesService milestonesService;

    @Autowired
    private PermissionService permissionService;

    @PostMapping("/add")
    public ResponseEntity<?> addMilestone(@RequestBody MilestoneInputDTO milestoneInput) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add milestone successfully");
            response.setData(milestonesService.add(milestoneInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add milestone fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getDetail")
    public ResponseEntity<?> findMilestoneByID(@RequestParam(name = "id") int id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get milestone successfully");
            response.setData(milestonesService.showDetail(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get milestone fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editMilestone(@RequestBody MilestoneInputDTO milestoneInput) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update milestone successfully");
            response.setData(milestonesService.edit(milestoneInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update milestone fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findMilestone(
            @RequestParam(value = "iterationId", required = false) List<Integer> iterationId,
            @RequestParam(value = "classId", required = false) List<Integer> classId,
            @RequestParam(value = "trainerId", required = false) List<Integer> trainerId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam("page") int page, @RequestParam("limit") int limit) {
        MilestoneResponse response = new MilestoneResponse();
        try {
            if (classId == null) {
                List<Integer> authoritySubjectIds = permissionService.getClassAuthor(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurClassId(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                classId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurClassId(classId.get(0));
            Page<MilestonesDTO> milestones = milestonesService.listBy(iterationId, classId, trainerId, title, status, page, limit);
            List<MilestonesDTO> milestonesDTOS = milestones.getContent();
            response.setSuccess(true);
//            if (milestonesDTOS.size() > 0) {
//                response.setCurClassId(milestonesDTOS.get(0).getClassId());
//            }
            response.setMessage("Get milestone list successfully");
            response.setData(milestonesDTOS);
            response.setTotal(milestones.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(limit);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get iteration list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getList")
    public ResponseEntity<?> getListMilestone(
            @RequestParam(value = "classId", required = false) List<Integer> classId,
            @RequestParam(value = "projectId", required = false) List<Integer> projectId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get milestone list successfully");
            response.setData(milestonesService.listMilestoneByClass(classId, projectId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get milestone list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getListMilestoneSync")
    public ResponseEntity<?> getListMilestoneSync(
            @RequestParam(value = "projectId", required = false) Integer projectId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get milestone list successfully");
            response.setData(milestonesService.listMilestoneSync(projectId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get milestone list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
