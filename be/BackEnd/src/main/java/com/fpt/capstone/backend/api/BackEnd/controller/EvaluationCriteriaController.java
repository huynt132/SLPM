package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.EvaluationCriteriaDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.EvalCriteriaResponse;
import com.fpt.capstone.backend.api.BackEnd.service.EvaluationCriteriaService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("criteria")
@CrossOrigin(origins = "*", maxAge = 3600)
@PreAuthorize("hasAuthority('Admin')||hasAuthority('Trainer')||hasAuthority('Author')")
public class EvaluationCriteriaController {
    @Autowired
    EvaluationCriteriaService evaluationCriteriaService;
    @Autowired
    private PermissionService permissionService;

    @PostMapping("/add")
    public ResponseEntity<?> addCriteria(@RequestBody EvaluationCriteriaDTO evaluationCriteriaDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add evaluation criteria successfully");
            response.setData(evaluationCriteriaService.addCriteria(evaluationCriteriaDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add evaluation criteria fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findCriteria(
            @RequestParam(value = "subjectId", required = false) List<Integer> subjectId,
            @RequestParam(value = "iterationId", required = false) List<Integer> iterationId,
            @RequestParam(value = "authorId", required = false) List<Integer> authorId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        EvalCriteriaResponse response = new EvalCriteriaResponse();
        try {
            if (subjectId == null) {
                List<Integer> authoritySubjectIds = permissionService.getSubjectAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurSubjectId(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                subjectId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurSubjectId(subjectId.get(0));
            if (iterationId == null) {
                List<Integer> authoritySubjectIds = permissionService.getIteration(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurIterationId(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                iterationId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurIterationId(iterationId.get(0));
            response.setSuccess(true);
            response.setTotal((long) 0);
            Page<EvaluationCriteriaDTO> evaluationCriteriaDTO = evaluationCriteriaService.listBy(subjectId, iterationId, authorId, status, type, page, per_page);
            List<EvaluationCriteriaDTO> evaluationCriteriaDTOs = new ArrayList<>();
            if (evaluationCriteriaDTO != null) {
                evaluationCriteriaDTOs = evaluationCriteriaDTO.getContent();
                response.setTotal(evaluationCriteriaDTO.getTotalElements());
            }

//            if (evaluationCriteriaDTOs.size() > 0) {
//                response.setCurSubjectId(evaluationCriteriaDTOs.get(0).getSubjectId());
//                response.setCurIterationId(evaluationCriteriaDTOs.get(0).getIterationId());
//            }

            response.setMessage("Get Criteria list successfully");
            response.setData(evaluationCriteriaDTOs);
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get Criteria list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editCriteria(@RequestBody EvaluationCriteriaDTO evaluationCriteriaDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update evaluation criteria successfully");
            response.setData(evaluationCriteriaService.updateCriteria(evaluationCriteriaDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update evaluation criteria fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
