package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.FeaturesDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.FeaturesListDTO;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
//Transactional(rollbackFor = {Exception.class})
public interface FeaturesService {
    FeaturesDTO addFeature(FeaturesDTO featuresDTO) throws Exception;

    FeaturesDTO updateFeature(FeaturesDTO featuresDTO) throws Exception;

    FeaturesDTO findById(int id) throws Exception;

    Page<FeaturesDTO> listBy(List<Integer> projectID, String name, String status, int page, int limit) throws Exception;
    public FeaturesDTO getFeatureDetail(int id);
    public List<FeaturesListDTO> showFeatureList(String status, List<Integer> projectId);
}
