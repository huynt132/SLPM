package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.repository.IterationEvaluationsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.MemberEvaluationRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.TeamEvaluationsRepository;
import com.fpt.capstone.backend.api.BackEnd.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class DashboardServiceImpl implements DashboardService {
    @Autowired
    private IterationEvaluationsRepository iterationEvaluationsRepository;
    @Autowired
    private TeamEvaluationsRepository teamEvaluationsRepository;
    @Autowired
    private MemberEvaluationRepository memberEvaluationRepository;

    /**
     * Service dùng để lấy data cho dashboard class
     *
     * @param classId
     * @param milestoneId
     * @return
     */
    @Override
    public DashboardGradeDTO getClassStatistic(Integer classId, List<Integer> milestoneId) {
        DashboardGradeDTO dashboardStatistic = new DashboardGradeDTO();
        List<DashboardDatumDTO> datasets = new ArrayList<>();
        HashMap<Integer, String> labels = new HashMap<>();

        DashboardDatumDTO total = new DashboardDatumDTO(ModelStatus.LABEL_TOTAL, ModelStatus.BACKGROUND_LABEL_TOTAL, new ArrayList<>());
        DashboardDatumDTO teamBased = new DashboardDatumDTO(ModelStatus.LABEL_TEAM_BASED, ModelStatus.BACKGROUND_LABEL_TEAM_BASED, new ArrayList<>());
        DashboardDatumDTO individualBased = new DashboardDatumDTO(ModelStatus.LABEL_INDIVIDUAL_BASED, ModelStatus.BACKGROUND_LABEL_INDIVIDUAL_BASED, new ArrayList<>());

        HashMap<Integer, BigDecimal> teamMap = new HashMap<>(); //map để tính điểm total cho team -> Luôn dùng map này khi tính các đầu điểm để thự tự projectId luôn đúng order
        HashMap<Integer, HashMap<Integer, Integer>> countMap = new HashMap<>(); //map để lưu tổng số để chia trung bình

        List<IterationEvaluationsDTO> totalGrades = iterationEvaluationsRepository.getTotalEvaluationStatistic(classId, milestoneId, null);
        for (IterationEvaluationsDTO totalGrade : totalGrades) {
            labels.putIfAbsent(totalGrade.getProjectId(), totalGrade.getProjectName());
            // count tổng thành viên từng team
            countMap.putIfAbsent(totalGrade.getProjectId(), new HashMap<>());

            //check xem bản ghi này đã được cộng rồi hay chưa
            if (countMap.get(totalGrade.getProjectId()).get(totalGrade.getId()) != null) continue;

            countMap.get(totalGrade.getProjectId()).putIfAbsent(totalGrade.getStudentId(), totalGrade.getStudentId());

            // + tổng điểm total (iteration nhân với trọng số) theo từng team
            teamMap.putIfAbsent(totalGrade.getProjectId(), BigDecimal.valueOf(0));
            BigDecimal convertedGrade = totalGrade.getIndividualEval();
            if (milestoneId == null) convertedGrade = convertedGrade.multiply(totalGrade.getEvalWeight());
            teamMap.put(totalGrade.getProjectId(), teamMap.get(totalGrade.getProjectId()).add(convertedGrade));
        }

        // Tính trung bình điểm total
        for (Integer projectId : teamMap.keySet()) {
            if (countMap.get(projectId) == null || countMap.get(projectId).size() == 0) {
                total.getData().add(BigDecimal.valueOf(0));
            } else {
                total.getData().add(teamMap.get(projectId).divide(BigDecimal.valueOf(countMap.get(projectId).size()), 1, RoundingMode.HALF_EVEN));
            }

            // reset lại value của teamMap để tính điểm khác
            teamMap.put(projectId, BigDecimal.valueOf(0));
            countMap.put(projectId, new HashMap<>());
        }

        List<TeamEvaluationsDTO> teamGrades = teamEvaluationsRepository.getTeamGradeStatistic(new ArrayList<>(teamMap.keySet()), milestoneId);
        for (TeamEvaluationsDTO teamGrade : teamGrades) {
            //check xem bản ghi này đã được cộng rồi hay chưa
            if (countMap.get(teamGrade.getProjectId()).get(teamGrade.getId()) != null) continue;

            teamMap.put(teamGrade.getProjectId(), teamMap.get(teamGrade.getProjectId()).add(teamGrade.getGrade()));
            countMap.get(teamGrade.getProjectId()).put(teamGrade.getId(), teamGrade.getId());
        }

        // Tính trung bình điểm team
        for (Integer projectId : teamMap.keySet()) {
            if (countMap.get(projectId) == null || countMap.get(projectId).size() == 0) {
                teamBased.getData().add(BigDecimal.valueOf(0));
            } else {
                teamBased.getData().add(teamMap.get(projectId).divide(BigDecimal.valueOf(countMap.get(projectId).size()), 1, RoundingMode.HALF_EVEN));
            }

            // reset lại value của teamMap để tính điểm khác
            teamMap.put(projectId, BigDecimal.valueOf(0));
            countMap.put(projectId, new HashMap<>());
        }

        List<MemberEvaluationsDTO> memberGrades = memberEvaluationRepository.getMemberGradeStatistic(new ArrayList<>(teamMap.keySet()), milestoneId);
        for (MemberEvaluationsDTO memberGrade : memberGrades) {
            //check xem bản ghi này đã được cộng rồi hay chưa
            if (countMap.get(memberGrade.getProjectId()).get(memberGrade.getId()) != null) continue;

            teamMap.put(memberGrade.getProjectId(), teamMap.get(memberGrade.getProjectId()).add(memberGrade.getGrade()));
            countMap.get(memberGrade.getProjectId()).put(memberGrade.getId(), memberGrade.getId());
        }

        // Tính trung bình điểm individual
        for (Integer projectId : teamMap.keySet()) {
            if (countMap.get(projectId) == null || countMap.get(projectId).size() == 0) {
                individualBased.getData().add(BigDecimal.valueOf(0));
            } else {
                individualBased.getData().add(teamMap.get(projectId).divide(BigDecimal.valueOf(countMap.get(projectId).size()), 1, RoundingMode.HALF_EVEN));
            }

            // reset lại value của teamMap để tính điểm khác
            teamMap.put(projectId, BigDecimal.valueOf(0));
            countMap.put(projectId, new HashMap<>());
        }

        datasets.add(total);
        datasets.add(teamBased);
        datasets.add(individualBased);

        dashboardStatistic.setLabels(new ArrayList<>(labels.values()));
        dashboardStatistic.setDatasets(datasets);
        return dashboardStatistic;
    }

    /**
     * Service dùng để lấy data cho dashboard team
     *
     * @param projectId
     * @param milestoneId
     * @return
     */
    @Override
    public DashboardGradeDTO getTeamStatistic(Integer projectId, List<Integer> milestoneId) {
        DashboardGradeDTO dashboardStatistic = new DashboardGradeDTO();
        List<DashboardDatumDTO> datasets = new ArrayList<>();
        HashMap<Integer, String> labels = new HashMap<>();

        DashboardDatumDTO total = new DashboardDatumDTO(ModelStatus.LABEL_TOTAL, ModelStatus.BACKGROUND_LABEL_TOTAL, new ArrayList<>());
        DashboardDatumDTO teamBased = new DashboardDatumDTO(ModelStatus.LABEL_TEAM_BASED, ModelStatus.BACKGROUND_LABEL_TEAM_BASED, new ArrayList<>());
        DashboardDatumDTO individualBased = new DashboardDatumDTO(ModelStatus.LABEL_INDIVIDUAL_BASED, ModelStatus.BACKGROUND_LABEL_INDIVIDUAL_BASED, new ArrayList<>());

        HashMap<Integer, BigDecimal> memberMap = new HashMap<>(); //map để tính điểm total cho team -> Luôn dùng map này khi tính các đầu điểm để thự tự projectId luôn đúng order
        HashMap<Integer, Integer> countMap = new HashMap<>(); //map để lưu tổng số để chia trung bình

        List<IterationEvaluationsDTO> totalGrades = iterationEvaluationsRepository.getTotalEvaluationStatistic(null, milestoneId, projectId);
        for (IterationEvaluationsDTO totalGrade : totalGrades) {
            labels.putIfAbsent(totalGrade.getStudentId(), totalGrade.getStudent());

            //check xem bản ghi này đã được cộng rồi hay chưa
            if (countMap.get(totalGrade.getStudentId()) != null) continue;

            // count tổng thành viên từng team member
            countMap.put(totalGrade.getStudentId(), totalGrade.getStudentId());

            // + tổng điểm total (iteration nhân với trọng số) theo từng team
            memberMap.putIfAbsent(totalGrade.getStudentId(), BigDecimal.valueOf(0));
            memberMap.put(totalGrade.getStudentId(), memberMap.get(totalGrade.getStudentId()).add(totalGrade.getIndividualEval()));
        }

        // Tính trung bình điểm total
        for (Integer studentId : memberMap.keySet()) {
//            if (countMap.size() == 0) {
//                total.getData().add(BigDecimal.valueOf(0));
//            } else {
            total.getData().add(memberMap.get(studentId));
//            }

            // reset lại value của memberMap để tính điểm khác
            memberMap.put(studentId, BigDecimal.valueOf(0));
            countMap = new HashMap<>();
        }

        BigDecimal teamEval = BigDecimal.valueOf(0);
        List<Integer> projectIds = new ArrayList<>();
        projectIds.add(projectId);

        List<TeamEvaluationsDTO> teamGrades = teamEvaluationsRepository.getTeamGradeStatistic(projectIds, milestoneId);
        for (TeamEvaluationsDTO teamGrade : teamGrades) {
            //check xem bản ghi này đã được cộng rồi hay chưa
            if (countMap.get(teamGrade.getId()) != null) continue;

            teamEval = teamEval.add(teamGrade.getGrade());
            countMap.put(teamGrade.getId(), teamGrade.getId());
        }

        if (countMap.size() > 0) {
            teamEval = teamEval.divide(BigDecimal.valueOf(countMap.size()), 1, RoundingMode.HALF_EVEN);
        }

        // Tính trung bình điểm team
        for (Integer studentId : memberMap.keySet()) {
            teamBased.getData().add(teamEval);

            // reset lại value của memberMap để tính điểm khác
            countMap = new HashMap<>();
        }

        HashMap<Integer, HashMap<Integer, Integer>> countMemberMap = new HashMap<>(); //map để lưu tổng số để chia trung bình
        List<MemberEvaluationsDTO> memberGrades = memberEvaluationRepository.getMemberGradeStatistic(projectIds, milestoneId);
        for (MemberEvaluationsDTO memberGrade : memberGrades) {
            //check xem bản ghi này đã được cộng rồi hay chưa
            countMemberMap.putIfAbsent(memberGrade.getStudentId(), new HashMap<>());
            if (countMemberMap.get(memberGrade.getStudentId()).get(memberGrade.getId()) != null) continue;

            memberMap.put(memberGrade.getStudentId(), memberMap.get(memberGrade.getStudentId()).add(memberGrade.getGrade()));
            countMemberMap.get(memberGrade.getStudentId()).put(memberGrade.getId(), memberGrade.getId());
        }

        // Tính trung bình điểm individual
        for (Integer studentId : memberMap.keySet()) {
            if (countMemberMap.get(studentId) == null || countMemberMap.get(studentId).size() == 0) {
                individualBased.getData().add(BigDecimal.valueOf(0));
            } else {
                individualBased.getData().add(memberMap.get(studentId).divide(BigDecimal.valueOf(countMemberMap.get(studentId).size()), 1, RoundingMode.HALF_EVEN));
            }

            // reset lại value của memberMap để tính điểm khác
//            memberMap.put(studentId, BigDecimal.valueOf(0));
//            countMemberMap = new HashMap<>();
        }

        datasets.add(total);
        datasets.add(teamBased);
        datasets.add(individualBased);

        dashboardStatistic.setLabels(new ArrayList<>(labels.values()));
        dashboardStatistic.setDatasets(datasets);
        return dashboardStatistic;
    }
}
