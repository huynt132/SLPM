package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.FeaturesDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.FunctionResponse;
import com.fpt.capstone.backend.api.BackEnd.service.FeaturesService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("feature")
@CrossOrigin(origins = "*", maxAge = 3600)
//@PreAuthorize("hasAuthority('Admin')||hasAuthority('Trainer')||hasAuthority('Teacher')||hasAuthority('Student')")
public class FeaturesController {
    @Autowired
    FeaturesService featuresService;
    @Autowired
    private PermissionService permissionService;

    @PostMapping("/add")
    public ResponseEntity<?> addFeature(@RequestBody FeaturesDTO featuresDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add feature successfully");
            response.setData(featuresService.addFeature(featuresDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add feature fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editCriteria(@RequestBody FeaturesDTO featuresDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update feature successfully");
            response.setData(featuresService.updateFeature(featuresDTO));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update feature fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findFeature(
            @RequestParam(value = "projectId", required = false) List<Integer> projectId,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        FunctionResponse response = new FunctionResponse();
        try {
            if (projectId == null) {
                List<Integer> authoritySubjectIds = permissionService.getProjectAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurProjectId(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                projectId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurProjectId(projectId.get(0));
            Page<FeaturesDTO> featuresDTOPage = featuresService.listBy(projectId, name, status, page, per_page);
            List<FeaturesDTO> featuresDTOS = featuresDTOPage.getContent();
            response.setSuccess(true);
            response.setMessage("Get feature list successfully");

            response.setData(featuresDTOS);
            response.setTotal(featuresDTOPage.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get feature list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getList")
    public ResponseEntity<?> getListClass(
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "projectId", required = false) List<Integer> projectId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get feature list successfully");
            response.setData(featuresService.showFeatureList(status, projectId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get feature list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
