package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.service.LocEvaluationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("locEvaluations")
@CrossOrigin(origins = "*", maxAge = 3600)

public class LocEvaluationsController {

    @Autowired
    private LocEvaluationsService locEvaluationsService;

    @GetMapping("/getList")
    public ResponseEntity<?> getListLocEvaluations(
            @RequestParam(value = "userId", required = true) Integer userId,
            @RequestParam(value = "projectId", required = true) Integer projectId,
            @RequestParam(value = "milestoneId", required = true) Integer milestoneId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get locEvaluations list successfully");
            response.setData(locEvaluationsService.showList(userId, projectId, milestoneId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get locEvaluations list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getStudentEvaluation")
    public ResponseEntity<?> getStudentEvaluation(
            @RequestParam(value = "userId") Integer userId,
            @RequestParam(value = "projectId") Integer projectId,
            @RequestParam(value = "milestoneId") Integer milestoneId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get student evaluations successfully");
            response.setData(locEvaluationsService.getStudentEvaluation(userId, projectId, milestoneId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get student evaluation fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
