package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectGitIdDTO {

    private Integer classId;
    private Integer gitId;

    private List<MilestonesSyncDTO> milestones;
    private List<String> gitLabMilestones;

    public ProjectGitIdDTO( Integer classId, Integer gitId) {

        this.classId = classId;
        this.gitId = gitId;

    }
}
