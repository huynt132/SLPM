package com.fpt.capstone.backend.api.BackEnd.controller;


import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.service.impl.AuthenticationServiceImpl;
import com.fpt.capstone.backend.api.BackEnd.service.impl.JwtUserDetailsService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/*
Expose a POST API /authenticate using the JwtAuthenticationController. The POST API gets username and password in the
body- Using Spring Authentication Manager we authenticate the username and password.If the credentials are valid,
a JWT token is created using the JWTTokenUtil and provided to the client.
 */
@RestController
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

//    @Autowired
//    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;
    @Autowired
    private ModelMapper modelMapper;


//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    public ResponseEntity<?> saveUser(@RequestBody UsersDTO usersDTO) throws Exception {
//        ResponseObject response = new ResponseObject();
//
//        try {
//            Users users = userDetailsService.createUser(usersDTO);
//            response.setSuccess(true);
//            response.setMessage("Register successfully");
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            response.setSuccess(false);
//            response.setMessage("Register fail:" + e.getMessage());
//            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//        }
//    }

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationController.class);

    @Autowired
    private AuthenticationServiceImpl authenticationService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(
            @Valid @RequestBody UserLoginDto userLoginDto) {
        try {
            return authenticationService.authenticate(userLoginDto);
        } catch (AuthenticationException e) {
            logger.error("Authenticate failed for email: " + userLoginDto.getEmail());
            logger.error(e.getMessage());
            return ResponseEntity.status(401).body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Wrong user name or password!").build()
            );
        } catch (Exception e) {
            logger.error("Wrong user name or password");
            logger.error(e.getMessage());
            return ResponseEntity.status(401).body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Wrong user name or password!").build()
            );
        }
    }

    @PostMapping("/login-google")
    public ResponseEntity<?> authenticateByEmail(
            @Valid @RequestBody UserLoginGGTO userLoginDto) {
        try {
            return authenticationService.authenticateByEmail(userLoginDto);
        } catch (AuthenticationException e) {
            logger.error("Authenticate failed for email: " + userLoginDto.getEmail());
            logger.error(e.getMessage());
            return ResponseEntity.status(401).body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Email or password is invalid!").build()
            );
        } catch (Exception e) {
            logger.error("Undefined error");
            logger.error(e.getMessage());
            return ResponseEntity.status(401).body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Undefined error!" + e.getMessage()).build()
            );
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> processRegister(@RequestBody UserRegisterDTO user) {
        try {
            authenticationService.register(user);

            return ResponseEntity.ok().body(
                    ApiResponse.builder()
                            .success(true)
                            .message("Register successfully")
                            .build()
            );
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Register fail, " + e.getMessage())
                            .build()
            );
        }

    }

    @PostMapping("/verify-email")
    public ResponseEntity<?> verifyEmail(@RequestBody VerifyMailDTO verifyMailDTO) {
        try {
            return authenticationService.verify(verifyMailDTO.getCode());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Verify fail!" + e)
                            .build()
            );
        }

    }


    @PostMapping("/changePassword")
    public ResponseEntity<?> changePassword(@RequestBody UserChangePWDTO userChangePWDTO) {
        try {
            authenticationService.changePassword(userChangePWDTO);
            return ResponseEntity.ok().body(
                    ApiResponse.builder()
                            .success(true)
                            .message("Change Password successfully")
                            .build()
            );
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Change Password fail: OldPassword wrong").build()
            );
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Change Password fail:" + e.getMessage())
                            .build()
            );
        }
    }
    @PostMapping("/forgotPassWord")
    public ResponseEntity<?> forgotPassWord(@RequestBody EmailDTO email) {
        try {
            authenticationService.forgotPassWord(email.getEmail());
            return ResponseEntity.ok().body(
                    ApiResponse.builder()
                            .success(true)
                            .message("Send Password to mail successfully")
                            .build()
            );
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Send Password fail: OldPassword wrong").build()
            );
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                    ApiResponse.builder()
                            .success(false)
                            .message("Send Password fail:" + e.getMessage())
                            .build()
            );
        }
    }
}
