package com.fpt.capstone.backend.api.BackEnd.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sync")
@CrossOrigin(origins = "*", maxAge = 3600)
public class SyncGitLabController {
//    @Autowired
//    private MilestonesService milestoneService;
//    @PostMapping("/milestones")
//    public ResponseEntity<?> addMilestone(@RequestParam(value = "classId", required = false) Integer classId)
//    {
//        ApiResponse response = new ApiResponse();
//        try {
//            String result = milestoneService.syncGitLab(classId);
//            response.setSuccess(true);
//            response.setMessage(result);
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            response.setSuccess(false);
//            response.setMessage("Add Milestone fail, " + e.getMessage());
//            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//        }
//    }
}
