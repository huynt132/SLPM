package com.fpt.capstone.backend.api.BackEnd.dto.subject_setting;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SubjectSettingsDTO {
    private Integer id;
    private Integer subjectId;
    private String subjectName;
    private Integer typeId;
    private String settingType;
    private String title;
    private String value;

    private String status;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date created;
    private Integer createdBy;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date modified;
    private Integer modifiedBy;
    private String createdByUser;
    private String modifiedByUser;

    public SubjectSettingsDTO(Integer id, Integer subjectId, String subjectName, Integer typeId, String settingType,
                              String title, String value, String status, String description,
                              Date created, Integer createdBy, Date modified, Integer modifiedBy, String createdByUser,
                              String modifiedByUser) {
        this.id = id;
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.typeId = typeId;
        this.settingType = settingType;
        this.title = title;
        if (Integer.valueOf(typeId).equals(ModelStatus.QUALITY_SETTING_TYPE_ID)) {
            BigDecimal valueDisplay = (new BigDecimal(value)).multiply(new BigDecimal(100));
            this.value = String.valueOf((valueDisplay.intValue()));
        } else this.value = value;

        this.status = status;
        this.description = description;
        this.created = created;
        this.createdBy = createdBy;
        this.modified = modified;
        this.modifiedBy = modifiedBy;
        this.createdByUser = createdByUser;
        this.modifiedByUser = modifiedByUser;
    }

    public SubjectSettingsDTO(Integer id, Integer subjectId, String title, String value) {
        this.id = id;
        this.subjectId = subjectId;
        this.title = title;
        this.value = value;
    }
}
