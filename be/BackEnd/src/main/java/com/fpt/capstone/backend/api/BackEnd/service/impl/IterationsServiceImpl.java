package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.IterationListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationsInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectsListDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.Iterations;
import com.fpt.capstone.backend.api.BackEnd.entity.SubjectSettings;
import com.fpt.capstone.backend.api.BackEnd.repository.IterationsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.SubjectsRepository;
import com.fpt.capstone.backend.api.BackEnd.service.IterationsService;
import com.fpt.capstone.backend.api.BackEnd.service.SubjectsService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsStatus;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IterationsServiceImpl implements IterationsService {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private IterationsRepository iterationsRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private Validate validate;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private SubjectsService subjectsService;

    @Override
    public IterationsDTO getIterationDetail(int id) throws Exception {
//        UserDetails u = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if (u.getAuthorities().stream().findFirst().get().toString().equals("Author")) {
//                      return iterationsRepository.getIterationsDetail(id);         List<SubjectsListDTO> subjectsListDTOS = subjectsRepository
//                    .searchSubjectsByAuthor(u.getUsername());
//            boolean isAccessDenied = true;
//            for (SubjectsListDTO subjects : subjectsListDTOS) {
//                if (id == subjects.getId()) {
//                    isAccessDenied = false;
//                }
//            }
//            if (isAccessDenied) {
//                throw new Exception("you don't have permission!");
//            }
//        }
        return iterationsRepository.getIterationsDetail(id);
    }

    @Override
    public IterationsDTO addIterations(IterationsInputDTO iterationsInputDTO) throws Exception {
        validate.validateIterations(iterationsInputDTO);
        if (iterationsRepository.findByIterationsNameAndSubjectId(iterationsInputDTO.getName(), Integer.valueOf(iterationsInputDTO.getSubjectId())) != null) {
            throw new Exception("iterations " + iterationsInputDTO.getName() + " already exist!");
        }
        BigDecimal totalWeight = new BigDecimal(0);
        if (iterationsInputDTO.getStatus().equals(ModelStatus.STATUS_ACTIVE)) {
            List<BigDecimal> listWeight = iterationsRepository.getEvaluationWeight(Integer.valueOf(iterationsInputDTO.getSubjectId()), null);
            for (int i = 0; i < listWeight.size(); i++) {
                totalWeight = totalWeight.add(listWeight.get(i));
            }
            BigDecimal newWeight = (new BigDecimal(iterationsInputDTO.getEvaluationWeight())).divide(new BigDecimal(100), 2, RoundingMode.HALF_EVEN);
            BigDecimal newTotal = totalWeight.add(newWeight);
            if (newTotal.compareTo(new BigDecimal(1)) == 1) {
                throw new Exception("total Evaluation weight in this subject can not over 100%!");
            }
        }
        Iterations iterations = modelMapper.map(iterationsInputDTO, Iterations.class);
        BigDecimal evalWeight = new BigDecimal(iterationsInputDTO.getEvaluationWeight());
        iterations.setEvaluationWeight(evalWeight.divide((new BigDecimal(100)), 2, RoundingMode.HALF_EVEN));

        // check điều kiện được phép sửa khi có lớp đang diễn ra (status = active)
        List<Integer> activeClass = subjectsRepository.getActiveClassBySubject(iterations.getSubjectId());
        if (!activeClass.isEmpty()) {
            Iterations oldIteration = iterationsRepository.getById(iterations.getId());
            if (!oldIteration.getStatus().equals(oldIteration.getStatus())) {
                throw new Exception("can not change status because there's an ongoing class!");
            }
            if (oldIteration.getEvaluationWeight().compareTo(iterations.getEvaluationWeight()) != 0) {
                throw new Exception("can not change evaluation weight because there's an ongoing class!");
            }
            if (oldIteration.getIsOngoing() != iterations.getIsOngoing()) {
                throw new Exception("can not change is ongoing because there's an ongoing class!");
            }
        }

        iterations = iterationsRepository.save(iterations);
        return getIterationDetail(iterations.getId());
    }

    @Override
    public IterationsDTO deleteIterations(int id) throws Exception {
        Iterations iterations = iterationsRepository.getOne(id);

        UserDetails u = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (u.getAuthorities().stream().findFirst().get().toString().equals("Author")) {
            List<SubjectsListDTO> subjectsListDTOS = subjectsRepository
                    .searchSubjectsByAuthor(u.getUsername());
            boolean isAccessDenied = true;
            for (SubjectsListDTO subjects : subjectsListDTOS) {
                if (id == subjects.getId()) {
                    isAccessDenied = false;
                    modelMapper.map(iterations, IterationsDTO.class);
                    iterations.setStatus(ConstantsStatus.inactive.toString());
                    iterationsRepository.save(iterations);
                    iterations = entityManager.find(Iterations.class, iterations.getId());
                    return getIterationDetail(iterations.getId());
                }
            }
            if (isAccessDenied) {
                throw new Exception("you don't have permission!");
            }
        }
        modelMapper.map(iterations, IterationsDTO.class);
        iterations.setStatus(ConstantsStatus.inactive.toString());
        iterationsRepository.save(iterations);
        iterations = entityManager.find(Iterations.class, iterations.getId());
        return getIterationDetail(iterations.getId());
    }

    @Override
    public List<IterationsDTO> showIterationsList() {
        List<Iterations> iterations = iterationsRepository.findAll();
        List<IterationsDTO> iterationsDTOS = iterations.stream()
                .map(iteration -> modelMapper.map(iteration, IterationsDTO.class))
                .collect(Collectors.toList());
        return iterationsDTOS;
    }

    @Override
    public IterationsDTO updateIterations(IterationsInputDTO iterationsInputDTO) throws Exception {
        validate.validateIterations(iterationsInputDTO);
        if (ObjectUtils.isEmpty(iterationsInputDTO.getId())) {
            throw new Exception("iteration id can't be empty");
        }
        if (!iterationsInputDTO.getId().matches(ConstantsRegex.NUMBER_PATTERN.toString())) {
            throw new Exception("iteration id must be integer");
        }
        if (iterationsRepository.getIterationsDetail(Integer.valueOf(iterationsInputDTO.getId())) == null) {
            throw new Exception("iteration not exist");
        }
        Iterations iterations = iterationsRepository.getById(Integer.valueOf(iterationsInputDTO.getId()));
        if (!iterations.getName().equals(iterationsInputDTO.getName())) {
            if (iterationsRepository.findByIterationsNameAndSubjectId(iterationsInputDTO.getName(), Integer.valueOf(iterationsInputDTO.getSubjectId())) != null) {
                throw new Exception("iterations " + iterationsInputDTO.getName() + " already exist!");
            }
            iterations.setName(iterationsInputDTO.getName());
        }
        if (iterations.getEvaluationWeight() != new BigDecimal(iterationsInputDTO.getEvaluationWeight()).divide(new BigDecimal(100))) {
            // Chỉ check total weight với case status = active
            if (iterationsInputDTO.getStatus().equals(ModelStatus.STATUS_ACTIVE)) {
                BigDecimal totalWeight = new BigDecimal(0);
                List<BigDecimal> listWeight = iterationsRepository.getEvaluationWeight(Integer.valueOf(iterationsInputDTO.getSubjectId()), Integer.valueOf(iterationsInputDTO.getId()));
                for (int i = 0; i < listWeight.size(); i++) {
                    totalWeight = totalWeight.add(listWeight.get(i));
                }
                BigDecimal newWeight = (new BigDecimal(iterationsInputDTO.getEvaluationWeight())).divide(BigDecimal.valueOf(100));
                BigDecimal newTotal = totalWeight.add(newWeight);
                if (newTotal.compareTo(new BigDecimal(1)) == 1) {
                    throw new Exception("total Evaluation weight in this subject maximum is 100%!");
                }
            }
        }
        iterations = modelMapper.map(iterationsInputDTO, Iterations.class);
        //iterations.setSubject(subjectsRepository.getById(Integer.valueOf(iterationsInputDTO.getSubjectId())));
        BigDecimal evalWeight = new BigDecimal(iterationsInputDTO.getEvaluationWeight());
        iterations.setEvaluationWeight(evalWeight.divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_EVEN));
        iterationsRepository.save(iterations);
        iterations = entityManager.find(Iterations.class, iterations.getId());
        return getIterationDetail(iterations.getId());
    }


    @Override
    public Page<IterationsDTO> listBy(List<Integer> id, String name, List<Integer> manageId, String status, int page, int per_page) throws Exception {
        Pageable pageable = PageRequest.of(page - 1, per_page);
        if (!ObjectUtils.isEmpty(status) && !status.matches(ConstantsRegex.STATUS_PATTERN.toString())) {
            throw new Exception("input status Empty to search all or active/inactive");
        }

        List<Integer> authorityIterationIds = permissionService.getIteration(null);
        if (ObjectUtils.isEmpty(id)) {
            id = new ArrayList<>();
            List<SubjectsListDTO> list = subjectsService.listBy();
            if (list.size() < 0) return null;
            Integer firstSubjectId = list.get(0).getId();
            id.add(firstSubjectId);
        }

        return iterationsRepository.search(id, name, manageId, status, pageable, authorityIterationIds);
    }

    public List<IterationsDTO> showIterationList() {
        List<Iterations> iterations = iterationsRepository.findAll();
        List<IterationsDTO> iterationsDTOS = iterations.stream()
                .map(iterations1 -> modelMapper.map(iterations1, IterationsDTO.class))
                .collect(Collectors.toList());
        return iterationsDTOS;
    }

    @Override
    public List<IterationListDTO> listIteration(String status, Integer subjectId, Integer classId) throws Exception {
        List<Integer> authorityIterationIds = permissionService.getIteration(null);
        return iterationsRepository.showAll(status, authorityIterationIds, subjectId, classId);
    }

    @Override
    public Page<IterationsDTO> listIterationEvalCriteria() throws Exception {
        Pageable pageable = PageRequest.of(1 - 1, 1);
        List<Integer> authorityIterationIds = permissionService.getIteration(null);
        return iterationsRepository.getIterationInCriteria(authorityIterationIds, pageable);
    }

    @Override
    public BigDecimal getTotalWeightInSubject(Integer subjectId) {
        BigDecimal totalWeight = new BigDecimal(0);
        List<BigDecimal> list = iterationsRepository.getListWeight(subjectId);
        for (BigDecimal weight : list) {
            totalWeight = totalWeight.add(weight);
        }
        return totalWeight.multiply(new BigDecimal(100));
    }

}
