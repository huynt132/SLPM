package com.fpt.capstone.backend.api.BackEnd.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class ResponseErrorHandler implements org.springframework.web.client.ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
            if (response.getStatusCode()== HttpStatus.BAD_REQUEST){
                try {
                    throw new Exception("please check your Gitlab id");
                } catch (Exception e) {
                   return;
                }
            }
        if (response.getStatusCode()== HttpStatus.UNAUTHORIZED){
            try {
                throw new Exception("please check your Gitlab token");
            } catch (Exception e) {
                return;
            }
        }
        if (response.getStatusCode()== HttpStatus.FORBIDDEN){
            try {
                throw new Exception("please check your Gitlab token's permission");
            } catch (Exception e) {
                return;
            }
        }
    }

}
