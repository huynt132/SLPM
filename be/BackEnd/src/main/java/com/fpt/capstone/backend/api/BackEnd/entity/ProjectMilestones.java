package com.fpt.capstone.backend.api.BackEnd.entity;

import lombok.Data;

import javax.persistence.*;
@Data
@Entity
@Table(name = "project_milestones")
public class ProjectMilestones {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "project_id")
    private Integer projectId;

    @Column(name = "milestone_id")
    private Integer milestoneId;

    @Column(name = "gitlab_milestone_id")
    private Integer gitlabMilestoneId;


}
