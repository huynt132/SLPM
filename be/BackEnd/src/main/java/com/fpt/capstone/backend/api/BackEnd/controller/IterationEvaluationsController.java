package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.ClassEvalDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationEvaluationsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.UpdateBonusDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.IterationEvalResponse;
import com.fpt.capstone.backend.api.BackEnd.service.IterationEvaluationsService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("evaluation")
@CrossOrigin(origins = "*", maxAge = 3600)

public class IterationEvaluationsController {
    @Autowired
    IterationEvaluationsService iterationEvaluationsService;
    @Autowired
    PermissionService permissionService;

    @GetMapping("/iterationEvaluation")
    public ResponseEntity<?> findIterationEvaluations(
            @RequestParam(value = "iterationId", required = false) Integer iterationId,
            @RequestParam(value = "projectId", required = false) List<Integer> projectId,
            @RequestParam(value = "milestoneId", required = false) List<Integer> milestoneId,
            @RequestParam(value = "classId", required = false) Integer classId) {
        IterationEvalResponse<Map<String, Object>> response = new IterationEvalResponse();
        try {
            if (milestoneId == null) {
                List<Integer> authoritySubjectIds = permissionService.getMilestoneAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(null);
                    response.setTotal(0);
                    response.setCurMilestone(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                milestoneId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurMilestone(milestoneId.get(0));
            List<IterationEvaluationsDTO> iterationEvaluationsDTOPage = iterationEvaluationsService.showList(iterationId, projectId, milestoneId, classId);
            //    List<IterationEvaluationsDTO> iterationEvaluationsDTOS = iterationEvaluationsDTOPage.getContent();
            response.setSuccess(true);
            response.setMessage("Get iteration evaluations list successfully");
            Map<String, Object> result = new HashMap<>();
            result.put("iteration_evaluations", iterationEvaluationsDTOPage);
//            if (iterationEvaluationsDTOPage.size() > 0) {
//                result.put("milestoneName", iterationEvaluationsDTOPage.get(0).getMilestoneName());
//                response.setCurMilestone(iterationEvaluationsDTOPage.get(0).getMilestoneId());
//            }
            response.setData(result);
            response.setTotal(iterationEvaluationsDTOPage.size());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get iteration evaluations list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/classEvaluation")
    public ResponseEntity<?> findClassEvaluations(
            @RequestParam(value = "classId", required = false) Integer classId) {
        ApiResponse response = new ApiResponse();
        try {
            ClassEvalDTO listIterationEvalDTOS = iterationEvaluationsService.listIterationClass(classId);
            response.setSuccess(true);
            response.setMessage("Get class evaluations list successfully");
            response.setData(listIterationEvalDTOS);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get class evaluations list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/classEvaluation/export")
    public ResponseEntity<?> exportClassEvaluations(
            @RequestParam(value = "classId", required = false) Integer classId) {
        ApiResponse response = new ApiResponse();
        try {

            response.setSuccess(true);
            response.setMessage("Export class evaluations list successfully");
            response.setData(iterationEvaluationsService.exportExcel(classId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get class evaluations list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/updateBonus")
    public ResponseEntity<?> updateBonus(
            @RequestBody UpdateBonusDTO updateBonusDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update bonus successfully");
            iterationEvaluationsService.updateBonus(updateBonusDTO.getStudentId(), updateBonusDTO.getMilestoneId(), updateBonusDTO.getBonus());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update bonus fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
