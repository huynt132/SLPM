package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.service.FunctionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("common")

public class CollumnTypeController {
    @Autowired
    private FunctionsService functionsService;

    @GetMapping("/getColumnType")
    public ResponseEntity<?> getFiedlValue(@RequestParam(value = "table", required = false) String table,
                                           @RequestParam(value = "field", required = false) String field) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setData(functionsService.showColumns(table, field));
            response.setMessage("Show column successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("column fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
