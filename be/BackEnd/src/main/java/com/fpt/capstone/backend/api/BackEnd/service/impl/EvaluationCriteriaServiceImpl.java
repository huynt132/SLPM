package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.EvaluationCriteriaDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.IterationsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.entity.EvaluationCriteria;
import com.fpt.capstone.backend.api.BackEnd.entity.SubjectSettings;
import com.fpt.capstone.backend.api.BackEnd.repository.EvaluationCriteriaRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.IterationsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.SubjectsRepository;
import com.fpt.capstone.backend.api.BackEnd.service.EvaluationCriteriaService;
import com.fpt.capstone.backend.api.BackEnd.service.IterationsService;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsStatus;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EvaluationCriteriaServiceImpl implements EvaluationCriteriaService {
    @Autowired
    private EvaluationCriteriaRepository evaluationCriteriaRepository;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private Validate validate = new Validate();
    @Autowired
    private UserService userService;

    @Override
    public EvaluationCriteriaDTO addCriteria(EvaluationCriteriaDTO evaluationCriteriaDTO) throws Exception {
        validate.validateCriteria(evaluationCriteriaDTO);
        if (evaluationCriteriaDTO.getStatus().equals(ModelStatus.STATUS_ACTIVE)) {
            // check xem đã tồn tại tiêu chí tính loc hay chưa
            if (evaluationCriteriaDTO.getType().equals(ModelStatus.CRITERIA_TYPE_DEMO)) {
                List<Integer> locCriteria = evaluationCriteriaRepository.getLocEvalCriteria(evaluationCriteriaDTO.getId(), evaluationCriteriaDTO.getIterationId());
                if (locCriteria != null && !locCriteria.isEmpty()) throw new Exception("maximum one loc evaluation criteria accepted!");
            }

            BigDecimal totalEvaluationWeight = evaluationCriteriaRepository.getTotalEvaluationWeight(evaluationCriteriaDTO.getIterationId(), evaluationCriteriaDTO.getId());
            if (totalEvaluationWeight == null) totalEvaluationWeight = BigDecimal.valueOf(0);
            totalEvaluationWeight = totalEvaluationWeight.add(evaluationCriteriaDTO.getEvaluationWeight().divide(new BigDecimal(100)));
            if (totalEvaluationWeight.compareTo(new BigDecimal(1)) == 1) {
                throw new Exception("total evaluation criteria weight in an iteration can not greater than 100%!");
            }
        }
        evaluationCriteriaDTO.setEvaluationWeight(evaluationCriteriaDTO.getEvaluationWeight().divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_EVEN));
        EvaluationCriteria evaluationCriteria = modelMapper.map(evaluationCriteriaDTO, EvaluationCriteria.class);

        evaluationCriteria.setStatus(ConstantsStatus.active.toString());
        evaluationCriteria = evaluationCriteriaRepository.save(evaluationCriteria);
        return getCriteriaDetail(evaluationCriteria.getId());
    }


    @Override
    public EvaluationCriteriaDTO deleteCriteria(int id) {
        return null;
    }

    @Override
    public List<EvaluationCriteriaDTO> showCriteria() {
        return null;
    }

    @Override
    public EvaluationCriteriaDTO updateCriteria(EvaluationCriteriaDTO evaluationCriteriaDTO) throws Exception {
        validate.validateCriteria(evaluationCriteriaDTO);
        evaluationCriteriaDTO.setEvaluationWeight(evaluationCriteriaDTO.getEvaluationWeight().divide(BigDecimal.valueOf(100)));
        EvaluationCriteria oldCriteria = evaluationCriteriaRepository.getById(evaluationCriteriaDTO.getId());
        if (!oldCriteria.getType().equals(evaluationCriteriaDTO.getType()) && evaluationCriteriaDTO.getType().equals(ModelStatus.CRITERIA_TYPE_DEMO) && evaluationCriteriaDTO.getStatus().equals(ModelStatus.STATUS_ACTIVE)) {
            List<Integer> locCriteria = evaluationCriteriaRepository.getLocEvalCriteria(evaluationCriteriaDTO.getId(), evaluationCriteriaDTO.getIterationId());
            if (locCriteria != null && !locCriteria.isEmpty()) {
                throw new Exception("maximum one loc evaluation criteria accepted!");
            }
        }

        EvaluationCriteria evaluationCriteria = evaluationCriteriaRepository.findById(evaluationCriteriaDTO.getId()).get();

        if (evaluationCriteria.getEvaluationWeight() != new BigDecimal(String.valueOf(evaluationCriteriaDTO.getEvaluationWeight()))) {
            if (evaluationCriteriaDTO.getStatus().equals(ModelStatus.STATUS_ACTIVE)) {
                BigDecimal totalEvaluationWeight = evaluationCriteriaRepository.getTotalEvaluationWeight(evaluationCriteriaDTO.getIterationId(), evaluationCriteriaDTO.getId());
                if (totalEvaluationWeight == null) totalEvaluationWeight = BigDecimal.valueOf(0);
                totalEvaluationWeight = totalEvaluationWeight.add((evaluationCriteriaDTO.getEvaluationWeight()));
                if (totalEvaluationWeight.compareTo(new BigDecimal(1)) == 1) {
                    throw new Exception("total evaluation criteria weight in an iteration can not greater than 100%!");
                }
            }
        }
        // check điều kiện được phép sửa khi có lớp đang diễn ra (status = active)
        List<Integer> activeClass = subjectsRepository.getActiveClassBySubject(evaluationCriteriaDTO.getSubjectId());
        if (!activeClass.isEmpty()) {
            if (!oldCriteria.getStatus().equals(evaluationCriteriaDTO.getStatus())) {
                throw new Exception("can not change status because there's an ongoing class!");
            }
            if (oldCriteria.getEvaluationWeight().compareTo(evaluationCriteriaDTO.getEvaluationWeight()) != 0) {
                throw new Exception("can not change evaluation weight because there's an ongoing class!");
            }
            if (!oldCriteria.getType().equals(evaluationCriteriaDTO.getType())) {
                throw new Exception("can not change criteria type because there's an ongoing class!");
            }
            if (oldCriteria.getMaxLoc() != null && evaluationCriteriaDTO.getMaxLoc() != null && oldCriteria.getMaxLoc() != evaluationCriteriaDTO.getMaxLoc()) {
                throw new Exception("can not change max loc because there's an ongoing class!");
            }
        }

        evaluationCriteria.setEvaluationWeight(evaluationCriteriaDTO.getEvaluationWeight());
        evaluationCriteria.setIterationId(evaluationCriteriaDTO.getIterationId());
        evaluationCriteria.setStatus(evaluationCriteriaDTO.getStatus());
        evaluationCriteria.setMaxLoc(evaluationCriteriaDTO.getMaxLoc());

        evaluationCriteria = evaluationCriteriaRepository.save(evaluationCriteria);
        return getCriteriaDetail(evaluationCriteria.getId());
    }

    @Override
    public EvaluationCriteriaDTO findById(int id) throws Exception {
        return null;
    }

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private IterationsService iterationsService;

    @Override
    public Page<EvaluationCriteriaDTO> listBy(List<Integer> subjectId, List<Integer> iterationId, List<Integer> authorId,
                                              String status, String type, int page, int per_page) throws Exception {
        Pageable pageable = PageRequest.of(page - 1, per_page);
        List<Integer> authorityCriteriaIds = permissionService.geEvalCrietia(null);
        if (ObjectUtils.isEmpty(status)) status = null;

        if (!ObjectUtils.isEmpty(status) && !status.matches(ConstantsRegex.STATUS_PATTERN.toString())) {
            throw new Exception("input status empty to search all or active/inactive");
        }

        if (ObjectUtils.isEmpty(subjectId)) {
            subjectId = new ArrayList<>();

            Page<IterationsDTO> listP = iterationsService.listIterationEvalCriteria();
            List<IterationsDTO> iterationsDTOList = listP.getContent();
            if (iterationsDTOList.size() == 0) return null;

            Integer firstSubjectId = iterationsDTOList.get(0).getSubjectId();
            subjectId.add(firstSubjectId);
            if (ObjectUtils.isEmpty(iterationId)) {
                iterationId = new ArrayList<>();
                Integer firstIterationId = iterationsDTOList.get(0).getId();
                iterationId.add(firstIterationId);
            }
        }

        return evaluationCriteriaRepository.search(subjectId, iterationId, authorId, status, type, pageable, authorityCriteriaIds);
    }

    @Override
    public EvaluationCriteriaDTO getCriteriaDetail(int id) {
        return evaluationCriteriaRepository.getEvaluationCriteriaDetail(id);
    }

    @Override
    public List<EvaluationCriteriaDTO> showCriteriaList() {
        return null;
    }
}
