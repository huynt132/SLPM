package com.fpt.capstone.backend.api.BackEnd.entity;

import lombok.Data;

@Data
public class FunctionResponse {
    private boolean success;
    private String message;
    private Object data;
    private Integer curProjectId;
    private Integer perPages;
    private Integer currentPage;
    private Long total;

}
