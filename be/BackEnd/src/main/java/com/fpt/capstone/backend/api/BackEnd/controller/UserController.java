package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.AuthorListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.UserList;
import com.fpt.capstone.backend.api.BackEnd.dto.UsersDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.repository.UserRepository;
import com.fpt.capstone.backend.api.BackEnd.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("users")
public class UserController {
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserDetailsService userDetailsService;

    public UserController() throws IOException {
    }

    @PreAuthorize("hasAuthority('Admin')")
    @GetMapping("/getAll")
    public ResponseEntity<?> findUserBy(@RequestParam(value = "email", required = false) String key_email,
                                        @RequestParam(value = "fullName", required = false) String key_fullName,
                                        @RequestParam(value = "rollNumber", required = false) String rollNumber,
                                        @RequestParam(value = "status", required = false) String status,
                                        @RequestParam("page") int page,
                                        @RequestParam("limit") int per_page) {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            Page<UsersDTO> userDTOS = userService
                    .listBy(key_email, key_fullName, rollNumber, status, page, per_page);
            List<UsersDTO> userDTOList = userDTOS.getContent();
            response.setSuccess(true);
            response.setMessage("Get user list successfully");
            response.setData(userDTOList);
            response.setTotal(userDTOS.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get user list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @GetMapping("/getAllAuthor")
    public ResponseEntity<?> getAllAuthor() {
        ApiResponse response = new ApiResponse();
        try {
            List<AuthorListDTO> userDTOList = userService
                    .listByAuthor();
            response.setSuccess(true);
            response.setMessage("Get user list successfully");
            response.setData(userDTOList);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get user list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

//    @PreAuthorize("hasAuthority('Admin')")
    @GetMapping("/getAllTrainer")
    public ResponseEntity<?> getAllTrainer(@RequestParam(value = "status", required = false) String status) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get trainer list successfully");
            response.setData(userService.listTrainer(status));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get trainer list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editUser(@RequestBody UsersDTO userDTO) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            userService.updateByID(userDTO);
            response.setSuccess(true);
            response.setMessage("Update user profile successfully");
            response.setData(userDTO);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update user fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @Autowired
    UserRepository userRepository;

    @PreAuthorize("hasAuthority('Admin')")
    @PostMapping("/inactiveUser")
    public ResponseEntity<?> inactiveUser(@RequestParam Integer id) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            userRepository.setInactiveUser(id);
            response.setSuccess(true);
            response.setMessage("Update user to inactive successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update user fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @SuppressWarnings("deprecation")
    @RequestMapping(method = RequestMethod.POST, value = "/imageUpload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile fileStream)
            throws IOException, ServletException {
        ApiResponse response = new ApiResponse();
        try {

            response.setSuccess(true);
            response.setMessage("Update user's avatar successfully");
            response.setData(userService.uploadAva(fileStream));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update user's avatar fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/getUser")
    public ResponseEntity<?> getUserInFeature(
            @RequestParam(value = "projectId", required = false) List<Integer> projectId,
            @RequestParam(value = "role", required = false) List<String> role,
            @RequestParam(value = "status", required = false) String status) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            List<UserList> userDTOList = userService.listUser(projectId, role, status);
            response.setSuccess(true);
            response.setMessage("Get user list successfully");
            response.setData(userDTOList);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get user list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getUserList")
    public ResponseEntity<?> getStudentList(@RequestParam(value = "status", required = false) String status,
                                            @RequestParam(value = "role", required = false) String role) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get user list successfully");
            response.setData(userService.getUserList(status, role));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get user list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getUserDetail")
    public ResponseEntity<?> getUserById(@RequestParam(value = "id", required = true) Integer id) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get user detail successfully");
            response.setData(userService.findUserById(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get user detail fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
