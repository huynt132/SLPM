package com.fpt.capstone.backend.api.BackEnd.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
public class EvaluationCriteriaDTO {
    private Integer id;
    private Integer iterationId;
    private String iterationName;
    private Integer subjectId;
    private BigDecimal evaluationWeight;
    private String guide;
    private String name;
//    private Integer teamEvaluation;
    private String type;
    private Integer maxLoc;
    private String status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date created;
    private Integer createdBy;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date modified;
    private Integer modifiedBy;
    private String createdByUser;
    private String modifiedByUser;

    public EvaluationCriteriaDTO(Integer id, Integer iterationId, String iterationName,Integer subjectId, BigDecimal evaluationWeight,
                                 Integer maxLoc, String status, Date created,
                                 Integer createdBy, Date modified, Integer modifiedBy, String createdByUser,
                                 String modifiedByUser, String name, String guide, String type)
    {
        this.id = id;
        this.iterationId = iterationId;
        this.iterationName = iterationName;
        this.subjectId=subjectId;
        this.evaluationWeight = evaluationWeight.multiply(BigDecimal.valueOf(100));
        this.maxLoc = maxLoc;
        this.status = status;
        this.created = created;
        this.createdBy = createdBy;
        this.modified = modified;
        this.modifiedBy = modifiedBy;
        this.createdByUser = createdByUser;
        this.modifiedByUser = modifiedByUser;
        this.guide = guide;
        this.name = name;
        this.type = type;
    }

    public EvaluationCriteriaDTO(Integer id, BigDecimal evaluationWeight, String type, String status) {
        this.id = id;
        this.evaluationWeight = evaluationWeight;
        this.type = type;
        this.status = status;
    }

    public EvaluationCriteriaDTO(Integer id, Integer iterationId, BigDecimal evaluationWeight, String type) {
        this.id = id;
        this.iterationId = iterationId;
        this.evaluationWeight = evaluationWeight;
        this.type = type;
    }


}
