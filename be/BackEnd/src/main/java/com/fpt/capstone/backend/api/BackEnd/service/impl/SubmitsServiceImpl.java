package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.dto.class_users.ClassUsersDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.*;
import com.fpt.capstone.backend.api.BackEnd.repository.*;
import com.fpt.capstone.backend.api.BackEnd.service.SubmitService;
import com.fpt.capstone.backend.api.BackEnd.service.UserLogService;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import com.fpt.capstone.backend.api.BackEnd.utils.CommonUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SubmitsServiceImpl implements SubmitService {
    @Autowired
    private MilestonesRepository milestonesRepository;
    @Autowired
    private FunctionsRepository functionsRepository;
    @Autowired
    private MemberEvaluationRepository memberEvaluationRepository;
    @Autowired
    private UserLogService userLogService;
    @Autowired
    private ClassUserRepository classUserRepository;
    @Autowired
    private IterationEvaluationsRepository iterationEvaluationsRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TeamEvaluationsRepository teamEvaluationsRepository;
    @Autowired
    private LocEvaluationsRepository locEvaluationsRepository;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private IterationsRepository iterationsRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private Validate validate;
    @Autowired
    private SubmitsRepository submitsRepository;
    @Autowired
    private TrackingsRepository trackingsRepository;
    @Autowired
    private UserService userService;

    /**
     * Api lấy danh sách thông tin submit milestones
     *
     * @param projectId
     * @param milestoneId
     * @return
     */
    @Override
    public List<MilestoneSubmitDTO> getMilestoneSubmits(Integer classId, List<Integer> projectId, Integer milestoneId) throws Exception {
        //Check quyền
        List<Integer> authorityProjectIds = permissionService.getProjectAuthority(null);
        if (projectId == null) projectId = authorityProjectIds;
        else projectId.retainAll(authorityProjectIds);

        HashMap<Integer, MilestoneSubmitDTO> result = new HashMap<>();
        List<MilestoneSubmitDTO> milestoneSubmitDTOS = milestonesRepository.getMilestoneSubmits(classId, projectId, milestoneId);
        List<Integer> hasTeamEvalProjectIds = new ArrayList<>();

        for (MilestoneSubmitDTO t : milestoneSubmitDTOS) {
            result.putIfAbsent(t.getProjectId(), t);
            if (Arrays.asList(ModelStatus.SUBMIT_STATUS_HAS_TEAM_EVAL).contains(t.getSubmitStatus()) && !hasTeamEvalProjectIds.contains(t.getProjectId())) {
                hasTeamEvalProjectIds.add(t.getProjectId());
            }
            if (t.getFunctionStatus() != null && t.getTrackingMilestoneId() != null && t.getTrackingMilestoneId() == milestoneId && t.getFunctionStatus().equals(ModelStatus.STATUS_PENDING)) {
                result.get(t.getProjectId()).setTotalPendingFunctions((result.get(t.getProjectId()).getTotalPendingFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalPendingFunctions() + 1));
                result.get(t.getProjectId()).setTotalPlanCommitFunctions((result.get(t.getProjectId()).getTotalPlanCommitFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalPlanCommitFunctions() + 1));
            }
            if (t.getFunctionStatus() != null && t.getTrackingMilestoneId() != null && t.getTrackingMilestoneId() == milestoneId && t.getFunctionStatus().equals(ModelStatus.STATUS_COMMITTED)) {
                result.get(t.getProjectId()).setTotalCommittedFunctions((result.get(t.getProjectId()).getTotalCommittedFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalCommittedFunctions() + 1));
                result.get(t.getProjectId()).setTotalPlanCommitFunctions((result.get(t.getProjectId()).getTotalPlanCommitFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalPlanCommitFunctions() + 1));
            }
            if (t.getFunctionStatus() != null && t.getTrackingMilestoneId() != null && t.getTrackingMilestoneId() == milestoneId && t.getFunctionStatus().equals(ModelStatus.STATUS_SUBMITTED)) {
                result.get(t.getProjectId()).setTotalSubmittedFunctions((result.get(t.getProjectId()).getTotalSubmittedFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalSubmittedFunctions() + 1));
                result.get(t.getProjectId()).setTotalSubmitEvalFunctions((result.get(t.getProjectId()).getTotalSubmitEvalFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalSubmitEvalFunctions() + 1));
            }
            if (t.getFunctionStatus() != null && t.getTrackingMilestoneId() != null && t.getTrackingMilestoneId() == milestoneId && t.getFunctionStatus().equals(ModelStatus.STATUS_EVALUATED)) {
                result.get(t.getProjectId()).setTotalEvaluatedFunctions((result.get(t.getProjectId()).getTotalEvaluatedFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalEvaluatedFunctions() + 1));
                result.get(t.getProjectId()).setTotalSubmitEvalFunctions((result.get(t.getProjectId()).getTotalSubmitEvalFunctions() == null) ? 1 : (result.get(t.getProjectId()).getTotalSubmitEvalFunctions() + 1));
            }
        }
        if (!hasTeamEvalProjectIds.isEmpty()) result = getTeamGrade(hasTeamEvalProjectIds, milestoneId, result);
        return new ArrayList<>(result.values());
    }

    /**
     * Hàm lấy điểm tổng của đánh giá team dựa theo list projectId và milestone truyền vào
     *
     * @param projectId
     * @param milestoneId
     * @return
     */
    @Override
    public HashMap<Integer, MilestoneSubmitDTO> getTeamGrade(List<Integer> projectId, Integer milestoneId, HashMap<Integer, MilestoneSubmitDTO> result) {
        List<TeamEvaluationsDTO> teamEvaluationsDTOS = milestonesRepository.getMilestoneTeamEval(projectId, milestoneId);
        HashMap<Integer, BigDecimal> totalTeamEvaWeight = new HashMap<>();
        for (TeamEvaluationsDTO t : teamEvaluationsDTOS) {
            result.get(t.getProjectId()).getTeamEvaluations().add(t);
            if (t.getWeight() != null) {
                totalTeamEvaWeight.put(t.getProjectId(), (totalTeamEvaWeight.get(t.getProjectId()) == null ? t.getWeight() : totalTeamEvaWeight.get(t.getProjectId()).add(t.getWeight())));
                if (t.getGrade() != null) {
                    result.get(t.getProjectId()).setTeamGrade(result.get(t.getProjectId()).getTeamGrade() == null ? t.getGrade().multiply(t.getWeight()) : (result.get(t.getProjectId()).getTeamGrade()).add(t.getGrade().multiply(t.getWeight())));
                }
            }
        }

        for (Map.Entry<Integer, MilestoneSubmitDTO> entry : result.entrySet()) {
            if (entry.getValue().getTeamGrade() != null)
                entry.getValue().setTeamGrade(entry.getValue().getTeamGrade().divide(totalTeamEvaWeight.get(entry.getValue().getProjectId()), 1, RoundingMode.HALF_EVEN));
        }

        return result;
    }

    /**
     * Hãm dùng để update hoặc create team evalutation
     *
     * @param teamEvaluationsDTO
     * @return
     */
    @Override
    public TeamEvaluationsDTO editTeamEvaluation(TeamEvaluationsDTO teamEvaluationsDTO) throws Exception {
        Users auth = userService.getUserLogin();

        TeamEvaluations curTeamEval = teamEvaluationsRepository.findById(teamEvaluationsDTO.getId()).get();
        BigDecimal deviantGrade = teamEvaluationsDTO.getGrade().subtract(curTeamEval.getGrade()); // Phần điểm chênh lệch của điểm mới và cũ
        if (deviantGrade.compareTo(BigDecimal.valueOf(0)) == 0) {
            return teamEvaluationsDTO; //case k chênh lệch điểm cũ và mới thì return k cần xử lý
        }
        // Check permission
        // Là trainer quản lý team hoặc là admin,

        TeamEvaluations teamEvaluations = modelMapper.map(teamEvaluationsDTO, TeamEvaluations.class);
        teamEvaluations.setCreatedBy(curTeamEval.getCreatedBy());
        teamEvaluations.setCreated(curTeamEval.getCreated());
        teamEvaluations.setModified(CommonUtils.getCurrentTime(null));
        teamEvaluations.setModifiedBy(auth.getId());
        teamEvaluationsRepository.save(teamEvaluations);
        evaluateTeamGrade(teamEvaluationsDTO, deviantGrade);

        return teamEvaluationsDTO;
    }

    /**
     * Hãm dùng để update hoặc create điểm individual (có type là other)
     *
     * @param memberEvaluationsDTO
     * @return
     */
    @Override
    public MemberEvaluationsDTO editIndividualEvaluation(MemberEvaluationsDTO memberEvaluationsDTO) throws Exception {
        // lấy điểm hiện tại:
        IndividualEvalDTO individualEval = memberEvaluationRepository.getIndividualEval(memberEvaluationsDTO.getId());

        BigDecimal deviantMemberEval = memberEvaluationsDTO.getGrade().subtract(individualEval.getMemberGrade());
        if (deviantMemberEval.compareTo(BigDecimal.valueOf(0)) == 0) return memberEvaluationsDTO;

        //cập nhật điểm member mới;
        individualEval.setMemberGrade(memberEvaluationsDTO.getGrade());

        BigDecimal deviantIterationEval = deviantMemberEval.multiply(individualEval.getMemberWeight());
        individualEval.setIterationGrade(individualEval.getIterationGrade().add(deviantIterationEval));

        // Cập nhật điểm class user
        BigDecimal deviantTotalFinalGrade;
        if (individualEval.getIterationIsOngoing().equals(ModelStatus.IS_ONGOING)) {
            // lấy config điểm ongoing
            IterationsDTO finalEvalIterationWeight = iterationsRepository.findFinalEvalWeight(memberEvaluationsDTO.getProjectId(), 0, ModelStatus.STATUS_ACTIVE);
            BigDecimal finalEvalWeight = BigDecimal.valueOf(0);
            if (finalEvalIterationWeight != null) finalEvalWeight = finalEvalIterationWeight.getEvaluationWeight();
            BigDecimal ongoingWeight = BigDecimal.valueOf(1).subtract(finalEvalWeight);

            // Điểm tiến trình
            BigDecimal deviantOngoingGrade = deviantIterationEval.multiply(individualEval.getIterationWeight());
            individualEval.setClassUserOngoingGrade(reRangeGrade(individualEval.getClassUserOngoingGrade().add(deviantOngoingGrade)));

            // Set lại điểm tổng
            deviantTotalFinalGrade = deviantOngoingGrade.multiply(ongoingWeight);

        } else {
            // Điểm final eval
            deviantTotalFinalGrade = deviantIterationEval.multiply(individualEval.getIterationWeight());
            individualEval.setClassUserFinalEvalGrade(reRangeGrade(individualEval.getIterationGrade()));
        }
        // Set lại điểm tổng
        individualEval.setClassUserFinalTopicGrade(reRangeGrade(individualEval.getClassUserFinalTopicGrade().add(deviantTotalFinalGrade)));

        //Update lại các điểm vào db
        memberEvaluationRepository.updateMemberGrade(individualEval.getMemberEvalId(), individualEval.getMemberGrade());
        iterationEvaluationsRepository.updateIterationEvaluation(individualEval.getIterationEvalId(), individualEval.getIterationGrade());
        if (individualEval.getIterationIsOngoing().equals(ModelStatus.IS_ONGOING)) {
            classUserRepository.updateOngoingGrade(individualEval.getClassUserId(), individualEval.getClassUserOngoingGrade(), individualEval.getClassUserFinalTopicGrade());
        } else {
            classUserRepository.updatePresGrade(individualEval.getClassUserId(), individualEval.getClassUserFinalEvalGrade(), individualEval.getClassUserFinalTopicGrade());
        }

        return memberEvaluationsDTO;
    }

    /**
     * Hàm dùng để update lại đánh giá iteration cho các thành viên trong team khi có điểm team mới được thay đổi
     *
     * @param newEvaluation
     * @param deviantGrade
     */
    public void evaluateTeamGrade(TeamEvaluationsDTO newEvaluation, BigDecimal deviantGrade) {
        // Lấy config điểm của ongoing và present
        IterationsDTO finalEvalIterationWeight = iterationsRepository.findFinalEvalWeight(newEvaluation.getProjectId(), 0, ModelStatus.STATUS_ACTIVE);
        BigDecimal finalEvalWeight = BigDecimal.valueOf(0);
        if (finalEvalIterationWeight != null) finalEvalWeight = finalEvalIterationWeight.getEvaluationWeight();
        BigDecimal ongoingWeight = BigDecimal.valueOf(1).subtract(finalEvalWeight);

        // Tính toán điểm mới được thêm vào iteration eval
        BigDecimal deviantIteGrade = deviantGrade.multiply(newEvaluation.getWeight());

        // Danh sách học viên trong lớp cần sửa điểm
        List<Integer> projectIds = Arrays.asList(newEvaluation.getProjectId());
        Page<ClassUsersDTO> classesUserDTOs = classUserRepository.searchClassUsers(null, projectIds, null, null, ModelStatus.STATUS_ACTIVE, null);
        List<ClassUsersDTO> classesUsers = classesUserDTOs.getContent();
        List<Integer> classUserIds = classesUsers.stream().map(t -> t.getId()).collect(Collectors.toList());

        // Danh sách điểm ite hiện tại của học sinh
        List<IterationEvaluations> iterationEvaluationList = iterationEvaluationsRepository.findAllByMilestoneIdAndClassUserIdIn(newEvaluation.getMilestoneId(), classUserIds);
        HashMap<String, IterationEvaluations> iterationEvaluations = new HashMap<>(); // Key = classUserId-milestoneId
        for (IterationEvaluations t : iterationEvaluationList) {
            iterationEvaluations.put(t.getClassUserId().toString() + "-" + newEvaluation.getMilestoneId().toString(), t);
        }

        // Lấy config điểm của iteration hiện tại
        Iterations curIteration = iterationsRepository.getById(newEvaluation.getIterationId());

        for (ClassUsersDTO t : classesUsers) {
            iterationEvaluations.get(t.getId().toString() + "-" + newEvaluation.getMilestoneId().toString()).setGrade(reRangeGrade(iterationEvaluations.get(t.getId().toString() + "-" + newEvaluation.getMilestoneId().toString()).getGrade().add(deviantIteGrade)));
            // update lại student eval trong bảng class_user cho các thành viên trong team
            BigDecimal deviantTotalFinalGrade;
            if (curIteration.getIsOngoing().equals(ModelStatus.IS_ONGOING)) {
                // Điểm tiến trình
                BigDecimal oldOngoingGrade = t.getOngoingEval();
                BigDecimal newOngoingGrade = oldOngoingGrade.add(deviantIteGrade.multiply(curIteration.getEvaluationWeight()));
                t.setOngoingEval(reRangeGrade(newOngoingGrade));
                deviantTotalFinalGrade = t.getOngoingEval().subtract(oldOngoingGrade).multiply(ongoingWeight);
            } else {
                // Điểm final eval
                t.setFinalEval(reRangeGrade(t.getFinalEval().add(deviantIteGrade)));
                deviantTotalFinalGrade = deviantIteGrade.multiply(curIteration.getEvaluationWeight());
            }
            // Set lại điểm tổng
            t.setFinalTopicEval(reRangeGrade(t.getFinalTopicEval().add(deviantTotalFinalGrade)));
        }

        List<IterationEvaluations> valueList = new ArrayList<>(iterationEvaluations.values());
        iterationEvaluationsRepository.saveAll(valueList);

        List<ClassUsers> classUsersList = classesUsers
                .stream()
                .map(user -> modelMapper.map(user, ClassUsers.class))
                .collect(Collectors.toList());

        classUserRepository.saveAll(classUsersList);
    }

    @Override
    public void updateStatus(Integer projectId, Integer milestoneId) {
        String curSubmitStatus = submitsRepository.getCurStatus(projectId, milestoneId);
        String newStatus = ModelStatus.NEXT_STATUS_STEP.get(curSubmitStatus);
        //String curTrackingStatus= trackingsRepository.getCurTrackingStatus(projectId,milestoneId);
        submitsRepository.updateStatus(projectId, milestoneId, newStatus);
        // Chỉ update status với các function đã được đánh giá
        trackingsRepository.updateToStatus(projectId, milestoneId, newStatus);
        trackingsRepository.updateFunctionToStatus(projectId, milestoneId, newStatus);
    }

    /**
     * Hàm dùng để đếm số lượng function dựa theo submit status và milestone id
     * Chỗ này cần đổi hướng query tách ra làm 2 để trong trường hợp k có function nào thì vẫn trả ra data bằng 0 thay vì rỗng
     *
     * @param projectId
     * @param milestoneId
     * @return
     */
    @Override
    public List<FunctionCountDTO> getFunctionCount(Integer projectId, List<Integer> milestoneId) {
        HashMap<Integer, FunctionCountDTO> mapResult = new HashMap<>();
        List<FunctionCountDTO> result = functionsRepository.getStudentByClass(projectId);
        Map<Integer, FunctionCountDTO> resultMap = new HashMap<>();
        for (FunctionCountDTO functionCountDTO : result) {
            resultMap.putIfAbsent(functionCountDTO.getStudentId(), functionCountDTO);
        }

        List<FunctionCountDTO> functionCounts = functionsRepository.getFunctionCount(projectId, milestoneId);
        for (FunctionCountDTO functionCount : functionCounts) {
            mapResult.putIfAbsent(functionCount.getStudentId(), functionCount);
            // tăng total dựa theo status
            Integer currentCount;
            if (functionCount.getStatus() == null) continue;
            switch (functionCount.getStatus()) {
                case "pending":
                    currentCount = mapResult.get(functionCount.getStudentId()).getTotalPending();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getStudentId()).setTotalPending(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "committed":
                    currentCount = mapResult.get(functionCount.getStudentId()).getTotalCommitted();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getStudentId()).setTotalCommitted(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "submitted":
                    currentCount = mapResult.get(functionCount.getStudentId()).getTotalSubmitted();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getStudentId()).setTotalSubmitted(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "evaluated":
                    currentCount = mapResult.get(functionCount.getStudentId()).getTotalEvaluated();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getStudentId()).setTotalEvaluated(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "rejected":
                    currentCount = mapResult.get(functionCount.getStudentId()).getTotalRejected();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getStudentId()).setTotalRejected(currentCount + functionCount.getStatusCount().intValue());
                    break;
            }
            currentCount = mapResult.get(functionCount.getStudentId()).getTotal();
            if (currentCount == null) currentCount = 0;
            mapResult.get(functionCount.getStudentId()).setTotal(currentCount + functionCount.getStatusCount().intValue());
        }

        for (FunctionCountDTO value : mapResult.values()) {
            resultMap.put(value.getStudentId(), value);
        }
        return new ArrayList<>(resultMap.values());
    }

    @Override
    public List<FunctionSubmitStatusCountDTO> getFunctionStatusCount(Integer classId, List<Integer> milestoneId) {
        HashMap<Integer, FunctionSubmitStatusCountDTO> mapResult = new HashMap<>();
        List<FunctionSubmitStatusCountDTO> result = projectRepository.getProjectsByClass(classId);
        Map<Integer, FunctionSubmitStatusCountDTO> resultMap = new HashMap<>();
        for (FunctionSubmitStatusCountDTO functionSubmitStatusCountDTO : result) {
            resultMap.putIfAbsent(functionSubmitStatusCountDTO.getProjectId(), functionSubmitStatusCountDTO);
        }
        List<FunctionSubmitStatusCountDTO> functionCounts = functionsRepository.getFunctionStatusCount(classId, milestoneId);
        for (FunctionSubmitStatusCountDTO functionCount : functionCounts) {
            mapResult.putIfAbsent(functionCount.getProjectId(), functionCount);
            // tăng total dựa theo status
            Integer currentCount;
            if (functionCount.getStatus() == null) continue;
            switch (functionCount.getStatus()) {
                case "pending":
                    currentCount = mapResult.get(functionCount.getProjectId()).getTotalPending();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getProjectId()).setTotalPending(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "committed":
                    currentCount = mapResult.get(functionCount.getProjectId()).getTotalCommitted();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getProjectId()).setTotalCommitted(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "submitted":
                    currentCount = mapResult.get(functionCount.getProjectId()).getTotalSubmitted();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getProjectId()).setTotalSubmitted(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "evaluated":
                    currentCount = mapResult.get(functionCount.getProjectId()).getTotalEvaluated();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getProjectId()).setTotalEvaluated(currentCount + functionCount.getStatusCount().intValue());
                    break;
                case "rejected":
                    currentCount = mapResult.get(functionCount.getProjectId()).getTotalRejected();
                    if (currentCount == null) currentCount = 0;
                    mapResult.get(functionCount.getProjectId()).setTotalRejected(currentCount + functionCount.getStatusCount().intValue());
                    break;
            }
            currentCount = mapResult.get(functionCount.getProjectId()).getTotal();
            if (currentCount == null) currentCount = 0;
            mapResult.get(functionCount.getProjectId()).setTotal(currentCount + functionCount.getStatusCount().intValue());
        }

        for (FunctionSubmitStatusCountDTO count : mapResult.values()) {
            resultMap.put(count.getProjectId(), count);
        }
        return new ArrayList<>(resultMap.values());
    }

    /**
     * Hãm dùng để lấy data của loc evaluation
     *
     * @param functionId
     * @return
     */
    @Override
    public FunctionsDTO getFunctionEvaluate(Integer functionId) throws Exception {
        FunctionsDTO function = functionsRepository.getFunctionEvaluate(functionId);
//        if (function != null) {
//            Page<UpdateLogDTO> updateLogDTOPage = userLogService.listBy(functionId, 1, 999);
//            List<UpdateLogDTO> updateLogDTOS = updateLogDTOPage.getContent();
//            function.setUpdateLog(updateLogDTOS);
//        }

        return function;
    }


    /**
     * Hãm dùng để lấy data của loc evaluation
     *
     * @param functionId
     * @return
     */
    @Override
    public FunctionsDTO getNewLocFunction(Integer functionId) throws Exception {
        FunctionsDTO function = functionsRepository.getNewLocFunction(functionId);
        if (function != null) {
            Page<UpdateLogDTO> updateLogDTOPage = userLogService.listBy(functionId, 1, 999);
            List<UpdateLogDTO> updateLogDTOS = updateLogDTOPage.getContent();
            function.setUpdateLog(updateLogDTOS);
        }

        return function;
    }

    /**
     * Hãm dùng để lấy thêm mới hoặc cập nhật điểm đánh giác loc của function
     *
     * @param function@return
     */
    @Override
    public FunctionsDTO evaluateFunction(FunctionsDTO function) throws Exception {
        FunctionsDTO oldFunction;
        Boolean isNewLoc = function.getStatus().equals(ModelStatus.STATUS_EVALUATED);
        if (isNewLoc) oldFunction = getNewLocFunction(function.getId());// case new loc evaluate
        else oldFunction = getFunctionEvaluate(function.getId());

        LocEvaluations locEvaluation = new LocEvaluations(function.getMilestoneId(),
                function.getId(), function.getComplexityId(), function.getQualityId(),
                function.getConvertedLoc(), function.getEvaluationComment(), function.getNewMilestoneId(),
                function.getNewComplexityId(), function.getNewQualityId(), function.getNewConvertedLoc());

        if (function.getLocEvaluationId() != null) locEvaluation.setId(oldFunction.getLocEvaluationId());// case update

        if (function.getStatus().equals(ModelStatus.STATUS_REJECTED)) {
            if (oldFunction.getConvertedLoc() != null) {
                throw new Exception("Can not reject because of function has already been evaluated!");
            }
            // update status tracking
            trackingsRepository.evaluateTracking(function.getId(), function.getStatus(), function.getTrackingNote());
            functionsRepository.evaluateFunction(function.getId(), function.getStatus());
            return function;
        }

        updateLocEvaluate(oldFunction, function, locEvaluation, isNewLoc);

        return function;
    }

    /**
     * Hàm dùng để tính lại điểm khi có điểm loc mới được đánh giá cho function
     *
     * @param oldFunction
     * @param newFunction
     * @param locEvaluation
     * @throws Exception
     */
    public void updateLocEvaluate(FunctionsDTO oldFunction, FunctionsDTO newFunction, LocEvaluations locEvaluation, Boolean isNewLoc) throws Exception {
        // Lấy config điểm của ongoing và present
        IterationsDTO finalEvalIterationWeight = iterationsRepository.findFinalEvalWeight(newFunction.getProjectId(), 0, ModelStatus.STATUS_ACTIVE);
        BigDecimal finalEvalWeight = BigDecimal.valueOf(0);
        if (finalEvalIterationWeight != null) finalEvalWeight = finalEvalIterationWeight.getEvaluationWeight();
        BigDecimal ongoingWeight = BigDecimal.valueOf(1).subtract(finalEvalWeight);

        Integer curMilestoneId;
        // case new loc thì sẽ tính vào điểm cho iteration cuối cùng
        if (isNewLoc) {
            curMilestoneId = milestonesRepository.getLastMilestone(newFunction.getProjectId(), PageRequest.of(0, 1)).get(0).getId();
        }
        else curMilestoneId = newFunction.getMilestoneId();

        EvaluationDTO evaluationDTO = locEvaluationsRepository.getDemoEvalInfo(curMilestoneId, newFunction.getAssigneeId(), newFunction.getProjectId());
        if (isNewLoc && evaluationDTO == null) throw new Exception("the final milestone for this subject has no loc evaluation criteria!");
        // Cập nhật điểm total loc
        // case new loc thì sẽ tính vào điểm cho iteration cuối cùng
        Integer deviantLoc = 0;
        if (isNewLoc) {
            if (oldFunction.getNewConvertedLoc() == null || oldFunction.getNewConvertedLoc() == 0) {
                // case chưa từng có new loc thì deviant với điểm cũ
                deviantLoc = oldFunction.getConvertedLoc() == null ? locEvaluation.getNewConvertedLoc() : locEvaluation.getNewConvertedLoc() - oldFunction.getConvertedLoc(); //số lượng loc thay đổi
            } else {
                //case đã có new loc thì deviant với điểm new Loc cũ
                deviantLoc = oldFunction.getNewConvertedLoc() == null ? locEvaluation.getNewConvertedLoc() : locEvaluation.getNewConvertedLoc() - oldFunction.getNewConvertedLoc(); //số lượng loc thay đổi
            }
        } else {
            deviantLoc = oldFunction.getConvertedLoc() == null ? locEvaluation.getConvertedLoc() : locEvaluation.getConvertedLoc() - oldFunction.getConvertedLoc(); //số lượng loc thay đổi
        }

        evaluationDTO.setMemberTotalLoc(evaluationDTO.getMemberTotalLoc() == null ? deviantLoc : evaluationDTO.getMemberTotalLoc() + deviantLoc);

        // check nếu total_loc lớn hơn max_loc
        if (evaluationDTO.getMemberMaxLoc().compareTo(evaluationDTO.getMemberTotalLoc()) == -1)
            throw new Exception("total loc has exceeded max loc, please contact trainer for more support!");

        // lưu loc mới vào db
        locEvaluation = locEvaluationsRepository.save(locEvaluation);

        //update status function, tracking
//        functionsRepository.updateFunctionStatus(newFunction.getId(), ModelStatus.STATUS_EVALUATED);
//        trackingsRepository.updateTrackingStatus(newFunction.getTrackingId(), ModelStatus.STATUS_EVALUATED);

        // cập nhật điểm grade = totalLoc / maxLoc * 10 để về điểm hệ số 10
        BigDecimal newMemberGrade = BigDecimal.valueOf(evaluationDTO.getMemberTotalLoc()).divide(BigDecimal.valueOf(evaluationDTO.getMemberMaxLoc()), 2, RoundingMode.HALF_EVEN).multiply(BigDecimal.valueOf(10));
        BigDecimal deviantMemberGrade = newMemberGrade.subtract(evaluationDTO.getMemberEvalGrade());
        evaluationDTO.setMemberEvalGrade(reRangeGrade(newMemberGrade));

        // Cập nhật điểm ite eval
        BigDecimal deviantIteGrade = deviantMemberGrade.multiply(evaluationDTO.getEvaluationWeight());
        BigDecimal newIteGrade = evaluationDTO.getIterationGrade().add(deviantIteGrade); // đem deviant member eval nhân với trọng số của nó để lấy deviant của iteration
        evaluationDTO.setIterationGrade(reRangeGrade(newIteGrade));

        // Cập nhật điểm class user
        BigDecimal deviantOngoingGrade = deviantIteGrade.multiply(evaluationDTO.getIterationWeight());
        BigDecimal newOngoingGrade = evaluationDTO.getOnGoingGrade().add(deviantOngoingGrade);
        evaluationDTO.setOnGoingGrade(reRangeGrade(newOngoingGrade));

        // Set lại điểm tổng
        BigDecimal deviantTotalFinalGrade = deviantOngoingGrade.multiply(ongoingWeight);
        evaluationDTO.setFinalTopicGrade(reRangeGrade(evaluationDTO.getFinalTopicGrade().add(deviantTotalFinalGrade)));

        // update điểm vào db
        locEvaluationsRepository.updateMemberEval(evaluationDTO.getMemberEvalGrade(), evaluationDTO.getMemberTotalLoc(), evaluationDTO.getMemberEvalId());
        locEvaluationsRepository.updateIteEval(evaluationDTO.getIterationGrade(), evaluationDTO.getIterationEvaluationId());
        locEvaluationsRepository.updateClassUserEval(evaluationDTO.getOnGoingGrade(), evaluationDTO.getFinalTopicGrade(), evaluationDTO.getClassUserId());
    }

    /**
     * Hàm format lại điểm
     *
     * @param grade
     * @return
     */
    public BigDecimal reRangeGrade(BigDecimal grade) {
        // Nếu bé hơn 0 -> set về = 0
        if (grade.compareTo(BigDecimal.valueOf(0)) == -1) return BigDecimal.valueOf(0);
        // Nếu lớn hơn 10 -> set về = 10
        if (grade.compareTo(BigDecimal.valueOf(10)) == 1) return BigDecimal.valueOf(10);

        return grade;
    }
}
