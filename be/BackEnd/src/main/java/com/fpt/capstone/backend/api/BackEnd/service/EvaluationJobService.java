package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.MilestoneCriteriaDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.entity.*;
import com.fpt.capstone.backend.api.BackEnd.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
//Transactional(rollbackFor = {Exception.class})
public class EvaluationJobService {
    @Autowired
    private MilestonesRepository milestonesRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private IterationEvaluationsRepository iterationEvaluationsRepository;
    @Autowired
    private TeamEvaluationsRepository teamEvaluationsRepository;
    @Autowired
    private MemberEvaluationRepository memberEvaluationRepository;
    @Autowired
    private SubmitsRepository submitsRepository;
    @Autowired
    private ProjectRepository projectRepository;

    public void createStudentEvaluation(ClassUsers student, Users auth) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String curTime = formatter.format(date);

        List<MilestoneCriteriaDTO> criteria = milestonesRepository.getIterationCriteria(student.getClassId());
        HashMap<Integer, List<MilestoneCriteriaDTO>> iterationCriteriaMap = new HashMap<>();

        //generate map lưu key là milestoneId và value chứa các tiêu chí của nó
        for (MilestoneCriteriaDTO milestoneCriteria : criteria) {
            List<MilestoneCriteriaDTO> iterationCriteria = new ArrayList<>();
            if (iterationCriteriaMap.get(milestoneCriteria.getMilestoneId()) != null) {
                iterationCriteria = iterationCriteriaMap.get(milestoneCriteria.getMilestoneId());
            }

            iterationCriteria.add(milestoneCriteria);
            iterationCriteriaMap.putIfAbsent(milestoneCriteria.getMilestoneId(), iterationCriteria);
        }

        List<MemberEvaluations> memberEvaluations = new ArrayList<>();
        // thực hiện thêm record
        for (Integer milestoneId : iterationCriteriaMap.keySet()) {
            List<MilestoneCriteriaDTO> MilestoneCriteriaList = iterationCriteriaMap.get(milestoneId);
            //Tạo bản ghi iteration evaluation
            IterationEvaluations newIterationEvaluation = new IterationEvaluations(milestoneId, student.getId(), BigDecimal.valueOf(0), BigDecimal.valueOf(0),date, auth.getId() );
            newIterationEvaluation = iterationEvaluationsRepository.save(newIterationEvaluation);

            // Thêm điểm cá nhân
            for (MilestoneCriteriaDTO milestoneCriteriaDTO : MilestoneCriteriaList) {
                if (Arrays.asList(ModelStatus.CRITERIA_TYPE_INDIVIDUAL).contains(milestoneCriteriaDTO.getCriteriaType())) {
                    memberEvaluations.add(new MemberEvaluations(newIterationEvaluation.getId(), milestoneCriteriaDTO.getCriteriaId(), 0, BigDecimal.valueOf(0), auth.getId(), curTime));
                }
            }
        }

        memberEvaluationRepository.saveAll(memberEvaluations);
    }

    /**
     * Hàm dùng để tạo mới các bản ghi teamEvaluation và Submit khi tạo mới team
     *
     * @param project
     */
    public void createTeamEvaluation(Projects project, Users auth) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String curTime = formatter.format(date);

        List<MilestoneCriteriaDTO> criteria = projectRepository.getTeamCriteria(project.getClassId());

        List<TeamEvaluations> teamEvaluations = new ArrayList<>();
        Map<Integer, Submits> submitsMap = new HashMap<>();
        for (MilestoneCriteriaDTO projectCriteria : criteria) {
            if (projectCriteria.getCriteriaType().equals(ModelStatus.CRITERIA_TYPE_TEAM)) {
                teamEvaluations.add(new TeamEvaluations(projectCriteria.getCriteriaId(), projectCriteria.getMilestoneId(), project.getId(), BigDecimal.valueOf(0), auth.getId(), curTime));
            }

            submitsMap.putIfAbsent(projectCriteria.getMilestoneId(), new Submits(projectCriteria.getMilestoneId(), project.getId(), ModelStatus.SUBMIT_STATUS_PENDING, auth.getId(), curTime));
        }

        teamEvaluationsRepository.saveAll(teamEvaluations);
        submitsRepository.saveAll(submitsMap.values());
    }
}
