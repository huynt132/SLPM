package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.TeamEvaluationsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.TeamEvaluations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface TeamEvaluationsRepository extends JpaRepository<TeamEvaluations, Integer> {
    @Modifying
    @Query("UPDATE TeamEvaluations te SET te.grade = :grade, te.comment = :comment, te.modified = :modified, te.modifiedBy = :modifiedBy where te.id = :id ")
    void updateTeamEvaluate(@Param("id") Integer id,
                            @Param("grade") BigDecimal grade,
                            @Param("comment") String comment,
                            @Param("modifiedBy") Integer modifiedBy,
                            @Param("modified") String modified);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.TeamEvaluationsDTO(te.id, te.milestoneId, " +
            "te.projectId, te.grade) FROM TeamEvaluations te " +
            "where te.projectId in :projectId " +
            "and (COALESCE(:milestoneId) is null or te.milestoneId in :milestoneId)"
    )
    List<TeamEvaluationsDTO> getTeamGradeStatistic(@Param("projectId") List<Integer> projectId,
                                                   @Param("milestoneId") List<Integer> milestoneId);

}
