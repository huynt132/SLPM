package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.*;
import com.fpt.capstone.backend.api.BackEnd.repository.*;
import com.fpt.capstone.backend.api.BackEnd.service.*;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class TrackingsServiceImpl implements TrackingsService {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ClassesRepository classesRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TrackingsRepository trackingsRepository;
    @Autowired
    FunctionsRepository functionsRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    UserService userService;
    @Autowired
    FunctionsService functionsService;
    @Autowired
    MilestonesRepository milestonesRepository;
    @Autowired
    SubmitsRepository submitsRepository;
    @Autowired
    LocEvaluationsRepository locEvaluationsRepository;


    @Override
    public void addTracking(List<Integer> functionList, Integer milestoneId, Integer assigneeId, Integer projectId) throws Exception {
        Users users = userService.getUserLogin();
        if (milestoneId == null) {
            throw new Exception("please chose one milestone to submit!");
        }
        if (assigneeId == null) {
            throw new Exception("please chose one assigneeId to submit!");
        }
        Integer milestoneGitLabId = milestonesRepository.getById(milestoneId).getGitLabId();
        Projects projects = projectRepository.getById(projectId);
        Integer gitProjectId = projects.getGitLabProjectId();
        Users usersAssign = userRepository.getById(assigneeId);
        String email = usersAssign.getEmail();
        String[] parts = email.split("@");
        String userName = parts[0];
        Integer userGitLabId = getGitLabUser(userName, gitProjectId);
        if (userGitLabId == null) {
            throw new Exception("This user has not yet joined this group on Gitlab!");
        }
        Integer gitLabFunctionId;
        List<Trackings> trackingsList = new ArrayList<>();
        for (Integer id : functionList) {

            Trackings trackings = new Trackings();
            trackings.setFunctionId(id);
            trackings.setMilestoneId(milestoneId);
            trackings.setAssigneeId(assigneeId);
            trackings.setAssignerId(users.getId());
            trackings.setStatus("pending");
            trackingsList.add(trackings);
            functionsRepository.updateFunctionStatus(id);
            gitLabFunctionId = functionsRepository.getById(id).getGitLabId();
            updateFunctionGitLab(gitLabFunctionId, userGitLabId, milestoneGitLabId, gitProjectId);

            id++;
        }
        trackingsRepository.saveAll(trackingsList);
        for (Trackings tracking : trackingsList) {
            addLog(tracking, tracking.getAssigneeId());
        }
    }

    private void updateFunctionGitLab(Integer gitLabFunctionId, Integer userGitLabId, Integer milestoneGitLabId, Integer gitProjectId) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/projects/" + gitProjectId + "/issues/" + gitLabFunctionId;
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("milestone_id", String.valueOf(milestoneGitLabId));
            map.add("assignee_ids", String.valueOf(userGitLabId));
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


    private Integer getGitLabUser(String userName, Integer gitProjectId) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            ObjectMapper mapper = new ObjectMapper();
            List<Integer> listUser = new ArrayList<>();

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/projects/" + gitProjectId + "/users";
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
            String body = response.getBody();
            List<GitLabUser> ppl2 = Arrays.asList(mapper.readValue(body, GitLabUser[].class));
            for (GitLabUser key : ppl2) {
                if (key.getUsername().equals(userName)) {
                    listUser.add(key.getId());
                }
            }
            if (listUser.size() == 0) return null;
            Integer userGitId = listUser.get(0);

            return userGitId;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void checkStatus(HttpStatus httpStatus) throws Exception {
        if (httpStatus == HttpStatus.NOT_FOUND) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.BAD_REQUEST) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.UNAUTHORIZED) {
            throw new Exception("please check your Gitlab token");
        }
        if (httpStatus == HttpStatus.FORBIDDEN) {
            throw new Exception("please check your Gitlab token's permission");
        }
    }

    @Override
    public TrackingsDTO deleteTracking(int id) {
        return null;
    }

    @Override
    public List<TrackingsDTO> showTracking() {
        return null;
    }

    @Override
    public void updateTracking(Integer trackingId, Integer newAssigneeId, Integer functionId, Integer projectId) throws Exception {
        if (newAssigneeId == null) {
            throw new Exception("please choose assignee to submit!");
        }
        Trackings trackings = trackingsRepository.getById(trackingId);
        if (trackings.getStatus().equals("evaluated") || trackings.getStatus().equals("submitted") || trackings.getStatus().equals("rejected")) {
            throw new Exception("This tracking can not update!");
        }
        Projects projects = projectRepository.getById(projectId);
        Functions functions = functionsRepository.getById(functionId);
        Integer gitLabProjectId = projects.getGitLabProjectId();
        Integer gitLabFunctionId = functions.getGitLabId();

        if (!trackings.getAssigneeId().equals(newAssigneeId)) {
            Users usersAssign = userRepository.getById(newAssigneeId);
            String email = usersAssign.getEmail();
            String[] parts = email.split("@");
            String userName = parts[0];
            Integer userGitLabId = getGitLabUser(userName, gitLabProjectId);
            if (userGitLabId == null) {
                throw new Exception("This user has not yet joined this group on Gitlab!");
            }
            updateGitAssignFunction(gitLabProjectId, gitLabFunctionId, userGitLabId);
            updateLog(trackings, newAssigneeId);
        }
        trackingsRepository.updateAssignee(trackingId, newAssigneeId);
    }

    private void updateGitAssignFunction(Integer gitLabProjectId, Integer gitLabFunctionId, Integer userGitLabId) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception(" gitlab token is invalid, please update your gitlab token in profile update!");
            }
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/projects/" + gitLabProjectId + "/issues/" + gitLabFunctionId;
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("assignee_ids", String.valueOf(userGitLabId));
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public void updateLog(Trackings trackings, Integer newAssigneeId) throws Exception {
        try {
            UserLogs userLogs = new UserLogs();
            userLogs.setGroupType("history");
            userLogs.setAction("changeData");
            userLogs.setRefId(trackings.getId());
            userLogs.setRefColumnName("assignee_id");
            userLogs.setRefTableName("trackings");
            userLogs.setOldValue(String.valueOf(trackings.getAssigneeId()));
            userLogs.setNewValue(String.valueOf(newAssigneeId));
            userLogsRespository.save(userLogs);
        } catch (Exception e) {
            throw new Exception("add log false!");
        }

    }

    public void addLog(Trackings trackings, Integer newAssigneeId) throws Exception {
        try {
            UserLogs userLogs = new UserLogs();
            userLogs.setGroupType("history");
            userLogs.setAction("addData");
            userLogs.setRefId(trackings.getId());
            userLogs.setRefColumnName("assignee_id");
            userLogs.setRefTableName("trackings");
            userLogs.setNewValue(String.valueOf(newAssigneeId));
            userLogsRespository.save(userLogs);
        } catch (Exception e) {
            throw new Exception("add log false!");
        }

    }

    public void addSubmit(Integer milestoneId, Integer projectId, String linkZip) throws Exception {
        try {
            Submits submits = new Submits();
            Date now = new Date();
            submits.setMilestoneId(milestoneId);
            submits.setProjectId(projectId);
            submits.setSubmitTime(now);
            submits.setPackageFileLink(linkZip);
            submits.setStatus("submitted");
            submitsRepository.save(submits);
        } catch (Exception e) {
            throw new Exception("add submit false!");
        }

    }


    @Override
    @Transactional( rollbackFor = {Exception.class})
    public void submitTracking(MultipartFile fileStream, List<FunctionTrackingSubmitListDTO> functionTrackingSubmitListDTO, Integer milestoneId, Integer projectId) throws Exception {
        try {
            Date now = new Date();
            String linkZip = null;

            //check bắt buộc phải có function hoặc file lúc submit
            if (fileStream == null && (functionTrackingSubmitListDTO == null || functionTrackingSubmitListDTO.isEmpty())) {
                throw new Exception("please choose file or function to submit!");
            }

            // upload file nếu có
            if (fileStream != null) {
                try {
                    File file = convertMultiPartToFile(fileStream);
                    checkFileExtension(file.getName());
                    linkZip = uploadZipToGS(fileStream, "milestones", "functions");
                } catch (Exception e) {
                    throw new Exception("an error occurred when uploading file to google storage!");
                }
            }
            // update function status và tracking nếu có submit function
            // check milestone này có tiêu chí đánh giá loc hay không
            if (functionTrackingSubmitListDTO != null) {
                List<Integer> criteria = milestonesRepository.checkLocCriteria(milestoneId);
                Boolean hasLocCriteria = criteria != null && !criteria.isEmpty();
                Trackings trackings;
                for (FunctionTrackingSubmitListDTO functions : functionTrackingSubmitListDTO) {
                    trackings = trackingsRepository.findByFunctionId(functions.getFunctionId());
                    if (functions.getChosen() == true) {
                        if (!hasLocCriteria) throw new Exception("can not submit function due to this milestone does not have loc evaluation criteria!");

                        functionsRepository.updateFunctionStatusToSubmit(ModelStatus.STATUS_SUBMITTED, functions.getFunctionId());
                        trackingsRepository.updateTrackingStauts(ModelStatus.STATUS_SUBMITTED, trackings.getFunctionId(), trackings.getMilestoneId());
                    }
                    if (trackings.getAssigneeId() != functions.getAssigneeId()) {
                        Integer functionGitId = functionsRepository.findGitId(functions.getFunctionId());
                        Integer gitLabProjectId = projectRepository.getById(projectId).getGitLabProjectId();
                        Users usersAssign = userRepository.getById(functions.getAssigneeId());
                        String email = usersAssign.getEmail();
                        String[] parts = email.split("@");
                        String userName = parts[0];
                        Integer userGitLabId = getGitLabUser(userName, gitLabProjectId);
                        if (userGitLabId == null) {
//                        functionsRepository.updateFunctionStatusToSubmit(ModelStatus.STATUS_COMMITTED, functions.getFunctionId());
//                        trackingsRepository.updateTrackingStauts(ModelStatus.STATUS_COMMITTED, trackings.getFunctionId(), trackings.getMilestoneId());
                            throw new Exception("This user has not yet joined this group on Gitlab!");
                        }
                        updateGitAssignFunction(gitLabProjectId, functionGitId, userGitLabId);
                        updateLog(trackings, functions.getAssigneeId());
                        trackingsRepository.updateAssignee(trackings.getId(), functions.getAssigneeId());
                    }
                }
            }

            //update lại data submit
            submitsRepository.submitMilestone(milestoneId, projectId, now, linkZip, ModelStatus.STATUS_SUBMITTED);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public String getMilestoneName(Integer functionId) {
        return trackingsRepository.getMilestoneName(functionId);
    }

    @Override
    public String getEvalComment(Integer functionId) {
        return locEvaluationsRepository.getEvalComment(functionId);
    }

    @Override
    public String getFunctionName(Integer functionId) {
        Functions functions = functionsRepository.getById(functionId);
        return functions.getName();
    }

    @Override
    public AddUpdateLogDTO addLogUpdate(AddUpdateLogDTO updateLogDTO) {
        UserLogs userLogs = new UserLogs();
        userLogs.setNewValue(updateLogDTO.getMilestoneId().toString());
        userLogs.setGroupType("update");
        userLogs.setAction("updateTracking");
        userLogs.setDesc(updateLogDTO.getDescription());
        Trackings trackings = trackingsRepository.findByFunctionId(updateLogDTO.getFunctionId());
        userLogs.setRefId(trackings.getId());
        userLogs.setRefColumnName("milestone_id");
        userLogs.setRefTableName("trackings");
        userLogsRespository.save(userLogs);
        updateLogDTO.setId(userLogs.getId());
        updateLogDTO.setMilestoneName(getMilestoneName(updateLogDTO.getFunctionId()));
        updateLogDTO.setCreated(userLogs.getCreated());
        return updateLogDTO;
    }

    @Override
    public AddUpdateLogDTO updateLogUpdate(AddUpdateLogDTO updateLogDTO) {
        UserLogs userLogs = userLogsRespository.getById(updateLogDTO.getId());
        userLogs.setNewValue(updateLogDTO.getMilestoneId().toString());
        userLogs.setGroupType("update");
        userLogs.setAction("updateTracking");
        userLogs.setDesc(updateLogDTO.getDescription());
        Trackings trackings = trackingsRepository.findByFunctionId(updateLogDTO.getFunctionId());
        userLogs.setRefId(trackings.getId());
        userLogs.setRefColumnName("milestone_id");
        userLogs.setRefTableName("trackings");
        userLogsRespository.save(userLogs);
        updateLogDTO.setMilestoneName(trackingsRepository.getMilestone(updateLogDTO.getMilestoneId()));
        updateLogDTO.setCreated(userLogs.getCreated());
        return updateLogDTO;
    }

    @Override
    public Integer getCurrClass(Integer functionId) {
        return trackingsRepository.getCurClass(functionId);
    }


    private void checkFileExtension(String fileName) throws ServletException {
        if (fileName != null && !fileName.isEmpty() && fileName.contains(".")) {
            String[] allowedExt = {"zip"};
            for (String ext : allowedExt) {
                if (fileName.endsWith(ext)) {
                    return;
                }
            }
            throw new ServletException("File must be zip!");
        }
    }

    private File convertMultiPartToFile(MultipartFile fileStream) throws IOException {
        if (fileStream.getSize() > 20000000) {
            throw new IOException("Update has not been successfully uploaded. Requires less than 20 MB size");
        }
        File convFile = new File(fileStream.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(fileStream.getBytes());
        fos.close();
        System.out.println("convert " + fileStream.getOriginalFilename() + " to " + convFile.getName());
        return convFile;
    }

    public String uploadZipToGS(MultipartFile fileStream, String milstoneName, String projectName) throws Exception {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
            String bucketName = "slpm";
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream is = classLoader.getResourceAsStream("ancient-ceiling-352503-4ad907700d34.json");
            Credentials credentials = GoogleCredentials.fromStream(is);
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
            String fileName = timeStamp + "-milestone-" + milstoneName + projectName + "-data";
            String filePath = fileStream.getOriginalFilename();
            BlobId blobId = BlobId.of(bucketName, fileName);
            BlobInfo blobInfo = storage.create(
                    BlobInfo
                            .newBuilder(blobId)
                            .setContentType("application/zip")
                            .build(), Files.readAllBytes(Paths.get(filePath))
            );
            System.out.println(blobInfo.getMediaLink());
            System.out.println(blobInfo.getSelfLink());
            System.out.println(blobInfo.getBlobId());
            return blobInfo.getMediaLink();
        } catch (Exception e) {

            throw new Exception("Upload to gs false");
        }
    }

    @Override
    public TrackingsDTO findById(int id) throws Exception {
        return null;
    }

    @Autowired
    private ProjectService projectService;

    @Override
    public Page<TrackingsDTO> listBy(List<Integer> projectId, List<Integer> featureId, List<Integer> milestoneId,
                                     List<Integer> assigneeId, List<Integer> assignerId, List<Integer> functionId,
                                     List<Integer> classId, String status, String functionName, int page, int per_page) throws Exception {
        String projectStatus = null;
        Pageable pageable = PageRequest.of(page - 1, per_page);
        if (ObjectUtils.isEmpty(status)) status = null;

        if (ObjectUtils.isEmpty(projectId)) {
            projectId = new ArrayList<>();
            List<ProjectsListDTO> listProject = projectService.showProjectList(null, classId, projectStatus);
            if (listProject.size() > 0) {
                Integer firstProjectId = listProject.get(0).getValue();
                projectId.add(firstProjectId);
//                return trackingsRepository.search(projectId, featureId, milestoneId, assigneeId, assignerId, functionId, classId, status, pageable);
            } else {
                projectId.add(0);
//                trackingsRepository.search(projectId, featureId, milestoneId, assigneeId, assignerId, functionId, classId, status, pageable);
            }
        }

        Page<TrackingsDTO> result = trackingsRepository.search(projectId, featureId, milestoneId, assigneeId, assignerId, functionId, classId, status, functionName, pageable);

        // check quyền được đánh giá lại loc
        if (result.getContent().isEmpty()) return result;

        // Check milestone hiện tại là mielstone cuối và trạng thái phải là Submitted
        Date now = new Date();
        List<MilestonesDTO> milestones = milestonesRepository.getLastMilestone(projectId.get(0), PageRequest.of(0, 1));

        if (milestones.isEmpty()) return result;
        MilestonesDTO lastMilestone = milestones.get(0);

        if (lastMilestone == null || lastMilestone.getFrom() == null || lastMilestone.getTo() == null || lastMilestone.getSubmitStatus() == null)
            return result;

        // status là submit và hiện tại là milestone cuối
        if (now.after(lastMilestone.getFrom()) && now.before(lastMilestone.getTo()) && lastMilestone.getSubmitStatus().equals(ModelStatus.STATUS_SUBMITTED)) {
            List<Integer> trackingIds = result.getContent().stream().map(t -> t.getId()).collect(Collectors.toList());

            List<Integer> trackingHaveUpdate = trackingsRepository.checkContainUpdate(trackingIds);
            Map<Integer, Integer> trackingHaveUpdateMap = new HashMap<>();
            for (Integer trackingId : trackingHaveUpdate) {
                trackingHaveUpdateMap.putIfAbsent(trackingId, trackingId);
            }
            if (trackingHaveUpdateMap.keySet() != null && trackingHaveUpdateMap.keySet().size() > 0) {
                result.getContent().stream().forEach(t -> {
                    if (trackingHaveUpdateMap.get(t.getId()) != null) {
                        t.setCanNewLoc(true);
                    }
                });
            }
        }

        return result;
    }

    @Autowired
    UserLogService userLogService;
    @Autowired
    UserLogsRespository userLogsRespository;

    @Override
    public TrackingsDTO getTrackingDetail(Integer id) {
        TrackingsDTO trackingsDTO = trackingsRepository.getTrackingDetail(id);
        List<UserLogsDTO> userLogsDTOS = userLogService.getListUserLog(id);
        List<Integer> userIds = new ArrayList<>();
        userLogsDTOS.forEach(t -> {
            if (t.getCreated() != null) userIds.add(t.getCreatedBy());
            if (t.getModifiedBy() != null) userIds.add(t.getModifiedBy());
            if (t.getRefColumnName().equals("assignee_id")) {
                if (t.getNewValue() != null) userIds.add(Integer.valueOf(t.getNewValue()));
                if (t.getOldValue() != null) userIds.add(Integer.valueOf(t.getOldValue()));
            }
        });
        List<Integer> userInfoIds = userIds.stream().distinct().collect(Collectors.toList());

        List<UsersDTO> userInfo = userLogsRespository.getUserDetails(userInfoIds);
        Map<Integer, UsersDTO> userInfoMap = new HashMap<>();
        for (UsersDTO usersDTO : userInfo) {
            userInfoMap.put(usersDTO.getId(), usersDTO);
        }
        List<String> log = new ArrayList<>();
        userLogsDTOS.forEach(t -> log.add(formatLog(userInfoMap, t)));
        trackingsDTO.setHistory(log);
        trackingsDTO.setAssigneeOptions(trackingsRepository.searchUserAssign(id));
        return trackingsDTO;
    }

    @Override
    public List<TrackingsDTO> showTrackingList(String status) {
        return null;
    }

    public String formatLog(Map<Integer, UsersDTO> userInfoMap, UserLogsDTO userLog) {
        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        String date = DateFor.format(userLog.getCreated());
        String log = "";
        if (userLog.getAction().equals(UserLogsDTO.CHANGE_DATA)) {
            String actor = formatUserString(userInfoMap.get(userLog.getCreatedBy()).getFullName());
            if (userLog.getRefColumnName().equals("status")) {
                log = "[" + date + "] " + actor + " changed submitting status to " + userLog.getNewValue()
                        + ((userLog.getDesc().isEmpty() && !userLog.getNewValue().equals("rejected")) ? "" : " (" + userLog.getDesc() + ")");
            }
            if (userLog.getRefColumnName().equals("assignee_id")) {
                log = "[" + date + "] " + actor + " assigned to "
                        + formatUserString(userInfoMap.get(Integer.valueOf(userLog.getNewValue())).getFullName());
            }
        }
        if (userLog.getAction().equals(UserLogsDTO.ADD_DATA)) {
            if (userLog.getRefColumnName().equals("assignee_id")) {
                log = "[" + date + "] " + "Added by " + formatUserString(userInfoMap.get(userLog.getCreatedBy()).
                        getFullName()) + ", assigned to "
                        + formatUserString(userInfoMap.get(Integer.valueOf(userLog.getNewValue())).getFullName());
            }
        }
        return log;
    }

    public static String removeAccent(String s) {

        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public String formatUserString(String fullName) {
        String[] parts = fullName.split("@");
        return parts[0];
    }
}
