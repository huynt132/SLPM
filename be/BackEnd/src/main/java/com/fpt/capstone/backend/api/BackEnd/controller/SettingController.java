package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.setting.SettingInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.setting.SettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.service.SettingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("setting")
@PreAuthorize("hasAuthority('Admin')")

@CrossOrigin(origins = "/*", maxAge = 3600)
public class SettingController {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private SettingService settingService;

    @GetMapping("/getType")
    public ResponseEntity<?> getType() {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get setting type successfully");
            response.setData(settingService.getTypeSetting());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get setting type fail, "+ e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/getRole")
    public ResponseEntity<?> getRole() {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get role successfully");
            response.setData(settingService.getSettingRole());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get role fail, "+ e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<?> addSubject(@RequestBody(required = false) SettingInputDTO settingsInput) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add setting successfully");
            response.setData(settingService.addSettings(settingsInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add setting fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editSubject(@RequestBody SettingInputDTO settingsInput) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update setting successfully");
            response.setData(settingService.updateSetting(settingsInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update setting fail, "  + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getSettingByType")
    public ResponseEntity<?> getSettingByTile(
            @RequestParam(value = "settingTypeID",required = false) List<Integer> key_id,
            @RequestParam(value = "settingTitle",required = false) String key_title,
            @RequestParam(value = "settingValue",required = false) String key_value,
            @RequestParam(value = "status",required = false) String status,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            Page<SettingsDTO> settings = settingService.getSetingByType(key_id,key_title, key_value,status, page, per_page);
            List<SettingsDTO> settingsDTOS = Arrays.asList(modelMapper.map(settings.getContent(), SettingsDTO[].class));
            response.setSuccess(true);
            response.setMessage("Get setting list successfully");
            response.setData(settingsDTOS);
            response.setTotal(settings.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get setting list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/delete")
    public ResponseEntity<?> deleteSetting(@RequestParam(name = "id") String id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update setting status successfully");
            response.setData(settingService.deleteSetting(Integer.parseInt(id)));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update setting status fail, "  + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/findById")
    public ResponseEntity<?> getSettingDetail(@RequestParam("key_id") int key_id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get setting detail successfully");
            response.setData(settingService.getSettingDetail(key_id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get setting detail fail, "  + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
