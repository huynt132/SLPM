package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.FunctionSubmitStatusCountDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.MilestoneCriteriaDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ProjectsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ProjectsListDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.Projects;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface ProjectRepository extends JpaRepository<Projects, Integer> {
    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ProjectsDTO(p.id,p.classId,c.code,p.code,p.name,p.topicCode," +
            "p.status,p.gitlabToken,p.gitlabUrl,p.gitLabProjectId,p.created,p.created_by,p.modified,p.modified_by,u1.fullName,u2.fullName)" +
            " FROM Projects p   " +
            " LEFT join Users u1 on u1.id= p.created_by " +
            " join Classes c on c.id=p.classId" +
            " LEFT join Users u2 on u2.id=p.modified_by " +
            " WHERE (:projectName is null or p.name LIKE %:projectName%) " +
            " AND (COALESCE(:classId) is null or c.id IN :classId)" +
            " AND ( c.status = 'active')" +
            " AND ( :status is null or p.status like :status% ) " +
            " AND (COALESCE(:authorityProjectIds) is null or p.id IN :authorityProjectIds) " +
            " ORDER BY p.code")
    Page<ProjectsDTO> search(@Param("classId") List<Integer> classId,
                             @Param("projectName") String projectName,
                             @Param("status") String status,
                             Pageable pageable,
                             @Param("authorityProjectIds") List<Integer> authorityProjectIds);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ProjectsDTO(p.id,p.classId,c.code,p.code,p.name,p.topicCode, " +
            "p.status,p.gitlabToken,p.gitlabUrl,p.gitLabProjectId,p.created,p.created_by,p.modified,p.modified_by,u1.fullName,u2.fullName)" +
            " FROM Projects p   " +
            " LEFT join Users u1 on u1.id= p.created_by " +
            " join Classes c on c.id=p.classId" +
            " LEFT join Users u2 on u2.id=p.modified_by " +
            "  WHERE p.id =:id")
    ProjectsDTO getProjectDetail(int id);

//    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ProjectGitIdDTO(p.id, p.classId,p.gitLabProjectId,p.gitlabToken)" +
//            " FROM Projects p   " +
//            " join Classes c on c.id=p.classId" +
//            " WHERE (COALESCE(:projectId) is null or p.id IN :projectId)  " +
//            " AND (COALESCE(:classId) is null or c.id IN :classId) ")
//    List<ProjectGitIdDTO> getGitProjectId(@Param("classId") List<Integer> classId,
//                                          @Param("projectId") List<Integer> projectId);

    Projects findByName(String name);

    Projects findByCodeAndClassId(String code, Integer classId);

    @Query(" SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ProjectsListDTO(p.id,CONCAT(p.code,'-',p.name))" +
            "  FROM Projects p " +
            " left join Classes c on c.id=p.classId " +
            " left join ClassUsers cu on cu.projectId = p.id" +
            " where (COALESCE(:authorityProjectIds) is null or p.id IN :authorityProjectIds)" +
            " AND (:status is null or p.status= :status)" +
            " AND (COALESCE(:classId) is null or c.id IN :classId)" +
            " AND (COALESCE(:userId) is null or cu.userId IN :userId)" +
            " AND (COALESCE(:subjectId) is null or c.subjectId IN :subjectId)" +
            " group by p.id")
    List<ProjectsListDTO> getLabelList(@Param("subjectId") List<Integer> subjectId,
                                       @Param("classId") List<Integer> classId,
                                       @Param("userId") List<Integer> userId,
                                       @Param("status") String status,
                                       @Param("authorityProjectIds") List<Integer> authorityProjectIds);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ProjectsDTO(p.id) FROM Projects p " +
            "left join Classes c on (c.id = p.classId and c.status = 'active')" +
            "left join ClassUsers cu on (cu.projectId = p.id and p.status = 'active' and cu.dropoutDate is null)" +
            " where cu.userId=:userId ")
    List<ProjectsDTO> getFirstProject(Integer userId, Pageable pageable);

    @Query("SELECT p.id FROM Projects p " +
            "join Classes c on (c.id = p.classId and (COALESCE(:trainerId) is null or c.trainerId IN :trainerId)) " +
            "join Subjects s on (s.id = c.subjectId and (COALESCE(:authorId) is null or s.authorId IN :authorId))")
    List<Integer> projectAuthority(Integer trainerId, Integer authorId);

    @Query("SELECT p.id FROM Projects p " +
            " left join ClassUsers  cu on ( cu.classId=p.classId and cu.projectId=p.id) " +
            " where cu.userId=:studentId " +
            " group by p.id")
    List<Integer> projectAuthorityStudent(Integer studentId);

    Projects findByCode(String code);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.MilestoneCriteriaDTO(m.id, ec.id, ec.type) FROM Milestones m " +
            "JOIN EvaluationCriteria ec on (ec.status = 'active' and m.iterationId = ec.iterationId) " +
            "WHERE m.status <> 'cancelled' and m.classId = :classId")
    List<MilestoneCriteriaDTO> getTeamCriteria(@Param("classId") Integer classId);

    List<Projects> findAllByCodeOrClassId(String code, Integer classId);

    @Query(" SELECT new com.fpt.capstone.backend.api.BackEnd.dto.ProjectsListDTO(p.id,CONCAT(p.code,'-',p.name))" +
            "  FROM Projects p " +
            " left join Features f on p.id=f.projectId " +
            " where (COALESCE(:authorityProjectIds) is null or p.id IN :authorityProjectIds) " +
            " AND (COALESCE(:featureId) is null or f.id IN :featureId)" +
            " group by p.id" +
            " order by p.id")
    List<ProjectsListDTO> ProjectListInFeatue(List<Integer> featureId, List<Integer> authorityProjectIds);
    @Query("SELECT p.id FROM Projects p " )
    List<Integer> projectAdmin();

    @Query("Select new com.fpt.capstone.backend.api.BackEnd.dto.FunctionSubmitStatusCountDTO(p.id, p.code) from Projects p " +
            "where p.classId = :classId")
    List<FunctionSubmitStatusCountDTO>getProjectsByClass(@Param("classId") Integer classId);
}
