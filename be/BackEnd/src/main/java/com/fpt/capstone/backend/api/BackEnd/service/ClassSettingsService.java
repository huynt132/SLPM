package com.fpt.capstone.backend.api.BackEnd.service;


import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingOptionDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.LabelListDTO;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
//Transactional(rollbackFor = {Exception.class})
public interface ClassSettingsService {
    ClassSettingsDTO add(ClassSettingsDTO classSettingsDTO) throws Exception;

    ClassSettingsDTO update(ClassSettingsDTO classSettingsDTO) throws Exception;

    ClassSettingsDTO showDetail(int id) throws Exception;

    Page<ClassSettingsDTO> searchBy(List<Integer> classId, String status, String titleOrValue, int page, int limit) throws Exception;

    List<ClassSettingsDTO> showList(String status);

    /**
     * Hàm lấy data cho select box dựa theo type
     * @param type
     * @return
     */
    List<ClassSettingOptionDTO> getOptions(String type, Integer classId);
    public List<ClassSettingOptionDTO> getFunctionStatusOption(Integer classId);
    public List<ClassSettingOptionDTO> getFunctionTypeOption( Integer classId);

    List<LabelListDTO> getListLabel(Integer projectId);
}
