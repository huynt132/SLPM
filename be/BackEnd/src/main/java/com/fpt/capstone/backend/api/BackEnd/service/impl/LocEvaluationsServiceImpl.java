package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.dto.loc_evaluations.LocEvaluationsDTO;
import com.fpt.capstone.backend.api.BackEnd.repository.FunctionsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.IterationEvaluationsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.LocEvaluationsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.SubmitsRepository;
import com.fpt.capstone.backend.api.BackEnd.service.LocEvaluationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LocEvaluationsServiceImpl implements LocEvaluationsService {

    @Autowired
    LocEvaluationsRepository locEvaluationsRepository;

    @Autowired
    IterationEvaluationsRepository iterationEvaluationsRepository;
    @Autowired
    SubmitsRepository submitsRepository;
    @Autowired
    FunctionsRepository functionsRepository;

    @Override
    public List<LocEvaluationsDTO> showList(int userId, int projectId, int milestoneId) throws Exception {
        return locEvaluationsRepository.getLocEvaluations(userId, projectId, milestoneId);
    }

    /**
     * Hàm lấy điểm team của 1 học sinh
     *
     * @param userId
     * @param projectId
     * @param milestoneId
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getStudentEvaluation(int userId, int projectId, int milestoneId) throws Exception {
        Map<String, Object> result = new HashMap<>();
        List<TeamEvaluationsDTO> teamEvaluations = iterationEvaluationsRepository.getStudentTeamGrade(projectId, userId, milestoneId);
        IterationEvaluationsDTO iterationEvaluation = new IterationEvaluationsDTO();
        BigDecimal demoWeight = BigDecimal.valueOf(0);
        BigDecimal demoGrade = BigDecimal.valueOf(0);
        BigDecimal totalTeamGrade = BigDecimal.valueOf(0); // tính điểm của team
        BigDecimal totalTeamWeight = BigDecimal.valueOf(0);
        BigDecimal totalIndividualGrade = BigDecimal.valueOf(0); // tính điểm của cá nhân
        BigDecimal totalIndividualWeight = BigDecimal.valueOf(0);

        String curSubmitStatus = submitsRepository.getCurStatus(projectId, milestoneId);

        for (TeamEvaluationsDTO teamEvaluation : teamEvaluations) {
            if (teamEvaluation.getGrade() != null && teamEvaluation.getWeight() != null) {
                totalTeamGrade = totalTeamGrade.add(teamEvaluation.getGrade().multiply(teamEvaluation.getWeight()));
                totalTeamWeight = totalTeamWeight.add(teamEvaluation.getWeight());
            }

            if (iterationEvaluation.getId() == null) {
                iterationEvaluation.setId(teamEvaluation.getIterationEvaluationId());
                iterationEvaluation.setBonus(teamEvaluation.getStudentIteBonus());
                iterationEvaluation.setTotalGrade(teamEvaluation.getStudentIteGrade());
            }
        }

        iterationEvaluation.setTeamEvalGrade(BigDecimal.valueOf(0)); // default sẽ = 0

        if (totalTeamWeight.compareTo(BigDecimal.valueOf(0)) == 1) iterationEvaluation.setTeamEvalGrade(totalTeamGrade.divide(totalTeamWeight, 1, RoundingMode.HALF_EVEN));

        List<MemberEvaluationsDTO> memberEvaluations = iterationEvaluationsRepository.getMemberEvaluation(projectId, userId, milestoneId);
        for (MemberEvaluationsDTO memberEvaluation : memberEvaluations) {
            //check được phép edit điểm nếu status = submitted và type điểm = other
            if (memberEvaluation.getSubmitStatus().equals(ModelStatus.STATUS_SUBMITTED) && memberEvaluation.getCriteriaType().equals(ModelStatus.CRITERIA_TYPE_OTHER)) {
                memberEvaluation.setCanEdit(true);
            }

            if (memberEvaluation.getCriteriaType().equals(ModelStatus.CRITERIA_TYPE_DEMO) && memberEvaluation.getCriteriaWeight() != null) {
                demoWeight = memberEvaluation.getCriteriaWeight().multiply(BigDecimal.valueOf(100)); // Vì 1 milestone chỉ có 1 tiêu chí demo nên gán luôn chứ k cần + tổng
                demoGrade = memberEvaluation.getGrade();
            }

            if (memberEvaluation.getGrade() != null && memberEvaluation.getCriteriaWeight() != null) {
                totalIndividualGrade = totalIndividualGrade.add(memberEvaluation.getGrade().multiply(memberEvaluation.getCriteriaWeight()));
                totalIndividualWeight = totalIndividualWeight.add(memberEvaluation.getCriteriaWeight());
            }
        }

        iterationEvaluation.setIndividualEval(BigDecimal.valueOf(0)); // default sẽ = 0
        if (totalIndividualWeight.compareTo(BigDecimal.valueOf(0)) == 1) iterationEvaluation.setIndividualEval(totalIndividualGrade.divide(totalIndividualWeight, 1, RoundingMode.HALF_EVEN));

        // Lấy thông tin bonus + count function
        FunctionCountDTO functionCount = functionsRepository.getTotalFunctionWithLoc(userId, milestoneId);
        UpdateBonusDTO bonusDTO = new UpdateBonusDTO(userId, milestoneId, iterationEvaluation.getBonus(), demoWeight, demoGrade);
        result.put("memberEvaluations", memberEvaluations);
        result.put("teamEvaluations", teamEvaluations);
        result.put("iterationEvaluation", iterationEvaluation);
        result.put("functionCount", functionCount);
        result.put("locEvaluation", bonusDTO);
        result.put("canBonus", curSubmitStatus.equals(ModelStatus.STATUS_SUBMITTED));
        return result;
    }
}
