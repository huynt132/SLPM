package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.DashboardGradeDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
//Transactional(rollbackFor = {Exception.class})
public interface DashboardService {
    /**
     * Service dùng để lấy data cho dashboard class
     * @param classId
     * @param milestoneId
     * @return
     */
    DashboardGradeDTO getClassStatistic(Integer classId, List<Integer> milestoneId);

    /**
     * Service dùng để lấy data cho dashboard team
     * @param projectId
     * @param milestoneId
     * @return
     */
    DashboardGradeDTO getTeamStatistic(Integer projectId, List<Integer> milestoneId);
}
