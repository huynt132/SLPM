package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.*;
import com.fpt.capstone.backend.api.BackEnd.repository.*;
import com.fpt.capstone.backend.api.BackEnd.service.ExcelService;
import com.fpt.capstone.backend.api.BackEnd.service.IterationEvaluationsService;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class IterationEvaluationsServiceImpl implements IterationEvaluationsService {
    @Autowired
    IterationEvaluationsRepository iterationEvaluationsRepository;
    @Autowired
    ProjectEvaluationRepository projectEvaluationRepository;
    @Autowired
    SubmitsServiceImpl submitsService;
    @Autowired
    UserService userService;
    @Autowired
    IterationsRepository iterationsRepository;
    @Autowired
    ClassUserRepository classUserRepository;
    @Autowired
    MilestonesRepository milestonesRepository;
    @Autowired
    ClassesRepository classesRepository;
    @Autowired
    SubjectSettingsRepository subjectSettingsRepository;

    @Override
    public List<IterationEvaluationsDTO> showList(Integer iterationId, List<Integer> projectId, List<Integer> milestoneId, Integer classId) {

        List<ProjectEvaluationDTO> list = new ArrayList<>();
        List<IterationEvaluationsDTO> iterationEvaluationsDTOSList = new ArrayList<>();

        if (ObjectUtils.isEmpty(milestoneId)) {
            milestoneId = new ArrayList<>();
            List<String> list1 = milestonesRepository.getListMilestoneClassId(classId);
            if (list1.size() > 0) {
                Integer milestoneList = Integer.valueOf(list1.get(0));
                milestoneId.add(milestoneList);
                list = projectEvaluationRepository.projectEvaluationList(milestoneId);
                iterationEvaluationsDTOSList = iterationEvaluationsRepository.showList(projectId, milestoneId, classId);
            } else {
                iterationEvaluationsDTOSList = new ArrayList<>();
            }
        } else {
            list = projectEvaluationRepository.projectEvaluationList(milestoneId);
            iterationEvaluationsDTOSList = iterationEvaluationsRepository.showList(projectId, milestoneId, classId);
        }

        HashMap<Integer, ProjectGradeDTO> hashMap = new HashMap<>();
        for (ProjectEvaluationDTO key : list) {
            if (hashMap.containsKey(key.getProjectId())) {
                hashMap.get(key.getProjectId()).setTotalGrade((hashMap.get(key.getProjectId()).getTotalGrade()).add((key.getGrade()).multiply(key.getEvaluationWeight())));
                hashMap.get(key.getProjectId()).setTotalWeight((hashMap.get(key.getProjectId()).getTotalWeight()).add(key.getEvaluationWeight()));
            } else {
                ProjectGradeDTO projectGradeDTO = new ProjectGradeDTO(key.getGrade().multiply(key.getEvaluationWeight()), key.getEvaluationWeight());
                hashMap.put(key.getProjectId(), projectGradeDTO);
            }
            System.out.println("project Id:" + key.getProjectId() + "/n" + "total grade:"
                    + hashMap.get(key.getProjectId()).getTotalGrade());
        }
        for (IterationEvaluationsDTO i : iterationEvaluationsDTOSList) {
            ProjectGradeDTO projectGradeDTO = hashMap.get(i.getProjectId());
            i.setTeamEvalGrade(projectGradeDTO.getTotalGrade().divide(projectGradeDTO.getTotalWeight(), 1, RoundingMode.HALF_EVEN));
        }
        return iterationEvaluationsDTOSList;
    }

    @Override
    public Page<ClassEvaluationDTO> classEvaluation(Integer classId, int page, int per_page) {
        return null;
    }

    @Override
    public ClassEvalDTO listIterationClass(Integer classId) {
        //iteration list of class
        List<ListIterationEvalDTO> listIterationEvalDTOS = iterationsRepository.findIterationInClass(classId);
        //Student list of class
        Users auth = userService.getUserLogin();
        Integer studentId = null;
        if (auth.getSettings().getId().equals(ModelStatus.ROLE_STUDENT_ID)) {
            studentId = auth.getId();
        }
        List<Integer> listStudent = classUserRepository.getByClassId(classId, studentId);
        //Grade list of student
        List<ListGradeDTO> listGradeDTOS = iterationEvaluationsRepository.getStudentGrade(listStudent, classId);
        Map<Integer, Map<String, Object>> listGrades = new HashMap<>();
        for (ListGradeDTO listGradeDTO : listGradeDTOS) {
            // nếu chưa có trong map thì tạo mới DTO, nếu có rồi thì chỉ put điểm ite vào
            if (listGrades.get(listGradeDTO.getStudentId()) == null) {
                Map<String, Object> listGradeItem = new HashMap<>();
                listGradeItem.put("iterationId", listGradeDTO.getIterationId());
                listGradeItem.put("iterationIdName", listGradeDTO.getIterationIdName());
                listGradeItem.put("milestoneId", listGradeDTO.getMilestoneId());
                listGradeItem.put("milestoneName", listGradeDTO.getMilestoneName());
                listGradeItem.put("projectId", listGradeDTO.getProjectId());
                listGradeItem.put("projectName", listGradeDTO.getProjectName());
                listGradeItem.put("rollNumber", listGradeDTO.getRollNumber());
                listGradeItem.put("studentId", listGradeDTO.getStudentId());
                listGradeItem.put("studentName", listGradeDTO.getStudentName());
                listGradeItem.put("totalGrade", listGradeDTO.getTotalGrade());
                listGradeItem.put(listGradeDTO.getKey(), listGradeDTO.getIterationGrade());
                listGrades.put(listGradeDTO.getStudentId(), listGradeItem);
            } else {
                listGrades.get(listGradeDTO.getStudentId()).put(listGradeDTO.getKey(), listGradeDTO.getIterationGrade());
            }
        }
        ClassEvalDTO classEvalDTO = new ClassEvalDTO();
        classEvalDTO.setListIterationEvalDTOS(listIterationEvalDTOS);
        classEvalDTO.setListGradeDTOS(new ArrayList<>(listGrades.values()));
        return classEvalDTO;
    }


    @Override
    public void updateBonus(Integer studentId, Integer milestoneId, BigDecimal bonus) throws Exception {
        if (bonus == null) {
            bonus = new BigDecimal(0);
        }

        MilestonesDTO milestones = milestonesRepository.getMilestonesDetail(milestoneId);
        Integer classId = milestones.getClassId();
        ClassUsers classUsers = classUserRepository.findClassUsersByClassIdAndUserId(classId, studentId);
        Integer classUserId = classUsers.getId();
        IterationEvaluations iterationEvaluations = iterationEvaluationsRepository.findByMilestoneIdAndAndClassUserId(milestoneId, classUserId);
        Iterations iterations = iterationsRepository.getById(milestones.getIterationId());
        BigDecimal iterationWeight = iterations.getEvaluationWeight();
        //old bonus
        BigDecimal oldBounus = iterationEvaluations.getBonus();
        //deviant oldBonus and newBounus
        BigDecimal deviant = bonus.subtract(oldBounus);
        BigDecimal newGrade = iterationEvaluations.getGrade().add(deviant);
        if (newGrade.compareTo(BigDecimal.valueOf(10)) == 1)
            throw new Exception("the allowed grade limit was exceeded\n");
        iterationEvaluationsRepository.updateBonus(milestoneId, classUserId, bonus, newGrade);

        // Lấy config điểm của ongoing và present
        IterationsDTO finalEvalIterationWeight = iterationsRepository.findFinalEvalWeight(classUsers.getProjectId(), 0, ModelStatus.STATUS_ACTIVE);
        BigDecimal finalEvalWeight = BigDecimal.valueOf(0);
        if (finalEvalIterationWeight != null) finalEvalWeight = finalEvalIterationWeight.getEvaluationWeight();
        BigDecimal ongoingWeight = BigDecimal.valueOf(1).subtract(finalEvalWeight);

        if (milestones.getIsOngoing() == 1) {
            BigDecimal deviantOngoingGrade = deviant.multiply(iterationWeight);
            BigDecimal newOngoingGrade = submitsService.reRangeGrade(classUsers.getOngoingEval().add(deviantOngoingGrade));

            BigDecimal deviantTotalFinalGrade = deviantOngoingGrade.multiply(ongoingWeight);
            BigDecimal newFinalGrade = submitsService.reRangeGrade(classUsers.getFinalTopicEval().add(deviantTotalFinalGrade));
            classUserRepository.updateOngoingGrade(classUserId, newOngoingGrade, newFinalGrade);
        } else {
            BigDecimal deviantPresGrade = deviant.multiply(iterationWeight);
            BigDecimal newPresGrade = submitsService.reRangeGrade(classUsers.getFinalEval().add(deviantPresGrade));

            BigDecimal deviantTotalFinalGrade = deviantPresGrade.multiply(finalEvalWeight);
            BigDecimal newFinalGrade = submitsService.reRangeGrade(classUsers.getFinalTopicEval().add(deviantTotalFinalGrade));
            classUserRepository.updatePresGrade(classUserId, newPresGrade, newFinalGrade);
        }
    }

    @Autowired
    private ExcelService excelService;

    @Override
    public String exportExcel(Integer classId) throws IOException {
        LinkedHashMap<String, String> excelFunctionHeader = generateFunctionExcelHeader(classId);
        List<Map> result = new ArrayList<>();
        //Student list of class
        Users auth = userService.getUserLogin();
        Integer studentId = null;
        if (auth.getSettings().getId().equals(ModelStatus.ROLE_STUDENT_ID)) {
            studentId = auth.getId();
        }
        List<Integer> listStudent = classUserRepository.getByClassId(classId, studentId);
        //Grade list of student
        List<ListGradeDTO> listGradeDTOS = iterationEvaluationsRepository.getStudentGrade(listStudent, classId);

        Map<Integer, Map<String, String>> listGrades = new HashMap<>();
        List<Map> result1 = new ArrayList<>();
        Map<String, String> tmp;
        for (ListGradeDTO listGradeDTO : listGradeDTOS) {
            // nếu chưa có trong map thì tạo mới DTO, nếu có rồi thì chỉ put điểm ite vào
            if (listGrades.get(listGradeDTO.getStudentId()) == null) {
                Map<String, String> listGradeItem = new HashMap<>();
                listGradeItem.put("iterationId", listGradeDTO.getIterationId().toString());
                listGradeItem.put(listGradeDTO.getIterationIdName(), listGradeDTO.getIterationGrade().toString());
                listGradeItem.put("projectName", listGradeDTO.getProjectName());
                listGradeItem.put("rollNumber", listGradeDTO.getRollNumber());
                listGradeItem.put("studentId", listGradeDTO.getStudentId().toString());
                listGradeItem.put("studentName", listGradeDTO.getStudentName());
                listGradeItem.put("totalGrade", listGradeDTO.getTotalGrade().toString());
//                listGradeItem.put(listGradeDTO.getKey(), listGradeDTO.getIterationGrade().toString());
                listGrades.put(listGradeDTO.getStudentId(), listGradeItem);
            } else {
                listGrades.get(listGradeDTO.getStudentId()).put(listGradeDTO.getIterationIdName(), listGradeDTO.getIterationGrade().toString());
            }
        }
        List<Map> a = new ArrayList<>();
        for (Map<String, String> value : listGrades.values()) {
            a.add(value);
        }
        return excelService.exportExcel(excelFunctionHeader, a, "function");
    }

    public LinkedHashMap<String, String> generateFunctionExcelHeader(Integer classId) {
        List<ListIterationEvalDTO> listIterationEvalDTOS = iterationsRepository.findIterationInClass(classId);
        LinkedHashMap<String, String> excelFunctionHeader = new LinkedHashMap<>();
        excelFunctionHeader.put("rollNumber", "Roll Number");
        excelFunctionHeader.put("studentName", "Student");
        excelFunctionHeader.put("projectName", "Team");
        excelFunctionHeader.put("totalGrade", "Total");
        for (ListIterationEvalDTO key : listIterationEvalDTOS) {
            excelFunctionHeader.put(key.getIterationName(), key.getIterationName());
        }
        return excelFunctionHeader;
    }
}
