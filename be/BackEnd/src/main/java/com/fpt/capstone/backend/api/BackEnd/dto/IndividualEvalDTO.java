package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class IndividualEvalDTO {
    private Integer memberEvalId;
    private BigDecimal memberGrade;
    private Integer iterationEvalId;
    private BigDecimal iterationGrade;
    private BigDecimal iterationWeight;
    private Integer iterationIsOngoing;
    private Integer memberCriteriaId;
    private BigDecimal memberWeight;
    private Integer classUserId;
    private BigDecimal classUserOngoingGrade;
    private BigDecimal classUserFinalEvalGrade;
    private BigDecimal classUserFinalTopicGrade;

    public IndividualEvalDTO(Integer memberEvalId, BigDecimal memberGrade, Integer iterationEvalId,
                             BigDecimal iterationGrade, BigDecimal iterationWeight, Integer memberCriteriaId,
                             BigDecimal memberWeight, Integer classUserId, BigDecimal classUserOngoingGrade,
                             BigDecimal classUserFinalTopicGrade, Integer iterationIsOngoing, BigDecimal classUserFinalEvalGrade) {
        this.memberEvalId = memberEvalId;
        this.memberGrade = memberGrade;
        this.iterationEvalId = iterationEvalId;
        this.iterationGrade = iterationGrade;
        this.iterationWeight = iterationWeight;
        this.memberCriteriaId = memberCriteriaId;
        this.memberWeight = memberWeight;
        this.classUserId = classUserId;
        this.classUserOngoingGrade = classUserOngoingGrade;
        this.classUserFinalTopicGrade = classUserFinalTopicGrade;
        this.iterationIsOngoing = iterationIsOngoing;
        this.classUserFinalEvalGrade = classUserFinalEvalGrade;
    }
}
