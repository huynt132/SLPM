package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.milestones.MilestonesDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.*;
import com.fpt.capstone.backend.api.BackEnd.repository.ClassSettingsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.ClassesRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.SubjectsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.UserRepository;
import com.fpt.capstone.backend.api.BackEnd.service.ClassesService;
import com.fpt.capstone.backend.api.BackEnd.service.ResponseErrorHandler;
import com.fpt.capstone.backend.api.BackEnd.service.SubjectsService;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import java.util.*;

@Service

public class ClassesServiceImpl implements ClassesService {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private Validate validate;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private ClassesRepository classesRepository;
    @Autowired
    private ClassSettingsRepository classSettingsRepository;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private UserService userService;
    @Autowired
    private SubjectsService subjectsService;
    @Autowired
    private UserRepository userRepository;

    @Override
    public ClassesDTO addClasses(ClassesInputDTO classesInputDTO) throws Exception {
        if (classesInputDTO.getId() != null) {
            classesInputDTO.setId(null);
        }
        validate.validateClasses(classesInputDTO);
        if (classesRepository.findByCode(classesInputDTO.getCode()) != null) {
            throw new Exception("class code already exist!");
        }
        //validate subject condition
        if (!subjectsService.checkSubjectInitCondition(Integer.valueOf(classesInputDTO.getSubjectId()))) {
            throw new Exception("this subject has not finished setting up!");
        }
        Classes classes = modelMapper.map(classesInputDTO, Classes.class);
        //User input git id system generate url
        if (classesInputDTO.getGitLabId() != null) {
            String gitUrl = generateGitUrl(Integer.valueOf(classesInputDTO.getGitLabId()));
            if (gitUrl == null) throw new Exception("Git group id not found");
            classes.setGitLabUrl(gitUrl);
            inviteTeacher(Integer.valueOf(classesInputDTO.getTrainerId()), Integer.valueOf(classesInputDTO.getGitLabId()));
        }
        classes = classesRepository.save(classes);
        Integer id = classes.getId();
        return classesRepository.getClassesDetail(id);
    }

    private String generateGitUrl(Integer gitLabId) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token=users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            if (gitLabId == null) return null;
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(users.getGitLabToken());
            String url = "https://gitlab.com/api/v4/groups/" + gitLabId;
            HttpEntity<String> request = new HttpEntity<String>(headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
            String body = response.getBody();
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> map = mapper.readValue(body, Map.class);
            String gitUrl = map.get("web_url");
            return gitUrl;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void checkStatus(HttpStatus httpStatus) throws Exception {
        if (httpStatus == HttpStatus.NOT_FOUND) {
            throw new Exception("please check your Gitlab Id");
        }
        if (httpStatus == HttpStatus.BAD_REQUEST) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.UNAUTHORIZED) {
            throw new Exception("please check your Gitlab token");
        }
        if (httpStatus == HttpStatus.FORBIDDEN) {
            throw new Exception("please check your Gitlab token's permission");
        }
    }


    private void inviteTeacher(Integer trainerId, Integer gitLabId) throws Exception {
        Users users = userService.getUserLogin();
        String token = users.getGitLabToken();

        if (ObjectUtils.isEmpty(token) || token == null) {
            throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
        }
        Users userInvinte = userRepository.getById(trainerId);
        String mailInvite = userInvinte.getEmail();
        if (ObjectUtils.isEmpty(token) || token == null) {
            throw new Exception("update you gitlab token in Profile Update Screen");
        }
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new ResponseErrorHandler());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBearerAuth(users.getGitLabToken());
        String url = "https://gitlab.com/api/v4/groups/" + gitLabId + "/invitations";
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("email", String.valueOf(mailInvite));
        map.add("access_level", String.valueOf(ModelStatus.GIT_LAB_TEACHER));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        HttpStatus httpStatus = response.getStatusCode();
        checkStatus(httpStatus);
    }

//    @Transactional
//    ClassesDTO syncClassToGit(ClassesInputDTO classesInputDTO) throws Exception {
//        try {
//            Users users = userService.getUserLogin();
//            String token = users.getGitLabToken();
//            Optional<Subjects> subject = subjectsRepository.findById(Integer.valueOf(classesInputDTO.getSubjectId()));
//            Integer gitGroupId = subject.get().getGitLabId();
//            if (ObjectUtils.isEmpty(token) || token == null) {
//                throw new Exception("Update you gitlab token in Update profile");
//            }
//            ClassesDTO classesDTO = new ClassesDTO();
//            RestTemplate restTemplate = new RestTemplate();
//            HttpHeaders headers = new HttpHeaders();
//            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//            headers.setBearerAuth(users.getGitLabToken());
//            String url = "https://gitlab.com/api/v4/groups";
//
//            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
//            map.add("parent_id", String.valueOf(gitGroupId));
//            map.add("name", classesInputDTO.getCode());
//            map.add("path", classesInputDTO.getCode());
//            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
//            ObjectMapper mapper = new ObjectMapper();
//            JsonNode root = mapper.readTree(response.getBody());
//            JsonNode idNode = root.path("id");
//            JsonNode webUrlNode = root.path("web_url");
//            Integer id = idNode.asInt();
//            String webUrl = webUrlNode.asText();
//            classesDTO.setGitLabId(id);
//            classesDTO.setGitLabUrl(webUrl);
//            return classesDTO;
//        } catch (Exception e) {
//            throw new Exception("Sync fail" + e);
//        }
//    }

    @Override
    public ClassesDTO updateClasses(ClassesInputDTO classesInput) throws Exception {
        if (ObjectUtils.isEmpty(classesInput.getId())) {
            throw new Exception("class id cannot be empty!");
        }
        if (!classesInput.getId().matches(ConstantsRegex.NUMBER_PATTERN.toString())) {
            throw new Exception("class id must be integer!");
        }
        if (classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId())) == null) {
            throw new Exception("class not exist!");
        }
        validate.validateClasses(classesInput);
        Classes classes = classesRepository.getOne(Integer.valueOf(classesInput.getId()));
        if (!classes.getCode().equals(classesInput.getCode())
                && classesRepository.findByCode(classesInput.getCode()) != null) {
            throw new Exception("class code already exist!");
        }

        //User input git id system generate url
        if (classesInput.getGitLabId() != null && classesInput.getGitlabUrl() == null) {
            String gitUrl = generateGitUrl(Integer.valueOf(classesInput.getGitLabId()));
            if (gitUrl == null) throw new Exception("Git group id not found");
            classes.setGitLabUrl(gitUrl);
            inviteTeacher(Integer.valueOf(classesInput.getTrainerId()), Integer.valueOf(classesInput.getGitLabId()));
        }
        classes = modelMapper.map(classesInput, Classes.class);
        // classes.setSubject(subjectsRepository.getById(Integer.valueOf(classesInput.getSubjectId())));
        classesRepository.save(classes);
        classes = entityManager.find(Classes.class, classes.getId());
        return showDetail(classes.getId());
    }

    @Override
    public ClassesDTO showDetail(Integer id) throws Exception {
        if (classesRepository.getClassesDetail(id) == null) {
            throw new Exception("class not exist");
        }
        return classesRepository.getClassesDetail(id);
    }

    @Override
    public Page<ClassesDTO> searchBy(String code, List<Integer> trainerId, List<String> subjectCode, List<Integer> subjectId,
                                     Integer year, String term, String status, Integer block5, int page, int limit, ResponsePaggingObject respone) throws Exception {
        Pageable pageable = PageRequest.of(page - 1, limit);

        if (ObjectUtils.isEmpty(code)) code = null;
        if (ObjectUtils.isEmpty(term)) term = null;
        if (ObjectUtils.isEmpty(status)) status = null;

        Users user = userService.getUserLogin();
        if (Objects.equals(user.getSettings().getId(), RoleID.ADMIN) || Objects.equals(user.getSettings().getId(), RoleID.AUTHOR)) {
            respone.getPermission().setAdd(Boolean.TRUE);
            respone.getPermission().setUpdate(Boolean.TRUE);
        }

        List<Integer> authorityClassIds = permissionService.getClassAuthor(null);

        return classesRepository.search(code, trainerId, subjectCode, subjectId, year, term, status, block5, pageable, authorityClassIds);

    }

    @Override
    public List<ClassesListDTO> showListClass(List<Integer> subjectId, String status) throws Exception {
        if (ObjectUtils.isEmpty(status)) {
            status = null;
        }
        List<Integer> authorityProjectIds = permissionService.getClassAuthor(null);
        List<ClassesListDTO> classesDTOS = classesRepository.getLabelList(subjectId, status, authorityProjectIds);
        return classesDTOS;
    }

    /**
     * Hàm dùng để check xem lớp đã đủ điều kiện để có thể thêm học sinh vào lớp hay chưa
     *
     * @param classId
     * @return
     */
    @Override
    public Boolean checkClassInitCondition(Integer classId) throws Exception {
        // Check đã có bản ghi class_settings cho function status và function type
        List<ClassSettings> classSettings = classSettingsRepository.findAllByClassIdAndStatusAndTypeIdIn(
                classId, ModelStatus.STATUS_ACTIVE,
                Arrays.asList(new Integer[]{ModelStatus.SETTING_FUNCTION_TYPE_ID, ModelStatus.SETTING_FUNCTION_STATUS_ID}));

        Boolean functionStatusOk = false;
        Boolean functionTypeOk = false;
        Boolean hasGitlabId = false;

        for (ClassSettings classSetting : classSettings) {
            if (functionStatusOk && functionTypeOk) break;
            if (classSetting.getTypeId().equals(ModelStatus.SETTING_FUNCTION_STATUS_ID)) functionStatusOk = true;
            if (classSetting.getTypeId().equals(ModelStatus.SETTING_FUNCTION_TYPE_ID)) functionTypeOk = true;
        }

        if (!functionStatusOk) throw new Exception("this class has not yet finished function status setting!");
        if (!functionTypeOk) throw new Exception("this class has not yet finished function type setting!");

        // Check milestones: mỗi 1 iteration phải có 1 và chỉ 1  bản ghi milestone
        List<MilestonesDTO> milestones = classesRepository.getMilestoneConfig(classId, ModelStatus.MILESTONE_STATUS_CANCELLED, ModelStatus.STATUS_ACTIVE);
        if (milestones.isEmpty()) throw new Exception("this class has not yet configure milestone!");
        HashMap<Integer, Integer> milestoneIterationMap = new HashMap<>();
        for (MilestonesDTO milestone : milestones) {
            if (milestone.getGitLabId() != null) hasGitlabId = true;
            if (milestone.getId() == null) {
                throw new Exception("no milestone for iteration " + milestone.getIterationName() + " yet!");
            }

            if (milestoneIterationMap.containsKey(milestone.getIterationId())) {
                throw new Exception("iteration " + milestone.getIterationName() + " has more than one milestone!");
            }

            milestoneIterationMap.put(milestone.getIterationId(), milestone.getId());
        }
        if (!hasGitlabId) throw new Exception("this class does not have a gitlab id!");
        return true;
    }
}
