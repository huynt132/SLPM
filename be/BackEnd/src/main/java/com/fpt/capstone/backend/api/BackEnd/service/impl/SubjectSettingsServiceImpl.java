package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.subject.SubjectsListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingInputDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingTypeDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.SubjectSettings;
import com.fpt.capstone.backend.api.BackEnd.repository.SubjectSettingsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.SubjectsRepository;
import com.fpt.capstone.backend.api.BackEnd.service.SubjectSettingsService;
import com.fpt.capstone.backend.api.BackEnd.service.SubjectsService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsRegex;
import com.fpt.capstone.backend.api.BackEnd.service.validate.ConstantsStatus;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SubjectSettingsServiceImpl implements SubjectSettingsService {

    @Autowired
    private SubjectSettingsRepository subjectSettingsRepository;
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private Validate validate = new Validate();
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    EntityManager entityManager;
    @Autowired
    private SubjectsService subjectsService;
    @Autowired
    private PermissionService permissionService;

    @Override
    public SubjectSettingsDTO addSubjectSetting(SubjectSettingInputDTO subjectSettingInputDTO) throws Exception {

        validate.validateSubjectSetting(subjectSettingInputDTO);
        if (subjectSettingsRepository.findBySubjectIdAndTitleAndTypeId(Integer.valueOf(subjectSettingInputDTO.getSubjectId())
                , subjectSettingInputDTO.getTitle(), Integer.valueOf(subjectSettingInputDTO.getTypeId())) != null) {
            throw new Exception("Title already exist");
        }

        SubjectSettings subjectSettings = convertSubjectSettingInputDTOToSubjectSetting(subjectSettingInputDTO);
        if (Integer.valueOf(subjectSettingInputDTO.getTypeId()).equals(ModelStatus.QUALITY_SETTING_TYPE_ID)) {
            BigDecimal qualityInput = (new BigDecimal(subjectSettingInputDTO.getValue())).divide(new BigDecimal(100), 1, RoundingMode.HALF_EVEN);
            subjectSettings.setValue(String.valueOf(qualityInput));
        }
        if (Integer.valueOf(subjectSettingInputDTO.getTypeId()).equals(ModelStatus.COMPLEXITY_SETTING_TYPE_ID)) {
            subjectSettings.setValue((Integer.valueOf(subjectSettingInputDTO.getValue()).toString()));
        }
        if (Integer.valueOf(subjectSettingInputDTO.getTypeId()).equals(ModelStatus.QUALITY_SETTING_TYPE_ID)) {
            BigDecimal qualityValue = new BigDecimal(subjectSettingInputDTO.getValue());
            subjectSettings.setValue(String.valueOf((qualityValue.divide(new BigDecimal(100)))));
        }
        subjectSettingsRepository.save(subjectSettings);
        subjectSettings = entityManager.find(SubjectSettings.class, subjectSettings.getId());
        return getSubjectSettingDetail(subjectSettings.getId());
    }

    @Override
    public SubjectSettingsDTO deleteSubjectSetting(int id) throws Exception {
        if (subjectSettingsRepository.getOne(id) == null) {
            throw new Exception("Setting not found");
        }
        SubjectSettings subjectSettings = subjectSettingsRepository.getOne(id);
        if (subjectSettings.getStatus().equals(ConstantsStatus.inactive.toString())) {
            throw new Exception("Setting is inactive");
        }
        subjectSettings.setStatus(ConstantsStatus.inactive.toString());
        subjectSettingsRepository.save(subjectSettings);
        subjectSettings = entityManager.find(SubjectSettings.class, subjectSettings.getId());
        return getSubjectSettingDetail(subjectSettings.getId());

    }

    @Override
    public SubjectSettingsDTO updateSubjectSetting(SubjectSettingInputDTO subjectSettingInputDTO) throws Exception {
        if (ObjectUtils.isEmpty(subjectSettingInputDTO.getId())) {
            throw new Exception("Subject can't be empty");
        }
        if (!subjectSettingInputDTO.getId().matches(ConstantsRegex.NUMBER_PATTERN.toString())) {
            throw new Exception("subjectSettingId must be integer and bigger than 0");
        }
        validate.validateSubjectSetting(subjectSettingInputDTO);
        if (subjectSettingsRepository.getSubjectSettingsDetail(Integer.parseInt(subjectSettingInputDTO.getId())) == null) {
            throw new Exception("Subject setting not found");
        }
        SubjectSettings subjectSettings = subjectSettingsRepository.getById(Integer.parseInt(subjectSettingInputDTO.getId()));
        if (Integer.parseInt(subjectSettingInputDTO.getTypeId()) != subjectSettings.getTypeId()) {
            throw new Exception("Type cannot change");
        }
        if (Integer.parseInt(subjectSettingInputDTO.getSubjectId()) != subjectSettings.getSubjectId()) {
            throw new Exception("Subject cannot change");
        }
        if (!subjectSettingInputDTO.getTitle().equals(subjectSettings.getTitle())) {
            if (subjectSettingsRepository.findSubjectSettingsByTitle(subjectSettingInputDTO.getTitle()) > 0) {
                throw new Exception("Title already exist");
            }
        }
        subjectSettings = convertSubjectSettingInputDTOToSubjectSetting(subjectSettingInputDTO);
        if (Integer.valueOf(subjectSettingInputDTO.getTypeId()).equals(ModelStatus.COMPLEXITY_SETTING_TYPE_ID)) {
            subjectSettings.setValue((Integer.valueOf(subjectSettingInputDTO.getValue()).toString()));
        }
        if (Integer.valueOf(subjectSettingInputDTO.getTypeId()).equals(ModelStatus.QUALITY_SETTING_TYPE_ID)) {
            BigDecimal qualityValue = new BigDecimal(subjectSettingInputDTO.getValue());
            subjectSettings.setValue(String.valueOf((qualityValue.divide(new BigDecimal(100)))));
        }

        // check điều kiện được phép sửa khi có lớp đang diễn ra (status = active)
        List<Integer> activeClass = subjectsRepository.getActiveClassBySubject(subjectSettings.getSubjectId());
        if (!activeClass.isEmpty()) {
            SubjectSettings oldSubjectSettings = subjectSettingsRepository.getById(subjectSettings.getId());
            if (!oldSubjectSettings.getStatus().equals(subjectSettings.getStatus())) {
                throw new Exception("can not change status because there's an ongoing class!");
            }
            if (!oldSubjectSettings.getSubjectId().equals(subjectSettings.getSubjectId())) {
                throw new Exception("can not change subject because there's an ongoing class!");
            }
            if (!oldSubjectSettings.getValue().equals(subjectSettings.getValue())) {
                throw new Exception("can not change value because there's an ongoing class!");
            }
            if (oldSubjectSettings.getTypeId() != subjectSettings.getTypeId()) {
                throw new Exception("can not change setting type because there's an ongoing class!");
            }
        }

        subjectSettingsRepository.save(subjectSettings);
        subjectSettings = entityManager.find(SubjectSettings.class, subjectSettings.getId());
        return getSubjectSettingDetail(subjectSettings.getId());
    }

    @Override
    public SubjectSettingsDTO getSubjectSettingDetail(int id) {
        return subjectSettingsRepository.getSubjectSettingsDetail(id);
    }

    @Override
    public List<SubjectSettingTypeDTO> getSubjectSettingType() throws Exception {
        return subjectSettingsRepository.getSubjectSettingType();
    }

    @Override
    public Page<SubjectSettingsDTO> searchBy(List<Integer> settingTypeId, List<Integer> subjectId, String title, String value, String status, int page, int per_page) throws Exception {
        Pageable pageable = PageRequest.of(page - 1, per_page);
        List<Integer> authorySubjectSetting = permissionService.getSubjectSetting(null);
        if (ObjectUtils.isEmpty(status)) {
            status = null;
        }
        if (!ObjectUtils.isEmpty(status) && !status.matches(ConstantsRegex.STATUS_PATTERN.toString())) {
            throw new Exception("Input status Empty to search all or active/inactive");
        }
        if (ObjectUtils.isEmpty(subjectId)) {
            subjectId = new ArrayList<>();
            List<SubjectsListDTO> listSubject = subjectsService.listBy();
            if (listSubject.size() > 0) {
                Integer firstSubjectId = listSubject.get(0).getId();
                subjectId.add(firstSubjectId);
                return subjectSettingsRepository.getSubjectSettings(settingTypeId, subjectId, title, value, status, pageable, authorySubjectSetting);
            } else {
                return subjectSettingsRepository.getSubjectSettings(settingTypeId, Collections.singletonList(0), title, value, status, pageable, authorySubjectSetting);
            }
        }
        return subjectSettingsRepository.getSubjectSettings(settingTypeId, subjectId, title, value, status, pageable, authorySubjectSetting);
    }

    private SubjectSettings convertSubjectSettingInputDTOToSubjectSetting(SubjectSettingInputDTO subjectSettingInputDTO) {
        SubjectSettings subjectSettings = modelMapper.map(subjectSettingInputDTO, SubjectSettings.class);
        subjectSettings.setSubjectId(Integer.valueOf(subjectSettingInputDTO.getSubjectId()));
        subjectSettings.setTypeId(Integer.valueOf(subjectSettingInputDTO.getTypeId()));
        return subjectSettings;
    }
}
