package com.fpt.capstone.backend.api.BackEnd.repository;

import com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingTypeDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.SubjectSettings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRES_NEW)
public interface SubjectSettingsRepository extends JpaRepository<SubjectSettings, Integer> {
    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingsDTO( " +
            "s1.id,s1.subjectId,subject.name, s1.typeId,s.title, s1.title , s1.value" +
            ", s1.status,s1.description, s1.created, s1.created_by," +
            " s1.modified, s1.modified_by,u1.fullName,u2.fullName) " +
            "FROM SubjectSettings s1 " +
            " JOIN Settings s ON s1.typeId = s.id " +
            "JOIN Subjects subject ON s1.subjectId=subject.id " +
            "LEFT join Users u1 on u1.id=s1.created_by " +
            "LEFT join Users u2 on u2.id=s1.modified_by " +
            "WHERE (COALESCE(:authoritySSIds) is null or s1.id in :authoritySSIds)" +
            "AND (COALESCE(:settingTypeId) IS NULL OR s.id IN :settingTypeId)  " +
            "AND (COALESCE(:subjectId) IS NULL OR s1.subjectId IN :subjectId) " +
            "AND (:title is null or s1.title LIKE %:title%) " +
            "AND (:value is null or s1.value LIKE %:value%) " +
            "AND (:status IS NULL OR s1.status = :status)")
    public Page<SubjectSettingsDTO> getSubjectSettings(
            @Param("settingTypeId") List<Integer> settingTypeId,
            @Param("subjectId") List<Integer> subjectId,
            @Param("title") String title,
            @Param("value") String value,
            @Param("status") String status,
            Pageable pageable, @Param("authoritySSIds") List<Integer> authoritySSIds);


    @Query("SELECT count(p.id) FROM SubjectSettings p WHERE p.title = ?1 ")
    Integer findSubjectSettingsByTitle(String title);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingsDTO( " +
            "s1.id,s1.subjectId,subject.name, s1.typeId,s2.title, s1.title, s1.value" +
            ", s1.status,s1.description, s1.created, s1.created_by," +
            " s1.modified, s1.modified_by,u1.fullName,u2.fullName) " +
            "FROM SubjectSettings s1 " +
            "JOIN Settings s2 ON s1.typeId = s2.id " +
            "JOIN Subjects subject ON s1.subjectId=subject.id " +
            "LEFT join Users u1 on u1.id=s1.created_by " +
            "LEFT join Users u2 on u2.id=s1.modified_by " +
            "WHERE s1.id = ?1 AND s2.id <> 0 AND s2.id <> 1")
    public SubjectSettingsDTO getSubjectSettingsDetail(int id);

    @Query("SELECT new com.fpt.capstone.backend.api.BackEnd.dto.subject_setting.SubjectSettingTypeDTO( s1.id, s1.title, s1.value)" +
            "FROM Settings s1 " +
            " where s1.typeId=69 ")
    List<SubjectSettingTypeDTO> getSubjectSettingType();

    @Query("SELECT count(s1.id) " +
            "FROM SubjectSettings s1 " +
            "JOIN SubjectSettings s2 ON s1.typeId = s2.id " +
            "LEFT join Users u1 on u1.id=s1.created_by " +
            "LEFT join Users u2 on u2.id=s1.modified_by " +
            "WHERE s1.id = ?1 AND s2.id=1")
    Integer findSubjectSettingsByTypeId(int id);

    SubjectSettings findBySubjectIdAndTypeId(Integer subjectId, Integer typeId);

    SubjectSettings findBySubjectIdAndTitleAndTypeId(Integer subjectId, String title, Integer typeId);

    @Query("select s.id from SubjectSettings  s join Classes c on c.subjectId=s.subjectId" +
            " where c.trainerId=:id " +
            " group by s.id " +
            " order by s.id")
    List<Integer> getSubjectSettingsTrainer(Integer id);

    @Query("select st.id from SubjectSettings  st join Subjects s on s.id=st.subjectId" +
            " where s.authorId=:id " +
            " group by st.id " +
            " order by st.id")
    List<Integer> getSubjectSettingsAuthor(Integer id);

    @Query("select s.id from SubjectSettings  s join Classes c on c.subjectId=s.subjectId" +
            " left join ClassUsers cu on cu.classId=c.id" +
            " where cu.userId=:id " +
            " group by s.id " +
            " order by s.id")
    List<Integer> getSubjectSettingsStudent(Integer id);

    @Query("select s.id from SubjectSettings s")
    List<Integer> getSubjectSettingsAdmin();
}
