package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesInputDTO;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.ResponsePaggingObject;
import com.fpt.capstone.backend.api.BackEnd.service.ClassesService;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("classes")
@CrossOrigin(origins = "*", maxAge = 3600)

public class ClassesController {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ClassesService classesService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')")
    @PostMapping("/add")
    public ResponseEntity<?> addClasses(@RequestBody ClassesInputDTO classesInput) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Add class successfully");
            response.setData(classesService.addClasses(classesInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add class fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')||hasAuthority('Trainer')")
    @PostMapping("/edit")
    public ResponseEntity<?> editClasses(@RequestBody ClassesInputDTO classesInput) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Edit class successfully");
            response.setData(classesService.updateClasses(classesInput));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Edit class fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')||hasAuthority('Trainer')")
    @GetMapping("/detail")
    public ResponseEntity<?> getDetailClasses(@RequestParam(name = "id") int id) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get detail class successfully");
            response.setData(classesService.showDetail(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get detail class fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

//    @PreAuthorize("hasAuthority('Admin')||hasAuthority('Author')||hasAuthority('Trainer')")
    @GetMapping("/getAll")
    public ResponseEntity<?> findClasses(
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "trainerId", required = false) List<Integer> trainerId,
            @RequestParam(value = "subjectCode", required = false) List<String> subjectCode,
            @RequestParam(value = "subjectId", required = false) List<Integer> subjectId,
            @RequestParam(value = "year", required = false) Integer year,
            @RequestParam(value = "term", required = false) String term,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "block5", required = false) Integer block5,
            @RequestParam("page") int page, @RequestParam("limit") int limit) {
        ResponsePaggingObject response = new ResponsePaggingObject();
        try {
            if (subjectId == null) {
                List<Integer> authoritySubjectIds = permissionService.getSubjectAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurSubject(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }

                subjectId = Arrays.asList(authoritySubjectIds.get(0));
            }

            response.setCurSubject(subjectId.get(0));

            Page<ClassesDTO> classes = classesService.searchBy(code, trainerId, subjectCode, subjectId, year, term, status, block5, page, limit, response);
            List<ClassesDTO> classesDTOS = Arrays.asList(modelMapper.map(classes.getContent(), ClassesDTO[].class));
            response.setSuccess(true);
            response.setMessage("Get class list successfully");
            response.setData(classesDTOS);
            response.setTotal(classes.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(limit);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get class list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/getList")
    public ResponseEntity<?> getListClass(@RequestParam(value = "subjectId", required = false) List<Integer> subjectId,
                                          @RequestParam(value = "status", required = false) String status) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get class list successfully");
            response.setData(classesService.showListClass(subjectId, status));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get class list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
