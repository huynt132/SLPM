package com.fpt.capstone.backend.api.BackEnd.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.capstone.backend.api.BackEnd.dto.*;
import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.TrackingResponse;
import com.fpt.capstone.backend.api.BackEnd.entity.TrackingUpdateResponse;
import com.fpt.capstone.backend.api.BackEnd.service.*;
import com.fpt.capstone.backend.api.BackEnd.service.impl.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("tracking")
@CrossOrigin(origins = "*", maxAge = 3600)
public class TrackingController {
    @Autowired
    private TrackingsService trackingsService;
    @Autowired
    private ClassUserService classUserService;
    @Autowired
    private UserService userService;
    @Autowired
    private FunctionsService functionsService;
    @Autowired
    private UserLogService userLogService;
    @Autowired
    private PermissionService permissionService;

    @SuppressWarnings("deprecation")
    @PostMapping("/add")
    public ResponseEntity<?> addTracking(@RequestBody addTrackingDTO data) {
        ApiResponse response = new ApiResponse();
        try {
            Integer projectId = functionsService.getProjectByFunction(data.getFunctionList().get(0));
            //check leader
            if (!classUserService.checkLeader(userService.getUserLogin().getId(), projectId)) {
                response.setSuccess(false);
                response.setMessage("You must be a project leader to create new tracking!");
                return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
            }
            response.setSuccess(true);
            response.setMessage("Add tracking successfully");
            trackingsService.addTracking(data.getFunctionList(), data.getMilestoneId(), data.getAssigneeId(), projectId);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Add tracking fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editTracking(@RequestBody TrackingsDTO updateTrackingDTO) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Update tracking successfully");
            trackingsService.updateTracking(updateTrackingDTO.getId(), updateTrackingDTO.getAssigneeId(), updateTrackingDTO.getFunctionId(), updateTrackingDTO.getProjectId());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Update tracking fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> findTracking(
            @RequestParam(value = "projectId", required = false) List<Integer> projectId,
            @RequestParam(value = "featureId", required = false) List<Integer> featureId,
            @RequestParam(value = "milestoneId", required = false) List<Integer> milestoneId,
            @RequestParam(value = "assigneeId", required = false) List<Integer> assigneeId,
            @RequestParam(value = "assignerId", required = false) List<Integer> assignerId,
            @RequestParam(value = "functionId", required = false) List<Integer> functionId,
            @RequestParam(value = "classId", required = false) List<Integer> classId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "functionName", required = false) String functionName,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        TrackingResponse response = new TrackingResponse();
        try {
            if (projectId == null) {
                List<Integer> authoritySubjectIds = permissionService.getProjectAuthority(null);
                // nếu k có quyền với bất kỳ subject nào thì trả ra empty array
                if (authoritySubjectIds.size() == 0 || authoritySubjectIds.get(0) == -1) {
                    response.setData(ModelStatus.EMPTY_ARRAY);
                    response.setTotal((long) 0);
                    response.setCurProjectId(null);
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
                projectId = Arrays.asList(authoritySubjectIds.get(0));
            }
            response.setCurProjectId(projectId.get(0));
            Page<TrackingsDTO> trackingsDTOPage = trackingsService.listBy(projectId, featureId, milestoneId, assigneeId, assignerId, functionId, classId, status, functionName, page, per_page);
            List<TrackingsDTO> trackingsDTOS = trackingsDTOPage.getContent();
            response.setSuccess(true);
            response.setMessage("Get tracking list successfully");
            response.setData(trackingsDTOS);
            response.setTotal(trackingsDTOPage.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
//            if (trackingsDTOS.size() > 0) {
//                response.setCurProjectId(trackingsDTOS.get(0).getProjectId());
//            }

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get tracking list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getTrackingDetail")
    public ResponseEntity<?> getUserById(@RequestParam(value = "id", required = true) Integer id) throws Exception {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Get tracking detail successfully");
            response.setData(trackingsService.getTrackingDetail(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get tracking detail fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @SuppressWarnings("deprecation")
    @PostMapping(value = "/submitFunction")
    public ResponseEntity<?> submitFunction(
            @RequestParam(value = "file", required = false) MultipartFile fileStream,
            @RequestParam(value = "list", required = false) String list,
            @RequestParam(value = "milestoneId", required = false) Integer milestoneId,
            @RequestParam(value = "projectId", required = false) Integer projectId) {
        ApiResponse response = new ApiResponse();
        try {
            response.setSuccess(true);
            response.setMessage("Submit tracking successfully");
            if (list != null) {
                ObjectMapper mapper = new ObjectMapper();
                List<FunctionTrackingSubmitListDTO> functionRequestDTO = Arrays.asList(mapper.readValue(list, FunctionTrackingSubmitListDTO[].class));
                trackingsService.submitTracking(fileStream, functionRequestDTO, milestoneId, projectId);
            } else {
                trackingsService.submitTracking(fileStream, null, milestoneId, projectId);
            }
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Submit tracking fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getListUpdate")
    public ResponseEntity<?> getTrackingUpdate(
            @RequestParam(value = "functionId", required = false) Integer functionId,
            @RequestParam("page") int page,
            @RequestParam("limit") int per_page) {
        TrackingUpdateResponse response = new TrackingUpdateResponse();
        try {
            Page<UpdateLogDTO> updateLogDTOPage = userLogService.listBy(functionId, page, per_page);
            List<UpdateLogDTO> updateLogDTOS = updateLogDTOPage.getContent();
            response.setSuccess(true);
            response.setMessage("Get tracking list successfully");
            response.setData(updateLogDTOS);
            response.setTotal(updateLogDTOPage.getTotalElements());
            response.setCurrentPage(page);
            response.setPerPages(per_page);
            response.setMilestoneName(trackingsService.getMilestoneName(functionId));
            response.setEvaluateComment(trackingsService.getEvalComment(functionId));
            response.setFunctionName(trackingsService.getFunctionName(functionId));
            response.setCurClass(trackingsService.getCurrClass(functionId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setSuccess(false);
            response.setMessage("Get tracking list fail, " + e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
