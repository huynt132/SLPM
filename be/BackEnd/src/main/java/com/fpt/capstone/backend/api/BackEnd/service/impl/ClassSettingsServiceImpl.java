package com.fpt.capstone.backend.api.BackEnd.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingOptionDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ClassSettingsDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.LabelListDTO;
import com.fpt.capstone.backend.api.BackEnd.dto.ModelStatus;
import com.fpt.capstone.backend.api.BackEnd.entity.ClassSettings;
import com.fpt.capstone.backend.api.BackEnd.entity.Classes;
import com.fpt.capstone.backend.api.BackEnd.entity.EvaluationCriteria;
import com.fpt.capstone.backend.api.BackEnd.entity.Users;
import com.fpt.capstone.backend.api.BackEnd.repository.ClassSettingsRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.ClassesRepository;
import com.fpt.capstone.backend.api.BackEnd.repository.ProjectRepository;
import com.fpt.capstone.backend.api.BackEnd.service.ClassSettingsService;
import com.fpt.capstone.backend.api.BackEnd.service.ResponseErrorHandler;
import com.fpt.capstone.backend.api.BackEnd.service.UserService;
import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ClassSettingsServiceImpl implements ClassSettingsService {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private Validate validate;
    @Autowired
    private ClassSettingsRepository classSettingsRepository;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ClassesRepository classesRepository;
    @Autowired
    private UserService userService;

    @Override
    public ClassSettingsDTO add(ClassSettingsDTO classSettingsDTO) throws Exception {
        validate.validateClassSettings(classSettingsDTO);
        if (classSettingsRepository.findByTitleAndAndClassId(classSettingsDTO.getTitle(), classSettingsDTO.getClassId()) != null) {
            throw new Exception("title is already exist");
        }
        if (classSettingsRepository.findByValueAndTypeIdAndClassId(classSettingsDTO.getValue(), classSettingsDTO.getTypeId(), classSettingsDTO.getClassId()) != null) {
            throw new Exception("value is already exist");
        }
        ClassSettings classSettings = modelMapper.map(classSettingsDTO, ClassSettings.class);
        Classes classes = classesRepository.getById(classSettingsDTO.getClassId());
        if (classSettingsDTO.getTypeId().equals(ModelStatus.SETTING_LABEL_TYPE_ID)) {
            if (classes.getGitLabId() == null || classes.getGitLabId().toString().isEmpty()) {
                throw new Exception("please insert git group id to create a label");
            }
            ClassSettingsDTO classSettingsDTOsync = syncLabelToGitLab(classes.getGitLabId(), classSettingsDTO.getTitle(), classSettingsDTO.getValue());
            classSettings.setGitLabId(classSettingsDTOsync.getGitLabId());
        }
        classSettings = classSettingsRepository.save(classSettings);
        return showDetail(classSettings.getId());
    }

    private ClassSettingsDTO syncLabelToGitLab(Integer gitLabId, String title, String value) throws Exception {
        try {
            Users users = userService.getUserLogin();
            String token = users.getGitLabToken();
            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            ClassSettingsDTO classSettingsDTO = new ClassSettingsDTO();
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(token);
            String url = "https://gitlab.com/api/v4/groups/" + gitLabId + "/labels";
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("name", String.valueOf(title));
            map.add("color", value);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode idNode = root.path("id");
            Integer id = idNode.asInt();
            classSettingsDTO.setGitLabId(id);
            return classSettingsDTO;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


    @Override
    public ClassSettingsDTO update(ClassSettingsDTO classSettingsDTO) throws Exception {
        validate.validateClassSettings(classSettingsDTO);
        ClassSettings classSettings = classSettingsRepository.getById(classSettingsDTO.getId());

        if (!classSettings.getTitle().equals(classSettingsDTO.getTitle())) {
            if (classSettingsRepository.findByTitleAndAndClassId(classSettingsDTO.getTitle(), classSettingsDTO.getClassId()) != null) {
                throw new Exception("title is already exist");
            }
        }
        if (!classSettings.getValue().equals(classSettingsDTO.getValue())) {
            if (classSettingsRepository.findByValueAndTypeIdAndClassId(classSettingsDTO.getValue(), classSettingsDTO.getTypeId(), classSettingsDTO.getClassId()) != null) {
                throw new Exception("value is already exist");
            }
        }
        Classes classes = classesRepository.getById(classSettingsDTO.getClassId());
        if (classSettingsDTO.getTypeId().equals(ModelStatus.SETTING_LABEL_TYPE_ID)) {
            if (classes.getGitLabId().toString().isEmpty()) {
                throw new Exception("please insert git group id to create a label");
            }
            if (!classSettings.getTitle().equals(classSettingsDTO.getTitle()) || !classSettings.getValue().equals(classSettingsDTO.getValue()))
                syncLabelUpdateToGitLab(classes.getGitLabId(), classSettings.getGitLabId(), classSettingsDTO.getTitle(), classSettingsDTO.getValue());

        }
        classSettings = modelMapper.map(classSettingsDTO, ClassSettings.class);
        classSettingsRepository.save(classSettings);
        classSettings = entityManager.find(ClassSettings.class, classSettings.getId());
        return showDetail(classSettings.getId());
    }

    private void syncLabelUpdateToGitLab(Integer gitLabId, Integer labelGitLabId, String title, String value) throws Exception {
        try {
            Users users = userService.getUserLogin();

            String token = users.getGitLabToken();

            if (ObjectUtils.isEmpty(token) || token == null) {
                throw new Exception("gitlab token is invalid, please update your gitlab token in profile update!");
            }
            ClassSettingsDTO classSettingsDTO = new ClassSettingsDTO();
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ResponseErrorHandler());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.setBearerAuth(token);
            String url = "https://gitlab.com/api/v4/groups/" + gitLabId + "/labels/" + labelGitLabId;
            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("new_name", title);
            map.add("color", value);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
            ResponseEntity<String>  response= restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            HttpStatus httpStatus = response.getStatusCode();
            checkStatus(httpStatus);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public ClassSettingsDTO showDetail(int id) throws Exception {
        if (classSettingsRepository.getById(id) == null) {
            throw new Exception("classes not exist");
        }
        return classSettingsRepository.getClassesDetail(id);
    }

    @Override
    public Page<ClassSettingsDTO> searchBy(List<Integer> classId, String status, String titleOrValue, int page, int limit) throws Exception {
        Pageable pageable = PageRequest.of(page - 1, limit);
        if (ObjectUtils.isEmpty(status)) {
            status = null;
        }
        List<Integer> authorityProjectIds = permissionService.getClassSetting(null);
        Page<ClassSettingsDTO> classSettingsDTOS = classSettingsRepository.search(classId, status, titleOrValue, pageable, authorityProjectIds);
        return classSettingsDTOS;
    }

    @Override
    public List<ClassSettingsDTO> showList(String status) {
        return null;
    }

    /**
     * Hàm lấy data cho select box dựa theo type
     *
     * @param type
     * @return
     */
    @Override
    public List<ClassSettingOptionDTO> getOptions(String type, Integer classId) {
        return classSettingsRepository.getOptions(ModelStatus.SETTING_TYPE_MAPPER.get(type), ModelStatus.STATUS_ACTIVE, classId);
    }

    public List<ClassSettingOptionDTO> getFunctionTypeOption(Integer projectId) {
        return classSettingsRepository.getOptionProject(ModelStatus.SETTING_FUNCTION_TYPE_ID, ModelStatus.STATUS_ACTIVE, projectId);
    }

    @Override
    public List<LabelListDTO> getListLabel(Integer projectId) {
        Integer classId = projectRepository.getById(projectId).getClassId();
        return classSettingsRepository.getLabelList(classId);
    }

    public List<ClassSettingOptionDTO> getFunctionStatusOption(Integer projectId) {
        return classSettingsRepository.getOptionProject(ModelStatus.SETTING_FUNCTION_STATUS_ID, ModelStatus.STATUS_ACTIVE, projectId);
    }
    public void checkStatus(HttpStatus httpStatus) throws Exception {
        if (httpStatus == HttpStatus.NOT_FOUND) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.BAD_REQUEST) {
            throw new Exception("please check your Gitlab id");
        }
        if (httpStatus == HttpStatus.UNAUTHORIZED) {
            throw new Exception("please check your Gitlab token");
        }
        if (httpStatus == HttpStatus.FORBIDDEN) {
            throw new Exception("please check your Gitlab token's permission");
        }
    }
}
