package com.fpt.capstone.backend.api.BackEnd.service;

import com.fpt.capstone.backend.api.BackEnd.dto.setting.*;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
//Transactional(rollbackFor = {Exception.class})
public interface SettingService {
    //  public List<SettingsDTO> getTypeSetting();
    public List<SettingTypeDTO> getTypeSetting();

    public Page<SettingsDTO> getSetingByType(List<Integer> id, String title, String value, String status, int page, int per_page) throws Exception;

    public SettingsDTO deleteSetting(int id) throws Exception;

    public SettingsDTO addSettings(SettingInputDTO settingsInput) throws Exception;

    public SettingsDTO updateSetting(SettingInputDTO settingsInput) throws Exception;

    public SettingsDTO getSettingDetail(int id);

    public List<SettingRoleDTO> getSettingRole();

    public List<SettingListDTO> getSettingList();

}
