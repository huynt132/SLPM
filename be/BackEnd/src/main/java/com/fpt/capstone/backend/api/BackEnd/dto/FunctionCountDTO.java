package com.fpt.capstone.backend.api.BackEnd.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;


@Data
@NoArgsConstructor

public class FunctionCountDTO {
    private Integer studentId;
    private String studentFullName;
    private String studentRollNumber;
    private String status;
    private Long statusCount;
    private Long totalFunction;
    private Long sumLocEvaluation;
    private String member;
    private Integer total;
    private Integer totalPending;
    private Integer totalCommitted;
    private Integer totalSubmitted;
    private Integer totalRejected;
    private Integer totalEvaluated;

    public FunctionCountDTO(Integer studentId, String studentFullName, String studentRollNumber, String member,
                            Integer total, Integer totalPending, Integer totalCommitted, Integer totalSubmitted,
                            Integer totalRejected, Integer totalEvaluated) {
        this.studentId = studentId;
        this.studentFullName = studentFullName;
        this.studentRollNumber = studentRollNumber;
        this.member = member;
        this.total = total;
        this.totalPending = totalPending;
        this.totalCommitted = totalCommitted;
        this.totalSubmitted = totalSubmitted;
        this.totalRejected = totalRejected;
        this.totalEvaluated = totalEvaluated;
    }

    public FunctionCountDTO(Integer studentId, String studentFullName, String studentRollNumber, String status, long statusCount) {
        this.studentId = studentId;
        this.studentFullName = studentFullName;
        this.studentRollNumber = studentRollNumber;
        this.status = status;
        this.statusCount = statusCount;
        this.member=formatUserString(studentFullName);
    }

    public FunctionCountDTO(Integer studentId, String studentFullName, String studentRollNumber) {
        this.studentId = studentId;
        this.studentFullName = studentFullName;
        this.studentRollNumber = studentRollNumber;
        this.member=formatUserString(studentFullName);
    }

    public static String removeAccent(String s) {

        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replace('đ','d').replace('Đ','D');
    }
    public String formatUserString(String fullName) {
        String[] parts = fullName.split("@");
        return parts[0];
    }

    public FunctionCountDTO(Long totalFunction, Long sumLocEvaluation) {
        this.totalFunction = totalFunction;
        this.sumLocEvaluation = sumLocEvaluation;
    }
}
