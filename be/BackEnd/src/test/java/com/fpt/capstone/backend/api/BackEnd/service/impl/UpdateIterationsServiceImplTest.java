//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import com.fpt.capstone.backend.api.BackEnd.dto.IterationsInputDTO;
//import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class UpdateIterationsServiceImplTest {
//
//    @MockBean
//    private Validate validate;
//
//    @InjectMocks
//    private IterationsServiceImpl iterationsService;
//
//
//    @Test
//    void updateIterationsSuccessfully() {
//        IterationsInputDTO iterationsInputDTO = new IterationsInputDTO();
//        iterationsInputDTO.setSubjectId("1");
//        iterationsInputDTO.setName("iteration1");
//        iterationsInputDTO.setEvaluationWeight("1.4");
//        iterationsInputDTO.setDuration("2");
//
//        try {
//            Mockito.doThrow(new Exception("Successfully!"))
//                    .when(validate).validateIterations(iterationsInputDTO);
//            iterationsService.addIterations(iterationsInputDTO);
//        } catch (Exception e) {
//            assertEquals("Successfully!", e.getMessage());
//        }
//    }
//
//    @Test
//    void updateIterationsSubjectNotExist() {
//        IterationsInputDTO iterationsInputDTO = new IterationsInputDTO();
//        iterationsInputDTO.setSubjectId("999");
//        iterationsInputDTO.setName("iteration1");
//        iterationsInputDTO.setEvaluationWeight("1.4");
//        iterationsInputDTO.setDuration("2");
//
//        try {
//            Mockito.doThrow(new Exception("Subject not found!"))
//                    .when(validate).validateIterations(iterationsInputDTO);
//            iterationsService.addIterations(iterationsInputDTO);
//        } catch (Exception e) {
//            assertEquals("Subject not found!", e.getMessage());
//        }
//    }
//
//    @Test
//    void updateIterationsEmptyName() {
//        IterationsInputDTO iterationsInputDTO = new IterationsInputDTO();
//        iterationsInputDTO.setSubjectId("1");
//        iterationsInputDTO.setEvaluationWeight("1.4");
//        iterationsInputDTO.setDuration("2");
//
//        try {
//            Mockito.doThrow(new Exception("Iterations name can't be empty!"))
//                    .when(validate).validateIterations(iterationsInputDTO);
//            iterationsService.addIterations(iterationsInputDTO);
//        } catch (Exception e) {
//            assertEquals("Iterations name can't be empty!", e.getMessage());
//        }
//    }
//
//
//    @Test
//    void updateIterationsWrongFormatName() {
//        IterationsInputDTO iterationsInputDTO = new IterationsInputDTO();
//        iterationsInputDTO.setSubjectId("1");
//        iterationsInputDTO.setName("iteration@##1");
//        iterationsInputDTO.setEvaluationWeight("1.4");
//        iterationsInputDTO.setDuration("2");
//
//        try {
//            Mockito.doThrow(new Exception("Iterations name is not contain special characters!"))
//                    .when(validate).validateIterations(iterationsInputDTO);
//            iterationsService.addIterations(iterationsInputDTO);
//        } catch (Exception e) {
//            assertEquals("Iterations name is not contain special characters!", e.getMessage());
//        }
//    }
//
//
//    @Test
//    void updateIterationsEvaluationWeightEmpty() {
//        IterationsInputDTO iterationsInputDTO = new IterationsInputDTO();
//        iterationsInputDTO.setSubjectId("1");
//        iterationsInputDTO.setName("iteration1");
//        iterationsInputDTO.setEvaluationWeight(null);
//        iterationsInputDTO.setDuration("2");
//
//        try {
//            Mockito.doThrow(new Exception("Evaluation weight can't be empty!"))
//                    .when(validate).validateIterations(iterationsInputDTO);
//            iterationsService.addIterations(iterationsInputDTO);
//        } catch (Exception e) {
//            assertEquals("Evaluation weight can't be empty!", e.getMessage());
//        }
//    }
//
//}