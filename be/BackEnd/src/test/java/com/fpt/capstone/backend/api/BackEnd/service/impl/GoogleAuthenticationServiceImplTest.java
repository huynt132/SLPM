//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import com.fpt.capstone.backend.api.BackEnd.dto.UserLoginGGTO;
//import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
//import com.fpt.capstone.backend.api.BackEnd.entity.Settings;
//import com.fpt.capstone.backend.api.BackEnd.entity.Users;
//import com.fpt.capstone.backend.api.BackEnd.entity.sercurity.security.TokenResponseInfo;
//import com.fpt.capstone.backend.api.BackEnd.repository.SettingsRepository;
//import com.fpt.capstone.backend.api.BackEnd.repository.UserRepository;
//import com.fpt.capstone.backend.api.BackEnd.utils.security.JwtUtils;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class GoogleAuthenticationServiceImplTest {
//
//    @MockBean
//    private UserRepository userRepository;
//
//    @MockBean
//    private AuthenticationManager authenticationManager;
//
//    @MockBean
//    private JwtUtils jwtUtils;
//
//    @MockBean
//    private SettingsRepository settingsRepository;
//
//    @InjectMocks
//    private AuthenticationServiceImpl authenticationService;
//
//    @Test
//    void authenticateByEmailSuccessfully() {
//        UserLoginGGTO userLoginDto = new UserLoginGGTO("haiha@fpt.edu.vn");
//
//        Users user = new Users();
//        user.setEmail("haiha@fpt.edu.vn");
//        user.setStatus("active");
//        user.setEnabled(true);
//        Mockito.when(userRepository.getUserByEmail(userLoginDto.getEmail())).thenReturn(user);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfoEmail(userLoginDto);
//        Mockito.when(jwtUtils.generateTokenResponseInfoEmail(userLoginDto))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.ok().body(
//                ApiResponse.builder()
//                        .success(true)
//                        .message("Login success!!")
//                        .data(tokenResponseInfo).build())
//                , authenticationService.authenticateByEmail(userLoginDto));
//    }
//
////    @Test
////    void authenticateByEmailWithNoEmail() {
////        UserLoginGGTO userLoginDto = new UserLoginGGTO("");
////
////        Users user = new Users();
////        user.setEmail("haiha@fpt.edu.vn");
////        user.setStatus("active");
////        user.setEnabled(true);
////        Mockito.when(userRepository.getUserByEmail(userLoginDto.getEmail())).thenReturn(user);
////
////        TokenResponseInfo tokenResponseInfo = jwtUtils.
////                generateTokenResponseInfoEmail(userLoginDto);
////        Mockito.when(jwtUtils.generateTokenResponseInfoEmail(userLoginDto))
////                .thenReturn(tokenResponseInfo);
////
////        assertEquals(ResponseEntity.status(401).body(
////                        ApiResponse.builder()
////                                .success(false)
////                                .message("Parameter invalid!").build()                )
////                , authenticationService.authenticateByEmail(userLoginDto));
////    }
//
////    @Test
////    void authenticateByEmailWithNewUser() {
////        UserLoginGGTO userLoginDto = new UserLoginGGTO("haiha@fpt.edu.vn");
////
////        Mockito.when(userRepository.getUserByEmail(userLoginDto.getEmail())).thenReturn(null);
////
////        TokenResponseInfo tokenResponseInfo = jwtUtils.
////                generateTokenResponseInfoEmail(userLoginDto);
////        Mockito.when(jwtUtils.generateTokenResponseInfoEmail(userLoginDto))
////                .thenReturn(tokenResponseInfo);
////        Mockito.when(settingsRepository.getById(10))
////                .thenReturn(new Settings());
////
////        assertEquals(ResponseEntity.ok().body(
////                        ApiResponse.builder()
////                                .success(true)
////                                .message("Login success")
////                                .data(tokenResponseInfo).build())
////                , authenticationService.authenticateByEmail(userLoginDto));
////    }
//
//    @Test
//    void authenticateByEmailWithInactiveUser() {
//        UserLoginGGTO userLoginDto = new UserLoginGGTO("haiha@fpt.edu.vn");
//
//        Users user = new Users();
//        user.setEmail("haiha@fpt.edu.vn");
//        user.setStatus("inactive");
//        user.setEnabled(true);
//        Mockito.when(userRepository.getUserByEmail(userLoginDto.getEmail())).thenReturn(user);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfoEmail(userLoginDto);
//        Mockito.when(jwtUtils.generateTokenResponseInfoEmail(userLoginDto))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.status(401).body(
//                ApiResponse.builder()
//                        .success(false)
//                        .message("Account INACTIVE!").build())
//                , authenticationService.authenticateByEmail(userLoginDto));
//    }
//
//    @Test
//    void authenticateByEmailWithUnEnabledUser() {
//        UserLoginGGTO userLoginDto = new UserLoginGGTO("haiha@fpt.edu.vn");
//
//        Users user = new Users();
//        user.setEmail("haiha@fpt.edu.vn");
//        user.setStatus("active");
//        user.setEnabled(false);
//        Mockito.when(userRepository.getUserByEmail(userLoginDto.getEmail())).thenReturn(user);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfoEmail(userLoginDto);
//        Mockito.when(jwtUtils.generateTokenResponseInfoEmail(userLoginDto))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.status(401).body(
//                        ApiResponse.builder()
//                                .success(false)
//                                .message("Please verify your account!").build())
//                , authenticationService.authenticateByEmail(userLoginDto));
//    }
//
//
//}