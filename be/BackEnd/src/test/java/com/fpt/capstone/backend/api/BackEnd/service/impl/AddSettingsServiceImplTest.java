//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import com.fpt.capstone.backend.api.BackEnd.dto.setting.SettingInputDTO;
//import com.fpt.capstone.backend.api.BackEnd.entity.Settings;
//import com.fpt.capstone.backend.api.BackEnd.repository.SettingsRepository;
//import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.modelmapper.ModelMapper;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import javax.persistence.EntityManager;
//
//import java.util.ArrayList;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class AddSettingsServiceImplTest {
//
//    @MockBean
//    private Validate validate;
//
//    @MockBean
//    private ModelMapper modelMapper;
//
//    @MockBean
//    private SettingsRepository settingsRepository;
//
//    @MockBean
//    EntityManager entityManager;
//
//    @InjectMocks
//    SettingsServiceImpl settingsService;
//
//    @Test
//    void addSettingsSuccessfully() throws Exception {
//        SettingInputDTO settingsInput = new SettingInputDTO();
//        settingsInput.setTypeId("4");
//        settingsInput.setTitle("To_do");
//        settingsInput.setValue("To do");
//        settingsInput.setDisplayOrder("4");
//        settingsInput.setStatus("active");
//
//        Settings settings = new Settings();
//        settings.setId(1);
//
//        Mockito.when(
//                settingsRepository.searchSettingsByTitle(settingsInput.getTitle())
//        ).thenReturn(new ArrayList<>());
////        Mockito.when(
////                settingsRepository.searchByTypeIdDisplayOrder(
////                        Integer.valueOf(settingsInput.getTypeId()),
////                        Integer.parseInt(settingsInput.getDisplayOrder()))
////        ).thenReturn(0);
//        Mockito.when(
//                modelMapper.map(settingsInput, Settings.class)
//        ).thenReturn(settings);
//        Mockito.when(
//                entityManager.find(Settings.class, settings.getId())
//        ).thenReturn(settings);
//        settingsService.addSettings(settingsInput);
//        Mockito.verify(settingsRepository, Mockito.times(1)).save(settings);
//    }
//
//    @Test
//    void addSettingsTitleEmpty() {
//        SettingInputDTO settingsInput = new SettingInputDTO();
//        settingsInput.setTypeId("4");
////        settingsInput.setTitle("To_do");
//        settingsInput.setValue("To do");
//        settingsInput.setDisplayOrder("4");
//        settingsInput.setStatus("active");
//
//        try {
//            Mockito.doThrow(new Exception("Title cannot be empty!"))
//                    .when(validate).validateSetting(settingsInput);
//            settingsService.addSettings(settingsInput);
//        } catch (Exception e) {
//            assertEquals("Title cannot be empty!", e.getMessage());
//        }
//    }
//
//    @Test
//    void addSettingsTitleWrongFormat() {
//        SettingInputDTO settingsInput = new SettingInputDTO();
//        settingsInput.setTypeId("4");
//        settingsInput.setTitle("abc @ 123");
//        settingsInput.setValue("To do");
//        settingsInput.setDisplayOrder("4");
//        settingsInput.setStatus("active");
//
//        try {
//            Mockito.doThrow(new Exception("Title cannot be contain special character and number!"))
//                    .when(validate).validateSetting(settingsInput);
//            settingsService.addSettings(settingsInput);
//        } catch (Exception e) {
//            assertEquals("Title cannot be contain special character and number!", e.getMessage());
//        }
//    }
//
//    @Test
//    void addSettingsValueEmpty() {
//        SettingInputDTO settingsInput = new SettingInputDTO();
//        settingsInput.setTypeId("4");
//        settingsInput.setTitle("To_do");
////        settingsInput.setValue("To do");
//        settingsInput.setDisplayOrder("4");
//        settingsInput.setStatus("active");
//
//        try {
//            Mockito.doThrow(new Exception("Value cannot be empty!"))
//                    .when(validate).validateSetting(settingsInput);
//            settingsService.addSettings(settingsInput);
//        } catch (Exception e) {
//            assertEquals("Value cannot be empty!", e.getMessage());
//        }
//    }
//
//    @Test
//    void addSettingsTitleIdEmpty() {
//        SettingInputDTO settingsInput = new SettingInputDTO();
////        settingsInput.setTypeId("4");
//        settingsInput.setTitle("To_do");
//        settingsInput.setValue("To do");
//        settingsInput.setDisplayOrder("4");
//        settingsInput.setStatus("active");
//
//        try {
//            Mockito.doThrow(new Exception("TypeID cannot be empty!"))
//                    .when(validate).validateSetting(settingsInput);
//            settingsService.addSettings(settingsInput);
//        } catch (Exception e) {
//            assertEquals("TypeID cannot be empty!", e.getMessage());
//        }
//    }
//
//    @Test
//    void addSettingsDisplayOrderEmpty() {
//        SettingInputDTO settingsInput = new SettingInputDTO();
//        settingsInput.setTypeId("4");
//        settingsInput.setTitle("To_do");
//        settingsInput.setValue("To do");
////        settingsInput.setDisplayOrder("4");
//        settingsInput.setStatus("active");
//
//        try {
//            Mockito.doThrow(new Exception("DisplayOrder cannot be empty!"))
//                    .when(validate).validateSetting(settingsInput);
//            settingsService.addSettings(settingsInput);
//        } catch (Exception e) {
//            assertEquals("DisplayOrder cannot be empty!", e.getMessage());
//        }
//    }
//
//    @Test
//    void addSettingsStatusWrongFormat() {
//        SettingInputDTO settingsInput = new SettingInputDTO();
//        settingsInput.setTypeId("4");
//        settingsInput.setTitle("To_do");
//        settingsInput.setValue("To do");
//        settingsInput.setDisplayOrder("4");
//        settingsInput.setStatus("abc");
//
//        try {
//            Mockito.doThrow(new Exception("Status must be active or inactive only!"))
//                    .when(validate).validateSetting(settingsInput);
//            settingsService.addSettings(settingsInput);
//        } catch (Exception e) {
//            assertEquals("Status must be active or inactive only!", e.getMessage());
//        }
//    }
//}