//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import com.fpt.capstone.backend.api.BackEnd.dto.UserRegisterDTO;
//import com.fpt.capstone.backend.api.BackEnd.entity.Settings;
//import com.fpt.capstone.backend.api.BackEnd.entity.Users;
//import com.fpt.capstone.backend.api.BackEnd.repository.SettingsRepository;
//import com.fpt.capstone.backend.api.BackEnd.repository.UserRepository;
//import com.fpt.capstone.backend.api.BackEnd.service.MailService;
//import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
//import net.bytebuddy.utility.RandomString;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.TestConfiguration;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import java.util.Date;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class RegisterAuthenticationServiceImplTest {
//
//    @MockBean
//    private Validate validate;
//
//    @MockBean
//    private ModelMapper modelMapper;
//
//    @MockBean
//    private SettingsRepository settingsRepository;
//
//    @MockBean
//    private UserRepository userRepository;
//
//    @MockBean
//    private MailService mailService;
//
//    @MockBean
//    private PasswordEncoder passwordEncoder;
//
//    @InjectMocks
//    AuthenticationServiceImpl authenticationService;
//
//
//    @Test
//    void registerSuccessfully() throws Exception {
//        UserRegisterDTO userRegister = new UserRegisterDTO();
//        userRegister.setEmail("hihi@fpt.edu.vn");
//        userRegister.setPassword("abc@123");
//        userRegister.setFullName("nguyen van a");
//        userRegister.setBirthday("22-01-2000");
//        userRegister.setGender("male");
//        userRegister.setRollNumber("he141547");
//
//        Users user = new Users();
//
//
//        Mockito.when(modelMapper.map(userRegister, Users.class)).thenReturn(user);
//        Mockito.when(passwordEncoder.encode(userRegister.getPassword())).thenReturn("abc@123");
//        Mockito.when(settingsRepository.getById(10)).thenReturn(new Settings());
//        Mockito.doNothing().when(mailService).sendMailVerifycode(userRegister, RandomString.make(32));
//
//        authenticationService.register(userRegister);
//        Mockito.verify(userRepository,Mockito.times(1)).save(user);
//    }
//
//    @Test
//    void registerMailWrongFormat() throws Exception {
//        UserRegisterDTO userRegister = new UserRegisterDTO();
//        userRegister.setEmail("hihi.hehe");
//        userRegister.setPassword("abc@123");
//        userRegister.setFullName("nguyen van a");
//        userRegister.setBirthday("22-01-2000");
//        userRegister.setGender("male");
//        userRegister.setRollNumber("he141547");
//
//        Users user = new Users();
//
//        Mockito.when(modelMapper.map(userRegister, Users.class)).thenReturn(user);
//        Mockito.when(passwordEncoder.encode(userRegister.getPassword())).thenReturn("abc@123");
//        Mockito.when(settingsRepository.getById(10)).thenReturn(new Settings());
//        Mockito.doNothing().when(mailService).sendMailVerifycode(userRegister, RandomString.make(32));
//
//        try {
//            Mockito.doThrow(new Exception("Email must be format abc@fpt.edu.vn!"))
//                    .when(validate).validateRegisterUser(userRegister);
//            authenticationService.register(userRegister);
//        } catch (Exception e) {
//            assertEquals("Email must be format abc@fpt.edu.vn!", e.getMessage());
//        }
//    }
//
//    @Test
//    void registerMailNotBelongFPT() throws Exception {
//        UserRegisterDTO userRegister = new UserRegisterDTO();
//        userRegister.setEmail("hehe@gmail.com.vn");
//        userRegister.setPassword("abc@123");
//        userRegister.setFullName("nguyen van a");
//        userRegister.setBirthday("22-01-2000");
//        userRegister.setGender("male");
//        userRegister.setRollNumber("he141547");
//
//        Users user = new Users();
//
//
//        Mockito.when(modelMapper.map(userRegister, Users.class)).thenReturn(user);
//        Mockito.when(passwordEncoder.encode(userRegister.getPassword())).thenReturn("abc@123");
//        Mockito.when(settingsRepository.getById(10)).thenReturn(new Settings());
//        Mockito.doNothing().when(mailService).sendMailVerifycode(userRegister, RandomString.make(32));
//
//        try {
//            Mockito.doThrow(new Exception("Email tail must be abc@fpt.edu.vn!"))
//                    .when(validate).validateRegisterUser(userRegister);
//            authenticationService.register(userRegister);
//        } catch (Exception e) {
//            assertEquals("Email tail must be abc@fpt.edu.vn!", e.getMessage());
//        }
//    }
//
//    @Test
//    void registerPasswordNull() throws Exception {
//        UserRegisterDTO userRegister = new UserRegisterDTO();
//        userRegister.setEmail("hihi@fpt.edu.vn");
//
//        userRegister.setFullName("nguyen van a");
//        userRegister.setBirthday("22-01-2000");
//        userRegister.setGender("male");
//        userRegister.setRollNumber("he141547");
//
//        Users user = new Users();
//
//        Mockito.when(modelMapper.map(userRegister, Users.class)).thenReturn(user);
//        Mockito.when(passwordEncoder.encode(userRegister.getPassword())).thenReturn("abc@123");
//        Mockito.when(settingsRepository.getById(10)).thenReturn(new Settings());
//        Mockito.doNothing().when(mailService).sendMailVerifycode(userRegister, RandomString.make(32));
//
//        try {
//            Mockito.doThrow(new Exception("Password cannot be empty!"))
//                    .when(validate).validateRegisterUser(userRegister);
//            authenticationService.register(userRegister);
//        } catch (Exception e) {
//            assertEquals("Password cannot be empty!", e.getMessage());
//        }
//    }
//
//    @Test
//    void registerBirthdayNull() throws Exception {
//        UserRegisterDTO userRegister = new UserRegisterDTO();
//        userRegister.setEmail("hihi@fpt.edu.vn");
//        userRegister.setPassword("abc@123");
//        userRegister.setFullName("nguyen van a");
//        userRegister.setGender("male");
//        userRegister.setRollNumber("he141547");
//
//        Users user = new Users();
//
//        Mockito.when(modelMapper.map(userRegister, Users.class)).thenReturn(user);
//        Mockito.when(passwordEncoder.encode(userRegister.getPassword())).thenReturn("abc@123");
//        Mockito.when(settingsRepository.getById(10)).thenReturn(new Settings());
//        Mockito.doNothing().when(mailService).sendMailVerifycode(userRegister, RandomString.make(32));
//
//        try {
//            Mockito.doThrow(new Exception("Birthday cannot be empty!"))
//                    .when(validate).validateRegisterUser(userRegister);
//            authenticationService.register(userRegister);
//        } catch (Exception e) {
//            assertEquals("Birthday cannot be empty!", e.getMessage());
//        }
//    }
//
//    @Test
//    void registerFullNameNull() throws Exception {
//        UserRegisterDTO userRegister = new UserRegisterDTO();
//        userRegister.setEmail("hihi@fpt.edu.vn");
//        userRegister.setPassword("abc@123");
//
//        userRegister.setBirthday("22-01-2000");
//        userRegister.setGender("male");
//        userRegister.setRollNumber("he141547");
//
//        Users user = new Users();
//
//        Mockito.when(modelMapper.map(userRegister, Users.class)).thenReturn(user);
//        Mockito.when(passwordEncoder.encode(userRegister.getPassword())).thenReturn("abc@123");
//        Mockito.when(settingsRepository.getById(10)).thenReturn(new Settings());
//        Mockito.doNothing().when(mailService).sendMailVerifycode(userRegister, RandomString.make(32));
//
//        try {
//            Mockito.doThrow(new Exception("Full name cannot be empty!"))
//                    .when(validate).validateRegisterUser(userRegister);
//            authenticationService.register(userRegister);
//        } catch (Exception e) {
//            assertEquals("Full name cannot be empty!", e.getMessage());
//        }
//    }
//
//    @Test
//    void registerWrongPasswordFormat() throws Exception {
//        UserRegisterDTO userRegister = new UserRegisterDTO();
//        userRegister.setEmail("hihi@fpt.edu.vn");
//        userRegister.setPassword("123");
//        userRegister.setFullName("nguyen van a");
//        userRegister.setBirthday("22-01-2000");
//        userRegister.setGender("male");
//        userRegister.setRollNumber("he141547");
//
//        Users user = new Users();
//
//        Mockito.when(modelMapper.map(userRegister, Users.class)).thenReturn(user);
//        Mockito.when(passwordEncoder.encode(userRegister.getPassword())).thenReturn("abc@123");
//        Mockito.when(settingsRepository.getById(10)).thenReturn(new Settings());
//        Mockito.doNothing().when(mailService).sendMailVerifycode(userRegister, RandomString.make(32));
//
//        try {
//            Mockito.doThrow(
//                    new Exception("Password must have minimum eight characters, at least one letter and one number!"))
//                    .when(validate).validateRegisterUser(userRegister);
//            authenticationService.register(userRegister);
//        } catch (Exception e) {
//            assertEquals("Password must have minimum eight characters, at least one letter and one number!", e.getMessage());
//        }
//    }
//
//}