//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import com.fpt.capstone.backend.api.BackEnd.dto.UserChangePWDTO;
//import com.fpt.capstone.backend.api.BackEnd.dto.UserLoginDto;
//import com.fpt.capstone.backend.api.BackEnd.entity.ApiResponse;
//import com.fpt.capstone.backend.api.BackEnd.entity.Users;
//import com.fpt.capstone.backend.api.BackEnd.entity.sercurity.security.TokenResponseInfo;
//import com.fpt.capstone.backend.api.BackEnd.repository.UserRepository;
//import com.fpt.capstone.backend.api.BackEnd.utils.security.JwtUtils;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class BaseAuthenticationServiceImplTest {
//
//    @MockBean
//    private UserRepository userRepository;
//
//    @MockBean
//    private AuthenticationManager authenticationManager;
//
//    @MockBean
//    private JwtUtils jwtUtils;
//
//    @InjectMocks
//    private AuthenticationServiceImpl authenticationService;
//
//    @Test
//    void testAuthenticateSuccessfully() {
//
//        String email = "admin@fpt.edu.vn";
//        String Password = "1234@1234";
//
//        Users userResult = new Users();
//        userResult.setEmail(email);
//        userResult.setPassword(Password);
//        userResult.setStatus("active");
//
//        UserLoginDto userLoginDto = new UserLoginDto(email, Password);
//
//        Mockito.when(userRepository.findByEmail(email)).thenReturn(userResult);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfo(userLoginDto.getEmail(),
//                        userLoginDto.getPassword(), authenticationManager);
//
//        Mockito.when(jwtUtils.generateTokenResponseInfo(email, Password, authenticationManager))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.ok().body(
//                ApiResponse.builder()
//                        .success(true)
//                        .message("Login success")
//                        .data(tokenResponseInfo).build()
//        ), authenticationService.authenticate(userLoginDto));
//    }
//
//    @Test
//    void testAuthenticateWithNoPassword() {
//        String email = "admin@fpt.edu.vn";
//        String Password = "";
//
//        UserLoginDto userLoginDto = new UserLoginDto(email, Password);
//
//        Users userResult = new Users();
//        userResult.setEmail("admin@fpt.edu.vn");
//        userResult.setPassword("1234@1234");
//        userResult.setStatus("active");
//
//        Mockito.when(userRepository.findByEmail(email)).thenReturn(userResult);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfo(userLoginDto.getEmail(),
//                        userLoginDto.getPassword(), authenticationManager);
//
//        Mockito.when(jwtUtils.generateTokenResponseInfo(email, Password, authenticationManager))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.status(401).body(
//                ApiResponse.builder()
//                        .success(false)
//                        .message("Parameter invalid!").build()
//        ), authenticationService.authenticate(userLoginDto));
//    }
//
//    @Test
//    void testAuthenticateWithNoUsername() {
//        String email = "";
//        String Password = "1234@1234";
//
//        UserLoginDto userLoginDto = new UserLoginDto(email, Password);
//
//        Users userResult = new Users();
//        userResult.setEmail("admin@fpt.edu.vn");
//        userResult.setPassword("1234@1234");
//        userResult.setStatus("active");
//
//        Mockito.when(userRepository.findByEmail(email)).thenReturn(userResult);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfo(userLoginDto.getEmail(),
//                        userLoginDto.getPassword(), authenticationManager);
//
//        Mockito.when(jwtUtils.generateTokenResponseInfo(email, Password, authenticationManager))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.status(401).body(
//                ApiResponse.builder()
//                        .success(false)
//                        .message("Parameter invalid!").build()
//        ), authenticationService.authenticate(userLoginDto));
//    }
//
//    @Test
//    void testAuthenticateWithAccountInactive() {
//        String email = "demo1@fpt.edu.vn";
//        String Password = "1234@1234";
//
//        UserLoginDto userLoginDto = new UserLoginDto(email, Password);
//
//        Users userResult =new Users();
//        userResult.setEmail("demo1@fpt.edu.vn");
//        userResult.setPassword("1234@1234");
//        userResult.setStatus("inactive");
//
//        Mockito.when(userRepository.findByEmail(email)).thenReturn(userResult);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfo(userLoginDto.getEmail(),
//                        userLoginDto.getPassword(), authenticationManager);
//
//        Mockito.when(jwtUtils.generateTokenResponseInfo(email, Password, authenticationManager))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.status(401).body(
//                ApiResponse.builder()
//                        .success(false)
//                        .message("Account INACTIVE!").build()
//        ), authenticationService.authenticate(userLoginDto));
//    }
//
//    @Test
//    void testAuthenticateWithAccountNotEnabled() {
//        String email = "demo2@fpt.edu.vn";
//        String Password = "1234@1234";
//
//        UserLoginDto userLoginDto = new UserLoginDto(email, Password);
//
//        Users userResult = new Users();
//        userResult.setEmail("demo2@fpt.edu.vn");
//        userResult.setPassword("1234@1234");
//        userResult.setStatus("active");
//        userResult.setEnabled(false);
//
//        Mockito.when(userRepository.findByEmail(email)).thenReturn(userResult);
//
//        TokenResponseInfo tokenResponseInfo = jwtUtils.
//                generateTokenResponseInfo(userLoginDto.getEmail(),
//                        userLoginDto.getPassword(), authenticationManager);
//
//        Mockito.when(jwtUtils.generateTokenResponseInfo(email, Password, authenticationManager))
//                .thenReturn(tokenResponseInfo);
//
//        assertEquals(ResponseEntity.status(401).body(
//                ApiResponse.builder()
//                        .success(false)
//                        .message("Please verify your account!").build()
//        ), authenticationService.authenticate(userLoginDto));
//    }
//
//
//
//
//}