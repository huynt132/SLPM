//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class AddClassesServiceTest {
////    @MockBean
////    private ClassesRepository classesRepository;
////
////    @MockBean
////    private SubjectsRepository subjectsRepository;
////
////    @MockBean
////    private EntityManager entityManager;
////
////    @MockBean
////    private Validate validate;
////
////    @MockBean
////    private ModelMapper modelMapper;
////
////    @InjectMocks
////    ClassesServiceImpl classesService;
////
////    private ClassesDTO showDetail(Integer id) {
////        return classesRepository.getClassesDetail(id);
////    }
////
////    @Test
////    void addClassesSuccessfully() throws Exception {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, "codeTest", "1", "1",
////                "2022", "spring", "active", "0");
////        Subjects subjects = new Subjects();
////        Mockito.when(classesRepository.findByCode(classesInput.getCode())).thenReturn(null);
////        Mockito.when(subjectsRepository.getById(Integer.valueOf(classesInput.getSubjectId()))).thenReturn(subjects);
////        Classes classes = new Classes();
////        classes.setId(1);
////        Mockito.when(entityManager.find(Classes.class, 1)).thenReturn(classes);
////        Mockito.when(modelMapper.map(classesInput, Classes.class)).thenReturn(classes);
////        ClassesDTO classesDTO = new ClassesDTO(1, "codeTest", 1, "trainer", 1, "MAE", "Math level 1",
////                2022, "spring", "active", 0, null, null, null, null, null, null);
////        Mockito.when(showDetail(classes.getId())).thenReturn(classesDTO);
////        assertEquals(classesDTO, classesService.addClasses(classesInput));
////    }
////
////
////    @Test
////    void addClassesWithoutCode() throws Exception {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, null, "1", "1",
////                "2022", "spring", "active", "0");
////        Mockito.doThrow(new Exception("Classes code cannot be empty!")).when(validate).validateClasses(classesInput);
////        Throwable throwable = assertThrows(Throwable.class, () -> {
////            classesService.addClasses(classesInput);
////        });
////        assertEquals("Classes code cannot be empty!", throwable.getMessage());
////    }
////
////    @Test
////    void addClassesWithCodeWrong() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, "asj@@", "1", "1",
////                "2022", "spring", "active", "0");
////        try {
////            Mockito.doThrow(new Exception("Classes code must have 3-50 character and don't have special character!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.addClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Classes code must have 3-50 character and don't have special character!", e.getMessage());
////        }
////    }
////
////    @Test
////    void addClassesWithTrainerIdNull() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, "codeTest", null, "1",
////                "2022", "spring", "active", "0");
////        try {
////            Mockito.doThrow(
////                    new Exception("Trainer Id cannot be empty!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.addClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Trainer Id cannot be empty!", e.getMessage());
////        }
////    }
////
////    @Test
////    void addClassesWithTrainerIdNotExist() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, "codeTest", "999", "1",
////                "2022", "spring", "active", "0");
////        try {
////            Mockito.doThrow(
////                            new Exception("Trainer not found!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.addClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Trainer not found!", e.getMessage());
////        }
////    }
////
////    @Test
////    void addClassesWithSubjectIdNull() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, "codeTest", "999", null,
////                "2022", "spring", "active", "0");
////        try {
////            Mockito.doThrow(
////                            new Exception("Trainer not found!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.addClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Trainer not found!", e.getMessage());
////        }
////    }
////
////    @Test
////    void addClassesWithBlock5ClassWrongFormat() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, "codeTest", "999", "1",
////                "2022", "spring", "active", "4");
////        try {
////            Mockito.doThrow(
////                            new Exception("Block5 Class must be 0 or 1!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.addClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Block5 Class must be 0 or 1!", e.getMessage());
////        }
////    }
////    @Test
////    void addClassesWithStatusWrongFormat() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                null, "codeTest", "999", "1",
////                "2022", "spring", "open", "0");
////        try {
////            Mockito.doThrow(
////                            new Exception("Class status must be active, closed or cancelled!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.addClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Class status must be active, closed or cancelled!", e.getMessage());
////        }
////    }
//}