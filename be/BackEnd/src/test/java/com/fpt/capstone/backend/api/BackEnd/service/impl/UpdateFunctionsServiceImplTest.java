//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import com.fpt.capstone.backend.api.BackEnd.dto.FunctionsDTO;
//import com.fpt.capstone.backend.api.BackEnd.entity.Functions;
//import com.fpt.capstone.backend.api.BackEnd.repository.FunctionsRepository;
//import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.modelmapper.ModelMapper;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import javax.persistence.EntityManager;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class UpdateFunctionsServiceImplTest {
//
////    @MockBean
////    private EntityManager entityManager;
////
////    @MockBean
////    private ModelMapper modelMapper;
////    @MockBean
////    private FunctionsRepository functionsRepository;
////    @MockBean
////    private Validate validate;
////    @InjectMocks
////    FunctionsServiceImpl functionsService;
////
////    @Test
////    void updateFunctionSuccessfully() throws Exception {
////        FunctionsDTO functionsDTO = new FunctionsDTO();
////        functionsDTO.setId(1);
////        functionsDTO.setFeatureId(2);
////        functionsDTO.setComplexity("simple");
//////        functionsDTO.setOwnerId(3);
////        functionsDTO.setName("change pw new");
//////        functionsDTO.setAccessRoles("all");
////        functionsDTO.setDescription("description");
////        functionsDTO.setPriority(1);
////        functionsDTO.setStatus("planned");
////
////        Functions function = new Functions();
////        function.setId(1);
////        function.setFeatureId(2);
//////        function.setComplexity("simple");
//////        function.setOwnerId(3);
////        function.setName("change pw new");
//////        function.setAccessRoles("all");
////        function.setDescription("description");
////        function.setPriority(1);
////        function.setStatus("planned");
////
////        Mockito.when(
////                functionsRepository.searchByNameOnFeature
////                        (functionsDTO.getFeatureId(), functionsDTO.getName())
////        ).thenReturn(0);
////
////        Mockito.when(modelMapper.map(functionsDTO, Functions.class)).thenReturn(function);
////        Mockito.when(entityManager.find(Functions.class, function.getId())).thenReturn(function);
////        Mockito.when(functionsRepository.getById(functionsDTO.getId())).thenReturn(function);
////
////        functionsService.updateFunction(functionsDTO);
////        Mockito.verify(functionsRepository, Mockito.times(1)).save(function);
////
////    }
////
////    @Test
////    void UpdateFunctionNameEmpty() {
////        FunctionsDTO functionsDTO = new FunctionsDTO();
////        functionsDTO.setFeatureId(2);
////        functionsDTO.setComplexity("simple");
//////        functionsDTO.setOwnerId(3);
////////        functionsDTO.setName("change pw new");
//////        functionsDTO.setAccessRoles("all");
////        functionsDTO.setDescription("description");
////        functionsDTO.setPriority(1);
////        functionsDTO.setStatus("planned");
////
////        Functions function = new Functions();
////        function.setId(1);
////        function.setFeatureId(2);
//////        function.setComplexity("simple");
//////        function.setOwnerId(3);
////        function.setName("change pw new");
//////        function.setAccessRoles("all");
////        function.setDescription("description");
////        function.setPriority(1);
////        function.setStatus("planned");
////
////        Mockito.when(
////                functionsRepository.searchByNameOnFeature
////                        (functionsDTO.getFeatureId(), functionsDTO.getName())
////        ).thenReturn(0);
////
////        Mockito.when(modelMapper.map(functionsDTO, Functions.class)).thenReturn(function);
////        Mockito.when(entityManager.find(Functions.class, function.getId())).thenReturn(function);
////        Mockito.when(functionsRepository.getById(functionsDTO.getId())).thenReturn(function);
////        try {
////            Mockito.doThrow(new Exception("Function name cannot be empty!"))
////                    .when(validate).validateFunction(functionsDTO);
////            functionsService.updateFunction(functionsDTO);
////        } catch (Exception e) {
////            assertEquals("Function name cannot be empty!", e.getMessage());
////        }
////    }
////
////    @Test
////    void UpdateFunctionWithFeatureNotExist() {
////        FunctionsDTO functionsDTO = new FunctionsDTO();
////        functionsDTO.setFeatureId(999);
////        functionsDTO.setComplexity("simple");
//////        functionsDTO.setOwnerId(3);
////        functionsDTO.setName("change pw new");
//////        functionsDTO.setAccessRoles("all");
////        functionsDTO.setDescription("description");
////        functionsDTO.setPriority(1);
////        functionsDTO.setStatus("planned");
////
////        Functions function = new Functions();
////        function.setId(1);
////        function.setFeatureId(2);
//////        function.setComplexity("simple");
//////        function.setOwnerId(3);
////        function.setName("change pw new");
//////        function.setAccessRoles("all");
////        function.setDescription("description");
////        function.setPriority(1);
////        function.setStatus("planned");
////
////        Mockito.when(
////                functionsRepository.searchByNameOnFeature
////                        (functionsDTO.getFeatureId(), functionsDTO.getName())
////        ).thenReturn(0);
////
////        Mockito.when(modelMapper.map(functionsDTO, Functions.class)).thenReturn(function);
////        Mockito.when(entityManager.find(Functions.class, function.getId())).thenReturn(function);
////        Mockito.when(functionsRepository.getById(functionsDTO.getId())).thenReturn(function);
////        try {
////            Mockito.doThrow(new Exception("Feature not found!"))
////                    .when(validate).validateFunction(functionsDTO);
////            functionsService.updateFunction(functionsDTO);
////        } catch (Exception e) {
////            assertEquals("Feature not found!", e.getMessage());
////        }
////
////    }
////
////    @Test
////    void UpdateFunctionStatusWrong() {
////        FunctionsDTO functionsDTO = new FunctionsDTO();
////        functionsDTO.setFeatureId(2);
////        functionsDTO.setComplexity("simple");
//////        functionsDTO.setOwnerId(3);
////        functionsDTO.setName("change pw new");
//////        functionsDTO.setAccessRoles("all");
////        functionsDTO.setDescription("description");
////        functionsDTO.setPriority(1);
////        functionsDTO.setStatus("Status");
////
////        Functions function = new Functions();
////        function.setId(1);
////        function.setFeatureId(2);
//////        function.setComplexity("simple");
//////        function.setOwnerId(3);
////        function.setName("change pw new");
//////        function.setAccessRoles("all");
////        function.setDescription("description");
////        function.setPriority(1);
////        function.setStatus("planned");
////
////        Mockito.when(
////                functionsRepository.searchByNameOnFeature
////                        (functionsDTO.getFeatureId(), functionsDTO.getName())
////        ).thenReturn(0);
////
////        Mockito.when(modelMapper.map(functionsDTO, Functions.class)).thenReturn(function);
////        Mockito.when(entityManager.find(Functions.class, function.getId())).thenReturn(function);
////        Mockito.when(functionsRepository.getById(functionsDTO.getId())).thenReturn(function);
////        try {
////            Mockito.doThrow(new Exception("Status must be pending/planned/evaluated/rejected/done only!"))
////                    .when(validate).validateFunction(functionsDTO);
////            functionsService.updateFunction(functionsDTO);
////        } catch (Exception e) {
////            assertEquals("Status must be pending/planned/evaluated/rejected/done only!", e.getMessage());
////        }
////
////    }
////
////    @Test
////    void UpdateFunctionComplexWrong() {
////        FunctionsDTO functionsDTO = new FunctionsDTO();
////        functionsDTO.setFeatureId(2);
////        functionsDTO.setComplexity("Complex");
//////        functionsDTO.setOwnerId(3);
////        functionsDTO.setName("change pw new");
//////        functionsDTO.setAccessRoles("all");
////        functionsDTO.setDescription("description");
////        functionsDTO.setPriority(1);
////        functionsDTO.setStatus("planned");
////
////        Functions function = new Functions();
////        function.setId(1);
////        function.setFeatureId(2);
//////        function.setComplexity("simple");
//////        function.setOwnerId(3);
////        function.setName("change pw new");
//////        function.setAccessRoles("all");
////        function.setDescription("description");
////        function.setPriority(1);
////        function.setStatus("planned");
////
////        Mockito.when(
////                functionsRepository.searchByNameOnFeature
////                        (functionsDTO.getFeatureId(), functionsDTO.getName())
////        ).thenReturn(0);
////
////        Mockito.when(modelMapper.map(functionsDTO, Functions.class)).thenReturn(function);
////        Mockito.when(entityManager.find(Functions.class, function.getId())).thenReturn(function);
////        Mockito.when(functionsRepository.getById(functionsDTO.getId())).thenReturn(function);
////        try {
////            Mockito.doThrow(new Exception("Complex must be complex/medium/simple only!"))
////                    .when(validate).validateFunction(functionsDTO);
////            functionsService.updateFunction(functionsDTO);
////        } catch (Exception e) {
////            assertEquals("Complex must be complex/medium/simple only!", e.getMessage());
////        }
////
////    }
////
////
////    @Test
////    void UpdateFunctionOwnerIdNotExist() {
////        FunctionsDTO functionsDTO = new FunctionsDTO();
////        functionsDTO.setFeatureId(2);
////        functionsDTO.setComplexity("simple");
//////        functionsDTO.setOwnerId(999);
////        functionsDTO.setName("change pw new");
//////        functionsDTO.setAccessRoles("all");
////        functionsDTO.setDescription("description");
////        functionsDTO.setPriority(1);
////        functionsDTO.setStatus("planned");
////
////        Functions function = new Functions();
////        function.setId(1);
////        function.setFeatureId(2);
//////        function.setComplexity("simple");
//////        function.setOwnerId(3);
////        function.setName("change pw new");
//////        function.setAccessRoles("all");
////        function.setDescription("description");
////        function.setPriority(1);
////        function.setStatus("planned");
////
////        Mockito.when(
////                functionsRepository.searchByNameOnFeature
////                        (functionsDTO.getFeatureId(), functionsDTO.getName())
////        ).thenReturn(0);
////
////        Mockito.when(modelMapper.map(functionsDTO, Functions.class)).thenReturn(function);
////        Mockito.when(entityManager.find(Functions.class, function.getId())).thenReturn(function);
////        Mockito.when(functionsRepository.getById(functionsDTO.getId())).thenReturn(function);
////        try {
////            Mockito.doThrow(new Exception("User not found!"))
////                    .when(validate).validateFunction(functionsDTO);
////            functionsService.updateFunction(functionsDTO);
////        } catch (Exception e) {
////            assertEquals("User not found!", e.getMessage());
////        }
////
////    }
//}