//package com.fpt.capstone.backend.api.BackEnd.service.impl;
//
//import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesDTO;
//import com.fpt.capstone.backend.api.BackEnd.dto.classes.ClassesInputDTO;
//import com.fpt.capstone.backend.api.BackEnd.entity.Classes;
//import com.fpt.capstone.backend.api.BackEnd.entity.Subjects;
//import com.fpt.capstone.backend.api.BackEnd.repository.ClassesRepository;
//import com.fpt.capstone.backend.api.BackEnd.repository.SubjectsRepository;
//import com.fpt.capstone.backend.api.BackEnd.service.validate.Validate;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.modelmapper.ModelMapper;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import javax.persistence.EntityManager;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//
//@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
//class UpdateClassesServiceTest {
////    @MockBean
////    private ClassesRepository classesRepository;
////
////    @MockBean
////    private SubjectsRepository subjectsRepository;
////
////    @MockBean
////    private EntityManager entityManager;
////
////    @MockBean
////    private Validate validate;
////
////    @MockBean
////    private ModelMapper modelMapper;
////
////    @InjectMocks
////    ClassesServiceImpl classesService;
////
////    private ClassesDTO showDetail(Integer id) {
////        return classesRepository.getClassesDetail(id);
////    }
////
////    @Test
////    void updateClassesSuccessfully() throws Exception {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", "codeTest", "1", "1",
////                "2022", "spring", "active", "0");
////        Subjects subjects = new Subjects();
////        Mockito.when(classesRepository.findByCode(classesInput.getCode())).thenReturn(null);
////        Mockito.when(subjectsRepository.getById(Integer.valueOf(classesInput.getSubjectId()))).thenReturn(subjects);
////        Classes classes = new Classes();
////        classes.setId(1);
////        classes.setCode("codeTest");
////        Mockito.when(entityManager.find(Classes.class, 1)).thenReturn(classes);
////        Mockito.when(modelMapper.map(classesInput, Classes.class)).thenReturn(classes);
////        Mockito.when(classesRepository.getOne(Integer.valueOf(classesInput.getId()))).thenReturn(classes);
////        ClassesDTO classesDTO = new ClassesDTO(1, "codeTest", 1, "trainer", 1, "MAE", "Math level 1"
////                ,null,null, 2022,"spring", "active", 0, null, null, null, null, null, null);
////        Mockito.when(showDetail(classes.getId())).thenReturn(classesDTO);
////        assertEquals(classesDTO, classesService.updateClasses(classesInput));
////    }
////
////
////    @Test
////    void updateClassesWithoutCode() throws Exception {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", null, "1", "1",
////                "2022", "spring", "active", "0");
////        Mockito.when(
////                classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId()))
////        ).thenReturn(new ClassesDTO());
////        Mockito.doThrow(new Exception("Classes code cannot be empty!")).when(validate).validateClasses(classesInput);
////        Throwable throwable = assertThrows(Throwable.class, () -> {
////            classesService.updateClasses(classesInput);
////        });
////        assertEquals("Classes code cannot be empty!", throwable.getMessage());
////    }
////
////    @Test
////    void updateClassesWithCodeWrong() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", "asj@@", "1", "1",
////                "2022", "spring", "active", "0");
////        Mockito.when(
////                classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId()))
////        ).thenReturn(new ClassesDTO());
////        try {
////            Mockito.doThrow(new Exception("Classes code must have 3-50 character and don't have special character!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.updateClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Classes code must have 3-50 character and don't have special character!", e.getMessage());
////        }
////    }
////
////    @Test
////    void updateClassesWithTrainerIdNull() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", "codeTest", null, "1",
////                "2022", "spring", "active", "0");
////        Mockito.when(
////                classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId()))
////        ).thenReturn(new ClassesDTO());
////        try {
////            Mockito.doThrow(
////                    new Exception("Trainer Id cannot be empty!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.updateClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Trainer Id cannot be empty!", e.getMessage());
////        }
////    }
////
////    @Test
////    void updateClassesWithTrainerIdNotExist() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", "codeTest", "999", "1",
////                "2022", "spring", "active", "0");
////        Mockito.when(
////                classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId()))
////        ).thenReturn(new ClassesDTO());
////        try {
////            Mockito.doThrow(
////                            new Exception("Trainer not found!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.updateClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Trainer not found!", e.getMessage());
////        }
////    }
////
////    @Test
////    void updateClassesWithSubjectIdNull() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", "codeTest", "999", null,
////                "2022", "spring", "active", "0");
////        Mockito.when(
////                classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId()))
////        ).thenReturn(new ClassesDTO());
////        try {
////            Mockito.doThrow(
////                            new Exception("Trainer not found!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.updateClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Trainer not found!", e.getMessage());
////        }
////    }
////
////    @Test
////    void updateClassesWithBlock5ClassWrongFormat() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", "codeTest", "999", "1",
////                "2022", "spring", "active", "4");
////        Mockito.when(
////                classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId()))
////        ).thenReturn(new ClassesDTO());
////        try {
////            Mockito.doThrow(
////                            new Exception("Block5 Class must be 0 or 1!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.updateClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Block5 Class must be 0 or 1!", e.getMessage());
////        }
////    }
////    @Test
////    void updateClassesWithStatusWrongFormat() {
////        ClassesInputDTO classesInput = new ClassesInputDTO(
////                "1", "codeTest", "999", "1",
////                "2022", "spring", "open", "0");
////        Mockito.when(
////                classesRepository.getClassesDetail(Integer.valueOf(classesInput.getId()))
////        ).thenReturn(new ClassesDTO());
////        try {
////            Mockito.doThrow(
////                            new Exception("Class status must be active, closed or cancelled!"))
////                    .when(validate).validateClasses(classesInput);
////            classesService.updateClasses(classesInput);
////        } catch (Exception e) {
////            assertEquals("Class status must be active, closed or cancelled!", e.getMessage());
////        }
////    }
//}