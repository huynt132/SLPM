# SWP490_G2

## How to run this project?

1. Requirement:
  - Any computer run Ubuntu operating system.
  - Connected to internet.
  - Ram size greater than equal to than 4GB.
  - Storage size greater than equal to 4GB.

2. Installation:
- Install docker engine on ubuntu.
- Run this following command.

```shell
cd BackEnd/
docker run -p 8000:9090 slpm_spring
```

## Database scheme folder:
- You can find all database schema this folder:
```shell
cd BackEnd/src/main/resources/ddl/
```