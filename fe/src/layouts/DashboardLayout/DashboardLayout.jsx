// import React from 'react';
import React from 'react';
import { Layout } from 'antd';
import Headers from 'src/components/Header/Header';
import styles from './DashboardLayout.module.css';
import Sidebar from 'src/components/Sidebar/Sidebar';
import PropTypes from 'prop-types';

const { Header, Content, Sider } = Layout;

const DashboardLayout = ({ children }) => (
  <Layout style={{ backgroundColor: '#F5F6FD' }}>
    <Sider className={styles.sidebar} width="300">
      <Sidebar />
    </Sider>
    <Layout>
      <Header className={styles.header}>
        <Headers />
      </Header>
      <Layout
        style={{
          padding: ' 24px',
          backgroundColor: '#F5F6FD'
        }}
      >
        <Content
          className={styles.siteLayoutBackground}
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
            borderRadius: '15px'
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  </Layout>
);

export default DashboardLayout;

DashboardLayout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};
