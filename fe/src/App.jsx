import React from 'react';
import 'normalize.css';
import 'src/assets/styles/global.scss';
import 'src/assets/styles/override.scss';
import RoutesIdentify from './Routes';
import { useSelector } from 'react-redux';
import LocalStorage from './constants/localStorage';
import { Route, Routes } from 'react-router-dom';
import { path } from './constants/path';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import Login from './pages/Auth/Login/Login';
import Home from './pages/Home/Home';
import UnAuthenticatedGuards from './guards/UnAuthenticatedGuards';
import Register from './pages/Auth/Register/Register';
import FormLayout from './layouts/FormLayout/FormLayout';
import ForgotPassword from './pages/Auth/ForgotPassword/ResetPassword';
import VerifyAccount from './pages/Auth/VerifyAccout/VerifyAccount';
import ErrorForbidden from './pages/ErrorForbidden/ErrorForbidden';
import ErrorServer from './pages/ErrorServer/ErrorServer';
import NotFound from './pages/NotFound/NotFound';

function App() {
  // useEffect(() => {
  //   logoutAllTab();
  // }, []);

  const token = useSelector(state => state.auth.token);

  if (!token && !LocalStorage.accessToken) {
    return (
      <div className="App">
        <Routes>
          <Route
            path={path.login}
            element={
              <ErrorBoundary>
                <Login />
              </ErrorBoundary>
            }
          />
        </Routes>
      </div>
    );
  }

  return (
    <div className="App">
      {/* {authenticated ? (
        <RoutesIdentify />
      ) : ( */}
      <Routes>
        <Route path={path.home} element={<Home />} exact />
        <Route
          path={path.login}
          element={
            <UnAuthenticatedGuards>
              <ErrorBoundary>
                <Login />
              </ErrorBoundary>
            </UnAuthenticatedGuards>
          }
        />
        <Route
          path={path.register}
          element={
            <UnAuthenticatedGuards>
              <ErrorBoundary>
                <Register />
              </ErrorBoundary>
            </UnAuthenticatedGuards>
          }
        />
        <Route
          path={path.resetPassword}
          element={
            <UnAuthenticatedGuards>
              <FormLayout title={'Recover Account'} style={{ height: '380px' }}>
                <ErrorBoundary>
                  <ForgotPassword />
                </ErrorBoundary>
              </FormLayout>
            </UnAuthenticatedGuards>
          }
        />
        <Route path={path.verifyAccount} element={<VerifyAccount />} />
        <Route path={path.errorForbidden} element={<ErrorForbidden />} />
        <Route path={path.errorServer} element={<ErrorServer />} />
        <Route path={path.notFound} element={<NotFound />} />
        <Route path={path.dashboard} element={<RoutesIdentify />} />
      </Routes>
      {/* )} */}
    </div>
  );
}

export default App;
