import { notification } from 'antd';
import { SmileOutlined } from '@ant-design/icons';
import { RobotOutlined } from '@ant-design/icons';
import React from 'react';

const getNotificationStyle = type => {
  return {
    success: {
      style: {
        color: 'rgba(0, 0, 0, 0.65)',
        border: '1px solid #b7eb8f',
        backgroundColor: '#f6ffed'
      },
      icon: <SmileOutlined style={{ color: '#108ee9' }} />
    },
    fail: {
      style: {
        color: 'rgba(0, 0, 0, 0.65)',
        border: '1px solid #ffe58f',
        backgroundColor: '#fffbe6'
      },
      icon: <SmileOutlined style={{ color: '#108ee9' }} />
    },
    error: {
      style: {
        color: 'rgba(0, 0, 0, 0.65)',
        border: '1px solid #ffa39e',
        backgroundColor: '#fff1f0'
      },
      icon: <RobotOutlined style={{ color: '#fff1f0' }} />
    },
    info: {
      style: {
        color: 'rgba(0, 0, 0, 0.65)',
        border: '1px solid #91d5ff',
        backgroundColor: '#e6f7ff'
      },
      icon: <SmileOutlined style={{ color: '#108ee9' }} />
    }
  }[type];
};

export const showMessage = (message, type = 'success', duration = 2) => {
  const args = {
    message: message,
    duration: duration,
    style: getNotificationStyle(type).style,
    icon: getNotificationStyle(type).icon
  };

  notification.open(args);
};
