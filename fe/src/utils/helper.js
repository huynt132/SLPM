import moment from 'moment';
import LocalStorage from 'src/constants/localStorage';

/* eslint-disable no-useless-escape */
export const isEmail = value => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);

export const payloadCreator = asyncFunc => async (arg, thunkApi) => {
  try {
    const res = await asyncFunc(arg);
    return res;
  } catch (error) {
    return thunkApi.rejectWithValue(error);
  }
};

/**
 * Check valid json string
 *
 * @param str
 * @returns {boolean}
 */
export const isJsonString = str => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
};

/**
 * Check is empty
 *
 * @param value
 * @returns {boolean}
 */
export const isEmpty = value => {
  if (value instanceof Date || value instanceof File) return;
  return !value || (typeof value === 'string' && !value.trim()) || (Array.isArray(value) && value.length === 0);
};

export const upperCaseEachFirstLetter = sentence => {
  if (sentence)
    return sentence
      .split(' ')
      .map(w => w.toLowerCase().charAt(0).toUpperCase() + w.toLowerCase().slice(1))
      .join(' ');
};

export const formatDate = (datetime, format = 'DD-MM-YYYY') => {
  if (datetime) {
    return moment(datetime).format(format);
  }
  return null;
};

export const openInNewTab = url => {
  const win = window.open(url, '_blank');
  win.focus();
};

// export const logout = () => {
//   localStorage.removeItem(LocalStorage.accessToken);
//   localStorage.removeItem(LocalStorage.role);
//   localStorage.removeItem(LocalStorage.user);
//   // window.location.href = window.location.origin + '/login';
// };

// export const logoutAllTab = () => {
//   logout();
// };
