import http from 'src/utils/http';

const evaluationManagementApi = {
  getLocEvaluations: params => http.get('locEvaluations/getList', { params: params }),
  getStudentEvaluation: params => http.get('locEvaluations/getStudentEvaluation', { params: params }),
  updateBonus: data => http.post('evaluation/updateBonus', data)
};

export default evaluationManagementApi;
