import http from 'src/utils/http';

const classEvalApi = {
  getClassEvaluation(config) {
    return http.get('evaluation/classEvaluation', config);
  },
  exportClassEvaluation(config) {
    return http.get('evaluation/classEvaluation/export', config);
  }
};

export default classEvalApi;
