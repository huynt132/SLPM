import http from 'src/utils/http';

const milestoneManagementApi = {
  getMilestones: params => http.get('milestones/getAll', { params: params }),
  getAllClasses: params => http.get('classes/getList', { params: params }),
  getAllMilestones: params => http.get('/milestones/getList', { params: params }),
  editMilestone: data => http.post('milestones/edit', data),
  syncMilestone: data => http.post('project/milestones', data),
  addMilestone: data => http.post('milestones/add', data)
};

export default milestoneManagementApi;
