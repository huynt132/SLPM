import http from 'src/utils/http';
const dashboardApi = {
  getFunctionCount: params => http.get('submits/getFunctionCount', { params: params }),
  getFunctionStatusCount: params => http.get('submits/getFunctionStatusCount', { params: params }),
  getClassStatistic: params => http.get('dashboard/getClassStatistic', { params: params }),
  getTeamStatistic: params => http.get('dashboard/getTeamStatistic', { params: params })
};

export default dashboardApi;
