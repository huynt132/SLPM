import http from 'src/utils/http';
const submitManagementApi = {
  getMilestoneSubmits: params => http.get('submits/getMilestoneSubmits', { params: params }),
  getFunctionEvaluate: params => http.get('submits/getFunctionEvaluate', { params: params }),
  getNewLocFunction: params => http.get('submits/getNewLocFunction', { params: params }),
  getEvaluateOptions: params => http.get('classSetting/getOptions', { params: params }),
  getFunctionCount: params => http.get('submits/getFunctionCount', { params: params }),
  editTeamEval: data => http.post('submits/editTeamEval', data),
  editIndividualEvaluation: data => http.post('submits/editIndividualEvaluation', data),
  updateStatus: data => http.post('submits/updateStatus', data),
  locEvaluate: data => http.post('submits/locEvaluate', data)
  // getAllClasses: params => http.get('classes/getList', { params: params }),
  // getAllMilestones: params => http.get('/submits/getList', { params: params }),
  // syncMilestone: data => http.post('project/submits', data),
  // addMilestone: data => http.post('submits/add', data)
};

export default submitManagementApi;
