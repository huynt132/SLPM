import http from 'src/utils/http';

const trackingManagementApi = {
  getTracking: params => http.get('tracking/getAll', { params: params }),
  getTrackingDetail: params => http.get('tracking/getTrackingDetail', { params: params }),
  getFunctionTracking: params => http.get('function/getFunctionTracking', { params: params }),
  editTracking: data => http.post('tracking/edit', data),
  addTracking: data => http.post('tracking/add', data),
  submitTracking: data => http.post('tracking/submitFunction', data),
  getListUpdateTracking: params => http.get('tracking/getListUpdate', params)
};

export default trackingManagementApi;
