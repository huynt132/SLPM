import http from 'src/utils/http';

const usersApi = {
  getAllTrainer(config) {
    return http.get('users/getAllTrainer', config);
  },
  getAllStudentActive(config) {
    return http.get('users/getUserList', config);
  },
  getAllUser(config) {
    return http.get('users/getAll', config);
  },
  editUser(data) {
    return http.post('users/edit', data);
  },
  editAvatarUser(data) {
    return http.post('users/imageUpload', data);
  },
  getUserDetail(config) {
    return http.get('users/getUserDetail', config);
  }
};

export default usersApi;
