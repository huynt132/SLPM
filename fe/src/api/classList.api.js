import http from 'src/utils/http';

const classListApi = {
  getAll(config) {
    return http.get('classes/getAll', config);
  },
  getListClass(config) {
    return http.get('classes/getList', config);
  },
  editClass(data) {
    return http.post('classes/edit', data);
  },
  addClass(data) {
    return http.post('classes/add', data);
  }
};

export default classListApi;
