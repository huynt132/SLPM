import http from 'src/utils/http';

const projectManagementApi = {
  getProjects: params => http.get('project/getAll', { params: params }),
  getAllProjects: params => http.get('project/getList', { params: params }),
  editProject: data => http.post('project/edit', data),
  addProject: data => http.post('project/add', data)
};

export default projectManagementApi;
