import http from 'src/utils/http';

const iterationEvalApi = {
  getIterationEvaluation(config) {
    return http.get('evaluation/iterationEvaluation', config);
  }
};

export default iterationEvalApi;
