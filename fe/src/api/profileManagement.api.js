import http from 'src/utils/http';

const profileManagement = {
  // getIterations: params => http.get('iterations/getAll', { params: params }),
  editProfile: data => http.post('users/edit', data),
  // addIteration: data => http.post('iterations/add', data),
  changePassword: data => http.post('/changePassword', data)
};

export default profileManagement;
