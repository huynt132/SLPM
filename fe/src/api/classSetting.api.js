import http from 'src/utils/http';

const classSettingApi = {
  getListSettingClass(config) {
    return http.get('classSetting/getAll', config);
  },
  getListSettingType(config) {
    return http.get('classSetting/getListSetting', config);
  },
  addClassSetting(data) {
    return http.post('classSetting/add', data);
  },
  editClassSetting(data) {
    return http.post('classSetting/edit', data);
  }
};

export default classSettingApi;
