import http from 'src/utils/http';

const evaluationCriteriaManagementApi = {
  getCriteria: params => http.get('criteria/getAll', { params: params }),
  editCriteria: data => http.post('criteria/edit', data),
  addCriteria: data => http.post('criteria/add', data)
};

export default evaluationCriteriaManagementApi;
