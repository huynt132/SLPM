import http from 'src/utils/http';

const subjectManagementApi = {
  getAllSubject(config) {
    return http.get('subject/getAll', config);
  },
  getAllAuthor(config) {
    return http.get('users/getAllAuthor', config);
  },
  getSubjectWithCondition(config) {
    return http.get('subject/search', config);
  },
  editSubject(data) {
    return http.post('subject/edit', data);
  },
  addSubject(data) {
    return http.post('subject/add', data);
  },
  getSubjectSetting: params => http.get('subject/getSubjectSetting', { params: params })
};

export default subjectManagementApi;
