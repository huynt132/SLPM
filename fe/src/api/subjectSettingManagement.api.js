import http from 'src/utils/http';

const subjectSettingManagementApi = {
  getSubjectSettings: params => http.get('subjectSettings/getAll', { params: params }),
  getSubjectSettingTypes: params => http.get('subjectSettings/getType', { params: params }),
  editSubjectSetting: data => http.post('subjectSettings/edit', data),
  addSubjectSetting: data => http.post('subjectSettings/add', data)
};

export default subjectSettingManagementApi;
