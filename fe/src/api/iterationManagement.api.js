import http from 'src/utils/http';

const iterationManagementApi = {
  getIterations: params => http.get('iterations/getAll', { params: params }),
  getAllIteration: params => http.get('iterations/getIterations', { params: params }),
  editIteration: data => http.post('iterations/edit', data),
  addIteration: data => http.post('iterations/add', data)
};

export default iterationManagementApi;
