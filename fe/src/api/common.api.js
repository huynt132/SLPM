import http from 'src/utils/http';

const commonApi = {
  getColumnType: params => http.get('common/getColumnType', { params: params }),
  getAllUsers: params => http.get('users/getUser', { params: params })
};

export default commonApi;
