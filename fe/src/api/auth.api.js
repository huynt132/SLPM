import http from 'src/utils/http';

const authApi = {
  register(data) {
    return http.post('register', data);
  },
  login(data) {
    return http.post('login', data);
  },
  loginWithGoogle(data) {
    return http.post('login-google', data);
  },
  verifyAccount(data, config) {
    return http.post('verify-email', data, config);
  },
  recoverPassword(data) {
    return http.post('forgotPassWord', data);
  }
};

export default authApi;
