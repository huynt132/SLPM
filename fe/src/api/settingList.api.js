import http from 'src/utils/http';

const settingListApi = {
  getAll(config) {
    return http.get('setting/getSettingByType', config);
  },
  getSettingType(config) {
    return http.get('setting/getType', config);
  },
  editSetting(data) {
    return http.post('setting/edit', data);
  },
  addSetting(data) {
    return http.post('setting/add', data);
  },
  getRole(data) {
    return http.get('setting/getRole', data);
  }
};

export default settingListApi;
