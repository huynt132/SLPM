import http from 'src/utils/http';

const classUserApi = {
  getAll(config) {
    return http.get('classUser/getAll', config);
  },
  editClassUser(data) {
    return http.post('classUser/edit', data);
  },
  addClassUser(data) {
    return http.post('classUser/add', data);
  },
  getProjectCodeAndName(config) {
    return http.get('project/getList', config);
  },
  importClassUser(data) {
    return http.post('classUser/import', data);
  }
};

export default classUserApi;
