import http from 'src/utils/http';

const featureManagementApi = {
  getAllFeature(config) {
    return http.get('feature/getAll', config);
  },
  getList(config) {
    return http.get('feature/getList', config);
  },
  getAllProject(config) {
    return http.get('project/getAll', config);
  },
  editFeature(data) {
    return http.post('feature/edit', data);
  },
  addFeature(data) {
    return http.post('feature/add', data);
  }
};

export default featureManagementApi;
