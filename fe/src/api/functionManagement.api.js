import http from 'src/utils/http';
import { getAllFunctionType } from '../pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';

const functionManagementApi = {
  getFunctions: params => http.get('function/getAll', { params: params }),
  getAllFeatures: params => http.get('feature/getList', { params: params }),
  getProjectLabel: params => http.get('function/getProjectLabel', { params: params }),
  getAllFunctionStatus: params => http.get('function/getListStatusFunction', { params: params }),
  getAllFunctionType: params => http.get('function/getListTypeFunction', { params: params }),
  getAllFunctions: params => http.get('function/getList', { params: params }),
  editFunction: data => http.post('function/edit', data),
  addFunction: data => http.post('function/add', data),
  importFunctions: data => http.post('function/import', data),
  getListFunction: params => http.get('function/getListFunction', { params: params }),
  addLogFunction: data => http.post('function/addLog', data),
  updateLogFunction: data => http.post('function/updateLog', data)
};

export default functionManagementApi;
