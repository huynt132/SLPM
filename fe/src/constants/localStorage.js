export const LocalStorage = {
  user: 'user',
  accessToken: 'accessToken',
  role: 'role'
};

export default LocalStorage;
