export const path = {
  home: '/',
  login: '/login',
  register: '/register',
  resetPassword: '/resetPassword',
  dashboard: 'adm/*',

  //setting
  settingList: 'settingList',
  classSetting: 'classSetting',
  subjectSetting: 'subjectSetting',

  //Common
  classDashboard: 'classDashboard',
  classDash: '/adm/classDashboard',
  projectDashboard: 'projectDashboard',
  projectDash: '/adm/projectDashboard',
  profile: 'profile',
  profileDash: '/adm/profile',

  //Admin management
  userManagement: 'userManagement',
  subjectManagement: 'subjectManagement',

  //Author management
  iteration: 'iteration',
  classList: 'class',
  evaluationCriteriaManagement: 'evaluationCriteria',

  //Trainer management
  milestone: 'milestone',
  projectManagement: 'project',
  classUser: 'classStudent',
  classEvaluationManagement: 'classEvaluation',
  classEvaluationDashboard: '/adm/classEvaluation',
  iterationEvaluationManagement: 'iterationEvaluation',
  iterationEvaluationDashboard: '/adm/iterationEvaluation',
  studentEvaluation: 'evaluation',
  studentEvaluationAdm: '/adm/evaluation',

  //Student management
  milestoneSubmit: 'milestoneSubmit',
  featureManagement: 'feature',
  functionManagement: 'function',
  trackingManagement: 'tracking',

  //error
  errorForbidden: '/errorForbidden',
  errorServer: '/errorServer',
  error: '/error',
  verifyAccount: '/verify-email',
  notFound: '*',

  // absolute path
  absClass: '/adm/class',
  absClassStudent: '/adm/classStudent',
  absUser: '/adm/userManagement',
  absFunction: '/adm/function',
  absProject: '/adm/project',
  absStudentEval: '/adm/evaluation',
  absTracking: '/adm/tracking',
  absClassSetting: '/adm/classSetting',
  absListSetting: '/adm/settingList',
  absSubjectSetting: '/adm/subjectSetting',
  absEvalCriteria: '/adm/evaluationCriteria',
  absIteration: '/adm/iteration',
  absSubject: '/adm/subjectManagement'
};
