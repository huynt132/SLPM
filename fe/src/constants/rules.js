import { isEmail } from 'src/utils/helper';

export const rules = {
  rollNumber: {
    minLength: {
      value: 8,
      message: 'Roll Number must have only 8 characters'
    },
    maxLength: {
      value: 8,
      message: 'Roll Number must have only 8 characters'
    }
  },
  text: {
    required: {
      value: true,
      message: 'This field is required'
    }
  },
  username: {
    required: {
      value: true,
      message: 'This field is required'
    }
  },
  email: {
    required: {
      value: true,
      message: 'This field is required'
    },
    minLength: {
      value: 5,
      message: 'Email must has length from 5 - 160 characters'
    },
    maxLength: {
      value: 160,
      message: 'Email must has length from 5 - 160 characters'
    },
    validate: {
      email: val => isEmail(val) || 'Email is incorrect'
    }
  },
  password: {
    required: {
      value: true,
      message: 'This field is required'
    },
    minLength: {
      value: 8,
      message: 'Password must has length from 8 - 20 characters'
    },
    maxLength: {
      value: 20,
      message: 'Password must has length from 8 - 20 characters'
    }
  },
  confirmedPassword: {
    required: {
      value: true,
      message: 'This field is required'
    },
    minLength: {
      value: 8,
      message: 'Confirm password must has length from 8 - 20 characters'
    },
    maxLength: {
      value: 20,
      message: 'Confirm password must has length from 8 - 20 characters'
    }
  },
  integer: {
    pattern: {
      value: '^(0|[1-9][0-9]*)$',
      message: 'This field require number only!'
    }
  }
};
