import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import authReducer from './pages/Auth/auth.slice';
import settingListReducer from './pages/Dashboard/SettingDashboard/SettingListDashboard/settingList.slice';
import subjectReducer from './pages/Dashboard/SubjectDashboard/SubjectManagement/subjectManagement.slice';
import subjectSettingManagementReducer from './pages/Dashboard/SettingDashboard/SubjectSettingManagement/SubjectSettingManagement.slice';
import profileManagementReducer from './pages/Profile/ProfileManagement/Profile.slice';
import classListReducer from './pages/Dashboard/ClassDashboard/ClassListDashboard/classList.slice';
import classUserListReducer from './pages/Dashboard/ClassDashboard/ClassUserListDashboard/classUser.slice';
import featureReducer from './pages/Dashboard/ProjectDashboard/FeatureManagement/featureManagement.slice';
import userListReducer from './pages/Dashboard/ClassDashboard/UserDashboard/userList.slice';
import functionManagementReducer from './pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';
import iterationEvaluationReducer from './pages/Dashboard/ProjectDashboard/IterationEvaluationManagement/iterationEvaluation.slice';
import classEvaluationReducer from './pages/Dashboard/ProjectDashboard/ClassEvaluationManagement/classEvaluation.slice';
import updateTrackingManagementReducer from './pages/Dashboard/ProjectDashboard/components/ModalUpdateTracking/updateTracking.slice';
import classSettingListReducer from './pages/Dashboard/SettingDashboard/ClassSettingDashboard/classSetting.slice';

const rootReducer = {
  auth: authReducer,
  settingList: settingListReducer,
  subjectManagement: subjectReducer,
  profileManagement: profileManagementReducer,
  subjectSettingManagement: subjectSettingManagementReducer,
  classListManagement: classListReducer,
  classUserListManagement: classUserListReducer,
  featureManagement: featureReducer,
  userListManagement: userListReducer,
  functionManagement: functionManagementReducer,
  iterationEvaluation: iterationEvaluationReducer,
  classEvaluation: classEvaluationReducer,
  updateTracking: updateTrackingManagementReducer,
  classSetting: classSettingListReducer
};

const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV === 'development',
  middleware: [...getDefaultMiddleware({ serializableCheck: false })]
});

export default store;
