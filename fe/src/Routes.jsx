import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { path } from './constants/path';
import FormLayout from './layouts/FormLayout/FormLayout';
import ForgotPassword from './pages/Auth/ForgotPassword/ResetPassword';
import Login from './pages/Auth/Login/Login';
import Register from './pages/Auth/Register/Register';
import Home from './pages/Home/Home';
import DashboardLayout from 'src/layouts/DashboardLayout/DashboardLayout';
import DashboardClass from './pages/Dashboard/HomeDashboard/DashboardClass';
import DashboardTeam from './pages/Dashboard/HomeDashboard/DashboardTeam';
import SettingListDashboard from './pages/Dashboard/SettingDashboard/SettingListDashboard/SettingListDashboard';
import UnAuthenticatedGuards from './guards/UnAuthenticatedGuards';
import AuthenticatedGuards from './guards/AuthenticatedGuards';
import NotFound from './pages/NotFound/NotFound';
import IterationManagement from './pages/Dashboard/SubjectDashboard/IterationManagement/IterationManagement';
import Profile from './pages/Profile/Profile';
import Authorization from './components/Authorization/Authorization';
import ErrorForbidden from './pages/ErrorForbidden/ErrorForbidden';
import ErrorServer from './pages/ErrorServer/ErrorServer';
import SubjectManagement from './pages/Dashboard/SubjectDashboard/SubjectManagement/SubjectManagement';
import SubjectSettingManagement from './pages/Dashboard/SettingDashboard/SubjectSettingManagement/SubjectSettingManagement';
import VerifyAccount from './pages/Auth/VerifyAccout/VerifyAccount';
import EvaluationCriteriaManagement from './pages/Dashboard/SubjectDashboard/EvaluationCriteriaManagement/EvaluationCriteriaManagement';
import ClassListDashboard from './pages/Dashboard/ClassDashboard/ClassListDashboard/ClassListDashboard';
import ClassUserListDashboard from './pages/Dashboard/ClassDashboard/ClassUserListDashboard/ClassUserListDashboard';
import MilestoneManagement from './pages/Dashboard/SubjectDashboard/MilestoneManagement/MilestoneManagement';
import ProjectManagement from './pages/Dashboard/ProjectDashboard/ProjectManagement/ProjectManagement';
import FeatureManagement from './pages/Dashboard/ProjectDashboard/FeatureManagement/FeatureManagement';
import FunctionManagement from './pages/Dashboard/ProjectDashboard/FunctionManagement/FunctionManagement';
import TrackingManagement from './pages/Dashboard/ProjectDashboard/TrackingManagement/TrackingManagement';
import UserDashboard from './pages/Dashboard/ClassDashboard/UserDashboard/UserDashboard';
import ClassEvaluationManagement from './pages/Dashboard/ProjectDashboard/ClassEvaluationManagement/ClassEvaluationManagement';
import MilestoneSubmit from './pages/Dashboard/ProjectDashboard/MilestoneSubmit/MilestoneSubmit';
import IterationEvaluationManagement from './pages/Dashboard/ProjectDashboard/IterationEvaluationManagement/IterationEvaluationManagement';
import StudentEvaluation from './pages/Dashboard/ProjectDashboard/StudentEvaluation/StudentEvaluation';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import ClassSettingDashboard from './pages/Dashboard/SettingDashboard/ClassSettingDashboard/ClassSettingDashboard';

export default function RoutesComponent() {
  return (
    <AuthenticatedGuards>
      <DashboardLayout>
        <Routes>
          <Route
            path={path.classDashboard}
            element={
              <ErrorBoundary>
                <DashboardClass />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.projectDashboard}
            element={
              <ErrorBoundary>
                <DashboardTeam />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.settingList}
            element={
              <Authorization authorType={'admin'}>
                <ErrorBoundary>
                  <SettingListDashboard />
                </ErrorBoundary>
              </Authorization>
            }
          />
          <Route
            path={path.subjectSetting}
            element={
              <Authorization authorType={'withoutStudent'}>
                <ErrorBoundary>
                  <SubjectSettingManagement />
                </ErrorBoundary>
              </Authorization>
            }
          />
          <Route
            path={path.profile}
            element={
              <ErrorBoundary>
                <Profile />
              </ErrorBoundary>
            }
          />

          <Route
            path={path.iteration}
            element={
              <Authorization authorType={'withoutStudent'}>
                <ErrorBoundary>
                  <IterationManagement />
                </ErrorBoundary>
              </Authorization>
            }
          />
          <Route
            path={path.evaluationCriteriaManagement}
            element={
              <Authorization authorType={'withoutStudent'}>
                <ErrorBoundary>
                  <EvaluationCriteriaManagement />
                </ErrorBoundary>
              </Authorization>
            }
          />
          <Route
            path={path.classSetting}
            element={
              <Authorization>
                <ErrorBoundary>
                  <ClassSettingDashboard />
                </ErrorBoundary>
              </Authorization>
            }
          />
          <Route
            path={path.milestone}
            element={
              <ErrorBoundary>
                <MilestoneManagement />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.subjectManagement}
            element={
              <Authorization authorType={'adminAndAuthor'}>
                <ErrorBoundary>
                  <SubjectManagement />
                </ErrorBoundary>
              </Authorization>
            }
          />

          <Route
            path={path.projectManagement}
            element={
              <ErrorBoundary>
                <ProjectManagement />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.milestoneSubmit}
            element={
              <ErrorBoundary>
                <MilestoneSubmit />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.studentEvaluation}
            element={
              <ErrorBoundary>
                <StudentEvaluation />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.milestoneSubmit}
            element={
              <ErrorBoundary>
                <MilestoneSubmit />
              </ErrorBoundary>
            }
          />
          {/*<Route*/}
          {/*  path={path.featureManagement}*/}
          {/*  element={*/}
          {/*    <ErrorBoundary>*/}
          {/*      <FeatureManagement />*/}
          {/*    </ErrorBoundary>*/}
          {/*  }*/}
          {/*/>*/}
          <Route
            path={path.functionManagement}
            element={
              <ErrorBoundary>
                <FunctionManagement />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.trackingManagement}
            element={
              <ErrorBoundary>
                <TrackingManagement />
              </ErrorBoundary>
            }
          />

          <Route
            path={path.classList}
            element={
              // <Authorization authorType={'withoutStudent'}>
              <ErrorBoundary>
                <ClassListDashboard />
              </ErrorBoundary>
              // </Authorization>
            }
          />
          <Route
            path={path.classUser}
            element={
              <ErrorBoundary>
                <ClassUserListDashboard />
              </ErrorBoundary>
            }
          />
          <Route
            path={path.userManagement}
            element={
              <Authorization authorType={'admin'}>
                <ErrorBoundary>
                  <UserDashboard />
                </ErrorBoundary>
              </Authorization>
            }
          />

          <Route
            path={path.classEvaluationManagement}
            element={
              <ErrorBoundary>
                <ClassEvaluationManagement />
              </ErrorBoundary>
            }
          />

          <Route
            path={path.iterationEvaluationManagement}
            element={
              // <Authorization authorType={'withoutStudent'}>
              <ErrorBoundary>
                <IterationEvaluationManagement />
              </ErrorBoundary>
              // </Authorization>
            }
          />
        </Routes>
      </DashboardLayout>
    </AuthenticatedGuards>
  );
}
