import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import common from 'src/api/common.api';
import { payloadCreator } from 'src/utils/helper';

export const getColumnType = createAsyncThunk('common/getColumnType', payloadCreator(common.getColumnType));
export const getAllUsers = createAsyncThunk('common/getUser', payloadCreator(common.getAllUsers));

const featureList = createSlice({
  name: 'common',
  initialState: {},
  reducers: {},
  extraReducers: {}
});

const commonReducer = featureList.reducer;

export default commonReducer;
