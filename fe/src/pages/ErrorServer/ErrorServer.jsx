import { Result } from 'antd';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { path } from 'src/constants/path';

export default function ErrorServer() {
  const navigate = useNavigate();
  return (
    <Result
      status="500"
      title="Oops!!"
      subTitle="Sorry, something went wrong."
      extra={
        <Link
          to={path.classDash}
          onClick={() => {
            navigate(path.classDash);
            window.location.reload();
          }}
        >
          Back Home
        </Link>
      }
    />
  );
}
