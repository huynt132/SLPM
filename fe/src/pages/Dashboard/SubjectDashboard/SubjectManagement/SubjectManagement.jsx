import React, { useEffect, useState } from 'react';
import { Button, Space, Table, Row, Col, Input, Tag, Popconfirm, Spin, Switch } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import { path } from 'src/constants/path';
import { AiOutlineEye, AiOutlinePlus } from 'react-icons/ai';
import { addSubject, editSubject, getSubjectWithCondition } from './subjectManagement.slice';
import ModalUpdate from './Components/UpdateModal/UpdateModal';
import ModalAdd from './Components/AddModal/AddModal';
import SelectStatus from 'src/components/Select_DA/StatusSelect/SelectStatus';
import SelectAuthor from 'src/components/Select_DA/AuthorSelect/SelectAuthor';
import { showMessage } from 'src/utils/commonHelper';
import ModalView from './Components/ModalView/ModalView';

const SubjectManagement = () => {
  const dispatch = useDispatch();

  const [paramsSearch, setParamsSearch] = useState({
    subjectCode: null,
    subjectName: null,
    status: null,
    limit: 8,
    page: 1,
    total: 0,
    authorId: null
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  const [errorType, setErrorType] = useState(''); // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  // state handle Edit
  const [isEditing, setIsEditing] = useState(false);
  const [editRecord, setEditRecord] = useState(null);
  // state handle Add
  const [isAdding, setIsAdding] = useState(false);
  const [addRecord, setAddRecord] = useState(null);
  // data source table
  const [dataSource, setDataSource] = useState([]);
  // state handle row select

  const userRole = useSelector(state => state.auth.role);

  // get api list subject
  const getListSubject = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getSubjectWithCondition({ params }));
      const res = unwrapResult(data);
      const dataTable = res.data.data.map(item => {
        return {
          key: item.id,
          id: item.id,
          code: item.code,
          name: item.name,
          author: item.authorName,
          created: item.createdByUser,
          status: item.status,
          authorId: item.authorId,
          ongoingEvaluationWeight: item.ongoingEvaluationWeight,
          presentEvaluationWeight: item.presentEvaluationWeight,
          gitLabUrl: item.gitLabUrl,
          gitLabId: item.gitLabId,
          description: item.description
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setDataSource(dataTable);
      setLoading(false);
      showMessage(res.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setIsShowModal(true);
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  // call api
  useEffect(() => {
    getListSubject(paramsSearch);
  }, [dispatch]);

  // =======================ADD============================
  const handleClickBtnAdd = () => {
    setIsAdding(true);
  };

  const handleAddingSetting = async () => {
    try {
      setButtonLoading(true);
      const dataAdd = { ...addRecord };
      dataAdd.code = dataAdd?.code?.trim();
      dataAdd.name = dataAdd?.name?.trim();
      dataAdd.description = dataAdd?.description?.trim();
      const res = await dispatch(addSubject(dataAdd));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setButtonLoading(false);
      showMessage(res.payload.data.message);
      setIsAdding(false);
      setAddRecord(null);
      setDataSource(prevList => {
        return [
          ...prevList,
          {
            key: result.data.data.id,
            id: result.data.data.id,
            code: result.data.data.code,
            name: result.data.data.name,
            // author: result.data.data.authorName,
            authorId: result.data.data.authorId,
            author: result.data.data.authorName,
            // created: result.data.data.createdByUser,
            status: result.data.data.status,
            description: result.data.data.description,
            gitLabUrl: result.data.data.gitLabUrl,
            gitLabId: result.data.data.gitLabId
          }
        ];
      });
    } catch (error) {
      if ([400, 401].includes(error.status)) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // =======================EDIT===========================

  const onClickBtnEdit = record => {
    setIsEditing(true);
    setEditRecord(prevState => {
      return { ...prevState, ...record };
    });
  };

  // get api edit subject
  const onEditSetting = async body => {
    try {
      setLoading(true);
      const res = await dispatch(editSubject(body));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setLoading(false);
      setDataSource(prevList =>
        prevList.map(item =>
          item.id === result.data.data.id
            ? {
                ...item,
                code: result.data.data.code,
                name: result.data.data.name,
                author: result.data.data.authorName,
                authorId: result.data.data.authorId,
                created: result.data.data.createdByUser,
                status: result.data.data.status,
                description: result.data.data.description,
                gitLabUrl: result.data.data.gitLabUrl,
                gitLabId: result.data.data.gitLabId
              }
            : item
        )
      );
      setIsEditing(false);
      showMessage(res.payload.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
        setLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  //handle btn active and inactive - edit setting
  const handleToggleActive = record => {
    const body = {
      id: record.id,
      code: record.code,
      name: record.name,
      authorId: record.authorId,
      status: record.status === 'active' ? 'inactive' : 'active',
      description: record.description,
      ongoingEvaluationWeight: record.ongoingEvaluationWeight,
      presentEvaluationWeight: record.presentEvaluationWeight,
      gitLabUrl: record.gitLabUrl,
      gitLabId: record.gitLabId
    };

    onEditSetting(body);
  };

  // handle accept edit
  const handleEditRecord = () => {
    // setIsEditing(false);
    const body = {
      id: editRecord.id,
      code: editRecord.code.trim(),
      // title: editRecord.title.trim(),
      name: editRecord.name.trim(),
      authorId: parseInt(editRecord.authorId),
      authorName: editRecord.author,
      status: editRecord.status,
      description: editRecord?.description?.trim(),
      ongoingEvaluationWeight: editRecord.ongoingEvaluationWeight,
      presentEvaluationWeight: editRecord.presentEvaluationWeight,
      gitLabUrl: editRecord.gitLabUrl,
      gitLabId: editRecord.gitLabId
    };
    onEditSetting(body);
  };

  const [isViewing, setIsViewing] = useState(false);
  const [viewRecord, setViewRecord] = useState(null);

  const handleViewDetail = record => {
    setIsViewing(true);
    setViewRecord(record);
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code'
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'Author',
      dataIndex: 'author',
      key: 'author'
    },
    // {
    //   title: 'Gitlab URL',
    //   dataIndex: 'gitLabUrl',
    //   key: 'gitLabUrl'
    // },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
      className: 'w-10',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-10`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Created',
    //   dataIndex: 'created',
    //   key: 'created'
    // },
    {
      title: 'Action',
      key: 'action',
      className: 'w-10',
      render: (_, record) => (
        <Space size="middle">
          {userRole !== 'Author' && (
            <Popconfirm
              placement="top"
              title={`Are you sure you want to ${record.status === 'active' ? 'deactivate' : 'activate'} this subject?`}
              onConfirm={() => handleToggleActive(record)}
              okText="Yes"
              cancelText="No"
            >
              {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
              <Switch
                className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
                checked={record.status === 'active' ? true : false}
              />
            </Popconfirm>
          )}
          <Button
            style={{
              borderRadius: '10px',
              width: '30px',
              height: '30px',
              border: 'none',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              color: '#fff',
              background: '#0CA0C5'
            }}
            onClick={() => {
              handleViewDetail(record);
            }}
          >
            <AiOutlineEye style={{ cursor: 'pointer', fontSize: '18px' }} />
          </Button>
          {userRole !== 'Author' && (
            <Button
              disabled={userRole === 'Author' ? true : false}
              style={{
                borderRadius: '10px',
                width: '30px',
                height: '30px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#6BBCB3'
              }}
              onClick={() => onClickBtnEdit(record)}
            >
              <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
            </Button>
          )}
        </Space>
      )
    }
  ];

  // handle pagination as antd
  const handleTableChange = value => {
    getListSubject({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const handleSearch = () => {
    getListSubject({
      ...paramsSearch,
      page: 1
    });
  };

  // handle reset filter
  const [selectedValuesSubject, setSelectedValuesSubject] = useState();
  const [selectedValuesStatus, setSelectedValuesStatus] = useState();
  const [selectedValueAuthor, setSelectedValueAuthor] = useState();

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        subjectCode: null,
        subjectName: null,
        status: null,
        authorId: null,
        limit: 8,
        page: 1
      };
    });
    setSelectedValuesSubject([]);
    setSelectedValuesStatus([]);
    setSelectedValueAuthor([]);

    getListSubject({
      ...{
        subjectCode: null,
        subjectName: null,
        status: null,
        authorId: null,
        limit: 8,
        page: 1
      },
      page: 1
    });
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absSubject}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />

      <Row>
        <Col span={18}>
          <h1 className={'m-0'}> Subject Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Author' && (
            <Button className="br-10 btn-action d-flex w-100 h-80" onClick={handleClickBtnAdd}>
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Subject</span>
            </Button>
          )}
        </Col>
      </Row>

      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="px-1">
          <SelectAuthor
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectAuthor={selectedValueAuthor}
            setSelectAuthor={setSelectedValueAuthor}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectStatus
            setParamsSearch={setParamsSearch}
            selectedValuesStatus={selectedValuesStatus}
            setSelectedValuesStatus={setSelectedValuesStatus}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.subjectCode}
            placeholder="Input Subject Code"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, subjectCode: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.subjectName}
            placeholder="Input Subject Name"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, subjectName: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={2} offset={4} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={handleResetFilter}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total
        }}
        // loading={loading}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />
      <ModalUpdate
        isEditing={isEditing}
        setIsEditing={setIsEditing}
        handleEditRecord={handleEditRecord}
        editRecord={editRecord}
        setEditRecord={setEditRecord}
        buttonLoading={loading}
      />
      <ModalAdd
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        handleAddingSetting={handleAddingSetting}
        setAddRecord={setAddRecord}
        buttonLoading={buttonLoading}
        addRecord={addRecord}
      />
      <ModalView isViewing={isViewing} setIsViewing={setIsViewing} recordView={viewRecord} />
    </Spin>
  );
};

export default SubjectManagement;
