import { Input, Modal, Radio, Select } from 'antd';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styles from './AddModal.module.css';
import PropTypes from 'prop-types';
import { isEmpty } from '../../../../../../utils/helper';

const { Option } = Select;
const { TextArea } = Input;

export default function ModalAdd({
  addRecord,
  isAdding,
  setIsAdding,
  handleAddingSetting,
  setAddRecord,
  buttonLoading
}) {
  const listAuthor = useSelector(state => state.subjectManagement.listAuthor);

  useEffect(() => {
    if (isEmpty(addRecord?.status))
      setAddRecord(prev => {
        return { ...prev, status: 'active' };
      });
  }, [addRecord?.status]);

  return (
    <Modal
      title="Add Subject"
      visible={isAdding}
      onOk={handleAddingSetting}
      okText={'Add'}
      onCancel={() => {
        setIsAdding(false);
        setAddRecord(null);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Code</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Code</h4> */}
          <Input
            value={addRecord?.code}
            placeholder="Input Subject Code"
            onChange={e =>
              setAddRecord(record => {
                return { ...record, code: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Name</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Name</h4> */}
          <Input
            placeholder="Input Subject Name"
            value={addRecord?.name}
            onChange={e =>
              setAddRecord(record => {
                return { ...record, name: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Author</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Author</h4> */}
          <div className={styles.rowSelect}>
            <Select
              value={addRecord?.authorId}
              placeholder="Select Author"
              style={{
                width: '100%'
              }}
              onChange={value => {
                setAddRecord(record => {
                  return { ...record, authorId: value };
                });
              }}
            >
              {listAuthor.map((item, key) => {
                return (
                  <Option key={key} value={item.id}>
                    {item.fullName}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        {/* <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Gitlab Project ID</h4>
          <Input
            placeholder="Input Gitlab Project ID"
            value={addRecord?.gitLabId}
            onChange={e =>
              setAddRecord(record => {
                return { ...record, gitLabId: e.target.value };
              })
            }
          />
        </div> */}
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Status</h4> */}
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              value={isEmpty(addRecord?.status) ? 'active' : addRecord?.status}
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}> Description</h4>
          <TextArea
            className={'text-black'}
            value={addRecord?.description}
            rows={4}
            placeholder="Description ... "
            onChange={e =>
              setAddRecord(record => {
                return { ...record, description: e.target.value };
              })
            }
          />
        </div>
      </div>
    </Modal>
  );
}

ModalAdd.propTypes = {
  isAdding: PropTypes.bool,
  setIsAdding: PropTypes.func,
  handleAddingSetting: PropTypes.func,
  setAddRecord: PropTypes.func,
  buttonLoading: PropTypes.bool
};
