import { Input, Modal, Radio, Select } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import styles from './ModalUpdate.module.css';
import PropTypes from 'prop-types';

const { Option } = Select;
const { TextArea } = Input;

export default function ModalUpdate({
  isEditing,
  setIsEditing,
  handleEditRecord,
  setEditRecord,
  editRecord,
  buttonLoading
}) {
  const listAuthor = useSelector(state => state.subjectManagement.listAuthor);
  return (
    <Modal
      title="Update Information"
      visible={isEditing}
      onOk={handleEditRecord}
      width={'600px'}
      okText={'Update'}
      onCancel={() => {
        setIsEditing(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Code</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Code</h4> */}
          <Input
            value={editRecord?.code}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, code: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Name</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Name</h4> */}
          <Input
            value={editRecord?.name}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, name: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Author</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Author</h4> */}
          <div className={styles.rowSelect}>
            <Select
              labelInValue
              value={{
                value: editRecord?.author,
                label: editRecord?.author
              }}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setEditRecord(record => {
                  return { ...record, author: value.label, authorId: value.value };
                })
              }
            >
              {listAuthor.map((item, key) => {
                return (
                  <Option key={key} value={item.id} label={item.fullName}>
                    {item.fullName}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        {/* <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Gitlab Project ID</h4>
          <Input
            disabled={true}
            placeholder="Input Gitlab Project ID"
            value={editRecord?.gitLabId}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, gitLabId: e.target.value };
              })
            }
          />
        </div> */}
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Status</h4> */}
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              value={editRecord?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}> Description</h4>
          <TextArea
            className={'text-black br-8'}
            value={editRecord?.description}
            rows={4}
            placeholder="Description ... "
            onChange={e => {
              setEditRecord(record => {
                return { ...record, description: e.target.value };
              });
            }}
          />
        </div>
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func,
  handleEditRecord: PropTypes.func,
  setEditRecord: PropTypes.func,
  editRecord: PropTypes.object,
  buttonLoading: PropTypes.bool
};
