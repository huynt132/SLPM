import { Modal } from 'antd';
import React from 'react';
import styles from './ModalView.module.css';

export default function ModalView({ isViewing, setIsViewing, recordView }) {
  return (
    <Modal
      width={'600px'}
      title="Subject Detail"
      visible={isViewing}
      onCancel={() => {
        setIsViewing(false);
      }}
      cancelText="Exit"
      okButtonProps={{ style: { display: 'none' } }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>SubjectCode</h4>
          <h4 className={styles.content}>{recordView?.code}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Subject Name</h4>
          <h4 className={styles.content}>{recordView?.name}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Author Name</h4>
          <h4 className={styles.content}>{recordView?.author}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Description</h4>
          <h4 className={styles.content}>{recordView?.description}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Status</h4>
          <h4 className={styles.content}>{recordView?.status}</h4>
        </div>
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Created date</h4>*/}
        {/*  <h4 className={styles.content}> {recordView?.created}</h4>*/}
        {/*</div>*/}
      </div>
    </Modal>
  );
}
