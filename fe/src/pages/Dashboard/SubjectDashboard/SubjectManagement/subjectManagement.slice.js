import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import subjectManagement from 'src/api/subjectManagement.api';
import { payloadCreator } from 'src/utils/helper';
import submitManagementApi from '../../../../api/submitManagement.api';
import SubjectManagementApi from 'src/api/subjectManagement.api';

export const getAllSubject = createAsyncThunk('subject/getAll', payloadCreator(subjectManagement.getAllSubject));

export const getAllAuthor = createAsyncThunk('subject/getListAuthor', payloadCreator(subjectManagement.getAllAuthor));

export const editSubject = createAsyncThunk('subject/editSubject', payloadCreator(subjectManagement.editSubject));

export const getSubjectWithCondition = createAsyncThunk(
  'subject/getSubjectWithCondition',
  payloadCreator(subjectManagement.getSubjectWithCondition)
);
export const getSubjectSetting = createAsyncThunk(
  'subject/getSubjectSetting',
  payloadCreator(SubjectManagementApi.getSubjectSetting)
);

export const addSubject = createAsyncThunk('subject/addSubject', payloadCreator(subjectManagement.addSubject));

const subjectList = createSlice({
  name: 'subjectManagement',
  initialState: {
    subjectListManager: [],
    listSubjectCode: [],
    listAuthor: []
  },
  reducers: {},
  extraReducers: {
    [getAllSubject.fulfilled]: (state, action) => {
      state.subjectListManager = action.payload.data.data.map(subject => ({
        label: subject.subjectName,
        value: subject.id
      }));
      state.listSubjectCode = action.payload.data.data;
    },
    [getAllAuthor.fulfilled]: (state, action) => {
      state.listAuthor = action.payload.data.data;
    }
  }
});

const subjectReducer = subjectList.reducer;

export default subjectReducer;
