// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Radio, Checkbox } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// style

// services
import { editIteration, addIteration } from '../../IterationManagement/iterationManagement.slice.js';

// custom component
import SelectSubject from 'src/components/Select/SelectSubject';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';

export default function ModalEditIteration({ isShowModal, type, iteration, action, search }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [newIteration, setNewIteration] = useState({ ...iteration });

  useEffect(() => {
    if (isEmpty(newIteration?.status)) {
      setNewIteration(prev => {
        return {
          ...prev,
          status: 'active'
        };
      });
    }
  }, [newIteration?.status]);

  // useEffect(() => {
  //   if (!newIteration?.isOngoing && newIteration?.isOngoing !== 0) {
  //     setNewIteration(prev => {
  //       return {
  //         ...prev,
  //         isOngoing: 1
  //       };
  //     });
  //   }
  // }, [newIteration?.isOngoing]);

  useEffect(() => {
    setNewIteration(iteration);
  }, [iteration]);

  useEffect(() => {
    if (isEmpty(newIteration?.status))
      setNewIteration(prev => {
        return { ...prev, status: 'active' };
      });
  }, [newIteration?.status]);

  async function updateIteration() {
    try {
      setLoading(true);
      const res = await dispatch(editIteration({ ...newIteration }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(result.data.data, { action: 'editIterationSuccess' });
        resetModal();
      }
    } catch (error) {
      action(null, {
        action: 'showMessage',
        data: {
          description: isEmpty(error.data.message)
            ? 'An error occurred while update iteration information!'
            : error.data.message,
          type: 'fail'
        }
      });
    } finally {
      setLoading(false);
    }
  }

  async function createIteration() {
    try {
      setLoading(true);
      const res = await dispatch(addIteration({ ...newIteration }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        resetModal();
        action(null, { action: 'isHideModalEditIteration' });
        search();
      }
    } catch (error) {
      action(null, {
        action: 'showMessage',
        data: {
          description: isEmpty(error.data.message)
            ? 'An error occurred while create new iteration!'
            : error.data.message,
          type: 'fail'
        }
      });
    } finally {
      setLoading(false);
    }
  }

  function handleSubmit() {
    if (type === 'Update') updateIteration();
    if (type === 'Add') createIteration();
  }

  function resetModal() {
    if (type === 'Add') setNewIteration({});
    if (type === 'Update') setNewIteration({ ...iteration });
  }

  return (
    <Modal
      confirmLoading={loading}
      title={`${type} iteration`}
      visible={isShowModal}
      onOk={() => {
        handleSubmit();
      }}
      afterClose={resetModal}
      okText={type}
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalEditIteration' });
      }}
    >
      <div>
        <Row className={`py-1`}>
          <Col span={7}>
            <div className="rowText">
              <h4>Subject name</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={17}>
            <SelectSubject
              customClassName={'border-radius-select'}
              onOptionSelected={value => {
                setNewIteration(prev => {
                  return { ...prev, subjectId: value };
                });
              }}
              mode={'single'}
              placeholder={'Select subject'}
              subjects={isEmpty(newIteration.subjectId) ? [] : newIteration.subjectId}
              disabled={type === 'Update'}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={7}>
            <div className="rowText">
              <h4>Iteration name</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={17}>
            <Input
              className={'br-8'}
              value={newIteration.name}
              placeholder="input iteration name"
              onChange={e => {
                setNewIteration(prev => {
                  return { ...prev, name: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={7}>
            <div className="rowText">
              <h4>Evaluation weight</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={4}>
            <Input
              value={+newIteration.evaluationWeight}
              className={'br-8'}
              type="number"
              placeholder="Input evaluation weight"
              onChange={e => {
                setNewIteration(prev => {
                  return { ...prev, evaluationWeight: e.target.value };
                });
              }}
            />
          </Col>
          <Col span={1} className={'font-2re ml-1'}>
            %
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={7}>
            <div className="rowText">
              <h4>Is ongoing</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={2}>
            <Checkbox
              className={'br-8'}
              checked={newIteration?.isOngoing}
              onChange={e => {
                setNewIteration(record => {
                  return { ...record, isOngoing: e.target.checked ? 1 : 0 };
                });
              }}
            ></Checkbox>
          </Col>
          <Col span={4} className={`pl-4`}>
            <div className="rowText">
              <h4>Status</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={11}>
            <Radio.Group
              onChange={e => {
                setNewIteration(prev => {
                  return { ...prev, status: e.target.value };
                });
              }}
              value={newIteration.status}
            >
              <Radio value={'active'}>active</Radio>
              <Radio value={'inactive'}>inactive</Radio>
            </Radio.Group>
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={7}>Description:</Col>
          <Col span={17}>
            <Input.TextArea
              className={'text-black br-8'}
              value={newIteration.description}
              autoSize={{ minRows: 3, maxRows: 3 }}
              placeholder="Input description"
              onChange={e => {
                setNewIteration(prev => {
                  return { ...prev, description: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
      </div>
    </Modal>
  );
}

ModalEditIteration.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  type: PropsTypes.string,
  iteration: PropsTypes.object,
  search: PropsTypes.func
};
ModalEditIteration.defaultProps = {
  isShowModal: false,
  type: 'Add',
  iteration: {}
};
