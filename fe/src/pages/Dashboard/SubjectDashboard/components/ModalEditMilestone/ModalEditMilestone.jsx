// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Radio, DatePicker, DatePickerProps } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// services
import { editMilestone, addMilestone } from '../../MilestoneManagement/milestoneManagement.slice.js';

// custom component
import SelectIteration from 'src/components/Select/SelectIteration';
import SelectClass from 'src/components/Select/SelectClass';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';
import moment from 'moment';

export default function ModalEditMilestone({ isShowModal, type, milestone, action }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [newMilestone, setNewMilestone] = useState({ ...milestone });
  const onFromChange: DatePickerProps['onFromChange'] = (date, dateString) => {
    setNewMilestone(prev => {
      return { ...prev, from: date };
    });
  };
  const onToChange: DatePickerProps['onToChange'] = (date, dateString) => {
    setNewMilestone(prev => {
      return { ...prev, to: date };
    });
  };

  useEffect(() => {
    setNewMilestone(milestone);
  }, [milestone]);

  useEffect(() => {
    if (isEmpty(newMilestone?.status))
      setNewMilestone(prev => {
        return { ...prev, status: 'open' };
      });
  }, [newMilestone?.status]);

  async function createMileStone() {
    try {
      setLoading(true);
      const res = await dispatch(addMilestone({ ...newMilestone }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        resetModal();
        action(null, { action: 'isHideModalEditMilestone' });
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while create data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  async function updateMileStone() {
    try {
      setLoading(true);
      // let body = {...newMilestone}
      // if (!isEmpty(body.from)) body.from = formatDate(Date(body.from))
      // if (!isEmpty(body.to)) body.to = formatDate(Date(body.to))
      const res = await dispatch(editMilestone({ ...newMilestone }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(result.data.data, { action: 'editMilestoneSuccess' });
        resetModal();
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  function handleSubmit() {
    if (type === 'Update') updateMileStone();
    if (type === 'Add') createMileStone();
  }

  function resetModal() {
    if (type === 'Add') setNewMilestone({});
    if (type === 'Update') setNewMilestone({ ...milestone });
  }

  return (
    <Modal
      confirmLoading={loading}
      title={`${type} milestone`}
      visible={isShowModal}
      onOk={() => {
        handleSubmit();
      }}
      afterClose={resetModal}
      okText={type}
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalEditMilestone' });
      }}
    >
      <div>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Class code</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <SelectClass
              customClassName={'border-radius-select'}
              mode={'single'}
              onOptionSelected={value => {
                setNewMilestone(prev => {
                  return { ...prev, classId: value };
                });
              }}
              placeholder={'Select class'}
              classes={isEmpty(newMilestone.classId) ? [] : newMilestone.classId}
              disabled={type === 'Update'}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Iteration name</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <SelectIteration
              disabled={isEmpty(newMilestone?.classId)}
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'Select iteration'}
              disa
              onOptionSelected={value => {
                setNewMilestone(prev => {
                  return { ...prev, iterationId: value };
                });
              }}
              filter={{
                classId: newMilestone.classId,
                status: 'active'
              }}
              iterations={isEmpty(newMilestone.iterationId) ? [] : newMilestone.iterationId}
              disabled={type === 'Update' || isEmpty(newMilestone?.classId)}
            />
          </Col>
        </Row>
        {/*<Row className={`py-1`}>*/}
        {/*  <Col span={6}>Title:</Col>*/}
        {/*  <Col span={18}>*/}
        {/*    <Input*/}
        {/*      className={'br-8'}*/}
        {/*      value={newMilestone.title}*/}
        {/*      placeholder="Input milestone title"*/}
        {/*      onChange={e => {*/}
        {/*        setNewMilestone(prev => {*/}
        {/*          return { ...prev, title: e.target.value };*/}
        {/*        });*/}
        {/*      }}*/}
        {/*    />*/}
        {/*  </Col>*/}
        {/*</Row>*/}
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>From</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <DatePicker
              className="w-100 br-8"
              value={isEmpty(newMilestone.from) ? null : moment(newMilestone.from, 'DD-MM-YYYY')}
              format="DD-MM-YYYY"
              placeholder="Select from date"
              locale="vi"
              onChange={(date, dateString) => {
                setNewMilestone(prev => {
                  return { ...prev, from: dateString };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>To</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <DatePicker
              className="w-100 br-8"
              value={isEmpty(newMilestone.to) ? null : moment(newMilestone.to, 'DD-MM-YYYY')}
              format="DD-MM-YYYY"
              placeholder="Select to date"
              locale="vi"
              onChange={(date, dateString) => {
                setNewMilestone(prev => {
                  return { ...prev, to: dateString };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Status</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Radio.Group
              onChange={e => {
                setNewMilestone(prev => {
                  return { ...prev, status: e.target.value };
                });
              }}
              value={newMilestone.status}
            >
              <Radio value={'open'}>open</Radio>
              <Radio value={'closed'}>closed</Radio>
              <Radio value={'cancelled'}>cancelled</Radio>
            </Radio.Group>
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>Description:</Col>
          <Col span={18}>
            <Input.TextArea
              className={'text-black br-8'}
              value={newMilestone.description}
              autoSize={{ minRows: 3, maxRows: 3 }}
              placeholder="Input milestone description"
              onChange={e => {
                setNewMilestone(prev => {
                  return { ...prev, description: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
      </div>
    </Modal>
  );
}

ModalEditMilestone.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  type: PropsTypes.string,
  milestone: PropsTypes.object
};
ModalEditMilestone.defaultProps = {
  isShowModal: false,
  type: 'Add',
  milestone: {}
};
