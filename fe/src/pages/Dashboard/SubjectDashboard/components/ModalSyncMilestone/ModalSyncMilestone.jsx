// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Radio, DatePicker, DatePickerProps } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// services
import { syncMilestone } from '../../MilestoneManagement/milestoneManagement.slice.js';

// custom component
import SelectClass from 'src/components/Select/SelectClass';
import SelectProject from 'src/components/Select/SelectProject';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';

export default function ModalSyncMilestone({ isShowModal, action }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [projectId, setProjectId] = useState();
  const [classId, setClassId] = useState();

  async function execSyncMilestone() {
    try {
      setLoading(true);
      // let body = {...newMilestone}
      // if (!isEmpty(body.from)) body.from = formatDate(Date(body.from))
      // if (!isEmpty(body.to)) body.to = formatDate(Date(body.to))
      const res = await dispatch(syncMilestone({ classId, projectId }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(result.data.data, { action: 'editMilestoneSuccess' });
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  function handleSubmit() {}

  return (
    <Modal
      confirmLoading={loading}
      title={`Sync milestone`}
      visible={isShowModal}
      onOk={() => {
        execSyncMilestone();
      }}
      okText={'Sync'}
      onCancel={() => {
        action(null, { action: 'isHideModalSyncMilestone' });
      }}
    >
      <div>
        <Row className={`py-1`}>
          <Col span={6}>Class code:</Col>
          <Col span={18}>
            <SelectClass
              mode={'single'}
              onOptionSelected={value => {
                setClassId(value);
              }}
              placeholder={'Select class'}
              classes={isEmpty(classId) ? [] : classId}
              disabled={!isEmpty(projectId)}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>Project name:</Col>
          <Col span={18}>
            <SelectProject
              mode={'single'}
              onOptionSelected={value => {
                setProjectId(value);
              }}
              placeholder={'Select project'}
              projects={isEmpty(projectId) ? [] : projectId}
              disabled={!isEmpty(classId)}
            />
          </Col>
        </Row>
      </div>
    </Modal>
  );
}

ModalSyncMilestone.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  type: PropsTypes.string,
  milestone: PropsTypes.object
};
ModalSyncMilestone.defaultProps = {
  isShowModal: false,
  type: 'Add',
  milestone: {}
};
