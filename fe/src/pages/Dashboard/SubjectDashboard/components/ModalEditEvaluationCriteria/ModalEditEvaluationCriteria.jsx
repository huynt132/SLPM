// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Radio } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// services
import {
  editEvaluationCriteria,
  addEvaluationCriteria
} from '../../EvaluationCriteriaManagement/evaluationCriteriaManagement.slice.js';

// custom component
import SelectIteration from 'src/components/Select/SelectIteration';
import SelectSubject from 'src/components/Select/SelectSubject';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';
import SelectCustomValue from '../../../../../components/Select/SelectCustomValue';

const { TextArea } = Input;
export default function ModalEditEvaluationCriteria({ isShowModal, type, criteria, action }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [newCriteria, setNewCriteria] = useState({ ...criteria });

  useEffect(() => {
    setNewCriteria(criteria);
  }, [criteria]);

  useEffect(() => {
    if (type === 'Add' && isEmpty(newCriteria?.teamEvaluation) && newCriteria?.teamEvaluation !== 0)
      setNewCriteria(prev => {
        return { ...prev, teamEvaluation: 1 };
      });
  }, [newCriteria?.teamEvaluation]);

  useEffect(() => {
    if (type === 'Add' && isEmpty(newCriteria?.status))
      setNewCriteria(prev => {
        return { ...prev, status: 'active' };
      });
  }, [newCriteria?.status]);

  useEffect(() => {
    if (type === 'Add' && isEmpty(newCriteria?.maxLoc))
      setNewCriteria(prev => {
        return { ...prev, maxLoc: 0 };
      });
  }, [newCriteria?.maxLoc]);

  async function updateCriteria() {
    try {
      setLoading(true);
      const res = await dispatch(editEvaluationCriteria({ ...newCriteria }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(result.data.data, { action: 'editEvaluationCriteriaSuccess' });
        resetModal();
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  async function createCriteria() {
    try {
      setLoading(true);
      const res = await dispatch(addEvaluationCriteria({ ...newCriteria }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        resetModal();
        action(null, { action: 'isHideModalEditEvaluationCriteria' });
        action(null, { action: 'triggerSearch' });
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while create data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  function handleSubmit() {
    if (type === 'Update') updateCriteria();
    if (type === 'Add') createCriteria();
  }

  function resetModal() {
    if (type === 'Add') setNewCriteria({});
    if (type === 'Update') setNewCriteria({ ...criteria });
  }

  return (
    <Modal
      confirmLoading={loading}
      title={`${type} evaluation criteria`}
      visible={isShowModal}
      onOk={() => {
        handleSubmit();
      }}
      afterClose={resetModal}
      okText={type}
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalEditEvaluationCriteria' });
      }}
      width="600px"
    >
      <div>
        {type === 'Add' && (
          <Row className={`py-1`}>
            <Col span={10}>
              <div className="rowText">
                <h4>Subject name</h4>
                <span className="importantInfor">*</span>
              </div>
            </Col>
            <Col span={14}>
              <SelectSubject
                customClassName={'border-radius-select'}
                onOptionSelected={value => {
                  setNewCriteria(prev => {
                    return { ...prev, subjectId: value };
                  });
                }}
                mode={'single'}
                placeholder={'Select subject'}
                subjects={isEmpty(newCriteria.subjectId) ? [] : newCriteria.subjectId}
              />
            </Col>
          </Row>
        )}
        <Row className={`py-1`}>
          <Col span={10}>
            <div className="rowText">
              <h4>Iteration name</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={14}>
            <SelectIteration
              customClassName={'border-radius-select w-100'}
              mode={'single'}
              placeholder={'Select iteration'}
              onOptionSelected={value => {
                setNewCriteria(prev => {
                  return { ...prev, iterationId: value };
                });
              }}
              filter={{
                subjectId: newCriteria?.subjectId,
                status: type === 'Add' ? 'active' : null
              }}
              iterations={isEmpty(newCriteria.iterationId) ? [] : newCriteria.iterationId}
              disabled={type === 'Update'}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={10}>
            <div className="rowText">
              <h4>Evaluation name</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={14}>
            <Input
              className={'br-8'}
              value={newCriteria?.name}
              placeholder="Input evaluation name"
              onChange={e => {
                setNewCriteria(prev => {
                  return { ...prev, name: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={10}>
            <div className="rowText">
              <h4>Evaluation weight(%)</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={5}>
            <Input
              className={'br-8 mr-1'}
              value={+newCriteria?.evaluationWeight}
              type="number"
              placeholder="Input evaluation weight"
              onChange={e => {
                setNewCriteria(prev => {
                  return { ...prev, evaluationWeight: e.target.value };
                });
              }}
            />
          </Col>
          {newCriteria?.type === 'demo' && (
            <>
              <Col offset={2} span={4}>
                <div className="rowText">
                  <h4>Max loc</h4>
                  <span className="importantInfor">*</span>
                </div>
              </Col>
              <Col span={3}>
                <Input
                  className={'br-8'}
                  value={+newCriteria.maxLoc}
                  type="number"
                  placeholder="Input max loc"
                  onChange={e => {
                    setNewCriteria(prev => {
                      return { ...prev, maxLoc: e.target.value };
                    });
                  }}
                />
              </Col>
            </>
          )}
        </Row>
        <Row className={`py-1`}>
          <Col span={10}>
            <div className="rowText">
              <h4>Type</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={14}>
            <SelectCustomValue
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'Select criteria type'}
              onOptionSelected={value => {
                setNewCriteria(prev => {
                  return { ...prev, type: value };
                });
              }}
              query={{
                table: 'evaluation_criterials',
                field: 'type'
              }}
              values={isEmpty(newCriteria.type) ? [] : newCriteria.type}
            />
          </Col>
        </Row>
        {/*<Row className={`py-1`}>*/}
        {/*  <Col span={8}>Is team evaluation:</Col>*/}
        {/*  <Col span={16}>*/}
        {/*    <Radio.Group*/}
        {/*      onChange={e => {*/}
        {/*        setNewCriteria(prev => {*/}
        {/*          return { ...prev, teamEvaluation: e.target.value };*/}
        {/*        });*/}
        {/*      }}*/}
        {/*      value={+newCriteria.teamEvaluation}*/}
        {/*    >*/}
        {/*      <Radio value={1}>yes</Radio>*/}
        {/*      <Radio value={0}>no</Radio>*/}
        {/*    </Radio.Group>*/}
        {/*  </Col>*/}
        {/*</Row>*/}
        <Row className={`py-1`}>
          <Col span={10}>
            <div className="rowText">
              <h4>Status</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={14}>
            <Radio.Group
              onChange={e => {
                setNewCriteria(prev => {
                  return { ...prev, status: e.target.value };
                });
              }}
              value={newCriteria?.status}
            >
              <Radio value={'active'}>active</Radio>
              <Radio value={'inactive'}>inactive</Radio>
            </Radio.Group>
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={10}>Evaluation guide:</Col>
          <Col span={14}>
            <TextArea
              className={'text-black br-8'}
              rows={4}
              value={newCriteria.guide}
              placeholder="Input evaluation guide"
              onChange={e => {
                setNewCriteria(prev => {
                  return { ...prev, guide: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
      </div>
    </Modal>
  );
}

ModalEditEvaluationCriteria.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  type: PropsTypes.string,
  criteria: PropsTypes.object
};
ModalEditEvaluationCriteria.defaultProps = {
  isShowModal: false,
  type: 'Add',
  criteria: {}
};
