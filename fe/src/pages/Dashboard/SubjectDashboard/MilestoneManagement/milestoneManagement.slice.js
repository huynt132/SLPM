import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import milestoneManagementApi from 'src/api/milestoneManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getList = createAsyncThunk('milestones/getAll', payloadCreator(milestoneManagementApi.getMilestones));
export const editMilestone = createAsyncThunk('milestones/edit', payloadCreator(milestoneManagementApi.editMilestone));
export const addMilestone = createAsyncThunk('milestones/add', payloadCreator(milestoneManagementApi.addMilestone));
export const getAllClasses = createAsyncThunk('classes/getList', payloadCreator(milestoneManagementApi.getAllClasses));
export const getAllMilestones = createAsyncThunk(
  'milestones/getList',
  payloadCreator(milestoneManagementApi.getAllMilestones)
);
export const syncMilestone = createAsyncThunk(
  'project/milestones',
  payloadCreator(milestoneManagementApi.syncMilestone)
);

const milestoneManagement = createSlice({
  name: 'milestoneManagement',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const milestoneManagementReducer = milestoneManagement.reducer;

export default milestoneManagementReducer;
