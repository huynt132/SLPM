// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Button, Space, Table, Row, Col, Input, Spin } from 'antd';
// style
import { VscEdit } from 'react-icons/vsc';
import { AiOutlinePlus, AiOutlineSync } from 'react-icons/ai';
// services
import { editMilestone, getList } from './milestoneManagement.slice.js';
// custom component
import SelectIteration from 'src/components/Select/SelectIteration';
import SelectClass from 'src/components/Select/SelectClass';
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import ModalEditMilestone from '../components/ModalEditMilestone/ModalEditMilestone';
import ModalSyncMilestone from '../components/ModalSyncMilestone/ModalSyncMilestone';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';

const MilestoneManagement = () => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [isShowSyncModal, setIsShowSyncModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const curMilestone = useRef({});
  const curMilestoneIdx = useRef(0);
  const editMilestoneType = useRef('add');

  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [errorType, setErrorType] = useState('');

  const pagination = useRef({
    limit: 8,
    page: 1,
    total: 0
  });

  const [filter, setFilter] = useState({
    title: null,
    iterationId: null,
    classId: null,
    status: null
  });

  useEffect(() => {
    getMilestoneList(filter);
  }, [dispatch]);

  const userRole = useSelector(state => state.auth.role);

  const getMilestoneList = async filters => {
    try {
      setLoading(true);
      let query = {
        ...filters,
        ...pagination.current
      };
      if (!isEmpty(query.iterationId)) query.iterationId = query.iterationId.join(',');
      // if (!isEmpty(query.classId)) query.classId = query.classId.join(',');
      const res = await dispatch(getList(query));
      const result = unwrapResult(res);
      if (result.data.success) {
        setDataSource(result.data.data);
        pagination.current.total = result.data.total;
        if (!isEmpty(result?.data?.curClassId)) {
          setFilter(prev => {
            return { ...prev, classId: result?.data?.curClassId };
          });
        }
        showMessage(result.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  const clearFilters = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    // need to re check this lines of code: because setState does not run immediately because it's asyn function
    setFilter(prev => {
      return { ...prev, title: null, iterationId: null, classId: null, status: null };
    });
    getMilestoneList(filter);
  };

  const search = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    getMilestoneList(filter);
  };

  async function editMilestoneStatus() {
    if (isEmpty(curMilestone.current)) return showMessage('Update data is invalid, please check again!', 'fail');
    try {
      setLoading(true);
      const res = await dispatch(
        editMilestone({
          ...curMilestone.current,
          status: curMilestone.current.status === 'active' ? 'inactive' : 'active'
        })
      );
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        curMilestone.current.status = curMilestone.current.status === 'active' ? 'inactive' : 'active';
      }
    } catch (error) {
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while update data!');
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  }

  const action = (rowData, action) => {
    if (!isEmpty(rowData)) curMilestone.current = rowData;
    if (action.action === 'isHideModalEditMilestone') {
      setIsEditing(false);
    }
    if (action.action === 'isHideModalSyncMilestone') {
      setIsShowSyncModal(false);
    }
    if (action.action === 'editMilestoneStatus') {
      editMilestoneStatus();
    }
    if (action.action === 'editMilestoneSuccess') {
      setDataSource(prevList => {
        let tmp = [...prevList];
        tmp[curMilestoneIdx.current] = rowData;
        return [...tmp];
      });
      setIsEditing(false);
    }
    if (action.action === 'editMilestone') {
      editMilestoneType.current = 'Update';
      curMilestoneIdx.current = action.data;
      setIsEditing(true);
    }
    if (action.action === 'addMilestone') {
      editMilestoneType.current = 'Add';
      setIsEditing(true);
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'No',
      className: 'w-5',
      key: 'id',
      render: (text, record, index) => (
        <span>{index + 1 + pagination.current.limit * (pagination.current.page - 1)}</span>
      )
    },
    // {
    //   title: 'Class Code',
    //   dataIndex: 'classCode',
    //   key: 'classCode'
    // },
    {
      title: 'Iteration',
      dataIndex: 'iterationName',
      key: 'iterationName'
    },
    // {
    //   title: 'Milestone Title',
    //   dataIndex: 'title',
    //   key: 'title'
    // },
    // {
    //   title: 'Milestone Description',
    //   dataIndex: 'description',
    //   key: 'description'
    // },
    {
      title: 'From',
      className: 'w-10',
      dataIndex: 'from',
      key: 'from'
    },
    {
      title: 'To',
      className: 'w-10',
      dataIndex: 'to',
      key: 'to'
    },
    {
      title: 'Status',
      className: 'w-10',
      key: 'status',
      dataIndex: 'status'
    },
    // {
    //   title: 'Created By',
    //   dataIndex: 'createdByUser',
    //   key: 'createdByUser'
    // },
    // {
    //   title: 'Created',
    //   dataIndex: 'created',
    //   key: 'created'
    // },
    {
      title: 'Action',
      key: 'action',
      className: 'w-15',
      render: (_, record, idx) => (
        <Space size="middle">
          {/*<Popconfirm*/}
          {/*  placement="top"*/}
          {/*  title={`Are you sure you want to ${record.status === 'active' ? 'deactivate' : 'activate'} this milestone?`}*/}
          {/*  onConfirm={() => action(record, { action: 'editMilestoneStatus' })}*/}
          {/*  okText="Yes"*/}
          {/*  cancelText="No"*/}
          {/*>*/}
          {/*  <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>*/}
          {/*    <span className='m-auto'>{record.status === 'active' ? 'deactivate' : 'activate'}</span>*/}
          {/*  </Button>*/}
          {/*</Popconfirm>*/}
          {userRole !== 'Student' && (
            // <Button
            //   className="br-10 btn-edit d-flex"
            //   onClick={() => {
            //     action(record, { action: 'editMilestone', data: idx });
            //   }}
            // >
            //   <VscEdit className="text-white m-auto" />
            // </Button>
            <Button
              style={{
                borderRadius: '10px',
                width: '30px',
                height: '30px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#6BBCB3'
              }}
              onClick={() => action(record, { action: 'editMilestone', data: idx })}
            >
              <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
            </Button>
          )}
        </Space>
      )
    }
  ];

  return (
    <Spin tip="Loading..." spinning={loading}>
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Milestone Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Student' && (
            <Button
              style={{ padding: '0 4rem' }}
              className="br-10 btn-action d-flex w-100 h-80 px-2"
              onClick={() =>
                action(
                  {
                    iterationId: null,
                    classId: null,
                    title: null,
                    description: null,
                    to: null,
                    from: null,
                    status: null
                  },
                  { action: 'addMilestone' }
                )
              }
            >
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Milestone</span>
            </Button>
          )}
        </Col>
      </Row>
      <hr className={'hr'} />
      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="pr-1">
          <SelectClass
            placeholder={'Select Class'}
            mode={'single'}
            customClassName={'border-radius-select'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, classId: value };
              });
            }}
            classes={isEmpty(filter.classId) ? [] : filter.classId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectIteration
            customClassName={'border-radius-select'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, iterationId: value };
              });
            }}
            iterations={isEmpty(filter.iterationId) ? [] : filter.iterationId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'Select Status'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, status: value };
              });
            }}
            query={{
              table: 'milestones',
              field: 'status'
            }}
            values={isEmpty(filter.status) ? [] : filter.status}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={filter.title}
            placeholder="Input Iteration Title"
            className="br-8"
            onChange={e => {
              setFilter(prev => {
                return { ...prev, title: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={2} offset={4} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={clearFilters}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={search}>
            Search
          </Button>
        </Col>
        {/* <Col offset={4} span={2} className="d-flex justify-content-between">
          <Button
            className="br-10 btn-action d-flex mx-1"
            onClick={() => setIsShowSyncModal(true)}
            disabled={userRole === 'Student' ? true : false}
          >
            <AiOutlineSync style={{ background: '#0d9caf', color: 'white' }} className="icon-plus m-auto mr-1" />
            <span className="m-auto">Sync to gitlab</span>
          </Button>
          <Button className="br-10 btn-grey w-100 mx-1" onClick={clearFilters}>
            Reset
          </Button>
          <Button className="br-10 btn-action w-100 mx-1" onClick={search}>
            Search
          </Button>
        </Col> */}
      </Row>
      <Table
        // loading={loading}
        columns={columns}
        rowKey={record => 'milestone_' + record.id}
        dataSource={dataSource}
        pagination={{
          className: 'border-radius-paging',
          position: 'bottomLeft',
          pageSize: pagination.current.limit,
          total: pagination.current.total,
          current: pagination.current.page,
          onChange: (page, pageSize) => {
            pagination.current.page = page;
            getMilestoneList(filter);
          }
        }}
      />
      <ModalEditMilestone
        isShowModal={isEditing}
        type={editMilestoneType.current}
        milestone={curMilestone.current}
        action={action}
      />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        navigateTo={null}
        text={messageText}
        iconname={messageType}
        type={errorType}
      />
      <ModalSyncMilestone isShowModal={isShowSyncModal} action={action} />
    </Spin>
  );
};

export default MilestoneManagement;
