import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import evaluationCriteriaManagementApi from 'src/api/evaluationCriteriaManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getList = createAsyncThunk('criteria/getAll', payloadCreator(evaluationCriteriaManagementApi.getCriteria));
export const editEvaluationCriteria = createAsyncThunk(
  'criteria/edit',
  payloadCreator(evaluationCriteriaManagementApi.editCriteria)
);
export const addEvaluationCriteria = createAsyncThunk(
  'criteria/add',
  payloadCreator(evaluationCriteriaManagementApi.addCriteria)
);

const evaluationCriteriaManagement = createSlice({
  name: 'evaluationCriteriaManagement',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const evaluationCriteriaManagementReducer = evaluationCriteriaManagement.reducer;

export default evaluationCriteriaManagementReducer;
