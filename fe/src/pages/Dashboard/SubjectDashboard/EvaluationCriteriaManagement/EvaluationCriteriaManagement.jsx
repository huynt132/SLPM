// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Popconfirm, Button, Space, Table, Tag, Row, Col, Input, Spin, Switch } from 'antd';
// style
import { VscEdit } from 'react-icons/vsc';
import { AiOutlinePlus } from 'react-icons/ai';
// services
import { editEvaluationCriteria, getList } from './evaluationCriteriaManagement.slice.js';
// custom component
import SelectSubject from 'src/components/Select/SelectSubject';
import SelectIteration from 'src/components/Select/SelectIteration';
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import ModalEditEvaluationCriteria from '../components/ModalEditEvaluationCriteria/ModalEditEvaluationCriteria';
import { path } from 'src/constants/path';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';

const EvaluationCriteriaManagement = () => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const curCriteria = useRef({});
  const curCriteriaIdx = useRef(0);
  const editEvaluationCriteriaType = useRef('add');

  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [errorType, setErrorType] = useState('');

  const pagination = useRef({
    limit: 8,
    page: 1,
    total: 0
  });

  const [filter, setFilter] = useState({
    iterationId: null,
    iterationName: null,
    status: null
  });

  const userRole = useSelector(state => state.auth.role);

  useEffect(() => {
    getCriteriaList(filter);
  }, [dispatch]);

  const getCriteriaList = async filters => {
    try {
      setLoading(true);
      let query = {
        ...filters,
        ...pagination.current
      };
      // if (!isEmpty(query.iterationId)) query.iterationId = query.iterationId.join(',');
      const res = await dispatch(getList(query));
      const result = unwrapResult(res);
      if (result.data.success) {
        setDataSource(result.data.data);
        if (!isEmpty(result.data.curIterationId)) {
          setFilter(prev => {
            return { ...prev, iterationId: result.data.curIterationId };
          });
        }
        if (!isEmpty(result.data.curSubjectId)) {
          setFilter(prev => {
            return { ...prev, subjectId: result.data.curSubjectId };
          });
        }
        pagination.current.total = result.data.total;
        showMessage(result.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setErrorType('tokenExpired');
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
      }
    } finally {
      setLoading(false);
    }
  };

  const clearFilters = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    setFilter(prev => {
      return { ...prev, iterationId: null, iterationName: null, status: null, type: null };
    });
    getCriteriaList({ ...filter, iterationId: null, iterationName: null, status: null, type: null });
  };

  const validateSearch = () => {
    if (isEmpty(filter.subjectId)) return 'Please select subject!';
    if (isEmpty(filter.iterationId)) return 'Please select iteration!';
    return null;
  };

  const search = () => {
    let errorMsg = validateSearch();
    if (!isEmpty(errorMsg)) {
      return action(null, {
        action: 'showMessage',
        data: {
          description: errorMsg,
          type: 'fail'
        }
      });
    }

    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    getCriteriaList(filter);
  };

  async function editEvaluationCriteriaStatus() {
    if (isEmpty(curCriteria.current)) return showMessage('Update data is invalid, please check again!', 'fail');
    try {
      setLoading(true);
      const res = await dispatch(
        editEvaluationCriteria({
          ...curCriteria.current,
          status: curCriteria.current.status === 'active' ? 'inactive' : 'active'
        })
      );
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        curCriteria.current.status = curCriteria.current.status === 'active' ? 'inactive' : 'active';
      }
    } catch (error) {
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while update data!');
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  }

  const action = (rowData, action) => {
    if (!isEmpty(rowData)) curCriteria.current = rowData;
    if (action.action === 'isHideModalEditEvaluationCriteria') {
      setIsEditing(false);
    }
    if (action.action === 'triggerSearch') {
      getCriteriaList(filter);
    }
    if (action.action === 'editEvaluationCriteriaStatus') {
      editEvaluationCriteriaStatus();
    }
    if (action.action === 'editEvaluationCriteriaSuccess') {
      setDataSource(prevList => {
        let tmp = [...prevList];
        tmp[curCriteriaIdx.current] = rowData;
        return [...tmp];
      });
      setIsEditing(false);
    }
    if (action.action === 'editEvaluationCriteria') {
      editEvaluationCriteriaType.current = 'Update';
      curCriteriaIdx.current = action.data;
      setIsEditing(true);
    }
    if (action.action === 'addCriteria') {
      editEvaluationCriteriaType.current = 'Add';
      setIsEditing(true);
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
      // render: (text, record, index) => (
      //   <span>{index + 1 + pagination.current.limit * (pagination.current.page - 1)}</span>
      // )
    },
    // {
    //   title: 'Iteration Name',
    //   dataIndex: 'iterationName',
    //   key: 'iterationName'
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'Evaluation Weight',
      dataIndex: 'evaluationWeight',
      className: 'w-15',
      key: 'evaluationWeight',
      render: text => <span>{`${text} %`}</span>
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
      className: 'w-10'
    },
    {
      title: 'Max Loc',
      dataIndex: 'maxLoc',
      className: 'w-15',
      key: 'maxLoc'
    },
    {
      title: 'Status',
      key: 'status',
      className: 'w-10',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-5`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Created By',
    //   dataIndex: 'createdByUser',
    //   key: 'createdByUser'
    // },
    // {
    //   title: 'Created',
    //   dataIndex: 'created',
    //   key: 'created'
    // },
    {
      title: 'Action',
      key: 'action',
      className: 'w-15',
      render: (_, record, idx) => {
        return (
          userRole !== 'Trainer' && (
            <Space size="middle">
              <Popconfirm
                placement="top"
                title={`Are you sure you want to ${
                  record.status === 'active' ? 'deactivate' : 'activate'
                } this criteria?`}
                onConfirm={() => action(record, { action: 'editEvaluationCriteriaStatus' })}
                okText="Yes"
                cancelText="No"
              >
                {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
                <Switch
                  className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
                  checked={record.status === 'active' ? true : false}
                />
              </Popconfirm>
              {/* <Button
                className="br-10 btn-edit d-flex"
                onClick={() => {
                  action(record, { action: 'editEvaluationCriteria', data: idx });
                }}
              >
                <VscEdit className="text-white m-auto" />
              </Button> */}
              <Button
                style={{
                  borderRadius: '10px',
                  width: '30px',
                  height: '30px',
                  border: 'none',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                  color: '#fff',
                  background: '#6BBCB3'
                }}
                onClick={() => action(record, { action: 'editEvaluationCriteria', data: idx })}
              >
                <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
              </Button>
            </Space>
          )
        );
      }
    }
  ];

  return (
    <Spin tip="Loading..." spinning={loading}>
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Evaluation Criteria Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Trainer' && (
            <Button
              style={{ padding: '0 4rem' }}
              className="br-10 btn-action d-flex w-100 h-80 px-2"
              onClick={() =>
                action(
                  { iterationId: null, evaluationWeight: null, teamEvaluation: null, status: null, maxLoc: null },
                  { action: 'addCriteria' }
                )
              }
            >
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Criteria</span>
            </Button>
          )}
        </Col>
      </Row>
      <hr className={'hr'} />
      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="px-1">
          <SelectSubject
            customClassName={'border-radius-select'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, subjectId: value, iterationId: null };
              });
            }}
            mode={'single'}
            placeholder={'Select subject'}
            subjects={isEmpty(filter.subjectId) ? [] : filter.subjectId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectIteration
            customClassName={'border-radius-select'}
            mode={'single'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, iterationId: value };
              });
            }}
            filter={{ subjectId: filter?.subjectId }}
            iterations={isEmpty(filter.iterationId) ? [] : filter.iterationId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'Select criteria type'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, type: value };
              });
            }}
            query={{
              table: 'evaluation_criterials',
              field: 'type'
            }}
            values={isEmpty(filter.type) ? [] : filter.type}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'Select status'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, status: value };
              });
            }}
            query={{
              table: 'evaluation_criterials',
              field: 'status'
            }}
            values={isEmpty(filter.status) ? [] : filter.status}
          />
        </Col>
        <Col span={4} className="pr-1">
          {/*<Input*/}
          {/*  value={filter.title}*/}
          {/*  placeholder="Input Criteria Name"*/}
          {/*  className="br-8"*/}
          {/*  onChange={e => {*/}
          {/*    setFilter(prev => {*/}
          {/*      return { ...prev, iterationName: e.target.value };*/}
          {/*    });*/}
          {/*  }}*/}
          {/*/>*/}
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={clearFilters}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={search}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        // loading={loading}
        columns={columns}
        rowKey={record => 'criteria_' + record.id}
        dataSource={dataSource}
        pagination={{
          className: 'border-radius-paging',
          position: 'bottomLeft',
          pageSize: pagination.current.limit,
          total: pagination.current.total,
          current: pagination.current.page,
          onChange: (page, pageSize) => {
            pagination.current.page = page;
            getCriteriaList(filter);
          }
        }}
      />
      <ModalEditEvaluationCriteria
        isShowModal={isEditing}
        type={editEvaluationCriteriaType.current}
        criteria={curCriteria.current}
        action={action}
      />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        navigateTo={path.absEvalCriteria}
        text={messageText}
        iconname={messageType}
        type={errorType}
      />
    </Spin>
  );
};

export default EvaluationCriteriaManagement;
