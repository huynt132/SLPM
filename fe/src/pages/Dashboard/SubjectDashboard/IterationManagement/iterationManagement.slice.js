import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import iterationManagementApi from 'src/api/iterationManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getList = createAsyncThunk('iterations/getAll', payloadCreator(iterationManagementApi.getIterations));

export const editIteration = createAsyncThunk('iterations/edit', payloadCreator(iterationManagementApi.editIteration));

export const addIteration = createAsyncThunk('iterations/add', payloadCreator(iterationManagementApi.addIteration));

export const getAllIteration = createAsyncThunk(
  'iterations/getIterations',
  payloadCreator(iterationManagementApi.getAllIteration)
);

const iterationManagement = createSlice({
  name: 'iterationManagement',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const iterationManagementReducer = iterationManagement.reducer;

export default iterationManagementReducer;
