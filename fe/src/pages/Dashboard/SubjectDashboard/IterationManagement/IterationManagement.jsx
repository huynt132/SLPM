// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Popconfirm, Button, Space, Table, Tag, Row, Col, Input, Spin, Switch } from 'antd';
// style
import { VscEdit } from 'react-icons/vsc';
import { AiOutlinePlus } from 'react-icons/ai';
// services
import { editIteration, getList } from './iterationManagement.slice.js';
// custom component
import SelectSubject from 'src/components/Select/SelectSubject';
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import ModalEditIteration from '../components/ModalEditIteration/ModalEditIteration';
import { path } from 'src/constants/path';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';

const IterationManage = () => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const curIteration = useRef({});
  const curIterationIdx = useRef(0);
  const editIterationType = useRef('add');

  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [errorType, setErrorType] = useState('');
  const userRole = useSelector(state => state.auth.role);

  const [totalWeight, setTotalWeight] = useState(null);

  const pagination = useRef({
    limit: 8,
    page: 1,
    total: 0
  });

  const [filter, setFilter] = useState({
    subjectId: null,
    iterationName: null,
    status: null
  });

  useEffect(() => {
    getIterationList(filter);
  }, [dispatch]);

  const getIterationList = async filters => {
    try {
      setLoading(true);
      let query = {
        ...filters,
        ...pagination.current
      };
      // if (!isEmpty(query.subjectId)) query.subjectId = query.subjectId.join(',');
      const res = await dispatch(getList(query));
      const result = unwrapResult(res);
      if (result.data.success) {
        setDataSource(result.data.data);
        if (!isEmpty(result?.data?.curSubject)) {
          setFilter(prev => {
            return { ...prev, subjectId: result.data.curSubject };
          });
        }
        setTotalWeight(result.data.totalWeight);
        pagination.current.total = result.data.total;
        showMessage(result.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  const clearFilters = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    // need to re check this lines of code: because setState does not run immediately because it's asyn function
    setFilter(prev => {
      return { ...prev, subjectId: null, iterationName: null, status: null };
    });
    getIterationList({
      subjectId: null,
      iterationName: null,
      status: null
    });
  };

  const search = () => {
    if (isEmpty(filter?.subjectId)) {
      setIsShowMessage(true);
      setMessageText('Please choose subject to search!');
      setMessageType('fail');
      return;
    }
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    getIterationList({ ...filter });
  };

  async function editIterationStatus() {
    if (isEmpty(curIteration.current)) return showMessage('Update data is invalid, please check again!', 'fail');
    try {
      setLoading(true);
      const res = await dispatch(
        editIteration({
          ...curIteration.current,
          status: curIteration.current.status === 'active' ? 'inactive' : 'active'
        })
      );
      const result = unwrapResult(res);

      if (result.data.success) {
        showMessage(result.data.message);
        curIteration.current.status = curIteration.current.status === 'active' ? 'inactive' : 'active';
      }
    } catch (error) {
      setIsShowMessage(true);
      setMessageText(isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message);
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  }

  const action = (rowData, action) => {
    if (!isEmpty(rowData)) curIteration.current = rowData;
    if (action.action === 'isHideModalEditIteration') {
      setIsEditing(false);
    }
    if (action.action === 'editIterationStatus') {
      editIterationStatus();
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
    if (action.action === 'editIterationSuccess') {
      setDataSource(prevList => {
        let tmp = [...prevList];
        tmp[curIterationIdx.current] = rowData;
        return [...tmp];
      });
      setIsEditing(false);
    }
    if (action.action === 'editIteration') {
      editIterationType.current = 'Update';
      curIterationIdx.current = action.data;
      setIsEditing(true);
    }
    if (action.action === 'addIteration') {
      editIterationType.current = 'Add';
      setIsEditing(true);
    }
  };

  const columns = [
    {
      title: 'Id',
      className: 'w-5',
      dataIndex: 'id',
      key: 'id'
    },
    // {
    //   title: 'SubjectName',
    //   dataIndex: 'subjectName',
    //   className: "w-25",
    //   key: 'subjectName'
    // },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'Evaluation Weight',
      className: 'w-20',
      dataIndex: 'evaluationWeight',
      key: 'evaluationWeight',
      render: evalWeight => <span>{<span>{isEmpty(evalWeight) ? '' : `${evalWeight} %`}</span>}</span>
    },
    {
      title: 'Ongoing',
      dataIndex: 'isOngoing',
      className: 'w-15',
      key: 'isOngoing',
      render: ongoing => <span>{<span>{ongoing === 1 ? 'Yes' : 'No'}</span>}</span>
    },
    {
      title: 'Status',
      key: 'status',
      className: 'w-10',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-5`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Description',
    //   dataIndex: 'description',
    //   key: 'description',
    //   width: 400
    // },
    {
      title: 'Action',
      key: 'action',
      className: 'w-15',
      render: (_, record, idx) => (
        <Space size="middle">
          {userRole !== 'Trainer' && (
            <Popconfirm
              placement="top"
              title={`Are you sure you want to ${
                record.status === 'active' ? 'deactivate' : 'activate'
              } this iteration?`}
              onConfirm={() => action(record, { action: 'editIterationStatus' })}
              okText="Yes"
              cancelText="No"
            >
              {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
              <Switch
                className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
                checked={record.status === 'active' ? true : false}
              />
            </Popconfirm>
          )}
          {userRole !== 'Trainer' && (
            // <Button
            //   disabled={userRole === 'Trainer' ? true : false}
            //   className="br-10 btn-edit d-flex"
            //   onClick={() => {
            //     action(record, { action: 'editIteration', data: idx });
            //   }}
            // >
            //   <VscEdit className="text-white m-auto" />
            // </Button>
            <Button
              style={{
                borderRadius: '10px',
                width: '30px',
                height: '30px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#6BBCB3'
              }}
              onClick={() => action(record, { action: 'editIteration', data: idx })}
            >
              <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
            </Button>
          )}
        </Space>
      )
    }
  ];

  return (
    <Spin tip="Loading..." spinning={loading}>
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Iteration Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Trainer' && (
            <Button
              style={{ padding: '0 4rem' }}
              className="br-10 btn-action d-flex w-100 h-80 px-2"
              onClick={() =>
                action({ subjectId: null, name: null, duration: null, status: null }, { action: 'addIteration' })
              }
            >
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Iteration</span>
            </Button>
          )}
        </Col>
      </Row>
      <hr className={'hr'} />
      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="px-1">
          <SelectSubject
            customClassName={'border-radius-select'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, subjectId: value };
              });
            }}
            mode={'single'}
            subjects={isEmpty(filter.subjectId) ? [] : filter.subjectId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'Select status'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, status: value };
              });
            }}
            query={{
              table: 'iterations',
              field: 'status'
            }}
            values={isEmpty(filter.status) ? [] : filter.status}
          />
        </Col>
        <Col span={4} className="pr-1">
          {/*<Input*/}
          {/*  value={filter.title}*/}
          {/*  placeholder="Input Iteration Name"*/}
          {/*  className="br-8"*/}
          {/*  onChange={e => {*/}
          {/*    setFilter(prev => {*/}
          {/*      return { ...prev, name: e.target.value };*/}
          {/*    });*/}
          {/*  }}*/}
          {/*/>*/}
        </Col>
        <Col offset={4} span={8} className="d-flex justify-content-between">
          <span
            className="br-10 btn-grey w-100 mx-1"
            style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', background: '#1e1e1e' }}
          >
            Total Weight {totalWeight || 0} %
          </span>
          <Button className="br-10 btn-grey w-100 mx-1" onClick={clearFilters}>
            Reset
          </Button>
          <Button className="br-10 btn-action w-100 mx-1" onClick={search}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        // loading={loading}
        columns={columns}
        rowKey={record => 'iteration_' + record.id}
        pagination={{
          className: 'border-radius-paging',
          position: 'bottomLeft',
          pageSize: pagination.current.limit,
          total: pagination.current.total,
          current: pagination.current.page,
          pageSizeOptions: [5, 10, 25, 100],
          onChange: (page, pageSize) => {
            pagination.current.page = page;
            getIterationList(filter);
          }
        }}
        dataSource={dataSource}
      />
      <ModalEditIteration
        isShowModal={isEditing}
        type={editIterationType.current}
        iteration={curIteration.current}
        action={action}
        search={search}
      />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        navigateTo={path.absIteration}
        text={messageText}
        iconname={messageType}
        type={errorType}
      />
    </Spin>
  );
};

export default IterationManage;
