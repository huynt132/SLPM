import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import functionManagementApi from 'src/api/functionManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getList = createAsyncThunk('function/getAll', payloadCreator(functionManagementApi.getFunctions));
export const editFunction = createAsyncThunk('function/edit', payloadCreator(functionManagementApi.editFunction));
export const addFunction = createAsyncThunk('function/add', payloadCreator(functionManagementApi.addFunction));
export const getAllFeatures = createAsyncThunk('feature/getList', payloadCreator(functionManagementApi.getAllFeatures));
export const getProjectLabel = createAsyncThunk(
  'function/getProjectLabel',
  payloadCreator(functionManagementApi.getProjectLabel)
);
export const getAllFunctionStatus = createAsyncThunk(
  'function/getListStatusFunction',
  payloadCreator(functionManagementApi.getAllFunctionStatus)
);
export const getAllFunctionType = createAsyncThunk(
  'function/getListTypeFunction',
  payloadCreator(functionManagementApi.getAllFunctionType)
);
export const getAllFunction = createAsyncThunk(
  'function/getList',
  payloadCreator(functionManagementApi.getAllFunctions)
);
export const importFunctions = createAsyncThunk(
  'function/import',
  payloadCreator(functionManagementApi.importFunctions)
);

export const getListFunction = createAsyncThunk(
  'function/getListFunction',
  payloadCreator(functionManagementApi.getListFunction)
);

const functionManagement = createSlice({
  name: 'functionManagement',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const functionManagementReducer = functionManagement.reducer;

export default functionManagementReducer;
