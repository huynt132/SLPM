// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Button, Space, Table, Row, Col, Spin } from 'antd';
// style
import { VscEdit } from 'react-icons/vsc';
import { AiOutlinePlus } from 'react-icons/ai';
import { RiFileExcel2Line } from 'react-icons/ri';
import { BiImport } from 'react-icons/bi';
// services
import { editFunction, getList } from './functionManagement.slice.js';
// custom component
import SelectClass from 'src/components/Select/SelectClass';
import SelectProject from 'src/components/Select/SelectProject';
import SelectSubject from 'src/components/Select/SelectSubject';
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import ModalEditFunction from '../components/ModalEditFunction/ModalEditFunction';
import ModalImportFunction from '../components/ModalImportFunction/ModalImportFunction';
import { path } from 'src/constants/path';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';
// import SelectFunctionStatus from '../../../../components/Select/SelectFunctionStatus';

const FunctionManagement = () => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [isImporting, setIsImporting] = useState(false);
  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [errorType, setErrorType] = useState('');
  const [messageType, setMessageType] = useState('');
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const curFunction = useRef({});
  const curFunctionIdx = useRef(0);
  const editFunctionType = useRef('add');
  const pagination = useRef({
    limit: 10,
    page: 1,
    total: 0
  });

  const [filter, setFilter] = useState({
    subjectId: null,
    projectId: null,
    featureId: null,
    complexityId: null,
    ownerId: null,
    getExcel: null,
    status: null
  });

  const userRole = useSelector(state => state.auth.role);

  useEffect(() => {
    getFunctionList(filter);
  }, [dispatch]);

  const getFunctionList = async paramSearch => {
    try {
      setLoading(true);
      const query = {
        ...paramSearch,
        ...pagination.current
      };
      let res = await dispatch(getList(query));
      res = unwrapResult(res);
      if (res.data.success) {
        if (query.getExcel) {
          query.getExcel = null;
          openInNewTab(res.data.data);
        } else {
          setDataSource(res.data.data);
          if (!isEmpty(res.data.curProjectId))
            setFilter(prev => {
              return { ...prev, projectId: res.data.curProjectId };
            });
          pagination.current.total = res.data.total;
        }
        showMessage(res.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  const openInNewTab = url => {
    const win = window.open(url, '_blank');
    win.focus();
  };

  const clearFilters = () => {
    pagination.current = {
      limit: 10,
      page: 1,
      total: 0
    };
    setFilter({
      subjectId: null,
      projectId: null,
      featureId: null,
      complexityId: null,
      ownerId: null,
      getExcel: null,
      status: null
    });
    getFunctionList(null);
  };

  const search = () => {
    pagination.current = {
      limit: 10,
      page: 1,
      total: 0
    };
    getFunctionList(filter);
  };

  async function editFunctionStatus() {
    if (isEmpty(curFunction.current)) return showMessage('Update data is invalid, please check again!', 'fail');
    try {
      setLoading(true);
      const res = await dispatch(
        editFunction({
          ...curFunction.current,
          status: curFunction.current.status === 'active' ? 'inactive' : 'active'
        })
      );
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        curFunction.current.status = curFunction.current.status === 'active' ? 'inactive' : 'active';
      }
    } catch (error) {
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  }

  const action = (rowData, action) => {
    if (!isEmpty(rowData)) curFunction.current = rowData;
    if (action.action === 'isHideModalEditFunction') {
      setIsEditing(false);
    }
    if (action.action === 'triggerSearch') {
      getFunctionList(filter);
    }
    if (action.action === 'editFunctionStatus') {
      editFunctionStatus();
    }
    if (action.action === 'editFunctionSuccess') {
      setDataSource(prevList => {
        let tmp = [...prevList];
        tmp[curFunctionIdx.current] = rowData;
        return [...tmp];
      });
      setIsEditing(false);
    }
    if (action.action === 'editFunction') {
      editFunctionType.current = 'Update';
      curFunctionIdx.current = action.data;
      setIsEditing(true);
    }
    if (action.action === 'addFunction') {
      editFunctionType.current = 'Add';
      setIsEditing(true);
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    // {
    //   title: 'Project code',
    //   dataIndex: 'projectCode',
    //   key: 'projectCode'
    // },
    {
      title: 'Function',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'Feature',
      dataIndex: 'featureName',
      key: 'featureName'
    },

    // {
    //   title: 'Description',
    //   dataIndex: 'description',
    //   key: 'description'
    // },
    {
      title: 'Priority',
      dataIndex: 'priority',
      className: 'w-5',
      key: 'priority'
    },
    {
      title: 'Type',
      key: 'typeName',
      dataIndex: 'typeName'
    },
    {
      title: 'Status',
      key: 'statusName',
      dataIndex: 'statusName'
    },
    {
      title: 'Submit status',
      className: 'w-10',
      key: 'status',
      dataIndex: 'status'
    },
    // {
    //   title: 'Created by',
    //   dataIndex: 'createdByUser',
    //   key: 'createdByUser'
    // },
    // {
    //   title: 'Created',
    //   dataIndex: 'created',
    //   key: 'created'
    // },
    {
      title: 'Action',
      key: 'action',
      className: 'w-10',
      render: (_, record, idx) => (
        <Space size="middle">
          {
            // <Button
            //   className="br-10 btn-edit d-flex"
            //   onClick={() => {
            //     action(record, { action: 'editFunction', data: idx });
            //   }}
            // >
            //   <VscEdit className="text-white m-auto" />
            // </Button>

            <Button
              style={{
                borderRadius: '10px',
                width: '30px',
                height: '30px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#6BBCB3'
              }}
              onClick={() => action(record, { action: 'editFunction', data: idx })}
            >
              <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
            </Button>
          }
        </Space>
      )
    }
  ];

  return (
    <Spin tip="Loading..." spinning={loading}>
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Function Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Trainer' && (
            <Button
              style={{ padding: '0 4rem' }}
              className="br-10 btn-action d-flex w-100 h-80 px-2"
              onClick={() =>
                action(
                  {
                    iterationId: null,
                    classId: null,
                    title: null,
                    description: null,
                    to: null,
                    from: null,
                    status: null
                  },
                  { action: 'addFunction' }
                )
              }
            >
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Function</span>
            </Button>
          )}
        </Col>
      </Row>
      {/*<hr className={'hr'} />*/}
      <Row className={'mt-2 mb-4'}>
        {/*<Col span={4} className="px-1">*/}
        {/*  <SelectSubject*/}
        {/*    customClassName={'border-radius-select'}*/}
        {/*    onOptionSelected={value => {*/}
        {/*      setFilter(prev => {*/}
        {/*        return { ...prev, subjectId: value };*/}
        {/*      });*/}
        {/*    }}*/}
        {/*    mode={'single'}*/}
        {/*    subjects={isEmpty(filter.subjectId) ? [] : filter.subjectId}*/}
        {/*  />*/}
        {/*</Col>*/}
        {/* <Col span={4} className="px-1">
          <SelectClass
            customClassName={'border-radius-select'}
            mode={'single'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, classId: value };
              });
            }}
            filter={filter}
            classes={isEmpty(filter.classId) ? [] : filter.classId}
          />
        </Col> */}
        <Col span={4} className="px-1">
          <SelectProject
            customClassName={'border-radius-select'}
            mode={'single'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, projectId: value };
              });
            }}
            projects={isEmpty(filter.projectId) ? [] : filter.projectId}
            // filter={filter}
          />
        </Col>
        <Col span={4} className="px-1">
          {/*<SelectFunctionStatus*/}
          {/*  customClassName={'border-radius-select'}*/}
          {/*  mode={'single'}*/}
          {/*  placeholder={'select status'}*/}
          {/*  onOptionSelected={value => {*/}
          {/*    setFilter(prev => {*/}
          {/*      return { ...prev, statusId: value };*/}
          {/*    });*/}
          {/*  }}*/}
          {/*  disabled={isEmpty(filter?.projectId)}*/}
          {/*  filter={{ projectId: filter?.projectId }}*/}
          {/*  functionStatus={isEmpty(filter.statusId) ? [] : filter.statusId}*/}
          {/*/>*/}
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            placeholder={'Select Submit Status'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, status: value };
              });
            }}
            query={{
              table: 'functions',
              field: 'status'
            }}
            mode={'single'}
            values={isEmpty(filter.status) ? [] : filter.status}
          />
        </Col>
        <Col offset={3} span={3}>
          {userRole !== 'Trainer' && (
            <Button
              className="br-10 btn-action d-flex mx-1"
              onClick={() => {
                setIsImporting(true);
              }}
            >
              <BiImport style={{ color: 'white', background: '#0d9caf' }} className="icon-plus m-auto mr-1" />
              <span className="m-auto"> Import Function</span>
            </Button>
          )}
        </Col>
        <Col span={6} className="d-flex justify-content-between">
          <Button
            className="br-10 btn-action d-flex mx-1"
            onClick={() => {
              getFunctionList({ ...filter, getExcel: true });
            }}
          >
            <RiFileExcel2Line style={{ color: '#069255' }} className="icon-plus m-auto mr-1" />
            <span className="m-auto"> Export Data</span>
          </Button>
          <Button className="br-10 btn-grey w-100 mx-1" onClick={clearFilters}>
            Reset
          </Button>
          <Button className="br-10 btn-action w-100 mx-1" onClick={search}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        // loading={loading}
        columns={columns}
        rowKey={record => 'function_' + record.id}
        dataSource={dataSource}
        pagination={{
          className: 'border-radius-paging',
          position: 'bottomLeft',
          pageSize: pagination.current.limit,
          total: pagination.current.total,
          current: pagination.current.page,
          onChange: (page, pageSize) => {
            pagination.current.page = page;
            getFunctionList(filter);
          },
          showSizeChanger: true
        }}
      />
      <ModalEditFunction
        isShowModal={isEditing}
        type={editFunctionType.current}
        projectFunction={curFunction.current}
        action={action}
      />
      <ModalImportFunction isShowModal={isImporting} setIsShowModal={setIsImporting} action={action} />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        navigateTo={path.absFunction}
        text={messageText}
        iconname={messageType}
        type={errorType}
      />
    </Spin>
  );
};

export default FunctionManagement;
