// services
import { getLocEvaluations, getStudentEvaluation, updateBonus } from './StudentEvaluation.slice';
import { editIndividualEvaluation } from '../MilestoneSubmit/MilestoneSubmit.slice';

// REACT
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Collapse, Button, Space, Table, Tag, Row, Col, Input, Tabs, Spin } from 'antd';
// icon
import { IoMdArrowBack } from 'react-icons/io';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import ModalLocEvaluation from '../components/ModalLocEvaluation/ModalLocEvaluation';
import { path } from '../../../../constants/path';
import { useLocation, useNavigate } from 'react-router-dom';
import { set } from 'object-path-immutable';

const { Panel } = Collapse;
const { TabPane } = Tabs;
const { TextArea } = Input;
const StudentEvaluation = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [errorType, setErrorType] = useState('');
  const [dataLocEvaluation, setDataLocEvaluation] = useState([]);
  const [dataStudentEvaluation, setDataStudentEvaluation] = useState([]);
  const [isEvaluating, setIsEvaluating] = useState(false);
  const [evaluateProps, setEvaluateProps] = useState({
    functionId: null
  });

  const userRole = useSelector(state => state.auth.role);

  useEffect(() => {
    getStudentEvaluations();
  }, [dispatch]);

  const { state } = useLocation();

  const navigate = useNavigate();

  const [filter, setFilter] = useState({
    title: null,
    iterationId: null,
    classId: null,
    status: null
  });
  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'Function',
      dataIndex: 'functionName',
      key: 'functionName'
    },
    {
      title: 'Feature',
      dataIndex: 'featureName',
      key: 'featureName'
    },
    {
      title: 'Complexity',
      dataIndex: 'complexityTitle',
      key: 'complexityTitle'
    },
    {
      title: 'quality',
      dataIndex: 'qualityTitle',
      key: 'qualityTitle'
    },
    {
      title: 'LOC',
      dataIndex: 'convertedLoc',
      key: 'convertedLoc'
    },
    {
      title: 'Action',
      key: 'action',
      className: 'w-10',
      render: (_, record, idx) => {
        return (
          userRole === 'Trainer' &&
          record.status === 'submitted' && (
            <Space size="middle">
              <Button
                className="br-10 btn-edit d-flex"
                onClick={() => {
                  action(record, { action: 'evaluateTracking', data: idx });
                }}
              >
                <VscEdit className="text-white m-auto" />
              </Button>
            </Space>
          )
        );
      }
    }
  ];

  const action = (rowData, action) => {
    if (action.action === 'evaluateTracking') {
      setEvaluateProps(prev => {
        return { ...prev, functionId: rowData.functionId };
      });
      setIsEvaluating(true);
    }
    if (action.action === 'isHideModalLocEvaluation') {
      setIsEvaluating(false);
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
  };

  const getLocEvaluation = async filters => {
    try {
      setLoading(true);
      let query = {
        userId: state.data.studentId,
        projectId: state.data.projectId,
        milestoneId: state.data.milestoneId
      };
      let res = await dispatch(getLocEvaluations(query));
      res = unwrapResult(res);
      if (res.data.success) {
        setDataLocEvaluation(res.data.data);
        showMessage(res.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataLocEvaluation([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  const getStudentEvaluations = async filters => {
    try {
      setLoading(true);
      let query = {
        userId: state.data.studentId,
        projectId: state.data.projectId,
        milestoneId: state.data.milestoneId
      };
      let res = await dispatch(getStudentEvaluation(query));
      res = unwrapResult(res);
      if (res.data.success) {
        setDataStudentEvaluation(res.data.data);
        showMessage(res.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataStudentEvaluation([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
      }
    } finally {
      setLoading(false);
    }
  };

  const editMemberEvaluation = async (memberEvaluation, memberEvalIndex) => {
    try {
      if (isEmpty(memberEvaluation.grade) || memberEvaluation.grade < 0 || memberEvaluation.grade > 10) {
        return action(null, {
          action: 'showMessage',
          data: {
            description: 'Grade can not be empty and must be in range of 0 to 10!',
            type: 'fail'
          }
        });
      }
      setLoading(true);
      const res = await dispatch(editIndividualEvaluation({ ...memberEvaluation, projectId: state.data.projectId }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        getStudentEvaluations();
        // setDataStudentEvaluation(set(dataStudentEvaluation, ['memberEvaluations', memberEvalIndex], result.data.data));
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  };

  const updateBonusGrade = async studentBonus => {
    try {
      if (!studentBonus?.bonus || studentBonus.bonus < 0 || studentBonus.bonus > 10) {
        return action(null, {
          action: 'showMessage',
          data: {
            description: 'Bonus can not be empty and must be in range of 0 to 10!',
            type: 'fail'
          }
        });
      }
      setLoading(true);
      const res = await dispatch(updateBonus(studentBonus));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        getStudentEvaluations();
        // setDataStudentEvaluation(set(dataStudentEvaluation, ['memberEvaluations', memberEvalIndex], result.data.data));
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  };

  const onChange = key => {
    if (+key === 1) {
      // evaluation
      getStudentEvaluations();
    }
    if (+key === 2) {
      // loc evaluation
      getLocEvaluation();
    }
  };

  const renderTeamEvaluation = () => {
    if (isEmpty(dataStudentEvaluation.teamEvaluations)) return <></>;
    const result = dataStudentEvaluation.teamEvaluations.map((evaluation, index) =>
      isEmpty(evaluation?.id) ? (
        <></>
      ) : (
        <div key={`teamEvaluations_${index}`}>
          <Row className={'mb-1 mx-4'}>
            <Col span={16}>
              <span>
                {evaluation.criteriaName} ({evaluation.weight * 100} %)
              </span>
            </Col>
            <Col offset={2} span={2} className={'d-flex'}>
              <b className={'mt-1'}>Grade/10:</b>
              <Input className="br-8 ml-1" disabled={true} value={evaluation.grade}></Input>
            </Col>
          </Row>
          <Row className={'mb-2 mx-4'}>
            <TextArea
              className={'text-black br-8'}
              disabled={true}
              value={evaluation?.comment}
              rows={2}
              placeholder={`Grading comment for ${evaluation.criteriaName}`}
            />
          </Row>
        </div>
      )
    );
    return <>{result}</>;
  };

  const renderMemberEvaluation = () => {
    if (isEmpty(dataStudentEvaluation.memberEvaluations)) return <></>;
    const result = dataStudentEvaluation.memberEvaluations.map((evaluation, index) => (
      <div key={`memberEvaluations_${index}`}>
        <Row className={'mb-1 mx-4'}>
          <Col span={16}>
            <span>
              {evaluation.criteriaName} ({evaluation.criteriaWeight * 100} %)
            </span>
          </Col>
          <Col offset={2} span={2} className={'d-flex'}>
            <b className={'mt-1'}>Grade/10:</b>
            <Input
              className="br-8 ml-1"
              disabled={evaluation?.canEdit !== true || userRole !== 'Trainer'}
              value={evaluation.grade}
              onChange={e => {
                setDataStudentEvaluation(
                  set(dataStudentEvaluation, ['memberEvaluations', index, 'grade'], e.target.value)
                );
              }}
            ></Input>
          </Col>
          <Col offset={2} span={2}>
            {evaluation?.canEdit === true && userRole === 'Trainer' && (
              <Button onClick={() => editMemberEvaluation(evaluation, index)} className={'br-10 w-100'}>
                Save
              </Button>
            )}
          </Col>
        </Row>
        <Row className={'mb-2 mx-4'}>
          <TextArea
            className={'text-black br-8'}
            disabled={evaluation?.canEdit !== true || userRole !== 'Trainer'}
            value={evaluation?.comment}
            rows={2}
            onChange={e => {
              setDataStudentEvaluation(
                set(dataStudentEvaluation, ['memberEvaluations', index, 'comment'], e.target.value)
              );
            }}
            placeholder={`Grading comment for ${evaluation.criteriaName}`}
          />
        </Row>
      </div>
    ));
    return <>{result}</>;
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <Row>
        <Col span={1}>
          <IoMdArrowBack
            className={'font-4re cursor-pointer'}
            onClick={() => {
              navigate(-1);
            }}
          />
        </Col>
        <Col span={20}>
          <h1 className={'m-0'}>Student Evaluation</h1>
        </Col>
        <Col span={24} className={'mb-2'}>
          <h2
            className={'m-0'}
          >{`Student: ${state.data.student} (${state.data.rollNumber}), ${state.data.project}, ${state.data.milestoneName}`}</h2>
        </Col>
      </Row>
      <Tabs onChange={onChange} type="card">
        <TabPane tab="Evaluations" key="1">
          <h3>Total Evaluation: {dataStudentEvaluation?.iterationEvaluation?.totalGrade || 0}/10, In which:</h3>
          <Collapse
            key={`collapse_evaluation`}
            // collapsible={statusDisableCollapse.includes(milestone.submitStatus) ? 'disabled' : 'header'}
            className={'w-100'}
          >
            <Panel
              extra={<></>}
              key={`Panel_team_evaluation}`}
              className={'w-100 collapse-header-90 collapse-header-extra-10 collapse-header-extra-flex'}
              header={
                <Row className={'w-100'}>
                  <Col className={'font-weight-bold'} span={14}>
                    {'Team Evaluation'}
                  </Col>
                  <Col offset={6} span={3}>
                    {`${dataStudentEvaluation?.iterationEvaluation?.teamEvalGrade || 0}/${
                      isEmpty(dataStudentEvaluation?.teamEvaluations) ? 0 : 10
                    }`}
                  </Col>
                </Row>
              }
            >
              {renderTeamEvaluation()}
            </Panel>
            <Panel
              extra={<></>}
              key={`Panel_individual_evaluation}`}
              className={'w-100 collapse-header-90 collapse-header-extra-10 collapse-header-extra-flex'}
              header={
                <Row className={'w-100'}>
                  <Col className={'font-weight-bold'} span={14}>
                    {'Individual Evaluation'}
                  </Col>
                  <Col offset={6} span={3}>
                    {`${dataStudentEvaluation?.iterationEvaluation?.individualEval || 0}/${
                      isEmpty(dataStudentEvaluation?.memberEvaluations) ? 0 : 10
                    }`}
                  </Col>
                </Row>
              }
            >
              {renderMemberEvaluation()}
            </Panel>
            <Panel
              // collapsible={"disabled"}
              key={`Panel_loc_evaluation}`}
              className={'w-100 collapse-header-80 collapse-header-extra-28 collapse-header-extra-flex'}
              extra={
                <>
                  <Row>
                    <Col span={4}>
                      <b>Bonus</b>
                    </Col>
                    <Col span={4}>
                      <Input
                        value={dataStudentEvaluation?.locEvaluation?.bonus}
                        placeholder="Bonus"
                        className="br-8"
                        onChange={e => {
                          setDataStudentEvaluation(
                            set(dataStudentEvaluation, ['locEvaluation', 'bonus'], e.target.value)
                          );
                        }}
                      />
                    </Col>
                    <Col offset={4} span={4}>
                      {dataStudentEvaluation?.canBonus === true && userRole === 'Trainer' && (
                        <Button
                          onClick={() => updateBonusGrade(dataStudentEvaluation?.locEvaluation)}
                          className={'br-10'}
                        >
                          Save
                        </Button>
                      )}
                    </Col>
                  </Row>
                </>
              }
              header={
                <>
                  <Row className={'w-100'}>
                    <Col className={'font-weight-bold'} span={16}>
                      {`LOC Evaluation (${dataStudentEvaluation?.locEvaluation?.demoWeight || 0}%)`}
                    </Col>
                  </Row>
                  <Row className={'w-100'}>
                    <Col span={4}>
                      <Input
                        value={dataStudentEvaluation?.locEvaluation?.demoGrade || 0}
                        placeholder="Demo Grade"
                        className="br-8"
                        disabled={true}
                      />
                    </Col>
                    <Col offset={1} span={4}>
                      <span className={'m-auto'}>
                        {dataStudentEvaluation?.functionCount?.sumLocEvaluation || 0} LOC (
                        {dataStudentEvaluation?.functionCount?.totalFunction} functions)
                      </span>
                    </Col>
                  </Row>
                </>
              }
            ></Panel>
          </Collapse>
        </TabPane>
        <TabPane tab="Loc evaluation" key="2">
          <Table
            // loading={loading}
            columns={columns}
            rowKey={record => 'loc_evaluation_' + record.id}
            dataSource={dataLocEvaluation}
          />
        </TabPane>
      </Tabs>
      <ModalLocEvaluation isShowModal={isEvaluating} action={action} props={evaluateProps} />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        text={messageText}
        iconname={messageType}
        type={errorType}
      />
    </Spin>
  );
};
export default StudentEvaluation;
