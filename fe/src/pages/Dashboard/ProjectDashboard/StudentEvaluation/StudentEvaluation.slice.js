import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import evaluationManagementApi from 'src/api/evaluationManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getLocEvaluations = createAsyncThunk(
  'locEvaluations/getList',
  payloadCreator(evaluationManagementApi.getLocEvaluations)
);
export const getStudentEvaluation = createAsyncThunk(
  'locEvaluations/getStudentEvaluation',
  payloadCreator(evaluationManagementApi.getStudentEvaluation)
);
export const updateBonus = createAsyncThunk(
  'evaluation/updateBonus',
  payloadCreator(evaluationManagementApi.updateBonus)
);
// export const getFunctionEvaluate = createAsyncThunk(
//   'evaluations/getFunctionEvaluate',
//   payloadCreator(evaluationManagementApi.getFunctionEvaluate)
// );
// export const getEvaluateOptions = createAsyncThunk(
//   'classSetting/getOptions',
//   payloadCreator(evaluationManagementApi.getEvaluateOptions)
// );
// export const editTeamEval = createAsyncThunk('evaluations/editTeamEval', payloadCreator(evaluationManagementApi.editTeamEval));
// export const updateStatus = createAsyncThunk('evaluations/updateStatus', payloadCreator(evaluationManagementApi.updateStatus));
// export const locEvaluate = createAsyncThunk('evaluations/locEvaluate', payloadCreator(evaluationManagementApi.locEvaluate));

const evaluationManagement = createSlice({
  name: 'evaluationManagement',
  initialState: {},
  reducers: {},
  extraReducers: {}
});

const evaluationManagementReducer = evaluationManagement.reducer;

export default evaluationManagementReducer;
