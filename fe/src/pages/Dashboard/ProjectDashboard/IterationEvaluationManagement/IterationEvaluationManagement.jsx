import React, { useEffect, useState } from 'react';
import { Button, Table, Row, Col, Input, Tag } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { path } from 'src/constants/path';
import { BiImport } from 'react-icons/bi';
import SelectProject from 'src/components/Select_DA/ProjectSelect/SelectProject';
import SelectMilestone from 'src/components/Select_DA/MilestoneSelect/SelectMilestone';
import { getIterationEvaluation } from './iterationEvaluation.slice';
import { useLocation, useNavigate } from 'react-router-dom';
import { IoMdArrowBack } from 'react-icons/io';

export default function IterationEvaluationManagement() {
  const dispatch = useDispatch();
  const { state } = useLocation();
  const [paramsSearch, setParamsSearch] = useState({
    classId: state.classId,
    // iterationId: state?.iterId,
    projectId: null
    // milestoneId: 22
    // limit: 8,
    // page: 1,
    // total: 200
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  // data source table
  const [dataSource, setDataSource] = useState([]);

  const [currentData, setCurrentData] = useState({});

  const [errorType, setErrorType] = useState('');

  const navigate = useNavigate();

  const columns = [
    {
      title: 'Roll Number',
      dataIndex: 'rollNumber',
      key: 'rollNumber'
    },
    {
      title: 'Student',
      dataIndex: 'student',
      key: 'student'
    },
    {
      title: 'Team',
      dataIndex: 'projectName',
      key: 'projectName'
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total'
    },
    {
      title: 'Team Evaluation',
      dataIndex: 'teamEval',
      key: 'teamEval'
    },
    {
      title: 'Individual Evaluation',
      dataIndex: 'individualEval',
      key: 'individualEval'
    },
    {
      title: 'LOC Evaluation',
      dataIndex: 'locEval',
      key: 'locEval'
    },
    {
      title: 'Bonus',
      dataIndex: 'bonus',
      key: 'bonus'
    }
    // {
    //   title: 'Edit',
    //   dataIndex: 'edit',
    //   key: 'edit',
    //   render: (_, record) => (
    //     <Button
    //       style={{
    //         borderRadius: '10px',
    //         border: 'none',
    //         display: 'flex',
    //         flexDirection: 'column',
    //         alignItems: 'center',
    //         justifyContent: 'center',
    //         color: '#fff',
    //         background: '#6BBCB3'
    //       }}
    //     >
    //       Edit
    //     </Button>
    //   )
    // }
  ];

  // get api list iteration evaluation
  const getListIterationEval = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getIterationEvaluation({ params }));
      const res = unwrapResult(data);
      setCurrentData(prevState => {
        return { ...prevState, currentMilestone: res.data.curMilestone, currentProject: res.data.curMilestone };
      });
      const dataTable = res.data.data.iteration_evaluations.map(item => {
        return {
          key: item.id,
          rollNumber: item.rollNumber,
          studentId: item.studentId,
          student: item.fullName,
          projectId: item.projectId,
          projectName: item.projectName,
          total: item.totalGrade,
          teamEval: item.teamEvalGrade,
          // individualEvalId: item.memberEvalId,
          individualEval: item.individualEval,
          locEval: item.memberEvalLoc,
          bonus: item.bonus
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setDataSource(dataTable);
      setLoading(false);
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowModal(true);
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setModalIcon('fail');
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  // call api
  useEffect(() => {
    getListIterationEval(paramsSearch);
  }, [dispatch]);

  const handleSearch = () => {
    getListIterationEval({
      ...paramsSearch,
      page: 1
    });
  };

  // handle pagination as antd
  const handleTableChange = value => {
    getListIterationEval({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  // handle reset filter
  const [selectedValuesProject, setSelectedValuesProject] = useState();
  const [selectedValuesClass, setSelectedValuesClass] = useState();
  const [selectedValuesMilestone, setSelectedValuesMilestone] = useState();

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        code: null,
        trainerId: null,
        subjectId: null,
        subjectCode: null,
        year: null,
        term: null,
        status: null,
        limit: 8,
        page: 1
      };
    });
    setSelectedValuesProject([]);
    setSelectedValuesClass([]);
    setSelectedValuesMilestone([]);
  };

  return (
    <div>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absClass}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />
      <Row>
        <Col span={1}>
          <IoMdArrowBack
            className={'font-4re cursor-pointer'}
            onClick={() => {
              navigate(-1);
            }}
          />
        </Col>
        <Col span={18}>
          <h1 className={'m-0'}> Iteration Evaluation Management</h1>
        </Col>
      </Row>

      <Row className={'mt-2 mb-4'}>
        {/* <Col span={4} className="pr-1">
          <SelectClass
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectedValuesClass={selectedValuesClass}
            setSelectedValuesClass={setSelectedValuesClass}
          />
        </Col> */}
        <Col span={4} className="px-1">
          <SelectProject
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectedValuesProject={selectedValuesProject}
            setSelectedValuesProject={setSelectedValuesProject}
            classId={state.classId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectMilestone
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectedValuesMilestone={selectedValuesMilestone}
            setSelectedValuesMilestone={setSelectedValuesMilestone}
            currentData={currentData}
          />
        </Col>
        <Col span={4} className="px-1"></Col>
        <Col span={4} className="px-1"></Col>

        <Col offset={4} span={4} className="d-flex justify-content-between">
          {/* <Button
            className="br-10 btn-action d-flex mx-1"
            onClick={() => {
              setIsImporting(true);
            }}
          >
            <BiImport style={{ color: 'white', background: '#0d9caf' }} className="icon-plus m-auto mr-1" />
            <span className="m-auto"> Import iteration</span>
          </Button> */}
          <Button className="br-10 btn-grey w-100 mx-1" onClick={handleResetFilter}>
            Reset
          </Button>
          <Button className="br-10 btn-action w-100 mx-1" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>

      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total
        }}
        loading={loading}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />
    </div>
  );
}
