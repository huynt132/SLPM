import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import iterationEvalApi from 'src/api/iterationEval.api';
import { payloadCreator } from 'src/utils/helper';

export const getIterationEvaluation = createAsyncThunk(
  'iteration/iteration_evaluation',
  payloadCreator(iterationEvalApi.getIterationEvaluation)
);

const iterationEvaluation = createSlice({
  name: 'iterationEvaluation',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const iterationEvaluationReducer = iterationEvaluation.reducer;

export default iterationEvaluationReducer;
