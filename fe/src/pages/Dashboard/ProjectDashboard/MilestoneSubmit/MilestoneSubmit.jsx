// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Button, Collapse, Row, Col, Input, Spin, Empty } from 'antd';
import { set } from 'object-path-immutable';

// services
import { getMilestoneSubmits, editTeamEval, updateStatus } from './MilestoneSubmit.slice';

// custom component
import SelectClass from 'src/components/Select/SelectClass';
import SelectProject from 'src/components/Select/SelectProject';
import SelectMilestone from 'src/components/Select/SelectMilestone';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty, openInNewTab } from 'src/utils/helper';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';
import ModalSubmitTracking from '../components/ModalSubmitTracking/ModalSubmitTracking';
import { ImDownload3 } from 'react-icons/im';

const { TextArea } = Input;
const { Panel } = Collapse;
const SubmitManagement = () => {
  const dispatch = useDispatch();
  const userRole = useSelector(state => state.auth.role);
  const [loading, setLoading] = useState(false);
  const [submitTracking, setSubmitTracking] = useState(false);
  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [dataSource, setDataSource] = useState([]);
  const [errorType, setErrorType] = useState('');

  const [filter, setFilter] = useState({
    classId: null,
    projectId: [],
    milestoneId: null
  });

  const [submitTrackingProps, setSubmitTrackingProps] = useState({
    projectId: null,
    milestoneId: null
  });

  useEffect(() => {
    getMilestoneSubmitList(filter);
  }, []);

  const validateSearch = () => {
    if (isEmpty(filter.classId)) return 'Please select class!';
    if (isEmpty(filter.milestoneId)) return 'Please select milestone!';
    return null;
  };

  const statusDisableCollapse = ['pending', 'committed'];

  const getMilestoneSubmitList = async filters => {
    try {
      setLoading(true);
      setDataSource([]);
      const params = {
        ...filters,
        projectId: filters.projectId
      };
      const res = await dispatch(getMilestoneSubmits(params));
      const result = unwrapResult(res);
      if (result.data.success) {
        setDataSource(result.data.data.milestoneSubmits);
        setFilter(prev => {
          return { ...prev, milestoneId: result.data.data.curMilestoneId, classId: result.data.data.curClassId };
        });
        showMessage(result.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  const editTeamEvaluation = async (teamEvaluation, milestoneIndex, teamEvalIndex) => {
    try {
      if (isEmpty(teamEvaluation.grade) || teamEvaluation.grade < 0 || teamEvaluation.grade > 10) {
        return action(null, {
          action: 'showMessage',
          data: {
            description: 'Grade can not be empty and must be in range of 0 to 10!',
            type: 'fail'
          }
        });
      }
      setLoading(true);
      const res = await dispatch(editTeamEval({ ...teamEvaluation, milestoneId: filter.milestoneId }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        set(dataSource, [milestoneIndex, 'teamEvaluations', teamEvalIndex], result.data.data);
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  };

  const action = (rowData, action) => {
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
    if (action.action === 'triggerSearch') {
      setSubmitTracking(false);
      getMilestoneSubmitList(filter);
    }
  };

  const render = () => {
    if (isEmpty(dataSource) && loading === false) {
      return (
        <Col span={24} className={'m-auto'}>
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        </Col>
      );
    }
    const data = dataSource.map((milestone, index) => (
      // Only do this if items have no stable IDs
      <Collapse
        key={`collapse_${milestone.projectId}`}
        collapsible={statusDisableCollapse.includes(milestone.submitStatus) ? 'disabled' : 'header'}
        className={'w-100'}
      >
        <Panel
          key={`Panel_${milestone.projectId}`}
          extra={actionButtonRender(milestone)}
          className={'collapse-header-90 collapse-header-extra-10 collapse-header-extra-flex'}
          header={
            <Row className={'w-100'}>
              <Col
                className={'font-weight-bold'}
                span={12}
              >{`${milestone.projectName} (${milestone.submitStatus})`}</Col>
              <Col offset={8} span={3}>
                {functionCountRender(milestone)}
              </Col>
            </Row>
          }
        >
          {!isEmpty(milestone?.submitDocument) && (
            <Row className={'mb-2 mx-4'}>
              <span
                className={'cursor-pointer'}
                style={{ color: '#1e70cd', textDecoration: 'underline' }}
                onClick={() => openInNewTab(milestone?.submitDocument)}
              >
                <ImDownload3 className="m-auto" style={{ marginLeft: 0, marginRight: '1rem' }} />
                <b>Download submitted document file</b>
              </span>
            </Row>
          )}
          {renderTeamEvaluation(milestone, index)}
        </Panel>
      </Collapse>
    ));
    return <>{data}</>;
  };

  const renderTeamEvaluation = (milestone, milestoneIndex) => {
    if (isEmpty(milestone.teamEvaluations) || statusDisableCollapse.includes(milestone.submitStatus)) return null;
    const result = milestone.teamEvaluations.map((evaluation, index) => (
      <div key={`teamEvaluations_${index}`}>
        <Row className={'mb-1 mx-4'}>
          <Col span={16}>
            <span>
              {evaluation.criteriaName} ({evaluation.weight * 100} %)
            </span>
          </Col>
          <Col span={2} className={'d-flex'}>
            <b className={'mt-1'}>Grade/10:</b>
            <Input
              className="br-8 ml-1"
              disabled={milestone.submitStatus === 'evaluated' || userRole !== 'Trainer'}
              value={evaluation.grade}
              onChange={e => {
                setDataSource(set(dataSource, [milestoneIndex, 'teamEvaluations', index, 'grade'], e.target.value));
              }}
            ></Input>
          </Col>
          <Col offset={4} span={2}>
            {milestone.submitStatus !== 'evaluated' && userRole === 'Trainer' && (
              <Button onClick={() => editTeamEvaluation(evaluation, milestoneIndex, index)} className={'br-10 w-100'}>
                Save
              </Button>
            )}
          </Col>
        </Row>
        <Row className={'mb-2 mx-4'}>
          <TextArea
            className={'text-black br-8'}
            disabled={milestone.submitStatus === 'evaluated' || userRole !== 'Trainer'}
            value={evaluation?.comment}
            rows={2}
            placeholder={`Grading comment for ${evaluation.criteriaName}`}
            onChange={e => {
              setDataSource(set(dataSource, [milestoneIndex, 'teamEvaluations', index, 'comment'], e.target.value));
            }}
          />
        </Row>
      </div>
    ));
    return <>{result}</>;
  };

  const functionCountRender = milestone => {
    let result = '';
    switch (milestone.submitStatus) {
      case 'evaluated': {
        result = `${milestone.totalEvaluatedFunctions || 0}/${milestone.totalSubmitEvalFunctions || 0} functions ${
          milestone.submitStatus
        }`;
        break;
      }
      case 'submitted': {
        result = `${milestone.totalSubmittedFunctions || 0} functions ${milestone.submitStatus}`;
        break;
      }
      case 'pending': {
        result = `${milestone.totalPendingFunctions || 0} functions selected`;
        break;
      }
      case 'committed': {
        result = `${milestone.totalCommittedFunctions || 0}/${milestone.totalPlanCommitFunctions || 0} functions ${
          milestone.submitStatus
        }`;
        break;
      }
    }
    return result;
  };

  const actionButtonRender = milestone => {
    let result = '';
    switch (milestone.submitStatus) {
      case 'pending': {
        result = (
          <Button className={'br-10 w-100'} onClick={() => changeSubmitStatus(milestone)}>
            Commit
          </Button>
        );
        break;
      }
      case 'committed': {
        result = (
          <Button className={'br-10 w-100'} onClick={() => handleSubmit(milestone)}>
            Submit
          </Button>
        );
        break;
      }
      case 'submitted': {
        result = userRole.toUpperCase() === 'TRAINER' && (
          <Button className={'br-10 w-100'} onClick={() => changeSubmitStatus(milestone)}>
            Fix Evaluating
          </Button>
        );
        break;
      }
      case 'evaluated': {
        result = <b className={'m-auto'}>{`${milestone?.teamGrade || 0}/10`}</b>;
        break;
      }
    }
    return result;
  };

  const handleSubmit = milestone => {
    setSubmitTrackingProps(prev => {
      return { ...prev, projectId: milestone.projectId, milestoneId: milestone.milestoneId };
    });
    setSubmitTracking(true);
  };

  const search = () => {
    let errorMsg = validateSearch();
    if (!isEmpty(errorMsg)) {
      return action(null, {
        action: 'showMessage',
        data: {
          description: errorMsg,
          type: 'fail'
        }
      });
    }

    getMilestoneSubmitList(filter);
  };

  const changeSubmitStatus = async milestone => {
    try {
      setLoading(true);
      const data = {
        projectId: milestone.projectId,
        milestoneId: milestone.milestoneId
      };
      const res = await dispatch(updateStatus(data));
      const result = unwrapResult(res);
      if (result.data.success) {
        setDataSource(result.data.data);
        showMessage(result.data.message);
      }
      search();
    } catch (error) {
      setDataSource([]);
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  };
  return (
    <Spin tip="Loading..." spinning={loading}>
      <h2 className={'font-weight-bold'}>Milestone Submits</h2>
      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="pr-1">
          <SelectClass
            mode={'single'}
            placeholder={'Select class'}
            customClassName={'border-radius-select'}
            classes={isEmpty(filter.classId) ? [] : filter.classId}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, classId: value, projectId: null, milestoneId: null };
              }); // Auto xóa milestone nếu user đổi classId classId
            }}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectProject
            customClassName={'border-radius-select'}
            filter={filter}
            mode={'single'}
            projects={isEmpty(filter.projectId) ? [] : filter.projectId}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, projectId: value, milestoneId: null };
              });
            }}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectMilestone
            customClassName={'border-radius-select'}
            filter={filter}
            mode={'single'}
            milestones={isEmpty(filter.milestoneId) ? [] : filter.milestoneId}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, milestoneId: value };
              });
            }}
          />
        </Col>
        <Col span={2}>
          <Button
            className="br-10 btn-action w-100 mx-1"
            onClick={() => {
              search();
            }}
          >
            Search
          </Button>
        </Col>
      </Row>
      <Row>{render()}</Row>
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        navigateTo={null}
        text={messageText}
        iconname={messageType}
        type={errorType}
      />
      <ModalSubmitTracking
        props={submitTrackingProps}
        isShowModal={submitTracking}
        setIsShowModal={setSubmitTracking}
        action={action}
      />
    </Spin>
  );
};

export default SubmitManagement;
