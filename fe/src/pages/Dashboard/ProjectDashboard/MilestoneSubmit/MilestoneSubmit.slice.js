import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import submitManagementApi from 'src/api/submitManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getMilestoneSubmits = createAsyncThunk(
  'submits/getMilestoneSubmits',
  payloadCreator(submitManagementApi.getMilestoneSubmits)
);
export const getFunctionEvaluate = createAsyncThunk(
  'submits/getFunctionEvaluate',
  payloadCreator(submitManagementApi.getFunctionEvaluate)
);
export const getNewLocFunction = createAsyncThunk(
  'submits/getNewLocFunction',
  payloadCreator(submitManagementApi.getNewLocFunction)
);
export const getEvaluateOptions = createAsyncThunk(
  'classSetting/getOptions',
  payloadCreator(submitManagementApi.getEvaluateOptions)
);
export const getFunctionCount = createAsyncThunk(
  'submits/getFunctionCount',
  payloadCreator(submitManagementApi.getFunctionCount)
);
export const editTeamEval = createAsyncThunk('submits/editTeamEval', payloadCreator(submitManagementApi.editTeamEval));
export const editIndividualEvaluation = createAsyncThunk(
  'submits/editIndividualEvaluation',
  payloadCreator(submitManagementApi.editIndividualEvaluation)
);
export const updateStatus = createAsyncThunk('submits/updateStatus', payloadCreator(submitManagementApi.updateStatus));
export const locEvaluate = createAsyncThunk('submits/locEvaluate', payloadCreator(submitManagementApi.locEvaluate));

const submitManagement = createSlice({
  name: 'submitManagement',
  initialState: {},
  reducers: {},
  extraReducers: {}
});

const submitManagementReducer = submitManagement.reducer;

export default submitManagementReducer;
