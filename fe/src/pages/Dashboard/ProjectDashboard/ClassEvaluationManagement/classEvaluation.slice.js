import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import classEvalApi from 'src/api/classEval.api';
import { payloadCreator } from 'src/utils/helper';

export const getClassEvaluation = createAsyncThunk(
  'class/classEvaluation',
  payloadCreator(classEvalApi.getClassEvaluation)
);
export const exportClassEvaluation = createAsyncThunk(
  'class/classEvaluation/export',
  payloadCreator(classEvalApi.exportClassEvaluation)
);

const classEvaluation = createSlice({
  name: 'classEvaluation',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const classEvaluationReducer = classEvaluation.reducer;

export default classEvaluationReducer;
