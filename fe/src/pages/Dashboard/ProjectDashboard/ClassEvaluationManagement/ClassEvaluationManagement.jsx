import React, { useEffect, useState, useRef } from 'react';
import { Button, Space, Table, Row, Col, Spin } from 'antd';

import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { path } from 'src/constants/path';
import { RiFileExcel2Line } from 'react-icons/ri';
import { useLocation, useNavigate } from 'react-router-dom';
import { getClassEvaluation, exportClassEvaluation } from './classEvaluation.slice';
import { IoMdArrowBack } from 'react-icons/io';
import { set } from 'object-path-immutable';
import { openInNewTab } from '../../../../utils/helper';

export default function ClassEvaluationManagement({ ...props }) {
  const dispatch = useDispatch();

  const { state } = useLocation();

  const [paramsSearch, setParamsSearch] = useState({
    classId: state.classId,
    // limit: 8,
    // page: 1,
    total: 0
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  const [errorType, setErrorType] = useState('');

  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  // data source table
  const [dataSource, setDataSource] = useState([]);

  const navigate = useNavigate();

  const columnRender = useRef([
    {
      title: 'Roll Number',
      dataIndex: 'rollNumber',
      key: 'rollNumber'
    },
    {
      title: 'Student',
      dataIndex: 'student',
      key: 'student'
    },
    {
      title: 'Project',
      dataIndex: 'project',
      key: 'project'
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
      className: 'grade'
    },
    {
      title: 'ongoing Mark',
      dataIndex: 'og',
      key: 'og'
    }
  ]);

  const [isRendering, setIsRendering] = useState(false);

  // get api list all Class
  const getListClassEval = async params => {
    try {
      setLoading(true);
      setIsRendering(true);
      columnRender.current.length = 4; //reset về fixed value

      const data = await dispatch(getClassEvaluation({ params }));
      const res = unwrapResult(data);
      columnRender.current = columnRender.current.concat(
        res.data.data.listIterationEvalDTOS.map(item => {
          return {
            title: item?.iterationName,
            dataIndex: item?.key,
            key: item.key,
            render: (_, data) => (
              <Space size="middle" className={'d-flex justify-content-between'}>
                <span className={'grade'}>{data[item.key]}</span>
                <Button
                  style={{
                    borderRadius: '10px',
                    border: 'none',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    color: '#fff',
                    background: '#008055'
                  }}
                  onClick={() => {
                    navigate(path.studentEvaluationAdm, {
                      state: {
                        data: {
                          studentId: data.studentId,
                          projectId: data.projectId,
                          milestoneId: item.milestoneId,
                          student: data.studentName,
                          rollNumber: data.rollNumber,
                          project: data.projectName,
                          milestoneName: item.iterationName
                        }
                      }
                    });
                  }}
                >
                  Detail
                </Button>
              </Space>
            )
          };
        })
      );

      const dataTable = res.data.data.listGradeDTOS.map((item, key) => {
        return {
          key: key,
          id: item.iterationId,
          rollNumber: item.rollNumber,
          project: item.projectName,
          total: item.totalGrade,
          og: item.ongoingEval,
          studentId: item.studentId,
          student: item.studentName,
          iterationName: item.iterationName,
          milestoneId: item.milestoneId,
          milestoneName: item.milestoneName,
          projectId: item.projectId,
          ...item
          // [item.key] :HeaderChoiceItem2
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setDataSource(dataTable);
    } catch (error) {
      if (error.status === 400) {
        setIsShowModal(true);
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setModalIcon('fail');
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
      setIsRendering(false);
    }
  };

  // get api list all Class
  const exportClassEvaluations = async params => {
    try {
      setLoading(true);
      setIsRendering(true);
      const data = await dispatch(exportClassEvaluation({ params }));
      const res = unwrapResult(data);
      openInNewTab(res?.data.data);
    } catch (error) {
      if (error.status === 400) {
        setIsShowModal(true);
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setModalIcon('fail');
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
      setIsRendering(false);
    }
  };

  // call api
  useEffect(() => {
    getListClassEval(paramsSearch);
  }, [dispatch]);

  const handleSearch = () => {
    getListClassEval({
      ...paramsSearch,
      page: 1
    });
  };

  // handle pagination as antd
  const handleTableChange = value => {
    getListClassEval({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  // handle reset filter
  const [selectedValuesProject, setSelectedValuesProject] = useState();
  const [selectedValuesClass, setSelectedValuesClass] = useState();
  const handleResetFilter = () => {
    getListClassEval(paramsSearch);
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absClass}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />
      <Row>
        <Col span={1}>
          <IoMdArrowBack
            className={'font-4re cursor-pointer'}
            onClick={() => {
              navigate(-1);
            }}
          />
        </Col>
        <Col span={18}>
          <h1 className={'m-0'}>Class Evaluation Management</h1>
        </Col>
      </Row>

      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="px-1"></Col>
        <Col span={4} className="px-1"></Col>
        <Col span={4} className="px-1"></Col>
        <Col span={4} className="px-1"></Col>

        <Col offset={6} span={2} className="d-flex justify-content-between">
          <Button
            className="br-10 btn-action d-flex mx-1"
            onClick={() => {
              exportClassEvaluations({ ...paramsSearch, getExcel: true });
            }}
          >
            <RiFileExcel2Line style={{ color: '#069255' }} className="icon-plus m-auto mr-1" />
            <span className="m-auto"> Export data</span>
          </Button>
          {/*<Button className="br-10 btn-grey w-100 mx-1" onClick={handleResetFilter}>*/}
          {/*  Reset*/}
          {/*</Button>*/}
          {/* <Button
            className="br-10 btn-action w-100 mx-1"
            // onClick={handleSearch}
          >
            Search
          </Button> */}
        </Col>
      </Row>

      {!isRendering && (
        <Table
          columns={columnRender.current}
          pagination={{
            className: 'border-radius-paging',
            current: paramsSearch.page,
            pageSize: paramsSearch.limit,
            total: paramsSearch.total,
            showSizeChanger: true
          }}
          // loading={loading}
          dataSource={dataSource}
          onChange={value => handleTableChange(value)}
        />
      )}
    </Spin>
  );
}
