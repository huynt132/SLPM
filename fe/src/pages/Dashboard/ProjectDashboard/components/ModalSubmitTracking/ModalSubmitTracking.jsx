import { unwrapResult } from '@reduxjs/toolkit';
import { Button, Modal, Select, Table, Upload } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { FiUpload } from 'react-icons/fi';
import { useDispatch } from 'react-redux';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { getAllUsers } from 'src/pages/Common/common.slice';
import { getListFunction } from '../../FunctionManagement/functionManagement.slice';
import { submitTracking } from '../../TrackingManagement/trackingManagement.slice';
import { showMessage } from '../../../../../utils/commonHelper';

export default function ModalSubmitTracking({ isShowModal, setIsShowModal, props, action }) {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  // data source table
  const [dataSource, setDataSource] = useState([]);
  // state loading
  const [loading, setLoading] = useState(false);
  // button loading

  //modal message show state
  const [isShowModalMessage, setIsShowModalMessage] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');

  const [paramsSearch, setParamsSearch] = useState({
    limit: 40,
    page: 1,
    total: 0
  });

  const [file, setFile] = useState(null);

  const [chooseRequest, setChooseRequest] = useState([]);

  const isUpdateRecord = useRef(null);

  const [requestUpdate, setRequestUpdate] = useState([]);

  const [userList, setUserList] = useState([]);

  const [buttonLoading, setButtonLoading] = useState(false);

  const dispatch = useDispatch();

  const onSelectChange = newSelectedRowKeys => {
    setSelectedRowKeys(newSelectedRowKeys);

    const arrayDataChoose = newSelectedRowKeys.map(item => {
      return { ...dataSource.find(itemD => itemD.functionId === item), chosen: true };
    });
    setChooseRequest(arrayDataChoose);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  };

  const beforeUpload = file => {
    const isLt20M = file.size / 1024 / 1024 < 20;
    if (!isLt20M) {
      setIsShowModalMessage(true);
      setModalText('File must smaller than 20MB!');
      setModalIcon('fail');
      return false;
    } else {
      return isLt20M;
    }
  };

  // get api list function
  const getListFunctions = async params => {
    try {
      setLoading(true);
      const data = await dispatch(
        getListFunction({ ...params, projectId: props.projectId, milestoneId: props.milestoneId })
      );
      const res = unwrapResult(data);
      const dataTable = res.data.data.functions.map((item, key) => {
        return {
          key: item.functionId,
          // id: item.id,
          functionId: item.functionId,
          functionName: item.functionName,
          featureId: item.featureId,
          featureName: item.featureName,
          status: item.status,
          assigneeId: item.assigneeId,
          assignee: item.assignee,
          rollNumber: item.rollNumber,
          chosen: item.chosen,
          projectId: item.projectId,
          projectName: item.projectName
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setDataSource(dataTable);
      setLoading(false);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModalMessage(true);
      }
    }
  };

  const getListUserInProject = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getAllUsers(params));
      const res = unwrapResult(data);
      setUserList(res.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (isShowModal) {
      getListFunctions(paramsSearch);
      getListUserInProject({
        projectId: props.projectId
      });
    }
  }, [isShowModal]);

  // handle pagination as antd
  const handleTableChange = value => {
    getListFunctions({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const columns = [
    {
      key: 'functionName',
      title: 'Function',
      dataIndex: 'functionName'
    },
    {
      title: 'Feature',
      dataIndex: 'featureName',
      key: 'featureName'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status'
    },
    {
      title: 'Assignee',
      key: 'assignee',
      render: (_, record) => (
        <Select
          labelInValue
          style={{
            width: '100%'
          }}
          defaultValue={{
            value: record?.assignee
          }}
          onChange={value => {
            isUpdateRecord.current = { ...record, assigneeId: value.value };
            setRequestUpdate(prevState => {
              if (prevState.length > 0) {
                const arrayFilter = prevState.filter(x => x.functionId !== isUpdateRecord.current.functionId);
                return [...arrayFilter, isUpdateRecord.current];
              } else {
                return [...prevState, isUpdateRecord.current];
              }
            });
          }}
        >
          {userList.map((item, key) => {
            return (
              <Select.Option key={key} value={item.value} l>
                {item.label}
              </Select.Option>
            );
          })}
        </Select>
      )
    }
  ];

  const handleSubmit = async () => {
    const arrayDataFinal = chooseRequest
      .filter(item => {
        let flag = true;
        for (let i = 0; i < requestUpdate.length; i++) {
          if (requestUpdate[i].functionId === item.functionId) {
            flag = false;
            requestUpdate[i].chosen = true;
            break;
          }
        }
        return flag;
      })
      .concat(requestUpdate);

    try {
      setButtonLoading(true);
      const body = new FormData();
      body.append('file', file);
      body.append('milestoneId', props.milestoneId);
      body.append('projectId', props.projectId);
      body.append('list', JSON.stringify(arrayDataFinal));
      const res = await dispatch(submitTracking(body));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(null, { action: 'triggerSearch' });
      } else {
        setIsShowModalMessage(true);
        setModalIcon('fail');
        setModalText(result.data.message);
      }
    } catch (error) {
      setIsShowModalMessage(true);
      setModalIcon('fail');
      setModalText(error?.data?.message || 'An error occurred while update data!');
    } finally {
      setButtonLoading(false);
    }
  };

  return (
    <>
      <ModalMessage
        isShowModal={isShowModalMessage}
        isHideModal={() => {
          setIsShowModalMessage(false);
          getListFunctions(paramsSearch);
          getListUserInProject({
            projectId: props.projectId
          });
        }}
        text={modalText}
        iconname={modalIcon}
      />
      <Modal
        title="Tracking Submit"
        visible={isShowModal}
        okText={'Add'}
        onCancel={() => {
          setIsShowModal(false);
          setSelectedRowKeys([]);
        }}
        width={1000}
        footer={false}
      >
        <div style={{ marginBottom: ' 2rem' }}>
          <h4>Choose submit package (zip file less than 20 MB)</h4>
          <Upload
            accept={'.zip'}
            maxCount={1}
            className={'d-flex'}
            beforeUpload={beforeUpload}
            onChange={file => {
              if (file.file) setFile(file.file.originFileObj);
            }}
            progress={{
              strokeColor: {
                '0%': '#108ee9',
                '100%': '#87d068'
              },
              strokeWidth: 3,
              format: percent => percent && `${parseFloat(percent.toFixed(2))}%`
            }}
          >
            <Button>
              <FiUpload style={{ marginLeft: 0, marginRight: '1rem' }} />
              Click to Upload
            </Button>
          </Upload>
        </div>
        <Table
          pagination={{
            className: 'border-radius-paging',
            current: paramsSearch.page,
            pageSize: paramsSearch.limit,
            total: paramsSearch.total
          }}
          onChange={value => handleTableChange(value)}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={dataSource}
          loading={loading}
        />
        <Button
          loading={buttonLoading}
          onClick={handleSubmit}
          style={{ marginTop: '2rem', borderRadius: '5px' }}
          type="primary"
        >
          Update Assignee & Submit for Selected Function
        </Button>
      </Modal>
    </>
  );
}
