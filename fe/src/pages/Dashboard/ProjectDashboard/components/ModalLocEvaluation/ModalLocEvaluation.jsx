// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Select, Button, Transfer } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
// services
import { getFunctionEvaluate, locEvaluate } from '../../MilestoneSubmit/MilestoneSubmit.slice';

// custom component
import SelectLocEvaluation from 'src/components/Select/SelectLocEvaluation';
import SelectEvaluate from 'src/components/Select/SelectEvaluate';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';

const { TextArea } = Input;
export default function ModalLocEvaluation({ isShowModal, action, props }) {
  const dispatch = useDispatch();
  const [locEvaluation, setLocEvaluation] = useState('');
  const [btnEvaluateLoading, setBtnEvaluateLoading] = useState(false);
  const [btnRejectLoading, setBtnRejectLoading] = useState(false);
  const [canReject, setCanReject] = useState(true);

  useEffect(() => {
    if (isShowModal) getFunctionEvaluateDetails();
  }, [isShowModal]);

  async function getFunctionEvaluateDetails() {
    try {
      setBtnEvaluateLoading(true);
      setBtnRejectLoading(true);
      const res = await dispatch(getFunctionEvaluate({ ...props }));
      const result = unwrapResult(res);
      if (result.data.success) {
        // showMessage(result.data.message);
        setLocEvaluation(result.data.data);
        if (!isEmpty(result.data.data.convertedLoc)) setCanReject(false);
        else setCanReject(true);
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message)
              ? 'An error occurred while getting function details!'
              : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setBtnEvaluateLoading(false);
      setBtnRejectLoading(false);
    }
  }

  async function submitEval(type) {
    try {
      if (type === 'evaluated') setBtnEvaluateLoading(true);
      if (type === 'rejected') setBtnRejectLoading(true);
      let error = validateEvaluate();
      if (type === 'evaluated' && !isEmpty(error)) return showErrorMessage(error);
      let data = { ...locEvaluation };
      if (type === 'rejected') data.status = 'rejected';
      const res = await dispatch(locEvaluate({ ...data }));
      const result = unwrapResult(res);
      if (result.data.success) showMessage(result.data.message);
      action(null, { action: 'isHideModalLocEvaluation' });
      action(null, { action: 'triggerSearch' });
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message)
              ? 'An error occurred while adding new tracking!'
              : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setBtnEvaluateLoading(false);
      setBtnRejectLoading(false);
    }
  }

  const validateEvaluate = () => {
    if (isEmpty(locEvaluation.complexityId)) return 'You need to evaluate complexity!';
    if (isEmpty(locEvaluation.qualityId)) return 'You need to evaluate quality!';
    if (isEmpty(locEvaluation.convertedLoc)) return 'Converted loc can not be empty!';
    return null;
  };

  const showErrorMessage = message => {
    action(null, {
      action: 'showMessage',
      data: {
        description: message,
        type: 'fail'
      }
    });
  };

  const resetModal = () => {
    setLocEvaluation({});
  };
  return (
    <Modal
      afterClose={resetModal}
      className={'modal-center-footer'}
      confirmLoading={btnEvaluateLoading}
      visible={isShowModal}
      width={800}
      footer={
        <>
          <Button loading={btnEvaluateLoading} className="" onClick={() => submitEval('evaluated')}>
            Evaluate
          </Button>
          <Button loading={btnRejectLoading} disabled={!canReject} className="" onClick={() => submitEval('rejected')}>
            Reject
          </Button>
        </>
      }
      title={
        <>
          <h2 className={'font-weight-bold'}>Loc Evaluation</h2>
          <h3 className={'font-weight-bold'}>
            {`Student: ${locEvaluation?.assigneeFullName} (${locEvaluation?.assigneeRollNumber}), ${locEvaluation?.projectName} , ${locEvaluation?.milestoneName}`}
          </h3>
        </>
      }
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalLocEvaluation' });
      }}
    >
      <Row className={'px-1'}>Function</Row>
      <Row className={'px-1 mb-2'}>
        <Input value={locEvaluation?.name} disabled={true} placeholder="Function name" className="br-8 text-black" />
      </Row>
      <Row className={'px-1'}>Function Description</Row>
      <Row className={'px-1 mb-2'}>
        <TextArea
          rows={3}
          placeholder="Description ... "
          value={locEvaluation?.description}
          disabled={true}
          className={'text-black br-8'}
        />
      </Row>
      <Row className={'px-1'}>Tracking note</Row>
      <Row className={'px-1 mb-3'}>
        <TextArea
          rows={3}
          placeholder="Tracking note ... "
          value={locEvaluation?.trackingNote}
          disabled={true}
          className={'text-black br-8'}
        />
      </Row>
      <Row className={'px-1 my-2'}>
        <Col span={10} className={'pr-2'}>
          Complexity
          <SelectEvaluate
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'select complexity'}
            onOptionSelected={value => {
              setLocEvaluation(prev => {
                return {
                  ...prev,
                  complexityId: value.value,
                  complexityValue: value.settingValue,
                  convertedLoc: parseFloat(value.settingValue || 0) * parseFloat(prev.qualityValue || 0)
                };
              });
            }}
            filter={{
              type: 'complexity',
              classId: locEvaluation?.classId
            }}
            evaluates={isEmpty(locEvaluation?.complexityId) ? [] : locEvaluation?.complexityId}
          />
        </Col>
        <Col span={10} className={'pr-2'}>
          Quality
          <SelectEvaluate
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'select quality'}
            onOptionSelected={value => {
              setLocEvaluation(prev => {
                return {
                  ...prev,
                  qualityId: value.value,
                  qualityValue: value.settingValue,
                  convertedLoc: parseFloat(prev.complexityValue || 0) * parseFloat(value.settingValue || 0)
                };
              });
            }}
            filter={{
              type: 'quality',
              classId: locEvaluation?.classId
            }}
            evaluates={isEmpty(locEvaluation?.qualityId) ? [] : locEvaluation?.qualityId}
          />
        </Col>
        <Col span={4} className={''}>
          Loc
          <Input
            type="number"
            value={locEvaluation?.convertedLoc}
            disabled={true}
            placeholder="Converted Loc"
            className="br-8 text-black"
          />
        </Col>
      </Row>
      <Row className={'px-1'}>Evaluation comment</Row>
      <Row className={'px-1'}>
        <TextArea
          rows={3}
          placeholder="Evaluation comment ... "
          value={locEvaluation?.evaluationComment}
          onChange={e =>
            setLocEvaluation(prev => {
              return { ...prev, evaluationComment: e.target.value };
            })
          }
          className={'text-black br-8'}
        />
      </Row>
    </Modal>
  );
}

ModalLocEvaluation.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  props: PropsTypes.object
};
ModalLocEvaluation.defaultProps = {
  isShowModal: false,
  props: {}
};
