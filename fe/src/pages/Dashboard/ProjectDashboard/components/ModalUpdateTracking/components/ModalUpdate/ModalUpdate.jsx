import { Input, InputNumber, Modal, Radio, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import TextArea from 'antd/lib/input/TextArea';
import { getAllMilestones } from 'src/pages/Dashboard/SubjectDashboard/MilestoneManagement/milestoneManagement.slice';

export default function ModalUpdate({
  isUpdating,
  setIsUpdating,
  handleUpdateTrackingUpdate,
  requestUpdate,
  setRequestUpdate,
  milestoneList
}) {
  const [buttonLoading, setButtonLoading] = useState(false);

  // const handleSelectChange = value => {
  //   setRequestUpdate(prevState => {
  //     return { ...prevState, milestoneId: value.toString() };
  //   });
  // };

  return (
    <Modal
      title="Update Infomation"
      visible={isUpdating}
      onOk={handleUpdateTrackingUpdate}
      okText={'Update'}
      width="800px"
      onCancel={() => {
        setIsUpdating(false);
      }}
    >
      <div style={{ marginBottom: '2rem' }}>
        <h4>Update milestone</h4>
        <Select
          style={{ width: '100%' }}
          placeholder="select milestone"
          filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
          onChange={value => {
            setRequestUpdate(record => {
              return { ...record, milestoneId: value.toString() };
            });
          }}
          // value={{
          //   value: requestUpdate?.milestone
          // }}
        >
          {milestoneList}
        </Select>
      </div>
      <div>
        <h4>Update comment</h4>
        <TextArea
          className={'text-black br-8'}
          placeholder="Description"
          onChange={e => {
            setRequestUpdate(prevState => {
              return { ...prevState, description: e.target.value };
            });
          }}
          value={requestUpdate?.description}
        />
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isUpdating: PropTypes.bool,
  setIsUpdating: PropTypes.func,
  handleUpdateTrackingUpdate: PropTypes.func,
  setRequestUpdate: PropTypes.func,
  requestUpdate: PropTypes.object,
  milestoneList: PropTypes.array
};
