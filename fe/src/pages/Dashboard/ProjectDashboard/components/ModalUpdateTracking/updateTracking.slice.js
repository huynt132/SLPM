import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import functionManagementApi from 'src/api/functionManagement.api';
import trackingManagementApi from 'src/api/trackingManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getListUpdateTracking = createAsyncThunk(
  'tracking/getListUpdate',
  payloadCreator(trackingManagementApi.getListUpdateTracking)
);

export const addLog = createAsyncThunk('function/addLog', payloadCreator(functionManagementApi.addLogFunction));

export const updateLog = createAsyncThunk(
  'function/updateLog',
  payloadCreator(functionManagementApi.updateLogFunction)
);

const updateTrackingManagement = createSlice({
  name: 'updateTrackingManagement',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const updateTrackingManagementReducer = updateTrackingManagement.reducer;

export default updateTrackingManagementReducer;
