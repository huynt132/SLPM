import { unwrapResult } from '@reduxjs/toolkit';
import { Button, Input, Modal, Select, Space, Table, Upload } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { Option } from 'antd/lib/mentions';
import React, { useEffect, useRef, useState } from 'react';
import { VscEdit } from 'react-icons/vsc';
import { useDispatch } from 'react-redux';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { getAllMilestones } from 'src/pages/Dashboard/SubjectDashboard/MilestoneManagement/milestoneManagement.slice';
import ModalUpdate from './components/ModalUpdate/ModalUpdate';
import styles from './ModalUpdateTracking.module.css';
import { addLog, getListUpdateTracking, updateLog } from './updateTracking.slice';

export default function ModalUpdateTracking({ isShowModal, setIsShowModal, paramModalUpdateTracking }) {
  const dispatch = useDispatch();
  const [dataSource, setDataSource] = useState([]);
  // state loading
  const [loading, setLoading] = useState(false);

  const [paramsSearch, setParamsSearch] = useState({
    limit: 6,
    page: 1,
    total: 200
  });

  //modal message show state
  const [isShowModalMessage, setIsShowModalMessage] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');

  const [listMilestone, setListMilestone] = useState([]);

  const [isUpdating, setIsUpdating] = useState(false);

  const [requestUpdate, setRequestUpdate] = useState(null);

  const [requestAdd, setRequestAdd] = useState(null);

  const [buttonLoading, setButtonLoading] = useState(false);

  const [comment, setComment] = useState('');

  const getAllMilestone = async params => {
    setLoading(true);
    const data = await dispatch(getAllMilestones(params));
    const res = unwrapResult(data);
    setListMilestone(res.data.data);
    setLoading(false);
  };

  const { Option } = Select;
  const milestones = listMilestone.map((item, key) => {
    return (
      <Option key={key} value={item.value}>
        {item.label}
      </Option>
    );
  });

  useEffect(() => {
    console.log(requestAdd);
  }, [requestAdd]);

  const handleSelectChange = value => {
    setRequestAdd(prevState => {
      return { ...prevState, milestoneId: value.toString() };
    });
  };

  // get api list log function
  const getListLogFunc = async params => {
    setLoading(true);
    const data = await dispatch(getListUpdateTracking({ params }));
    const res = unwrapResult(data);
    const dataTable = res.data.data.map(item => {
      return {
        key: item.logId,
        id: item.logId,
        description: item.description,
        updateAt: item.created,
        milestone: item.milestonTitle,
        update: item.description
      };
    });
    setParamsSearch({
      ...params,
      total: res.data.total
    });
    setDataSource(dataTable);
    setComment(res.data.evaluateComment);
    setLoading(false);
  };

  // call api
  useEffect(() => {
    if (isShowModal) {
      getListLogFunc({
        ...paramsSearch,
        functionId: paramModalUpdateTracking?.functionId
      });
      getAllMilestone({
        projectId: paramModalUpdateTracking?.projectId
      });
    }
  }, [isShowModal]);

  const columns = [
    {
      key: 'id',
      title: 'ID',
      dataIndex: 'id'
    },
    {
      key: 'update',
      title: 'Update',
      dataIndex: 'update'
    },
    {
      title: 'Update At',
      dataIndex: 'updateAt',
      key: 'updateAt'
    },
    {
      title: 'Milestone',
      dataIndex: 'milestone',
      key: 'milestone'
    },
    {
      title: 'Actions',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" style={{ display: 'flex', alignItems: ' center' }}>
          <Button
            style={{
              borderRadius: '10px',
              border: 'none',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              color: '#fff',
              background: '#1987b3'
            }}
            onClick={() => {
              onClickBtnUpdate(record);
            }}
          >
            <span className="m-auto">Update</span>
          </Button>
        </Space>
      )
    }
  ];

  const onClickBtnUpdate = record => {
    setIsUpdating(true);
    setRequestUpdate(prevState => {
      return { ...prevState, ...record };
    });
  };

  const handleTableChange = value => {
    getListLogFunc({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const handleAddTrackingUpdate = async () => {
    try {
      setButtonLoading(true);
      const res = await dispatch(addLog({ ...requestAdd, functionId: paramModalUpdateTracking.functionId }));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setButtonLoading(false);
      setModalIcon('success');
      setModalText('Add successfully');
      setIsShowModalMessage(true);
      setDataSource(prevList => {
        return [
          ...prevList,
          {
            key: result.data.data.id,
            id: result.data.data.id,
            description: result.data.data.description,
            updateAt: result.data.data.created,
            milestone: result.data.data.milestoneName,
            update: result.data.data.description
          }
        ];
      });
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModalMessage(true);
        setButtonLoading(false);
      }
    }
  };

  const handleUpdateTrackingUpdate = async () => {
    try {
      setIsUpdating(false);
      const body = {
        id: requestUpdate.id,
        functionId: paramModalUpdateTracking.functionId,
        milestoneId: requestUpdate.milestoneId,
        description: requestUpdate.description
      };
      const res = await dispatch(updateLog(body));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setButtonLoading(false);
      setModalIcon('success');
      setModalText('Update successfully');
      setIsShowModalMessage(true);
      setDataSource(prevList =>
        prevList.map(item =>
          item.id === result.data.data.id
            ? {
                ...item,
                key: result.data.data.id,
                id: result.data.data.id,
                description: result.data.data.description,
                updateAt: result.data.data.created,
                milestone: result.data.data.milestoneName,
                update: result.data.data.description
              }
            : item
        )
      );
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModalMessage(true);
        setButtonLoading(false);
      }
    }
  };

  return (
    <>
      <ModalMessage
        isShowModal={isShowModalMessage}
        isHideModal={() => {
          setIsShowModalMessage(false);
        }}
        text={modalText}
        iconname={modalIcon}
      />
      <Modal
        title="Update Evaluated Function"
        visible={isShowModal}
        onCancel={() => {
          setIsShowModal(false);
        }}
        width={1000}
        footer={false}
      >
        <div className={styles.container} style={{ width: '100%' }}>
          <div className={styles.contentTop}>
            <div className={styles.left}>
              <h3>Function: {paramModalUpdateTracking?.functionName}</h3>
              <h3>Evaluated in: {paramModalUpdateTracking?.milestoneName}, with below comments</h3>
              <TextArea
                className={'text-black br-8'}
                placeholder="Evaluating Comments"
                disabled
                value={comment}
              ></TextArea>
            </div>
            <div className={styles.right}>
              <div className={styles.rightItem}>
                <h3>Add new record</h3>
                <Button
                  type="primary"
                  style={{
                    borderRadius: '10px',
                    border: 'none',
                    color: '#fff'
                  }}
                  loading={buttonLoading}
                  onClick={handleAddTrackingUpdate}
                >
                  Submit
                </Button>
              </div>

              <div className={styles.rightItem}>
                <Select
                  style={{ width: '100%' }}
                  placeholder="select milestone"
                  filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
                  onChange={handleSelectChange}
                >
                  {milestones}
                </Select>
              </div>
              <div className={styles.rightItem}>
                <TextArea
                  className={'text-black br-8'}
                  placeholder="Content Comments"
                  onChange={e => {
                    setRequestAdd(prevState => {
                      return { ...prevState, description: e.target.value };
                    });
                  }}
                />
              </div>
            </div>
          </div>
          <h3>Update history</h3>
          <Table
            pagination={{
              className: 'border-radius-paging',
              current: paramsSearch.page,
              pageSize: paramsSearch.limit,
              total: paramsSearch.total
            }}
            onChange={value => handleTableChange(value)}
            columns={columns}
            dataSource={dataSource}
            loading={loading}
          />
        </div>
      </Modal>
      <ModalUpdate
        isUpdating={isUpdating}
        setIsUpdating={setIsUpdating}
        handleUpdateTrackingUpdate={handleUpdateTrackingUpdate}
        requestUpdate={requestUpdate}
        setRequestUpdate={setRequestUpdate}
        milestoneList={milestones}
      />
    </>
  );
}
