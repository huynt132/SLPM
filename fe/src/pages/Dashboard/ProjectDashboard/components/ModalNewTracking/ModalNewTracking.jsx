// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Select, Button, Transfer } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
// services
import { addTracking, getFunctionTracking } from '../../TrackingManagement/trackingManagement.slice';

// custom component
import SelectMilestone from 'src/components/Select/SelectMilestone';
import SelectUser from 'src/components/Select/SelectUser';
import SelectProject from 'src/components/Select/SelectProject';
import SelectFeature from 'src/components/Select/SelectFeature';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';

export default function ModalNewTracking({ isShowModal, action }) {
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [targetKeys, setTargetKeys] = useState([]);
  const [curMilestoneName, setCurMilestoneName] = useState('');
  const [assigneeId, setAssigneeId] = useState();
  const [milestoneId, setMilestoneId] = useState();
  const [loading, setLoading] = useState(false);

  const [filter, setFilter] = useState({
    projectId: null,
    featureId: null
  });

  useEffect(() => {
    if (isShowModal) getFunctionTrackings(filter);
  }, [isShowModal]);

  async function getFunctionTrackings(params) {
    try {
      setLoading(true);
      setOptions([]);
      const res = await dispatch(getFunctionTracking({ ...params }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        let data = result.data.data;
        if (!isEmpty(data?.functionTracking)) setOptions(data?.functionTracking);
        if (!isEmpty(data?.curProjectId)) {
          setFilter(prev => {
            return { ...prev, projectId: data.curProjectId };
          });
        }
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message)
              ? 'An error occurred while getting available function!'
              : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  async function newTracking(params) {
    try {
      let error = validateNewTracking();
      if (!isEmpty(error)) return showErrorMessage(error);
      setLoading(true);
      let data = {
        functionList: targetKeys,
        milestoneId: milestoneId,
        assigneeId: assigneeId
      };
      const res = await dispatch(addTracking(data));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
      }
      resetModal();
      action(null, { action: 'isHideModalNewTracking' });
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: error?.data?.message || 'An error occurred while adding new tracking!',
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  const validateNewTracking = () => {
    if (isEmpty(milestoneId)) return 'Please choose milestone for new tracking!';
    if (isEmpty(assigneeId)) return 'Please choose assignee for new tracking!';
    if (isEmpty(options) || isEmpty(targetKeys)) return 'Please choose function to create new tracking!';
  };

  const showErrorMessage = message => {
    action(null, {
      action: 'showMessage',
      data: {
        description: message,
        type: 'fail'
      }
    });
  };

  const handleChange = (newTargetKeys, direction, moveKeys) => {
    setTargetKeys(newTargetKeys);
  };

  const resetModal = () => {
    setOptions([]);
    setFilter({});
    setCurMilestoneName('');
    setTargetKeys([]);
    setAssigneeId(null);
    setMilestoneId(null);
  };
  return (
    <Modal
      afterClose={resetModal}
      className={'modal-center-footer'}
      confirmLoading={loading}
      width={1000}
      visible={isShowModal}
      okText={'Submit'}
      title={
        <>
          <h2 className={'font-weight-bold'}>New Tracking</h2>
          <h3 className={'font-weight-bold'}>{`Adjust functions for implementing in the ${
            curMilestoneName || 'milestone'
          }`}</h3>
        </>
      }
      footer={
        <Button
          loading={loading}
          className=""
          onClick={() => {
            newTracking();
          }}
        >
          Submit
        </Button>
      }
      onCancel={() => {
        action(null, { action: 'isHideModalNewTracking' });
      }}
    >
      <Row>
        <Col span={12} className={'pr-4'}>
          <Row className={'mb-2'}>
            <SelectProject
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'select project'}
              onOptionSelected={value => {
                setFilter(prev => {
                  return { ...prev, projectId: value };
                });
              }}
              projects={isEmpty(filter.projectId) ? [] : filter.projectId}
            />
          </Row>
          <Row className={'pr-1 mb-4'}>
            <Col span={19}>
              <SelectFeature
                customClassName={'border-radius-select'}
                mode={'single'}
                placeholder={'select feature'}
                filter={{ projectId: filter?.projectId }}
                onOptionSelected={value => {
                  setFilter(prev => {
                    return { ...prev, featureId: value };
                  });
                }}
                features={isEmpty(filter.featureId) ? [] : filter.featureId}
              />
            </Col>
            <Col offset={1} span={4}>
              <Button
                className={'br-8'}
                onClick={() => {
                  getFunctionTrackings(filter);
                }}
              >
                Search
              </Button>
            </Col>
          </Row>
        </Col>
        <Col span={12} className={'pl-4'}>
          <Row className={'mb-2'}>
            <SelectMilestone
              customClassName={'border-radius-select'}
              mode={'single'}
              filter={{ projectId: filter?.projectId }}
              placeholder={'select milestone'}
              onOptionSelected={value => {
                setMilestoneId(value);
              }}
              milestones={isEmpty(milestoneId) ? [] : milestoneId}
            />
          </Row>
          <Row className={'mb-4'}>
            <SelectUser
              customClassName={'border-radius-select'}
              mode={'single'}
              filter={{ projectId: filter?.projectId }}
              placeholder={'select assignee'}
              onOptionSelected={value => {
                setAssigneeId(value);
              }}
              users={isEmpty(assigneeId) ? [] : assigneeId}
            />
          </Row>
        </Col>
      </Row>
      <Transfer
        showSearch
        titles={[<b>Available functions</b>, <b>Tracking functions</b>]}
        dataSource={options}
        listStyle={{
          width: 500,
          height: 300
        }}
        targetKeys={targetKeys}
        onChange={handleChange}
        render={item => item.label}
      />
    </Modal>
  );
}

ModalNewTracking.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  track: PropsTypes.object
};
ModalNewTracking.defaultProps = {
  isShowModal: false,
  track: {}
};
