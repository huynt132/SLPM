// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Spin, Button, Transfer } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
// services
import { getNewLocFunction, locEvaluate } from '../../MilestoneSubmit/MilestoneSubmit.slice';

// custom component
import SelectEvaluate from 'src/components/Select/SelectEvaluate';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';

const { TextArea } = Input;
export default function ModalNewLocEvaluation({ isShowModal, action, props }) {
  const dispatch = useDispatch();
  const [locEvaluation, setLocEvaluation] = useState('');
  const [btnEvaluateLoading, setBtnEvaluateLoading] = useState(false);

  useEffect(() => {
    if (isShowModal) getFunctionEvaluateDetails();
  }, [isShowModal]);

  async function getFunctionEvaluateDetails() {
    try {
      setBtnEvaluateLoading(true);
      const res = await dispatch(getNewLocFunction({ ...props }));
      let result = unwrapResult(res);
      if (result.data.success) {
        // showMessage(result.data.message);
        if (!isEmpty(result?.data?.data?.updateLog)) {
          let updateConvert = '';
          result.data.data.updateLog.map(log => {
            updateConvert += `[${log.created}] ${log.createdByUser} update: ${log.description} \n`;
          });
          result.data.data.updateConvert = updateConvert;
        }
        setLocEvaluation(result.data.data);
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message)
              ? 'An error occurred while getting function details!'
              : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setBtnEvaluateLoading(false);
    }
  }

  async function submitEval(type) {
    try {
      setBtnEvaluateLoading(true);
      let error = validateEvaluate();
      if (type === 'evaluated' && !isEmpty(error)) return showErrorMessage(error);
      const res = await dispatch(locEvaluate({ ...locEvaluation, status: type }));
      const result = unwrapResult(res);
      if (result.data.success) showMessage(result.data.message);
      action(null, { action: 'isHideModalNewLocEvaluation' });
      action(null, { action: 'triggerSearch' });
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message)
              ? 'An error occurred while adding new tracking!'
              : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setBtnEvaluateLoading(false);
    }
  }

  const validateEvaluate = () => {
    if (isEmpty(locEvaluation.newComplexityId)) return 'You need to evaluate complexity!';
    if (isEmpty(locEvaluation.newQualityId)) return 'You need to evaluate quality!';
    if (isEmpty(locEvaluation.newConvertedLoc)) return 'Converted loc can not be empty!';
    return null;
  };

  const showErrorMessage = message => {
    action(null, {
      action: 'showMessage',
      data: {
        description: message,
        type: 'fail'
      }
    });
  };

  const resetModal = () => {
    setLocEvaluation({});
  };
  return (
    <Modal
      afterClose={resetModal}
      className={'modal-center-footer'}
      confirmLoading={btnEvaluateLoading}
      visible={isShowModal}
      width={800}
      footer={
        <>
          <Button loading={btnEvaluateLoading} className="" onClick={() => submitEval('evaluated')}>
            Submit
          </Button>
        </>
      }
      title={
        <>
          <h2 className={'font-weight-bold'}>New Loc Evaluation</h2>
          <h3 className={'font-weight-bold'}>
            {`Student: ${locEvaluation?.assigneeFullName} (${locEvaluation?.assigneeRollNumber}), team ${locEvaluation?.projectName}, class ${locEvaluation?.className}`}
          </h3>
        </>
      }
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalNewLocEvaluation' });
      }}
    >
      <Spin tip="Loading..." spinning={btnEvaluateLoading}>
        <Row className={'px-1 my-2'}>
          <Col span={10} className={'pr-2'}>
            Last Complexity
            <SelectEvaluate
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'select complexity'}
              onOptionSelected={value => {
                setLocEvaluation(prev => {
                  return {
                    ...prev,
                    complexityId: value.value,
                    complexityValue: value.settingValue,
                    convertedLoc: parseFloat(value.settingValue || 0) * parseFloat(prev.qualityValue || 0)
                  };
                });
              }}
              disabled={true}
              filter={{
                type: 'complexity',
                classId: locEvaluation?.classId
              }}
              evaluates={isEmpty(locEvaluation?.complexityId) ? [] : locEvaluation?.complexityId}
            />
          </Col>
          <Col span={10} className={'pr-2'}>
            Last Quality
            <SelectEvaluate
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'select quality'}
              onOptionSelected={value => {
                setLocEvaluation(prev => {
                  return {
                    ...prev,
                    qualityId: value.value,
                    qualityValue: value.settingValue,
                    convertedLoc: parseFloat(prev.complexityValue || 0) * parseFloat(value.settingValue || 0)
                  };
                });
              }}
              disabled={true}
              filter={{
                type: 'quality',
                classId: locEvaluation?.classId
              }}
              evaluates={isEmpty(locEvaluation?.qualityId) ? [] : locEvaluation?.qualityId}
            />
          </Col>
          <Col span={4} className={''}>
            Last Loc
            <Input
              type="number"
              value={locEvaluation?.convertedLoc}
              disabled={true}
              placeholder="Converted Loc"
              className="br-8 text-black"
            />
          </Col>
        </Row>
        <Row className={'px-1'}>Last Evaluation comment</Row>
        <Row className={'px-1'}>
          <TextArea
            rows={3}
            disabled={true}
            value={locEvaluation?.evaluationComment}
            onChange={e =>
              setLocEvaluation(prev => {
                return { ...prev, evaluationComment: e.target.value };
              })
            }
            className={'text-black br-8'}
          />
        </Row>
        <Row className={'px-1 mt-2'}>Updates</Row>
        <Row className={'px-1'}>
          <TextArea
            rows={6}
            disabled={true}
            value={locEvaluation?.updateConvert}
            onChange={e =>
              setLocEvaluation(prev => {
                return { ...prev, evaluationComment: e.target.value };
              })
            }
            className={'text-black br-8'}
          />
        </Row>
        <Row className={'px-1 my-2'}>
          <Col span={10} className={'pr-2'}>
            Complexity
            <SelectEvaluate
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'select complexity'}
              onOptionSelected={value => {
                setLocEvaluation(prev => {
                  return {
                    ...prev,
                    newComplexityId: value.value,
                    newComplexityValue: value.settingValue,
                    newConvertedLoc: parseFloat(value.settingValue || 0) * parseFloat(prev.newQualityValue || 0)
                  };
                });
              }}
              filter={{
                type: 'complexity',
                classId: locEvaluation?.classId
              }}
              evaluates={isEmpty(locEvaluation?.newComplexityId) ? [] : locEvaluation?.newComplexityId}
            />
          </Col>
          <Col span={10} className={'pr-2'}>
            Quality
            <SelectEvaluate
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'select quality'}
              onOptionSelected={value => {
                setLocEvaluation(prev => {
                  return {
                    ...prev,
                    newQualityId: value.value,
                    newQualityValue: value.settingValue,
                    newConvertedLoc: parseFloat(prev.newComplexityValue || 0) * parseFloat(value.settingValue || 0)
                  };
                });
              }}
              filter={{
                type: 'quality',
                classId: locEvaluation?.classId
              }}
              evaluates={isEmpty(locEvaluation?.newQualityId) ? [] : locEvaluation?.newQualityId}
            />
          </Col>
          <Col span={4} className={''}>
            Loc
            <Input
              type="number"
              value={locEvaluation?.newConvertedLoc}
              disabled={true}
              placeholder="Converted Loc"
              className="br-8 text-black"
            />
          </Col>
        </Row>
      </Spin>
    </Modal>
  );
}

ModalNewLocEvaluation.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  props: PropsTypes.object
};
ModalNewLocEvaluation.defaultProps = {
  isShowModal: false,
  props: {}
};
