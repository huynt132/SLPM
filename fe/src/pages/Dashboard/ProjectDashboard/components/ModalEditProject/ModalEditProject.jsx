// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Radio } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// services
import { editProject, addProject } from '../../ProjectManagement/projectManagement.slice.js';

// custom component
import SelectClass from 'src/components/Select/SelectClass';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';

export default function ModalEditProject({ isShowModal, type, project, action }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [newProject, setNewProject] = useState({ ...project });

  useEffect(() => {
    setNewProject(project);
  }, [project]);

  useEffect(() => {
    if (isEmpty(newProject?.status));
    setNewProject(prev => {
      return { ...prev, status: 'active' };
    });
  }, [newProject?.status]);

  async function updateProject() {
    try {
      setLoading(true);
      const res = await dispatch(editProject({ ...newProject }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(result.data.data, { action: 'editProjectSuccess' });
        resetModal();
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  async function createProject() {
    try {
      setLoading(true);
      const res = await dispatch(addProject({ ...newProject }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        resetModal();
        action(result.data.data, { action: 'addProjectSuccess' });
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while create data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  function handleSubmit() {
    if (type === 'Update') updateProject();
    if (type === 'Add') createProject();
  }

  function resetModal() {
    if (type === 'Add') setNewProject({});
    if (type === 'Update') setNewProject({ ...project });
  }

  return (
    <Modal
      confirmLoading={loading}
      title={`${type} team`}
      visible={isShowModal}
      onOk={() => {
        handleSubmit();
      }}
      afterClose={resetModal}
      okText={type}
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalEditProject' });
      }}
    >
      <div>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Class</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <SelectClass
              mode={'single'}
              onOptionSelected={value => {
                setNewProject(prev => {
                  return { ...prev, classId: value };
                });
              }}
              customClassName={'border-radius-select'}
              placeholder={'Select class'}
              classes={isEmpty(newProject.classId) ? [] : newProject.classId}
              disabled={type === 'Update'}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Team code</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Input
              className={'br-8'}
              value={newProject.code}
              placeholder="Input Team Code"
              onChange={e => {
                setNewProject(prev => {
                  return { ...prev, code: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Topic Name</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Input
              className={'br-8'}
              value={newProject.name}
              placeholder="Input Topic Name"
              onChange={e => {
                setNewProject(prev => {
                  return { ...prev, name: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Topic Code</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Input
              className={'br-8'}
              value={newProject.topicCode}
              placeholder="Input Topic Code"
              onChange={e => {
                setNewProject(prev => {
                  return { ...prev, topicCode: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        {/* <Row className={`py-1`}>
          <Col span={6}>Gitlab url:</Col>
          <Col span={18}>
            <Input.TextArea
              disabled={true}
              className={'br-8'}
              value={newProject.gitlabUrl}
              autoSize={{ minRows: 3, maxRows: 3 }}
              placeholder="Input gitlab url"
              onChange={e => {
                setNewProject(prev => {
                  return { ...prev, gitlabUrl: e.target.value };
                });
              }}
            />
          </Col>
        </Row> */}
        {/*<Row className={`py-1`}>*/}
        {/*  <Col span={6}>Gitlab token:</Col>*/}
        {/*  <Col span={18}>*/}
        {/*    <Input*/}
        {/*      className={'br-8'}*/}
        {/*      value={newProject.gitlabToken}*/}
        {/*      placeholder="Input gitlab token"*/}
        {/*      onChange={e => {*/}
        {/*        setNewProject(prev => {*/}
        {/*          return { ...prev, gitlabToken: e.target.value };*/}
        {/*        });*/}
        {/*      }}*/}
        {/*    />*/}
        {/*  </Col>*/}
        {/*</Row>*/}
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Status</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Radio.Group
              onChange={e => {
                setNewProject(prev => {
                  return { ...prev, status: e.target.value };
                });
              }}
              value={newProject?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </Col>
        </Row>
      </div>
    </Modal>
  );
}

ModalEditProject.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  type: PropsTypes.string,
  project: PropsTypes.object
};
ModalEditProject.defaultProps = {
  isShowModal: false,
  type: 'Add',
  project: {}
};
