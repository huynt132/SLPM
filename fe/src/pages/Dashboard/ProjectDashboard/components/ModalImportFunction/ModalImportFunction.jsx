// React
import React, { useState, useRef } from 'react';
import { Modal, Row, Upload, Button, Table } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// services
import { importFunctions } from '../../FunctionManagement/functionManagement.slice';

// custom component

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';

//icon
import { ImDownload3 } from 'react-icons/im';
import { FiUpload } from 'react-icons/fi';
import { RiErrorWarningFill } from 'react-icons/ri';

export default function ModalImportFunction({ isShowModal, action, setIsShowModal }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [file, setFile] = useState();
  const [logFile, setLogFile] = useState();
  const templateUrl = useRef('https://storage.googleapis.com/slpm/TemplateFunction.xlsx');

  const dataSource = [
    {
      key: '1',
      a: 'Project id',
      b: 'Feature name',
      c: 'Function name',
      d: 'Description',
      e: 'Priority',
      f: 'Status id',
      g: 'Function type id'
    }
  ];
  const columns = [
    {
      title: 'A',
      dataIndex: 'a',
      key: 'a'
    },
    {
      title: 'B',
      dataIndex: 'b',
      key: 'b'
    },
    {
      title: 'C',
      dataIndex: 'c',
      key: 'c'
    },
    {
      title: 'D',
      dataIndex: 'd',
      key: 'd'
    },
    {
      title: 'E',
      dataIndex: 'e',
      key: 'e'
    },
    {
      title: 'F',
      dataIndex: 'f',
      key: 'f'
    },
    {
      title: 'G',
      dataIndex: 'g',
      key: 'g'
    }
  ];

  const openInNewTab = url => {
    const win = window.open(url, '_blank');
    win.focus();
  };

  const beforeUpload = file => {
    const isLt20M = file.size / 1024 / 1024 < 20;
    if (!isLt20M)
      action(null, { action: 'showMessage', data: { description: 'File must smaller than 20MB!', type: 'fail' } });
    return isLt20M;
  };

  const resetModal = () => {
    setLogFile(null);
    setFile(null);
  };

  const uploadFile = async () => {
    try {
      setLoading(true);
      setLogFile(null);
      const data = new FormData();
      data.append('file', file);
      const res = await dispatch(importFunctions(data));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        setFile(null);
      }
    } catch (error) {
      action(null, {
        action: 'showMessage',
        data: {
          description: isEmpty(error.data.message)
            ? error.data.message
            : 'An error occur when importing functions, please re-check imported file and try again!',
          type: error.data.success ? 'success' : 'fail'
        }
      });
      setLogFile(error.data.data);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Modal
      confirmLoading={loading}
      title={`Import functions`}
      visible={isShowModal}
      width={'1000'}
      centered={true}
      className={'modal-center-footer modal-center-header'}
      footer={
        <Button className="br-8" onClick={uploadFile}>
          Import file
        </Button>
      }
      onCancel={() => setIsShowModal(false)}
      afterClose={resetModal}
    >
      <Row className={`text-danger`}>
        <RiErrorWarningFill className={'m-auto'} />
        Note that the fields in the excel file must be in the order below to import functions information to the
        database correctly and not in the wrong field!
      </Row>
      <Row className={`mb-4`}>
        <span
          className={'cursor-pointer'}
          style={{ color: '#1e70cd', textDecoration: 'underline' }}
          onClick={() => openInNewTab(templateUrl.current)}
        >
          <ImDownload3 className="m-auto" />
          <b>Download template file</b>
        </span>
      </Row>
      <Table className={'mb-4'} dataSource={dataSource} columns={columns} pagination={false} />
      <Row>
        <Upload
          accept={'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'}
          maxCount={1}
          className={'d-flex'}
          beforeUpload={beforeUpload}
          onChange={file => {
            if (file.file) setFile(file.file.originFileObj);
          }}
          progress={{
            strokeColor: {
              '0%': '#108ee9',
              '100%': '#87d068'
            },
            strokeWidth: 3,
            format: percent => percent && `${parseFloat(percent.toFixed(2))}%`
          }}
        >
          <Button>
            <FiUpload />
            Click to Upload
          </Button>
        </Upload>
      </Row>
      {!isEmpty(logFile) && (
        <Row className={`mt-3`}>
          <span
            className={'cursor-pointer'}
            style={{ color: 'red', textDecoration: 'underline' }}
            onClick={() => openInNewTab(logFile)}
          >
            <ImDownload3 className="m-auto" />
            <b>File uploaded fail, click here to download the error log file for more details!</b>
          </span>
        </Row>
      )}
    </Modal>
  );
}
ModalImportFunction.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  setIsShowModal: PropsTypes.func
};
ModalImportFunction.defaultProps = {
  isShowModal: false
};
