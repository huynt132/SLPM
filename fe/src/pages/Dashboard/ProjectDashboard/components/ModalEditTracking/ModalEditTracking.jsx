// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Select, Button, Spin } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// services
import { editTracking, getTrackingDetail } from '../../TrackingManagement/trackingManagement.slice';

// custom component

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';
const { TextArea } = Input;

export default function ModalEditTracking({ isShowModal, trackingId, action }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [tracking, setTracking] = useState({});

  useEffect(() => {
    if (!isEmpty(trackingId)) getTrackingDetails();
  }, [trackingId]);

  async function getTrackingDetails() {
    try {
      setLoading(true);
      const res = await dispatch(getTrackingDetail({ id: trackingId }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        let trackingDetail = { ...result.data.data, historyString: '' };
        trackingDetail.history.map(e => {
          trackingDetail.historyString += e + '\n';
        });
        setTracking(prev => {
          return { ...prev, ...trackingDetail };
        });
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message)
              ? 'An error occurred while get tracking detail!'
              : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  async function updateTracking() {
    try {
      setLoading(true);
      const res = await dispatch(editTracking(tracking));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        setTracking(prev => {
          return { ...prev, ...result.data.data };
        });
        action(null, { action: 'isHideModalEditTracking' });
        action(null, { action: 'triggerSearch' });
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message)
              ? 'An error occurred while get tracking detail!'
              : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  return (
    <Modal
      className={'modal-center-footer'}
      confirmLoading={loading}
      title={
        <>
          <h3>Tracking details</h3>
          <span>{`${tracking.projectName} - ${tracking.iterationName}`}</span>
        </>
      }
      visible={isShowModal}
      footer={
        <Button className="" onClick={updateTracking}>
          Submit
        </Button>
      }
      width={1000}
      okText={'Submit'}
      onCancel={() => {
        action(null, { action: 'isHideModalEditTracking' });
      }}
    >
      <Spin tip="Loading..." spinning={loading}>
        <Row className={`py-1`}>
          <Col span={16}>
            Function
            <Input value={tracking.functionName} disabled={true} placeholder="Function name" className="br-8" />
          </Col>
          <Col offset={1} span={7}>
            Feature
            <Input value={tracking.featureName} disabled={true} placeholder="Feature name" className="br-8" />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={24}>
            Function Description
            <TextArea
              className={'text-black br-8'}
              value={tracking.functionDescription}
              rows={3}
              placeholder="Function Description"
              disabled={true}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={16}>
            Assignee
            <Select
              mode={'single'}
              showSearch
              allowClear
              style={{
                width: '100%'
              }}
              value={tracking.assigneeId}
              filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
              placeholder={'Select assignee'}
              onChange={value => {
                setTracking(prev => {
                  return { ...prev, assigneeId: value };
                });
              }}
              options={tracking.assigneeOptions}
              className={'border-radius-select'}
            ></Select>
          </Col>
          <Col offset={1} span={7}>
            Submitting status
            <Input value={tracking.status} disabled={true} placeholder="Submit status" className="br-8" />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={24}>
            Tracking history
            <TextArea
              className={'text-black br-8'}
              value={tracking.historyString}
              rows={6}
              placeholder="Function Description"
              disabled={true}
            />
          </Col>
        </Row>
      </Spin>
    </Modal>
  );
}

ModalEditTracking.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  track: PropsTypes.object
};
ModalEditTracking.defaultProps = {
  isShowModal: false,
  track: {}
};
