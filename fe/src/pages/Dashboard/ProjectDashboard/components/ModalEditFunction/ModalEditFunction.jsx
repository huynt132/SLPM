// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// services
import { editFunction, addFunction } from '../../FunctionManagement/functionManagement.slice.js';

// custom component
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import SelectFeatureTag from 'src/components/Select/SelectFeatureTag';
import SelectLabel from 'src/components/Select/SelectLabel';

// helper
import { isEmpty } from 'src/utils/helper';
import { showMessage } from 'src/utils/commonHelper';
import SelectProject from 'src/components/Select/SelectProject';
import SelectFunctionStatus from 'src/components/Select/SelectFunctionStatus';
import SelectFunctionType from 'src/components/Select/SelectFunctionType';

export default function ModalEditFunction({ isShowModal, type, projectFunction, action }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [newFunction, setNewFunction] = useState({ ...projectFunction });

  useEffect(() => {
    setNewFunction(projectFunction);
  }, [projectFunction]);

  useEffect(() => {
    if (type === 'Add') {
      setNewFunction(prev => {
        return { ...prev, featureName: null };
      }); // Auto xóa feature nếu user bỏ chọn projectId
    }
  }, [newFunction?.projectId]);

  useEffect(() => {
    setNewFunction(prev => {
      return { ...prev, status: 'pending' };
    });
  }, [newFunction?.status]);

  async function createFunction() {
    try {
      setLoading(true);
      let data = { ...newFunction };
      if (Array.isArray(data.featureName)) data.featureName = data.featureName[0];
      const res = await dispatch(addFunction(data));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        resetModal();
        action(null, { action: 'isHideModalEditFunction' });
        action(null, { action: 'triggerSearch' });
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while create data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  async function updateFunction() {
    try {
      setLoading(true);
      let data = { ...newFunction };
      if (Array.isArray(data.featureName)) data.featureName = data.featureName[0];
      const res = await dispatch(editFunction(data));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(result.data.data, { action: 'editFunctionSuccess' });
        resetModal();
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  function validateFunction() {
    if (isEmpty(newFunction.projectId)) return 'Project can not be empty, please choose project!';
    if (isEmpty(newFunction.featureName)) return 'Feature can not be empty, please choose feature!';
    if (isEmpty(newFunction.name)) return 'Function name can not be empty, please input function name!';
    if (isEmpty(newFunction.priority)) return 'Function priority can not be empty, please choose priority!';
    return null;
  }

  function handleSubmit() {
    // validate
    let errMsg = validateFunction();
    if (!isEmpty(errMsg)) {
      return action(null, {
        action: 'showMessage',
        data: {
          description: errMsg,
          type: 'fail'
        }
      });
    }
    if (type === 'Update') updateFunction();
    if (type === 'Add') createFunction();
  }

  function resetModal() {
    if (type === 'Add') setNewFunction({});
    if (type === 'Update') setNewFunction({ ...projectFunction });
  }

  return (
    <Modal
      width={650}
      confirmLoading={loading}
      title={`${type} function`}
      visible={isShowModal}
      onOk={() => {
        handleSubmit();
      }}
      afterClose={resetModal}
      okText={type}
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalEditFunction' });
      }}
    >
      <div>
        {type === 'Add' && (
          <>
            <Row className={'pb-1 font-weight-500'}>Project:</Row>
            <Row className={`pb-2`}>
              <SelectProject
                customClassName={'border-radius-select'}
                mode={'single'}
                placeholder={'select project'}
                onOptionSelected={value => {
                  setNewFunction(prev => {
                    return { ...prev, projectId: value };
                  });
                }}
                projects={isEmpty(newFunction.projectId) ? [] : newFunction.projectId}
              />
            </Row>
          </>
        )}
        <Row className={'pb-1 font-weight-500'}>Function name:</Row>
        <Row className={`pb-2`}>
          <Input
            value={newFunction.name}
            className={'br-8'}
            placeholder="Input function name"
            onChange={e => {
              setNewFunction(prev => {
                return { ...prev, name: e.target.value };
              });
            }}
          />
        </Row>

        <Row className={'pb-1 font-weight-500'}>Function labels:</Row>
        <Row className={`pb-2`}>
          <SelectLabel
            customClassName={'border-radius-select'}
            mode={'multiple'}
            onOptionSelected={value => {
              setNewFunction(prev => {
                return { ...prev, labels: value };
              });
            }}
            filter={{ projectId: newFunction?.projectId }}
            labels={isEmpty(newFunction.labels) ? [] : newFunction.labels}
            disabled={isEmpty(newFunction.projectId)}
          />
        </Row>
        <Row className={'pb-1 font-weight-500'}>Type:</Row>
        <Row className={`pb-2`}>
          <SelectFunctionType
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'select type'}
            onOptionSelected={value => {
              setNewFunction(prev => {
                return { ...prev, typeId: value };
              });
            }}
            disabled={isEmpty(newFunction?.projectId)}
            filter={{ projectId: newFunction.projectId }}
            functionType={isEmpty(newFunction.typeId) ? [] : newFunction.typeId}
          />
        </Row>
        <Row className={'pb-1 font-weight-500'}>
          <Col span={16}>Feature:</Col>
          <Col span={8}>Priority:</Col>
        </Row>
        <Row className={`pb-2`}>
          <Col span={15}>
            <SelectFeatureTag
              customClassName={'border-radius-select'}
              onOptionSelected={value => {
                if (value.length > 1) return; // set max selected = 1
                setNewFunction(prev => {
                  return { ...prev, featureName: value };
                });
              }}
              filter={{ projectId: newFunction?.projectId }}
              features={isEmpty(newFunction.featureName) ? [] : newFunction.featureName}
              disabled={isEmpty(newFunction.projectId)}
            />
          </Col>
          <Col offset={1} span={8}>
            <Input
              value={newFunction.priority}
              className={'br-8'}
              type="number"
              placeholder="Input priority"
              onChange={e => {
                setNewFunction(prev => {
                  return { ...prev, priority: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={'pb-1 font-weight-500'}>
          <Col span={16}>Status:</Col>
          <Col span={8}>Submit status:</Col>
        </Row>
        <Row className={`pb-2`}>
          <Col span={15}>
            <SelectFunctionStatus
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'select status'}
              onOptionSelected={value => {
                setNewFunction(prev => {
                  return { ...prev, statusId: value };
                });
              }}
              disabled={isEmpty(newFunction?.projectId)}
              filter={{ projectId: newFunction?.projectId }}
              functionStatus={isEmpty(newFunction.statusId) ? [] : newFunction.statusId}
            />
          </Col>
          <Col offset={1} span={8}>
            <SelectCustomValue
              disabled={true}
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'Select submit status'}
              onOptionSelected={value => {
                setNewFunction(prev => {
                  return { ...prev, status: value };
                });
              }}
              query={{
                table: 'functions',
                field: 'status'
              }}
              values={isEmpty(newFunction.status) ? [] : newFunction.status}
            />
          </Col>
        </Row>
        <Row className={'pb-1 font-weight-500'}>Description:</Row>
        <Row className={`pb-2`}>
          <Input.TextArea
            value={newFunction.description}
            className={'text-black br-8'}
            autoSize={{ minRows: 3, maxRows: 3 }}
            placeholder="Input function description"
            onChange={e => {
              setNewFunction(prev => {
                return { ...prev, description: e.target.value };
              });
            }}
          />
        </Row>
      </div>
    </Modal>
  );
}

ModalEditFunction.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  type: PropsTypes.string,
  projectFunction: PropsTypes.object
};
ModalEditFunction.defaultProps = {
  isShowModal: false,
  type: 'Add',
  projectFunction: {}
};
