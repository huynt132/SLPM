// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Popconfirm, Button, Space, Table, Tag, Row, Col, Input, Spin, Switch } from 'antd';
// style
import { VscEdit } from 'react-icons/vsc';
import { AiOutlinePlus } from 'react-icons/ai';
// services
import { editProject, getList } from './projectManagement.slice.js';
// custom component
import SelectIteration from 'src/components/Select/SelectIteration';
import SelectClass from 'src/components/Select/SelectClass';
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import ModalEditProject from '../components/ModalEditProject/ModalEditProject';
import { path } from 'src/constants/path';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';

const ProjectManagement = () => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [errorType, setErrorType] = useState('');
  const [dataSource, setDataSource] = useState([]);
  const curProject = useRef({});
  const curProjectIdx = useRef(0);
  const editProjectType = useRef('add');
  const pagination = useRef({
    limit: 8,
    page: 1,
    total: 0
  });

  const userRole = useSelector(state => state.auth.role);

  const [filter, setFilter] = useState({
    name: null,
    iterationId: null,
    classId: null,
    status: null
  });

  useEffect(() => {
    getProjectList(filter);
  }, [dispatch]);

  const getProjectList = async filters => {
    try {
      setLoading(true);
      let query = {
        ...filters,
        ...pagination.current
      };
      if (!isEmpty(query.iterationId)) query.iterationId = query.iterationId.join(',');
      if (!isEmpty(query.classId)) query.classId = query.classId.join(',');
      let res = await dispatch(getList(query));
      res = unwrapResult(res);
      if (res.data.success) {
        setDataSource(res.data.data);
        pagination.current.total = res.data.total;
        showMessage(res.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setErrorType('tokenExpired');
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
      }
    } finally {
      setLoading(false);
    }
  };

  const clearFilters = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    setFilter(prev => {
      return { ...prev, name: null, iterationId: null, classId: null, status: null };
    });
    getProjectList(filter);
  };

  const search = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    getProjectList(filter);
  };

  async function editProjectStatus() {
    if (isEmpty(curProject.current)) return showMessage('Update data is invalid, please check again!', 'fail');
    try {
      setLoading(true);
      const res = await dispatch(
        editProject({
          ...curProject.current,
          status: curProject.current.status === 'active' ? 'inactive' : 'active'
        })
      );
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        curProject.current.status = curProject.current.status === 'active' ? 'inactive' : 'active';
      }
    } catch (error) {
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while update data!');
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  }

  const action = (rowData, action) => {
    if (!isEmpty(rowData)) curProject.current = rowData;
    if (action.action === 'isHideModalEditProject') {
      setIsEditing(false);
    }
    if (action.action === 'addProjectSuccess') {
      setDataSource(prevList => {
        return [...prevList, curProject.current];
      });
      setIsEditing(false);
    }
    if (action.action === 'editProjectStatus') {
      editProjectStatus();
    }
    if (action.action === 'editProjectSuccess') {
      setDataSource(prevList => {
        let tmp = [...prevList];
        tmp[curProjectIdx.current] = rowData;
        return [...tmp];
      });
      setIsEditing(false);
    }
    if (action.action === 'editProject') {
      editProjectType.current = 'Update';
      curProjectIdx.current = action.data;
      setIsEditing(true);
    }
    if (action.action === 'addProject') {
      editProjectType.current = 'Add';
      setIsEditing(true);
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    // {
    //   title: 'Class Code',
    //   dataIndex: 'classCode',
    //   key: 'classCode'
    // },
    {
      title: 'Team Code',
      dataIndex: 'code',
      className: 'w-10',
      key: 'code'
    },
    {
      title: 'Topic Code',
      dataIndex: 'topicCode',
      key: 'topicCode'
    },
    {
      title: 'Topic Name',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'Status',
      key: 'status',
      className: 'w-10',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-5`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Created By',
    //   dataIndex: 'createdByUser',
    //   key: 'createdByUser'
    // },
    // {
    //   title: 'Created',
    //   dataIndex: 'created',
    //   key: 'created'
    // },
    {
      title: 'Action',
      key: 'action',
      className: 'w-15',
      render: (_, record, idx) => {
        return (
          userRole !== 'Student' && (
            <Space size="middle">
              <Popconfirm
                placement="top"
                title={`Are you sure you want to ${record.status === 'active' ? 'deactivate' : 'activate'} this team?`}
                onConfirm={() => action(record, { action: 'editProjectStatus' })}
                okText="Yes"
                cancelText="No"
              >
                {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
                <Switch
                  className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
                  checked={record.status === 'active' ? true : false}
                />
              </Popconfirm>
              {/* <Button
                className="br-10 btn-edit d-flex"
                onClick={() => {
                  action(record, { action: 'editProject', data: idx });
                }}
              >
                <VscEdit className="text-white m-auto" />
              </Button> */}
              <Button
                style={{
                  borderRadius: '10px',
                  width: '30px',
                  height: '30px',
                  border: 'none',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                  color: '#fff',
                  background: '#6BBCB3'
                }}
                onClick={() => action(record, { action: 'editProject', data: idx })}
              >
                <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
              </Button>
            </Space>
          )
        );
      }
    }
  ];

  return (
    <Spin tip="Loading..." spinning={loading}>
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Team Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Student' && (
            <Button
              style={{ padding: '0 4rem' }}
              className="br-10 btn-action d-flex w-100 h-80 px-2"
              onClick={() =>
                action(
                  { classId: null, code: null, name: null, gitlabUrl: null, status: null },
                  { action: 'addProject' }
                )
              }
            >
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Team</span>
            </Button>
          )}
        </Col>
      </Row>
      {/*<hr className={'hr'} />*/}
      <Row className={'mt-2 mb-4'}>
        {/* <Col span={4} className="pr-1">
          <Input
            value={filter.name}
            placeholder="Input project code"
            className="br-8"
            onChange={e => {
              setFilter(prev => {
                return { ...prev, title: e.target.value };
              });
            }}
          />
        </Col> */}
        <Col span={4} className="pr-1">
          <SelectClass
            mode={'single'}
            customClassName={'border-radius-select'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, classId: value };
              });
            }}
            classes={isEmpty(filter.classId) ? [] : filter.classId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'Select status'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, status: value };
              });
            }}
            query={{
              table: 'projects',
              field: 'status'
            }}
            values={isEmpty(filter.status) ? [] : filter.status}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={filter.name}
            placeholder="Input topic name"
            className="br-8"
            onChange={e => {
              setFilter(prev => {
                return { ...prev, name: e.target.value };
              });
            }}
          />
        </Col>
        <Col offset={8} span={2} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={clearFilters}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={search}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        // loading={loading}
        columns={columns}
        rowKey={record => 'project_' + record.id}
        dataSource={dataSource}
        pagination={{
          className: 'border-radius-paging',
          position: 'bottomLeft',
          pageSize: pagination.current.limit,
          total: pagination.current.total,
          current: pagination.current.page,
          onChange: (page, pageSize) => {
            pagination.current.page = page;
            getProjectList(filter);
          }
          // showSizeChanger: true
        }}
      />
      <ModalEditProject
        isShowModal={isEditing}
        type={editProjectType.current}
        project={curProject.current}
        action={action}
      />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        text={messageText}
        iconname={messageType}
        navigateTo={path.absProject}
        type={errorType}
      />
    </Spin>
  );
};

export default ProjectManagement;
