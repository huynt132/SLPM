import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import projectManagementApi from 'src/api/projectManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getList = createAsyncThunk('projects/getAll', payloadCreator(projectManagementApi.getProjects));
export const editProject = createAsyncThunk('projects/edit', payloadCreator(projectManagementApi.editProject));
export const addProject = createAsyncThunk('projects/add', payloadCreator(projectManagementApi.addProject));
export const getAllProjects = createAsyncThunk('projects/getList', payloadCreator(projectManagementApi.getAllProjects));

const projectManagement = createSlice({
  name: 'projectManagement',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const projectManagementReducer = projectManagement.reducer;

export default projectManagementReducer;
