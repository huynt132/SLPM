import React, { useEffect, useState } from 'react';
import { Button, Space, Table, Row, Col, Input, Tag, Popconfirm, Spin, Switch } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import { path } from 'src/constants/path';
import { AiOutlinePlus } from 'react-icons/ai';
import { addFeature, editFeature, getAllFeature, getAllProject } from './featureManagement.slice';
import ModalUpdate from './Components/UpdateModal/UpdateModal';
import ModalAdd from './Components/AddModal/AddModal';
import SelectProject from 'src/components/Select/SelectProject';
import SelectCustomValue from '../../../../components/Select/SelectCustomValue';
import { isEmpty } from '../../../../utils/helper';
import { showMessage } from 'src/utils/commonHelper';

const FeatureManagement = () => {
  const dispatch = useDispatch();

  const [paramsSearch, setParamsSearch] = useState({
    projectId: null,
    name: null,
    status: null,
    page: 1,
    limit: 8,
    total: 200
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);

  const [errorType, setErrorType] = useState('');

  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  // state handle Edit
  const [isEditing, setIsEditing] = useState(false);
  const [editRecord, setEditRecord] = useState(null);
  // state handle Add
  const [isAdding, setIsAdding] = useState(false);
  const [addRecord, setAddRecord] = useState(null);
  // data source table
  const [dataSource, setDataSource] = useState([]);
  // state handle row select

  const [currentProject, setCurrentProject] = useState('');

  // get api list feature
  const getListFeature = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getAllFeature({ params }));
      const res = unwrapResult(data);
      const dataTable = res.data.data.map(item => {
        return {
          key: item.id,
          id: item.id,
          code: item.code,
          name: item.name,
          projectId: item.projectId,
          projectName: item.projectName,
          status: item.status,
          created: item.createdByUser
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setCurrentProject(res.data.curProjectId);
      setDataSource(dataTable);
      showMessage(res.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setIsShowModal(true);
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  // call api
  useEffect(() => {
    getListFeature(paramsSearch);
  }, [dispatch]);

  // =======================ADD============================
  const handleClickBtnAdd = () => {
    setIsAdding(true);
    setAddRecord(null);
  };

  const handleAddingSetting = async () => {
    try {
      setButtonLoading(true);
      const res = await dispatch(addFeature(addRecord));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setButtonLoading(false);
      showMessage(res.payload.data.message);
      setIsAdding(false);
      setDataSource(prevList => {
        return [
          ...prevList,
          {
            key: result.data.data.id,
            id: result.data.data.id,
            code: result.data.data.code,
            name: result.data.data.name,
            projectName: result.data.data.projectName,
            projectId: result.data.data.projectId,
            status: result.data.data.status,
            created: result.data.data.createdByUser
          }
        ];
      });
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // =======================EDIT===========================

  const onClickBtnEdit = record => {
    setIsEditing(true);
    setEditRecord(prevState => {
      return { ...prevState, ...record };
    });
  };

  // get api edit subject
  const onEditSetting = async body => {
    try {
      setLoading(true);
      const res = await dispatch(editFeature(body));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setLoading(false);
      setDataSource(prevList =>
        prevList.map(item =>
          item.id === result.data.data.id
            ? {
                ...item,
                code: result.data.data.code,
                name: result.data.data.name,
                projectId: result.data.data.projectId,
                projectName: result.data.data.projectName,
                status: result.data.data.status,
                created: result.data.data.createdByUser
              }
            : item
        )
      );
      showMessage(res.payload.data.message);
      setIsEditing(false);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
        setLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  //handle btn active and inactive - edit setting
  const handleToggleActive = record => {
    if (record.status === 'active') {
      const body = {
        id: record.id,
        code: record.code,
        name: record.name,
        projectId: record.projectId,
        status: 'inactive',
        created: record.createdByUser
      };
      onEditSetting(body);
    } else {
      const body = {
        id: record.id,
        code: record.code,
        name: record.name,
        projectId: record.projectId,
        status: 'active',
        created: record.createdByUser
      };
      onEditSetting(body);
    }
  };

  // handle accept edit
  const handleEditRecord = () => {
    // setIsEditing(false);
    const body = {
      id: editRecord.id,
      code: editRecord.code,
      name: editRecord.name,
      projectId: editRecord.projectId,
      status: editRecord.status,
      created: editRecord.createdByUser
    };
    onEditSetting(body);
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    {
      title: 'Feature Name',
      dataIndex: 'name',
      key: 'name'
    },
    // {
    //   title: 'Project Name',
    //   dataIndex: 'projectName',
    //   key: 'projectName'
    // },
    // {
    //   title: 'Created',
    //   dataIndex: 'created',
    //   key: 'created'
    // },
    {
      title: 'Status',
      key: 'status',
      className: 'w-10',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-10`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    {
      title: 'Action',
      className: 'w-15',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            placement="top"
            title={`Are you sure you want to ${record.status === 'active' ? 'deactivate' : 'activate'} this setting?`}
            onConfirm={() => handleToggleActive(record)}
            okText="Yes"
            cancelText="No"
          >
            {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}

            <Switch
              className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
              checked={record.status === 'active' ? true : false}
            />
          </Popconfirm>
          <Button
            style={{
              borderRadius: '10px',
              width: '30px',
              height: '30px',
              border: 'none',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              color: '#fff',
              background: '#6BBCB3'
            }}
            onClick={() => onClickBtnEdit(record)}
          >
            <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
          </Button>
        </Space>
      )
    }
  ];

  // handle pagination as antd
  const handleTableChange = value => {
    getListFeature({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const handleSearch = () => {
    getListFeature({
      ...paramsSearch,
      page: 1
    });
  };

  // handle reset filter

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        projectId: null,
        name: null,
        status: null,
        page: 1,
        limit: 8
      };
    });
    getListFeature({
      ...paramsSearch,
      projectId: null,
      name: null,
      status: null,
      page: 1,
      limit: 8
    });
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.featureManagement}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />

      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Feature Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          <Button className="br-10 btn-action d-flex w-100 h-80" onClick={handleClickBtnAdd}>
            <AiOutlinePlus className="icon-plus m-auto" />
            <span className="m-auto">Create feature</span>
          </Button>
        </Col>
      </Row>

      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="pr-1">
          <SelectProject
            customClassName={'border-radius-select'}
            mode={'single'}
            onOptionSelected={value => {
              setParamsSearch(prev => {
                return { ...prev, projectId: value };
              });
            }}
            currentProject={currentProject}
            placeholder={'Select project'}
            projects={isEmpty(paramsSearch?.projectId) ? [] : paramsSearch.projectId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'Select status'}
            onOptionSelected={value => {
              setParamsSearch(prev => {
                return { ...prev, status: value };
              });
            }}
            query={{
              table: 'features',
              field: 'status'
            }}
            values={isEmpty(paramsSearch.status) ? [] : paramsSearch.status}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.name}
            placeholder="Input feature name"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, name: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={2} className="px-1" offset={8}>
          <Button className="br-10 btn-grey w-100" onClick={handleResetFilter}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total,
          pageSizeOptions: [5, 10, 25, 100]
        }}
        // loading={loading}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />
      <ModalUpdate
        isEditing={isEditing}
        setIsEditing={setIsEditing}
        handleEditRecord={handleEditRecord}
        editRecord={editRecord}
        setEditRecord={setEditRecord}
        buttonLoading={loading}
      />
      <ModalAdd
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        handleAddingSetting={handleAddingSetting}
        setAddRecord={setAddRecord}
        buttonLoading={buttonLoading}
        addRecord={addRecord}
      />
    </Spin>
  );
};

export default FeatureManagement;
