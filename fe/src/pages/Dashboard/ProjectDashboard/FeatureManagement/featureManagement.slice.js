import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import featureManagement from 'src/api/featureManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getAllFeature = createAsyncThunk('feature/getAll', payloadCreator(featureManagement.getAllFeature));
export const getList = createAsyncThunk('feature/getList', payloadCreator(featureManagement.getList));

export const getAllProject = createAsyncThunk(
  'feature/getListProject',
  payloadCreator(featureManagement.getAllProject)
);

export const editFeature = createAsyncThunk('feature/edit', payloadCreator(featureManagement.editFeature));

export const addFeature = createAsyncThunk('feature/add', payloadCreator(featureManagement.addFeature));

const featureList = createSlice({
  name: 'featureManagement',
  initialState: {
    featureListManager: [],
    listProjectCode: [],
    listProject: []
  },
  reducers: {},
  extraReducers: {
    [getAllFeature.fulfilled]: (state, action) => {
      state.featureListManager = action.payload.data.data.map(feature => ({
        label: feature.featureName,
        value: feature.id
      }));
      state.listFeatureCode = action.payload.data.data;
    },
    [getAllProject.fulfilled]: (state, action) => {
      state.listProject = action.payload.data.data;
    }
  }
});

const featureReducer = featureList.reducer;

export default featureReducer;
