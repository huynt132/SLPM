import { Input, Modal, Radio, Select } from 'antd';
import React, { useEffect } from 'react';
import styles from './AddModal.module.css';
import PropTypes from 'prop-types';
import SelectProject from '../../../../../../components/Select/SelectProject';
import { isEmpty } from '../../../../../../utils/helper';

const { Option } = Select;
const { TextArea } = Input;

export default function ModalAdd({
  addRecord,
  isAdding,
  setIsAdding,
  handleAddingSetting,
  setAddRecord,
  buttonLoading
}) {
  useEffect(() => {
    if (isEmpty(addRecord?.status))
      setAddRecord(prev => {
        return { ...prev, status: 'active' };
      });
  }, [addRecord?.status]);

  return (
    <Modal
      title="Add Feature"
      visible={isAdding}
      onOk={handleAddingSetting}
      okText={'Add'}
      onCancel={() => {
        setIsAdding(false);
        setAddRecord(null);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Project</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectProject
              customClassName={'border-radius-select w-100'}
              mode={'single'}
              onOptionSelected={value => {
                setAddRecord(record => {
                  return { ...record, projectId: value };
                });
              }}
              placeholder={'Select project'}
              projects={isEmpty(addRecord?.projectId) ? [] : addRecord.projectId}
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Feature Name</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            value={addRecord?.name}
            placeholder="Input Feature Name"
            onChange={e =>
              setAddRecord(record => {
                return { ...record, name: e.target.value };
              })
            }
          />
        </div>

        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              value={addRecord?.status}
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
      </div>
    </Modal>
  );
}

ModalAdd.propTypes = {
  isAdding: PropTypes.bool,
  setIsAdding: PropTypes.func,
  handleAddingSetting: PropTypes.func,
  setAddRecord: PropTypes.func,
  buttonLoading: PropTypes.bool
};
