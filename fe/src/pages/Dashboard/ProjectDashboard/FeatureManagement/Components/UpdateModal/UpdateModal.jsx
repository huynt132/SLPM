import { Input, Modal, Radio, Select } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import styles from './ModalUpdate.module.css';
import PropTypes from 'prop-types';
import SelectProject from '../../../../../../components/Select/SelectProject';
import { isEmpty } from '../../../../../../utils/helper';

export default function ModalUpdate({
  isEditing,
  setIsEditing,
  handleEditRecord,
  setEditRecord,
  editRecord,
  buttonLoading
}) {
  const listProject = useSelector(state => state.featureManagement.listProject);
  return (
    <Modal
      title="Update Information"
      visible={isEditing}
      onOk={handleEditRecord}
      okText={'Update'}
      onCancel={() => {
        setIsEditing(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Feature Name</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            value={editRecord?.name}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, name: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Project Name</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectProject
              customClassName={'border-radius-select w-100'}
              mode={'single'}
              onOptionSelected={value => {
                setEditRecord(record => {
                  return { ...record, projectId: value };
                });
              }}
              placeholder={'Select project'}
              disabled={true}
              projects={isEmpty(editRecord?.projectId) ? [] : editRecord.projectId}
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Edit Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              value={editRecord?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func,
  handleEditRecord: PropTypes.func,
  setEditRecord: PropTypes.func,
  editRecord: PropTypes.object,
  buttonLoading: PropTypes.bool
};
