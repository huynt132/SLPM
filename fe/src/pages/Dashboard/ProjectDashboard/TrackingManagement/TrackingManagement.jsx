// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Input, Button, Space, Table, Row, Col, Spin } from 'antd';
// style
import { VscEdit } from 'react-icons/vsc';
import { AiOutlinePlus } from 'react-icons/ai';
// services
import { editTracking, getList } from './trackingManagement.slice.js';
// custom component
import ModalEditTracking from '../components/ModalEditTracking/ModalEditTracking';
import ModalNewTracking from '../components/ModalNewTracking/ModalNewTracking';
import ModalLocEvaluation from '../components/ModalLocEvaluation/ModalLocEvaluation';
import ModalNewLocEvaluation from '../components/ModalNewLocEvaluation/ModalNewLocEvaluation';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import SelectProject from 'src/components/Select/SelectProject';
import SelectMilestone from 'src/components/Select/SelectMilestone';
import SelectUser from 'src/components/Select/SelectUser';

import { path } from 'src/constants/path';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalUpdateTracking from '../components/ModalUpdateTracking/ModalUpdateTracking.jsx';

const TrackingManagement = () => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [isAdding, setIsAdding] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isShowMessage, setIsShowMessage] = useState(false);
  const [isEvaluating, setIsEvaluating] = useState(false);
  const [isNewLocEvaluating, setIsNewLocEvaluating] = useState(false);
  const [errorType, setErrorType] = useState('');
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [dataSource, setDataSource] = useState([]);
  const curTracking = useRef({});
  const curTrackingIdx = useRef(0);
  const editTrackingType = useRef('add');
  const pagination = useRef({
    limit: 8,
    page: 1,
    total: 0
  });

  const role = useSelector(state => state.auth.role);

  const [filter, setFilter] = useState({
    title: null,
    iterationId: null,
    classId: null,
    status: null,
    functionName: null
  });

  const [evaluateProps, setEvaluateProps] = useState({
    functionId: null
  });

  // Duc Anh
  const [isShowModalUpdateTracking, setIsShowModalUpdateTracking] = useState(false);
  const [paramModalUpdateTracking, setParamModalUpdateTracking] = useState({
    projectId: null,
    milestoneId: null,
    functionName: null,
    milestoneName: null
  });

  useEffect(() => {
    getTrackingList(filter);
  }, [dispatch]);

  const getTrackingList = async filters => {
    try {
      setLoading(true);
      let query = {
        ...filters,
        ...pagination.current
      };
      if (!isEmpty(query.iterationId)) query.iterationId = query.iterationId.join(',');
      if (!isEmpty(query.classId)) query.classId = query.classId.join(',');
      const res = await dispatch(getList(query));
      const result = unwrapResult(res);
      if (result.data.success) {
        setDataSource(result.data.data);
        if (!isEmpty(result.data.curProjectId))
          setFilter(prev => {
            return { ...prev, projectId: result.data.curProjectId };
          });
        pagination.current.total = result.data.total;
        showMessage(result.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  const clearFilters = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    setFilter(prev => {
      return { ...prev, title: null, iterationId: null, classId: null, status: null, functionName: null };
    });
    getTrackingList(null);
  };

  const validateSearch = () => {
    if (isEmpty(filter.projectId)) return 'Please select project!';
    return null;
  };

  const search = () => {
    let errorMsg = validateSearch();
    if (!isEmpty(errorMsg)) {
      return action(null, {
        action: 'showMessage',
        data: {
          description: errorMsg,
          type: 'fail'
        }
      });
    }

    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    getTrackingList(filter);
  };

  async function editTrackingStatus() {
    if (isEmpty(curTracking.current)) return showMessage('Update data is invalid, please check again!', 'fail');
    try {
      setLoading(true);
      const res = await dispatch(
        editTracking({
          ...curTracking.current,
          status: curTracking.current.status === 'active' ? 'inactive' : 'active'
        })
      );
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        curTracking.current.status = curTracking.current.status === 'active' ? 'inactive' : 'active';
      }
    } catch (error) {
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while update data!');
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  }

  const action = (rowData, action) => {
    if (!isEmpty(rowData)) curTracking.current = rowData;
    if (action.action === 'triggerSearch') {
      getTrackingList(filter);
    }
    if (action.action === 'isHideModalEditTracking') {
      setIsEditing(false);
    }
    if (action.action === 'isHideModalNewTracking') {
      setIsAdding(false);
    }
    if (action.action === 'isHideModalLocEvaluation') {
      setIsEvaluating(false);
    }
    if (action.action === 'isHideModalNewLocEvaluation') {
      setIsNewLocEvaluating(false);
    }
    if (action.action === 'addTrackingSuccess') {
      setDataSource(prevList => {
        return [...prevList, curTracking.current];
      });
      setIsEditing(false);
    }
    if (action.action === 'editTrackingStatus') {
      editTrackingStatus();
    }
    if (action.action === 'editTrackingSuccess') {
      setDataSource(prevList => {
        let tmp = [...prevList];
        tmp[curTrackingIdx.current] = rowData;
        return [...tmp];
      });
      setIsEditing(false);
    }
    if (action.action === 'editTracking') {
      editTrackingType.current = 'Update';
      curTrackingIdx.current = action.data;
      setIsEditing(true);
    }
    if (action.action === 'addTracking') {
      setIsAdding(true);
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
    if (action.action === 'evaluateTracking') {
      setEvaluateProps(prev => {
        return { ...prev, functionId: rowData.functionId };
      });
      setIsEvaluating(true);
    }

    if (action.action === 'newEvaluateTracking') {
      setEvaluateProps(prev => {
        return { ...prev, functionId: rowData.functionId };
      });
      setIsNewLocEvaluating(true);
    }
  };

  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      className: 'text-top',
      key: 'id'
    },
    {
      title: 'Function name',
      dataIndex: 'functionName',
      key: 'functionName',
      className: 'text-top'
    },
    {
      title: 'Feature name',
      dataIndex: 'featureName',
      key: 'featureName',
      className: 'text-top'
    },
    {
      title: 'Milestone',
      dataIndex: 'milestoneTitle',
      key: 'milestoneTitle',
      className: 'text-top'
    },
    {
      title: 'Assigner',
      dataIndex: 'assignerName',
      key: 'assignerName',
      className: 'text-top'
    },
    {
      title: 'Assignee',
      dataIndex: 'assigneeName',
      key: 'assigneeName',
      className: 'text-top'
    },
    {
      title: 'Is planned',
      dataIndex: 'isPlanned',
      key: 'isPlanned',
      render: record => <span>{record.status === 'planned' ? 'yes' : 'no'}</span>,
      className: 'text-top'
    },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
      className: 'text-top'
    },
    {
      title: 'Action',
      key: 'action',
      className: 'w-10 text-top',
      render: (_, record, idx) => (
        <Space size="middle">
          {/* <Button
            className="br-10 btn-edit d-flex"
            onClick={() => {
              action(record, { action: 'editTracking', data: idx });
            }}
          >
            <VscEdit className="text-white m-auto" />
          </Button> */}
          <Button
            style={{
              borderRadius: '10px',
              width: '30px',
              height: '30px',
              border: 'none',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              color: '#fff',
              background: '#6BBCB3'
            }}
            onClick={() => action(record, { action: 'editTracking', data: idx })}
          >
            <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
          </Button>
          <Button
            className="br-10 btn-edit d-flex"
            onClick={() => {
              setIsShowModalUpdateTracking(true);
              setParamModalUpdateTracking({
                projectId: record.projectId,
                milestoneId: record.milestoneId,
                functionName: record.functionName,
                functionId: record.functionId,
                milestoneName: record.milestoneTitle
              });
            }}
          >
            Tracking Update
          </Button>
          {role.toUpperCase() !== 'STUDENT' && record.status === 'submitted' && (
            <Button
              className="br-10 btn-edit d-flex text-white"
              onClick={() => {
                action(record, { action: 'evaluateTracking', data: idx });
              }}
            >
              Evaluate
            </Button>
          )}
          {role.toUpperCase() !== 'STUDENT' && record?.canNewLoc && (
            <Button
              className="br-10 btn-edit d-flex text-white"
              onClick={() => {
                action(record, { action: 'newEvaluateTracking', data: idx });
              }}
            >
              New Loc Evaluate
            </Button>
          )}
        </Space>
      )
    }
  ];
  return (
    <Spin tip="Loading..." spinning={loading}>
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Tracking Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          <Button
            style={{ padding: '0 4rem' }}
            className="br-10 btn-action d-flex w-100 h-80 px-2"
            onClick={() =>
              action(
                { classId: null, code: null, name: null, gitlabUrl: null, status: null },
                { action: 'addTracking' }
              )
            }
          >
            <AiOutlinePlus className="icon-plus m-auto" />
            <span className="m-auto">Create Tracking</span>
          </Button>
        </Col>
      </Row>
      <hr className={'hr'} />
      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="px-1">
          <SelectProject
            customClassName={'border-radius-select'}
            mode={'single'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, projectId: value };
              });
            }}
            projects={isEmpty(filter.projectId) ? [] : filter.projectId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectMilestone
            customClassName={'border-radius-select'}
            mode={'single'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, milestoneId: value };
              });
            }}
            filter={filter}
            milestones={isEmpty(filter.milestoneId) ? [] : filter.milestoneId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectUser
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'select assignee'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, assigneeId: value };
              });
            }}
            filter={filter}
            users={isEmpty(filter.assigneeId) ? [] : filter.assigneeId}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectCustomValue
            customClassName={'border-radius-select'}
            mode={'single'}
            placeholder={'Select status'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, status: value };
              });
            }}
            query={{
              table: 'trackings',
              field: 'status'
            }}
            values={isEmpty(filter.status) ? [] : filter.status}
          />
        </Col>
        <Col span={4} className="pr-1">
          <Input
            value={filter.functionName}
            placeholder="Input function name"
            className="br-8"
            onChange={e => {
              setFilter(prev => {
                return { ...prev, functionName: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={clearFilters}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={search}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        // loading={loading}
        columns={columns}
        rowKey={record => 'tracking_' + record.id}
        dataSource={dataSource}
        pagination={{
          className: 'border-radius-paging',
          position: 'bottomLeft',
          pageSize: pagination.current.limit,
          total: pagination.current.total,
          current: pagination.current.page,
          onChange: (page, pageSize) => {
            pagination.current.page = page;
            getTrackingList(filter);
          }
          // showSizeChanger: true
        }}
      />
      <ModalEditTracking isShowModal={isEditing} trackingId={curTracking.current.id} action={action} />
      <ModalNewTracking isShowModal={isAdding} action={action} />
      <ModalLocEvaluation isShowModal={isEvaluating} action={action} props={evaluateProps} />
      <ModalNewLocEvaluation isShowModal={isNewLocEvaluating} action={action} props={evaluateProps} />
      <ModalUpdateTracking
        isShowModal={isShowModalUpdateTracking}
        setIsShowModal={setIsShowModalUpdateTracking}
        paramModalUpdateTracking={paramModalUpdateTracking}
      />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        text={messageText}
        iconname={messageType}
        navigateTo={path.absTracking}
        type={errorType}
      />
    </Spin>
  );
};

export default TrackingManagement;
