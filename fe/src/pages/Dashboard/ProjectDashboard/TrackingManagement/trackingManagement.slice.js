import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import trackingManagementApi from 'src/api/trackingManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getList = createAsyncThunk('tracking/getAll', payloadCreator(trackingManagementApi.getTracking));
export const editTracking = createAsyncThunk('tracking/edit', payloadCreator(trackingManagementApi.editTracking));
export const addTracking = createAsyncThunk('tracking/add', payloadCreator(trackingManagementApi.addTracking));
export const getTrackingDetail = createAsyncThunk(
  'tracking/getTrackingDetail',
  payloadCreator(trackingManagementApi.getTrackingDetail)
);
export const getFunctionTracking = createAsyncThunk(
  'tracking/getFunctionTracking',
  payloadCreator(trackingManagementApi.getFunctionTracking)
);

export const submitTracking = createAsyncThunk(
  'tracking/submitTracking',
  payloadCreator(trackingManagementApi.submitTracking)
);

const trackingManagement = createSlice({
  name: 'trackingManagement',
  initialState: {},
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {}
});

const trackingManagementReducer = trackingManagement.reducer;

export default trackingManagementReducer;
