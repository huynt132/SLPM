import React, { useEffect, useState } from 'react';
import { Button, Space, Table, Row, Col, Tag, Popconfirm, Spin, Switch, Tooltip } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import { path } from 'src/constants/path';
import { AiOutlineEye, AiOutlinePlus } from 'react-icons/ai';
import SelectStatus from 'src/components/Select_DA/StatusSelect/SelectStatus';
import { addClassUser, getAll, updateClassUser } from './classUser.slice';
import ModalUpdate from './Components/ModalUpdate/ModalUpdate';
import SelectClass from 'src/components/Select_DA/ClassSelect/SelectClass';
import SelectProject from 'src/components/Select_DA/ProjectSelect/SelectProject';
import ModalAdd from './Components/ModalAdd/ModalAdd';
import ModalView from './Components/ModalView/ModalView';
import { BiImport } from 'react-icons/bi';
import { RiFileExcel2Line } from 'react-icons/ri';
import ModalImportClassUser from './Components/ModalImportClassUser/ModalImportClassUser';
import { isEmpty } from '../../../../utils/helper';
import { showMessage } from 'src/utils/commonHelper';

const ClassUserListDashboard = () => {
  const dispatch = useDispatch();
  const [paramsSearch, setParamsSearch] = useState({
    classId: null,
    projectId: null,
    userId: null,
    projectLead: null,
    status: null,
    limit: 10,
    page: 1,
    total: 0,
    getExcel: null
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  const [errorType, setErrorType] = useState('');
  // state handle Edit
  const [isEditing, setIsEditing] = useState(false);
  const [editRecord, setEditRecord] = useState(null);
  // state handle Add
  const [isAdding, setIsAdding] = useState(false);
  const [addRecord, setAddRecord] = useState({ projectLeader: 0, dropoutDate: '' });
  // state view detail
  const [isViewing, setIsViewing] = useState(false);
  const [viewRecord, setViewRecord] = useState(null);
  // data source table
  const [dataSource, setDataSource] = useState([]);
  // data import
  const [isImporting, setIsImporting] = useState(false);

  const [currentClass, setCurrentClass] = useState(null);

  const userRole = useSelector(state => state.auth.role);

  // get api list Setting
  const getListUser = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getAll({ params }));
      const res = unwrapResult(data);
      const dataTable = res.data.data.map((item, key) => {
        return {
          key: key,
          id: item.id,
          classId: item.classId,
          classCode: item.classCode,
          projectId: item.projectId,
          rollNumber: item.rollNumber,
          fullName: item.fullName,
          projectCode: item.projectCode,
          projectName: item.projectName,
          userId: item.userId,
          userEmail: item.userEmail,
          projectLeader: item.projectLeader === 1 ? 'Yes' : 'No',
          note: item.note,
          status: item.status,
          created: item.created,
          createdBy: item.createdBy,
          createdByUser: item.createdByUser,
          modifiedByUser: item.modifiedByUser,
          modified: item.modified,
          dropoutDate: item.dropoutDate,
          ongoingEval: item.ongoingEval,
          finalPresentEval: item.finalPresentEval,
          finalTopicEval: item.finalTopicEval,
          finalEval: item.finalEval
        };
      });
      setCurrentClass(res.data.curClass);

      setParamsSearch({
        ...params,
        classId: res.data.curClass,
        total: res.data.total
      });
      setDataSource(dataTable);
      setLoading(false);
      showMessage(res.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
      } else if (error.status === 401) {
        setErrorType('tokenExpired');
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
      }
    } finally {
      setLoading(false);
    }
  };

  const openInNewTab = url => {
    const win = window.open(url, '_blank');
    win.focus();
  };

  const exportClassUser = async params => {
    setLoading(true);
    const data = await dispatch(getAll({ params }));
    const res = unwrapResult(data);
    openInNewTab(res.data.data);
    setLoading(false);
  };

  // call api
  useEffect(() => {
    getListUser(paramsSearch);
  }, [dispatch]);

  // ======================View Detail ====================

  const handleViewDetail = record => {
    setIsViewing(true);
    setViewRecord(record);
  };

  // =======================ADD============================
  const handleClickBtnAdd = () => {
    setIsAdding(true);
  };

  const handleAddClass = async () => {
    try {
      setButtonLoading(true);
      let newRecord = { ...addRecord };
      if (isEmpty(newRecord.dropoutDate)) newRecord.dropoutDate = null;
      const res = await dispatch(addClassUser(newRecord));
      // handle when dispatch action have error
      unwrapResult(res);
      setButtonLoading(false);
      showMessage(res.payload.data.message);
      setIsAdding(false);
      setDataSource(prevList => {
        return [
          ...prevList,
          {
            id: res.payload.data.data.id,
            classId: res.payload.data.data.classId,
            classCode: res.payload.data.data.classCode,
            rollNumber: res.payload.data.data.rollNumber,
            fullName: res.payload.data.data.fullName,
            projectId: res.payload.data.data.projectId,
            projectCode: res.payload.data.data.projectCode,
            projectName: res.payload.data.data.projectName,
            userId: res.payload.data.data.userId,
            userEmail: res.payload.data.data.userEmail,
            projectLeader: res.payload.data.data.projectLeader === 1 ? 'Yes' : 'No',
            note: res.payload.data.data.note,
            status: res.payload.data.data.status,
            created: res.payload.data.data.created,
            createdBy: res.payload.data.data.createdBy,
            createdByUser: res.payload.data.data.createdByUser,
            modifiedByUser: res.payload.data.data.modifiedByUser,
            dropoutDate: res.payload.data.data.dropoutDate,
            ongoingEval: res.payload.data.data.ongoingEval,
            finalPresentEval: res.payload.data.data.finalPresentEval,
            finalTopicEval: res.payload.data.data.finalTopicEval,
            finalEval: res.payload.data.data.finalEval
          }
        ];
      });
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // =======================EDIT===========================

  const onClickBtnEdit = record => {
    setIsEditing(true);
    setEditRecord(prevState => {
      return { ...prevState, ...record };
    });
  };

  // get api edit class user
  const onUpdateClassUser = async body => {
    try {
      setLoading(true);
      setButtonLoading(true);
      const res = await dispatch(updateClassUser(body));
      // handle when dispatch action have error
      unwrapResult(res);
      setLoading(false);
      setButtonLoading(false);
      setDataSource(prevList =>
        prevList.map((item, key) =>
          item.id === res.payload.data.data.id
            ? {
                ...item,
                key: key,
                id: res.payload.data.data.id,
                classId: res.payload.data.data.classId,
                classCode: res.payload.data.data.classCode,
                rollNumber: res.payload.data.data.rollNumber,
                fullName: res.payload.data.data.fullName,
                projectId: res.payload.data.data.projectId,
                projectCode: res.payload.data.data.projectCode,
                projectName: res.payload.data.data.projectName,
                userId: res.payload.data.data.userId,
                userEmail: res.payload.data.data.userEmail,
                projectLeader: res.payload.data.data.projectLeader === 1 ? 'Yes' : 'No',
                note: res.payload.data.data.note,
                status: res.payload.data.data.status,
                created: res.payload.data.data.created,
                createdBy: res.payload.data.data.createdBy,
                createdByUser: res.payload.data.data.createdByUser,
                modifiedByUser: res.payload.data.data.modifiedByUser,
                dropoutDate: res.payload.data.data.dropoutDate,
                ongoingEval: res.payload.data.data.ongoingEval,
                finalPresentEval: res.payload.data.data.finalPresentEval,
                finalTopicEval: res.payload.data.data.finalTopicEval,
                finalEval: res.payload.data.data.finalEval
              }
            : item
        )
      );
      setIsEditing(false);
      showMessage(res.payload.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // handle btn active and deactivate - edit setting
  const handleToggleActive = record => {
    let isProjectLeader;
    if (typeof editRecord.projectLeader === 'number') {
      isProjectLeader = editRecord.projectLeader;
    } else {
      isProjectLeader = editRecord.projectLeader === 'No' ? 0 : 1;
    }
    if (record.status === 'active') {
      const body = {
        id: record.id,
        classId: record.classId,
        projectId: record.projectId,
        userId: record.userId,
        projectLeader: isProjectLeader,
        dropoutDate: record.dropoutDate,
        note: record.note,
        ongoingEval: record.ongoingEval,
        finalPresentEval: record.finalPresentEval,
        finalTopicEval: record.finalTopicEval,
        finalEval: record.finalEval,

        status: 'inactive'
      };
      onUpdateClassUser(body);
    } else {
      const body = {
        id: record.id,
        classId: record.classId,
        projectId: record.projectId,
        userId: record.userId,
        projectLeader: isProjectLeader,
        dropoutDate: record.dropoutDate,
        note: record.note,
        ongoingEval: record.ongoingEval,
        finalPresentEval: record.finalPresentEval,
        finalEval: record.finalEval,
        finalTopicEval: record.finalTopicEval,
        status: 'active'
      };
      onUpdateClassUser(body);
    }
  };

  // handle accept edit
  const handleEditRecord = () => {
    let isProjectLeader;
    if (typeof editRecord.projectLeader === 'number') {
      isProjectLeader = editRecord.projectLeader;
    } else {
      isProjectLeader = editRecord.projectLeader === 'No' ? 0 : 1;
    }
    const body = {
      id: editRecord.id,
      classId: editRecord.classId,
      projectId: editRecord.projectId,
      userId: editRecord.userId,
      projectLeader: isProjectLeader,
      dropoutDate: editRecord.dropoutDate,
      note: editRecord.note,
      ongoingEval: editRecord.ongoingEval,
      finalPresentEval: editRecord.finalPresentEval,
      finalTopicEval: editRecord.finalTopicEval,
      finalEval: editRecord.finalEval,
      status: editRecord.status
    };
    onUpdateClassUser(body);
  };

  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id'
      // render: (text, record, index) => <span>{index + 1 + paramsSearch.limit * (paramsSearch.page - 1)}</span>
    },
    // {
    //   title: 'Class code',
    //   dataIndex: 'classCode',
    //   key: 'classCode',
    //   className: 'w-5'
    // },
    {
      title: 'Team',
      dataIndex: 'projectCode',
      key: 'projectCode'
    },
    {
      title: 'Full Name',
      dataIndex: 'fullName',
      key: 'fullName'
    },
    {
      title: 'Email',
      dataIndex: 'userEmail',
      key: 'userEmail'
    },
    {
      title: 'Roll Number',
      dataIndex: 'rollNumber',
      key: 'rollNumber'
    },
    {
      title: 'Project Leader',
      dataIndex: 'projectLeader',
      key: 'projectLeader',
      className: 'w-10'
      // render: projectLeader => (
      //   <span>
      //         {projectLeader ? 'Yes' : 'No'}
      //       </span>
      // )
    },
    {
      title: 'Final Topic Evaluation',
      className: 'w-15',
      key: 'finalTopicEval',
      dataIndex: 'finalTopicEval',
      render: (_, record, idx) => (
        <Tooltip
          title={
            <Row>
              <Col span={24}>In which:</Col>
              <Col span={24}>- Ongoing Evaluation: {record.ongoingEval}</Col>
              <Col span={24}>- Final Evaluation: {record.finalEval}</Col>
            </Row>
          }
        >
          <span>{record.finalTopicEval}</span>
        </Tooltip>
      )
      // children: [
      //   {
      //     title: 'Ongoing Eval',
      //     dataIndex: 'ongoingEval',
      //     key: 'ongoingEval',
      //     width: 150
      //   },
      //   {
      //     title: 'Final Topic Eval',
      //     dataIndex: 'finalTopicEval',
      //     key: 'finalTopicEval',
      //     width: 150
      //   },
      //   {
      //     title: 'Final Eval',
      //     dataIndex: 'finalEval',
      //     key: 'finalEval',
      //     width: 150
      //   }
      // ]
    },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
      className: 'w-5',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-10`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Join date',
    //   dataIndex: 'created',
    //   key: 'created',
    //   className: 'w-10'
    // },
    // {
    //   title: 'Create By User',
    //   dataIndex: 'createdByUser',
    //   key: 'createdByUser'
    // },
    // {
    //   title: 'Modified By User',
    //   dataIndex: 'modifiedByUser',
    //   key: 'modifiedByUser'
    // },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" style={{ display: 'flex', alignItems: ' center' }}>
          {userRole !== 'Student' && (
            <Popconfirm
              placement="top"
              title={`Are you sure you want to ${
                record.status === 'active' ? 'deactivate' : 'activate'
              } this class user?`}
              onConfirm={() => handleToggleActive(record)}
              okText="Yes"
              cancelText="No"
              disabled={userRole === 'Student' ? true : false}
            >
              {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
              <Switch
                className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
                checked={record.status === 'active' ? true : false}
              />
            </Popconfirm>
          )}
          <Button
            style={{
              borderRadius: '10px',
              width: '30px',
              height: '30px',
              border: 'none',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              color: '#fff',
              background: '#0CA0C5'
            }}
            onClick={() => {
              handleViewDetail(record);
            }}
          >
            <AiOutlineEye style={{ cursor: 'pointer', fontSize: '18px' }} />
          </Button>
          {userRole !== 'Student' && (
            <Button
              style={{
                borderRadius: '10px',
                width: '30px',
                height: '30px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#6BBCB3'
              }}
              disabled={userRole === 'Student' ? true : false}
              onClick={() => onClickBtnEdit(record)}
            >
              <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
            </Button>
          )}
        </Space>
      )
    }
  ];

  // handle pagination as antd
  const handleTableChange = value => {
    getListUser({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const handleSearch = () => {
    if (isEmpty(selectedValuesClass) && selectedValuesClass !== undefined) {
      setModalIcon('fail');
      setModalText('Please select class before search !!');
      setIsShowModal(true);
    } else {
      getListUser({
        ...paramsSearch,
        page: 1
      });
    }
  };

  // handle reset filter
  const [selectedValuesProject, setSelectedValuesProject] = useState();
  const [selectedValuesStatus, setSelectedValuesStatus] = useState();
  const [selectedValuesClass, setSelectedValuesClass] = useState();

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        settingTypeID: null,
        settingValue: null,
        settingTitle: null,
        status: null,
        limit: 10,
        page: 1
      };
    });
    setSelectedValuesProject([]);
    setSelectedValuesStatus([]);
    // setSelectedValuesClass([]);
    getListUser({
      classId: paramsSearch.classId,
      settingTypeID: null,
      settingValue: null,
      settingTitle: null,
      status: null,
      limit: 10,
      page: 1
    });
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absClassStudent}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Class Students</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Student' && (
            <Button
              disabled={userRole === 'Student' ? true : false}
              className="br-10 btn-action d-flex w-100 h-80"
              onClick={handleClickBtnAdd}
            >
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Class Student</span>
            </Button>
          )}
        </Col>
      </Row>

      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="pr-1">
          <SelectClass
            mode={'single'}
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectedValuesClass={selectedValuesClass}
            setSelectedValuesClass={setSelectedValuesClass}
            currentClass={[currentClass]}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectProject
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectedValuesProject={selectedValuesProject}
            setSelectedValuesProject={setSelectedValuesProject}
            classId={currentClass}
            selectedValuesClass={selectedValuesClass}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectStatus
            setParamsSearch={setParamsSearch}
            selectedValuesStatus={selectedValuesStatus}
            setSelectedValuesStatus={setSelectedValuesStatus}
          />
        </Col>
        <Col offset={2} span={6} className="d-flex justify-content-end">
          {userRole !== 'Student' && (
            <Button
              className="br-10 btn-action d-flex mx-1"
              onClick={() => {
                setIsImporting(true);
              }}
              disabled={userRole === 'Student' ? true : false}
            >
              <BiImport style={{ color: 'white', background: '#0d9caf' }} className="icon-plus m-auto mr-1" />
              <span className="m-auto"> Import Class Students</span>
            </Button>
          )}
          {userRole !== 'Student' && (
            <Button
              className="br-10 btn-action d-flex mx-1"
              onClick={() => {
                exportClassUser({ ...paramsSearch, page: 1, getExcel: true });
              }}
              disabled={userRole === 'Student' ? true : false}
            >
              <RiFileExcel2Line style={{ color: '#069255' }} className="icon-plus m-auto mr-1" />
              <span className="m-auto"> Export Data</span>
            </Button>
          )}
        </Col>
        <Col span={4} className="d-flex justify-content-between">
          <Button className="br-10 btn-grey w-100 mx-1" onClick={handleResetFilter}>
            Reset
          </Button>
          <Button className="br-10 btn-action w-100 mx-1" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total,
          showSizeChanger: true
        }}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />

      <ModalUpdate
        isEditing={isEditing}
        setIsEditing={setIsEditing}
        handleEditRecord={handleEditRecord}
        editRecord={editRecord}
        setEditRecord={setEditRecord}
        buttonLoading={buttonLoading}
      />
      <ModalAdd
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        handleAddClass={handleAddClass}
        setAddRecord={setAddRecord}
        buttonLoading={buttonLoading}
        addRecord={addRecord}
      />
      <ModalView isViewing={isViewing} setIsViewing={setIsViewing} recordView={viewRecord} />
      <ModalImportClassUser
        isImporting={isImporting}
        setIsImporting={setIsImporting}
        setIsShowModalMessage={setIsShowModal}
        setModaltext={setModalText}
        setModalIconname={setModalIcon}
      />
    </Spin>
  );
};

export default ClassUserListDashboard;
