import { Modal } from 'antd';
import React from 'react';
import styles from './ModalView.module.css';

export default function ModalView({ isViewing, setIsViewing, recordView }) {
  return (
    <Modal
      width={'600px'}
      title="Student Detail"
      visible={isViewing}
      onCancel={() => {
        setIsViewing(false);
      }}
      cancelText="Exit"
      okButtonProps={{ style: { display: 'none' } }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Class code</h4>
          <h4 className={styles.content}>{recordView?.classCode}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Project code</h4>
          <h4 className={styles.content}>{recordView?.projectCode}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Project name</h4>
          <h4 className={styles.content}>{recordView?.projectName}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>User email</h4>
          <h4 className={styles.content}>{recordView?.userEmail}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Dropout date</h4>
          <h4 className={styles.content}>{recordView?.dropoutDate}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Note</h4>
          <h4 className={styles.content}>{recordView?.note}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Ongoing evaluation</h4>
          <h4 className={styles.content}>{recordView?.ongoingEval || 0}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Final present evaluation</h4>
          <h4 className={styles.content}>{recordView?.finalPresentEval || 0}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Final topic evaluation</h4>
          <h4 className={styles.content}>{recordView?.finalTopicEval || 0}</h4>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Status</h4>
          <h4 className={styles.content}>{recordView?.status}</h4>
        </div>
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Created date</h4>*/}
        {/*  <h4>{recordView?.created}</h4>*/}
        {/*</div>*/}
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Created by</h4>*/}
        {/*  <h4>{recordView?.createdByUser}</h4>*/}
        {/*</div>*/}
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Last modified</h4>*/}
        {/*  <h4>{recordView?.modified}</h4>*/}
        {/*</div>*/}
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Modified by</h4>*/}
        {/*  <h4>{recordView?.modifiedByUser}</h4>*/}
        {/*</div>*/}
      </div>
    </Modal>
  );
}
