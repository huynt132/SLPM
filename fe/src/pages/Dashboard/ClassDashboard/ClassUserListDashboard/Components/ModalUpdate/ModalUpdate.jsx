import { Checkbox, Input, Modal, Radio } from 'antd';
import styles from './ModalUpdate.module.css';
import PropTypes from 'prop-types';
import SelectProject from 'src/components/Select/SelectProject';
import { isEmpty } from '../../../../../../utils/helper';
import SelectClass from 'src/components/Select/SelectClass';
import React, { useEffect } from 'react';

const TextArea = { Input };
export default function ModalUpdate({
  isEditing,
  setIsEditing,
  handleEditRecord,
  setEditRecord,
  editRecord,
  buttonLoading
}) {
  return (
    <Modal
      title="Update Information"
      visible={isEditing}
      onOk={handleEditRecord}
      okText={'Update'}
      onCancel={() => {
        setIsEditing(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Class Code</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectClass
              mode={'single'}
              disabled={true}
              onOptionSelected={value => {
                setEditRecord(record => {
                  return { ...record, classId: value };
                });
              }}
              placeholder={'Select class'}
              classes={isEmpty(editRecord?.classId) ? [] : editRecord?.classId}
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Project</h4>
          <div className={styles.rowSelect}>
            <SelectProject
              mode={'single'}
              onOptionSelected={value => {
                setEditRecord(record => {
                  return { ...record, projectId: value };
                });
              }}
              placeholder={'Select project'}
              projects={isEmpty(editRecord?.projectId) ? [] : editRecord?.projectId}
              filter={{ classId: editRecord?.classId }}
            />
          </div>
        </div>
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Note</h4>*/}
        {/*  <TextArea*/}
        {/*    value={editRecord?.note}*/}
        {/*    rows={4}*/}
        {/*    placeholder="Note ... "*/}
        {/*    onChange={e =>*/}
        {/*      setEditRecord(record => {*/}
        {/*        return { ...record, note: e.target.value };*/}
        {/*      })*/}
        {/*    }*/}
        {/*  />*/}
        {/*</div>*/}
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              value={editRecord?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Project leader:</h4>
          <Checkbox
            defaultChecked={editRecord?.projectLeader === 'Yes' ? true : false}
            onChange={e => {
              setEditRecord(record => {
                return { ...record, projectLeader: e.target.checked ? 1 : 0 };
              });
            }}
          ></Checkbox>
        </div>
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func,
  handleEditRecord: PropTypes.func,
  setEditRecord: PropTypes.func,
  editRecord: PropTypes.object,
  buttonLoading: PropTypes.bool
};
