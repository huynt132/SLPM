import { Checkbox, DatePicker, InputNumber, Modal, Radio, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './ModalAdd.module.css';
import PropTypes from 'prop-types';
import { getAllStudentActive } from '../../classUser.slice';
import { unwrapResult } from '@reduxjs/toolkit';
import TextArea from 'antd/lib/input/TextArea';
import moment from 'moment';
import { isEmpty } from '../../../../../../utils/helper';
import SelectClass from 'src/components/Select/SelectClass';
import SelectUser from 'src/components/Select/SelectUser';
import SelectProject from 'src/components/Select/SelectProject';

const { Option } = Select;

export default function ModalAdd({ addRecord, isAdding, setIsAdding, handleAddClass, setAddRecord, buttonLoading }) {
  const dispatch = useDispatch();
  const [listStudent, setListStudent] = useState([]);

  useEffect(() => {
    setAddRecord(prev => {
      return { ...prev, projectId: null };
    }); // Auto xóa feature nếu user bỏ chọn projectId
  }, [addRecord?.classId]);

  const getAllStudentsActive = async params => {
    const data = await dispatch(getAllStudentActive({ params }));
    const res = unwrapResult(data);
    setListStudent(res.data.data);
  };

  useEffect(() => {
    if (isEmpty(addRecord?.status))
      setAddRecord(prev => {
        return { ...prev, status: 'active' };
      });
  }, [addRecord?.status]);

  useEffect(() => {
    setAddRecord(prev => {
      return { ...prev, projectId: null };
    }); // Auto xóa feature nếu user bỏ chọn projectId
  }, [addRecord?.classId]);

  useEffect(() => {
    const body = {
      status: 'active',
      role: 'student'
    };
    getAllStudentsActive(body);
  }, []);
  return (
    <Modal
      width={'700px'}
      title="Add student to Class"
      visible={isAdding}
      onOk={handleAddClass}
      okText={'Add'}
      onCancel={() => {
        setIsAdding(false);
        setAddRecord(prevState => {
          return {
            ...prevState,
            classId: null,
            projectId: null,
            userId: null,
            status: null,
            projectLeader: null,
            ongoingEval: null,
            finalPresentEval: null,
            finalTopicEval: null,
            note: null,
            dropoutDate: null
          };
        });
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Class</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectClass
              mode={'single'}
              onOptionSelected={value => {
                setAddRecord(record => {
                  return { ...record, classId: value };
                });
              }}
              placeholder={'Select class'}
              classes={isEmpty(addRecord?.classId) ? [] : addRecord?.classId}
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Project</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectProject
              customClassName={'border-radius-select w-100'}
              mode={'single'}
              onOptionSelected={value => {
                setAddRecord(record => {
                  return { ...record, projectId: value };
                });
              }}
              filter={{ classId: addRecord?.classId }}
              placeholder={'Select project'}
              projects={isEmpty(addRecord?.projectId) ? [] : addRecord.projectId}
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Student</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectUser
              customClassName={'border-radius-select'}
              mode={'single'}
              filter={{
                role: 'student'
              }}
              placeholder={'select assignee'}
              onOptionSelected={value => {
                setAddRecord(record => {
                  return { ...record, userId: value };
                });
              }}
              users={isEmpty(addRecord.userId) ? [] : addRecord.userId}
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              value={addRecord?.status}
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Project leader</h4>
          <Checkbox
            // defaultChecked={editRecord?.projectLeader === 'Yes' ? true : false}
            checked={addRecord?.projectLeader}
            onChange={e => {
              setAddRecord(record => {
                return { ...record, projectLeader: e.target.checked ? 1 : 0 };
              });
            }}
          ></Checkbox>
        </div>
        {/* <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Drop out date</h4>
            <span className="importantInfor">*</span>
          </div>
          <DatePicker
            className={styles.formatWidth}
            placeholder="Drop out date"
            format={'DD-MM-YYYY'}
            value={isEmpty(addRecord?.dropoutDate) ? null : moment(addRecord?.dropoutDate, 'DD-MM-YYYY')}
            defaultPickerValue={''}
            onChange={(date, dateString) =>
              setAddRecord(record => {
                return { ...record, dropoutDate: dateString };
              })
            }
          />
        </div> */}
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>On going Evaluation:</h4>*/}
        {/*  <InputNumber*/}
        {/*    className={styles.formatWidth}*/}
        {/*    value={addRecord?.ongoingEval}*/}
        {/*    min={0}*/}
        {/*    max={10.0}*/}
        {/*    onChange={value =>*/}
        {/*      setAddRecord(record => {*/}
        {/*        return { ...record, ongoingEval: value };*/}
        {/*      })*/}
        {/*    }*/}
        {/*  />*/}
        {/*</div>*/}
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Final Present Evaluation:</h4>*/}
        {/*  <InputNumber*/}
        {/*    className={styles.formatWidth}*/}
        {/*    value={addRecord?.finalPresentEval}*/}
        {/*    min={0}*/}
        {/*    max={10.0}*/}
        {/*    onChange={value =>*/}
        {/*      setAddRecord(record => {*/}
        {/*        return { ...record, finalPresentEval: value };*/}
        {/*      })*/}
        {/*    }*/}
        {/*  />*/}
        {/*</div>*/}
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Final Topic Evaluation:</h4>*/}
        {/*  <InputNumber*/}
        {/*    className={styles.formatWidth}*/}
        {/*    value={addRecord?.finalTopicEval}*/}
        {/*    min={0}*/}
        {/*    max={10.0}*/}
        {/*    onChange={value =>*/}
        {/*      setAddRecord(record => {*/}
        {/*        return { ...record, finalTopicEval: value };*/}
        {/*      })*/}
        {/*    }*/}
        {/*  />*/}
        {/*</div>*/}
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Note:</h4>
          <TextArea
            className={'text-black'}
            value={addRecord?.note}
            rows={4}
            placeholder="Note ... "
            onChange={e =>
              setAddRecord(record => {
                return { ...record, note: e.target.value };
              })
            }
          />
        </div>
      </div>
    </Modal>
  );
}

ModalAdd.propTypes = {
  isAdding: PropTypes.bool,
  setIsAdding: PropTypes.func,
  handleAddClass: PropTypes.func,
  setAddRecord: PropTypes.func,
  buttonLoading: PropTypes.bool
};
