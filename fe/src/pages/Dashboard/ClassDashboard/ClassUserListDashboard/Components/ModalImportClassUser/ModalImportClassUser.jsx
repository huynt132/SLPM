import { Button, Modal, Row, Table, Upload } from 'antd';
import React, { useRef, useState } from 'react';
import PropsTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { RiErrorWarningFill } from 'react-icons/ri';
import { unwrapResult } from '@reduxjs/toolkit';
import { ImDownload3 } from 'react-icons/im';
import { FiUpload } from 'react-icons/fi';
import { importClassUser } from '../../classUser.slice';
import { isEmpty } from 'lodash';

export default function ModalImportClassUser({
  isImporting,
  setIsImporting,
  setIsShowModalMessage,
  setModaltext,
  setModalIconname
}) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [file, setFile] = useState();
  const [logFile, setLogFile] = useState();
  const templateUrl = useRef('https://storage.googleapis.com/slpm/TemplateClassUser.xlsx');

  const dataSource = [
    {
      key: '1',
      a: 'Class code',
      b: 'Project code',
      c: 'Student email',
      d: 'project leader',
      e: 'Note',
      f: 'Status'
    }
  ];
  const columns = [
    {
      title: 'A',
      dataIndex: 'a',
      key: 'a'
    },
    {
      title: 'B',
      dataIndex: 'b',
      key: 'b'
    },
    {
      title: 'C',
      dataIndex: 'c',
      key: 'c'
    },
    {
      title: 'D',
      dataIndex: 'd',
      key: 'd'
    },
    {
      title: 'E',
      dataIndex: 'e',
      key: 'e'
    },
    {
      title: 'F',
      dataIndex: 'f',
      key: 'f'
    }
  ];

  const openInNewTab = url => {
    const win = window.open(url, '_blank');
    win.focus();
  };

  const beforeUpload = file => {
    const isLt20M = file.size / 1024 / 1024 < 20;
    if (!isLt20M) {
      setIsShowModalMessage(true);
      setModaltext('File must smaller than 20MB!');
      setModalIconname('fail');
    } else {
      return isLt20M;
    }
  };

  const resetModal = () => {
    setLogFile(null);
    setFile(null);
  };

  const uploadFile = async () => {
    try {
      setLoading(true);
      setLogFile(null);
      const data = new FormData();
      data.append('file', file);
      const res = await dispatch(importClassUser(data));
      const resWrap = unwrapResult(res);
      setIsShowModalMessage(true);
      setModaltext(resWrap.data.message || 'Import data successfully');
      setModalIconname('success');
      setFile(null);
    } catch (error) {
      setLogFile(error.data.data);
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Modal
        confirmLoading={loading}
        title={`Import Class Students`}
        visible={isImporting}
        width={'1000'}
        centered={true}
        className={'modal-center-footer modal-center-header'}
        footer={
          <Button loading={loading} className="" onClick={uploadFile}>
            Import file
          </Button>
        }
        onCancel={() => setIsImporting(false)}
        afterClose={resetModal}
      >
        <Row className={`text-danger`}>
          <RiErrorWarningFill className={'m-auto'} style={{ marginLeft: 0, marginRight: '1rem' }} />
          Note that the fields in the excel file must be in the order below to import class students information to the
          database correctly and not in the wrong field!
        </Row>
        <Row className={`mb-4`}>
          <span
            className={'cursor-pointer'}
            style={{ color: '#1e70cd', textDecoration: 'underline' }}
            onClick={() => openInNewTab(templateUrl.current)}
          >
            <ImDownload3 className="m-auto" style={{ marginLeft: 0, marginRight: '1rem' }} />
            <b>Download template file</b>
          </span>
        </Row>
        <Table className={'mb-4'} dataSource={dataSource} columns={columns} pagination={false} />
        <Row>
          <Upload
            accept={'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'}
            maxCount={1}
            className={'d-flex'}
            beforeUpload={beforeUpload}
            onChange={file => {
              if (file.file) setFile(file.file.originFileObj);
            }}
            ty
            progress={{
              strokeColor: {
                '0%': '#108ee9',
                '100%': '#87d068'
              },
              strokeWidth: 3,
              format: percent => percent && `${parseFloat(percent.toFixed(2))}%`
            }}
          >
            <Button>
              <FiUpload style={{ marginLeft: 0, marginRight: '1rem' }} />
              Click to Upload
            </Button>
          </Upload>
        </Row>
        {!isEmpty(logFile) && (
          <Row className={`mt-3`}>
            <span
              className={'cursor-pointer'}
              style={{ color: 'red', textDecoration: 'underline' }}
              onClick={() => openInNewTab(logFile)}
            >
              <ImDownload3 className="m-auto" />
              <b>File uploaded fail, click here to download the error log file for more details!</b>
            </span>
          </Row>
        )}
      </Modal>
    </>
  );
}

ModalImportClassUser.prototype = {
  isImporting: PropsTypes.bool,
  setIsImporting: PropsTypes.func,
  setIsShowModalMessage: PropsTypes.func,
  setModaltext: PropsTypes.func,
  setModalIconname: PropsTypes.func
};
