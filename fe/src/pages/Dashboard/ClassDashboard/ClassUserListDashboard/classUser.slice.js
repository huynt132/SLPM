import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import classUserApi from 'src/api/classUser.api';
import usersApi from 'src/api/users.api';

import { payloadCreator } from 'src/utils/helper';

export const getAll = createAsyncThunk('classUser/getAll', payloadCreator(classUserApi.getAll));

export const updateClassUser = createAsyncThunk('classUser/editClassUser', payloadCreator(classUserApi.editClassUser));

export const addClassUser = createAsyncThunk('classUser/addClassUser', payloadCreator(classUserApi.addClassUser));

export const getProjectCodeAndName = createAsyncThunk(
  'project/getList',
  payloadCreator(classUserApi.getProjectCodeAndName)
);

export const getAllStudentActive = createAsyncThunk('users/getUserList', payloadCreator(usersApi.getAllStudentActive));

export const importClassUser = createAsyncThunk('users/importClassUser', payloadCreator(classUserApi.importClassUser));

const classUserList = createSlice({
  name: 'classUserList',
  initialState: { listClass: [], listClassCode: [], listProjectCode: [] },
  reducers: {},
  extraReducers: {
    [getAll.fulfilled]: (state, action) => {
      state.listClass = action.payload.data.data;
    },
    [getProjectCodeAndName.fulfilled]: (state, action) => {
      state.listProjectCode = action.payload.data.data;
    }
  }
});

const classUserListReducer = classUserList.reducer;

export default classUserListReducer;
