import React, { useEffect, useState } from 'react';
import { Button, Space, Table, Row, Col, Input, Tag, Popconfirm, Spin, Switch } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import { path } from 'src/constants/path';
import SelectStatus from 'src/components/Select_DA/StatusSelect/SelectStatus';
import { editUser, getAll } from './userList.slice';
import ModalUpdate from './Components/ModalUpdate/ModalUpdate';
import { showMessage } from 'src/utils/commonHelper';

const UserDashboard = () => {
  const dispatch = useDispatch();

  const [paramsSearch, setParamsSearch] = useState({
    rollNumber: null,
    email: null,
    fullName: null,
    status: null,
    limit: 8,
    page: 1,
    total: 0
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  const [errorType, setErrorType] = useState('');
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  // state handle Edit
  const [isEditing, setIsEditing] = useState(false);
  const [editRecord, setEditRecord] = useState(null);
  // data source table
  const [dataSource, setDataSource] = useState([]);
  // state handle row select

  // get api list user
  const getListUser = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getAll({ params }));
      const res = unwrapResult(data);
      const dataTable = res.data.data.map(item => {
        return {
          key: item.id,
          id: item.id,
          email: item.email,
          fullName: item.fullName,
          rollNumber: item.rollNumber,
          school: item.schoolName,
          status: item.status,
          tel: item.tel,
          address: item.address,
          birthday: item.birthday,
          role: item.role,
          roleId: item.roleId,
          linkedinLink: item.linkedinLink,
          facebookLink: item.facebookLink,
          avatarLink: item.avatarLink
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setDataSource(dataTable);
      setLoading(false);
      showMessage(res.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setIsShowModal(true);
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  // call api
  useEffect(() => {
    getListUser(paramsSearch);
  }, [dispatch]);

  // =======================EDIT===========================

  const onClickBtnEdit = record => {
    setIsEditing(true);
    setEditRecord(prevState => {
      return { ...prevState, ...record };
    });
  };

  // get api edit user
  const onEditUser = async body => {
    try {
      setLoading(true);
      const res = await dispatch(editUser(body));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setLoading(false);
      setDataSource(prevList =>
        prevList.map(item =>
          item.id === result.data.data.id
            ? {
                ...item,
                id: result.data.data.id,
                email: result.data.data.email,
                fullName: result.data.data.fullName,
                rollNumber: result.data.data.rollNumber,
                school: result.data.data.schoolName,
                status: result.data.data.status,
                tel: result.data.data.tel,
                address: result.data.data.address,
                birthday: result.data.data.birthday,
                role: result.data.data.role,
                roleId: result.data.data.roleId,
                linkedinLink: result.data.data.linkedinLink,
                facebookLink: result.data.data.facebookLink,
                avatarLink: result.data.data.avatarLink
              }
            : item
        )
      );
      setIsEditing(false);
      showMessage(res.payload.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
        setLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // handle btn active and inactive - edit user
  const handleToggleActive = record => {
    if (record.status === 'active') {
      const body = {
        id: record.id,
        email: record.email,
        fullName: record.fullName,
        rollNumber: record.rollNumber,
        school: record.schoolName,
        status: 'inactive',
        tel: record.tel,
        address: record.address,
        birthday: record.birthday,
        role: record.role,
        roleId: record.roleId,
        linkedinLink: record.linkedinLink,
        facebookLink: record.facebookLink,
        avatarLink: record.avatarLink
      };
      onEditUser(body);
    } else {
      const body = {
        id: record.id,
        email: record.email,
        fullName: record.fullName,
        rollNumber: record.rollNumber,
        schoolName: record.school,
        status: 'active',
        tel: record.tel,
        address: record.address,
        birthday: record.birthday,
        role: record.role,
        roleId: record.roleId,
        linkedinLink: record.linkedinLink,
        facebookLink: record.facebookLink,
        avatarLink: record.avatarLink
      };
      onEditUser(body);
    }
  };

  // handle accept edit
  const handleEditRecord = () => {
    // setIsEditing(false);
    const body = {
      id: editRecord.id,
      email: editRecord.email,
      fullName: editRecord.fullName,
      rollNumber: editRecord.rollNumber,
      schoolName: editRecord.school,
      status: editRecord.status,
      tel: editRecord.tel,
      address: editRecord.address,
      birthday: editRecord.birthday,
      role: editRecord.role,
      roleId: editRecord.roleId,
      linkedinLink: editRecord.linkedinLink,
      facebookLink: editRecord.facebookLink,
      avatarLink: editRecord.avatarLink
    };
    onEditUser(body);
  };

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'Roll Number',
      dataIndex: 'rollNumber',
      key: 'rollNumber'
    },
    {
      title: 'Full Name',
      dataIndex: 'fullName',
      key: 'fullName'
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    },
    {
      title: 'Phone number',
      dataIndex: 'tel',
      key: 'tel'
    },
    // {
    //   title: 'School',
    //   dataIndex: 'school',
    //   key: 'school'
    // },
    {
      title: 'Role',
      dataIndex: 'role',
      key: 'role'
    },
    {
      title: 'Status',
      key: 'status',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-10`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Address',
    //   dataIndex: 'address',
    //   key: 'address'
    // },
    // {
    //   title: 'Birthday',
    //   dataIndex: 'birthday',
    //   key: 'birthday'
    // },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Popconfirm
            placement="top"
            title={`Are you sure you want to ${record.status === 'active' ? 'deactivate' : 'activate'} this user?`}
            onConfirm={() => handleToggleActive(record)}
            okText="Yes"
            cancelText="No"
          >
            {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
            <Switch
              className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
              checked={record.status === 'active' ? true : false}
            />
          </Popconfirm>
          <Button
            style={{
              borderRadius: '10px',
              width: '30px',
              height: '30px',
              border: 'none',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              color: '#fff',
              background: '#6BBCB3'
            }}
            onClick={() => onClickBtnEdit(record)}
          >
            <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
          </Button>
        </Space>
      )
    }
  ];

  // handle pagination as antd
  const handleTableChange = value => {
    getListUser({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const handleSearch = () => {
    getListUser({
      ...paramsSearch,
      page: 1
    });
  };

  // handle reset filter
  const [selectedValuesStatus, setSelectedValuesStatus] = useState();

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        rollNumber: null,
        email: null,
        fullName: null,
        status: null,
        limit: 8,
        page: 1
      };
    });
    setSelectedValuesStatus([]);
    getListUser({
      rollNumber: null,
      email: null,
      fullName: null,
      status: null,
      limit: 8,
      page: 1
    });
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absUser}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />

      <Row>
        <Col span={18}>
          <h1 className={'m-0'}> User Management</h1>
        </Col>
        {/* <Col span={4} offset={2} className="px-1">
          <Button className="br-10 btn-action d-flex w-100 h-80" onClick={handleClickBtnAdd}>
            <AiOutlinePlus className="icon-plus m-auto" />
            <span className="m-auto">Create subject</span>
          </Button>
        </Col> */}
      </Row>

      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="pr-1">
          <Input
            value={paramsSearch.rollNumber}
            placeholder="Input Roll Number User"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, rollNumber: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectStatus
            setParamsSearch={setParamsSearch}
            selectedValuesStatus={selectedValuesStatus}
            setSelectedValuesStatus={setSelectedValuesStatus}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.email}
            placeholder="Input Email User"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, email: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.fullName}
            placeholder="Input Full Name User"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, fullName: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={2} offset={4} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={handleResetFilter}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total
          // showSizeChanger: true
        }}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />
      <ModalUpdate
        isEditing={isEditing}
        setIsEditing={setIsEditing}
        handleEditRecord={handleEditRecord}
        editRecord={editRecord}
        setEditRecord={setEditRecord}
        buttonLoading={loading}
      />
      {/* <ModalAdd
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        handleAddingSetting={handleAddingSetting}
        setAddRecord={setAddRecord}
        buttonLoading={buttonLoading}
      /> */}
    </Spin>
  );
};

export default UserDashboard;
