import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import settingListApi from 'src/api/settingList.api';
import usersApi from 'src/api/users.api';

import { payloadCreator } from 'src/utils/helper';

export const getAll = createAsyncThunk('user/getAll', payloadCreator(usersApi.getAllUser));

export const editUser = createAsyncThunk('user/editUser', payloadCreator(usersApi.editUser));

export const getRole = createAsyncThunk('user/getAll', payloadCreator(settingListApi.getRole));

export const uploadAvatar = createAsyncThunk('user/imageUpload', payloadCreator(usersApi.editAvatarUser));

export const getUserDetail = createAsyncThunk('user/getUserDetail', payloadCreator(usersApi.getUserDetail));

const userList = createSlice({
  name: 'userList',
  initialState: { userList: [], roleList: [], userInfo: { avatarLink: 'https://joeschmoe.io/api/v1/random' } },
  reducers: {},
  extraReducers: {
    [getAll.fulfilled]: (state, action) => {
      state.userList = action.payload.data.data;
    },
    [getRole.fulfilled]: (state, action) => {
      state.roleList = action.payload.data.data;
    },
    [getUserDetail.fulfilled]: (state, action) => {
      state.userInfo = action.payload.data.data;
    },
    [uploadAvatar.fulfilled]: (state, action) => {
      state.userInfo.avatarLink = action.payload.data.data;
    }
  }
});

const userListReducer = userList.reducer;

export default userListReducer;
