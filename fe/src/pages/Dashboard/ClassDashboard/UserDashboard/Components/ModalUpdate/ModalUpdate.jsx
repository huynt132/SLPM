import { DatePicker, Input, Modal, Radio, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './ModalUpdate.module.css';
import PropTypes from 'prop-types';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { getRole } from '../../userList.slice';
import { unwrapResult } from '@reduxjs/toolkit';

const { Option } = Select;

export default function ModalUpdate({
  isEditing,
  setIsEditing,
  handleEditRecord,
  setEditRecord,
  editRecord,
  buttonLoading
}) {
  const dispatch = useDispatch();
  const [listRole, setListRole] = useState([]);
  const getListRole = async params => {
    const data = await dispatch(getRole({ params }));
    const res = unwrapResult(data);
    setListRole(res.data.data);
  };

  useEffect(() => {
    getListRole();
  }, []);

  return (
    <Modal
      title="Update Information"
      visible={isEditing}
      onOk={handleEditRecord}
      okText={'Update'}
      onCancel={() => {
        setIsEditing(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Email</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Email</h4> */}
          <Input
            disabled={true}
            value={editRecord?.email}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, email: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Full Name</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Full Name</h4> */}
          <Input
            value={editRecord?.fullName}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, fullName: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Roll Number</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Roll Number</h4> */}
          <Input
            value={editRecord?.rollNumber}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, rollNumber: e.target.value };
              })
            }
          />
        </div>
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>School</h4>*/}
        {/*  <Input*/}
        {/*    value={editRecord?.school}*/}
        {/*    onChange={e =>*/}
        {/*      setEditRecord(record => {*/}
        {/*        return { ...record, schoolName: e.target.value };*/}
        {/*      })*/}
        {/*    }*/}
        {/*  />*/}
        {/*</div>*/}
        {/*<div className={styles.rowModal}>*/}
        {/*  <h4 className={styles.rowText}>Address</h4>*/}
        {/*  <Input*/}
        {/*    value={editRecord?.address}*/}
        {/*    onChange={e =>*/}
        {/*      setEditRecord(record => {*/}
        {/*        return { ...record, address: e.target.value };*/}
        {/*      })*/}
        {/*    }*/}
        {/*  />*/}
        {/*</div>*/}
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Role</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Role</h4> */}
          <div className={styles.rowSelect}>
            <Select
              labelInValue
              value={{
                value: editRecord?.roleId
              }}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setEditRecord(record => {
                  return { ...record, roleId: value.value };
                })
              }
            >
              {listRole.map((item, key) => {
                return (
                  <Option key={key} value={item.value}>
                    {item.label}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Birthday</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Birthday</h4> */}
          <div className={styles.rowSelect}>
            <DatePicker
              className="datePicker"
              placeholder="Birthday"
              format={'DD-MM-YYYY'}
              value={moment(editRecord?.birthday, 'DD-MM-YYYY')}
              onChange={(date, dateString) =>
                setEditRecord(record => {
                  return { ...record, birthday: dateString };
                })
              }
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Status</h4> */}
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              value={editRecord?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func,
  handleEditRecord: PropTypes.func,
  setEditRecord: PropTypes.func,
  editRecord: PropTypes.object,
  buttonLoading: PropTypes.bool
};
