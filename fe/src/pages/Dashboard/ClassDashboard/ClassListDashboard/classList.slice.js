import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import classListApi from 'src/api/classList.api';
import usersApi from 'src/api/users.api';

import { payloadCreator } from 'src/utils/helper';

export const getAll = createAsyncThunk('classList/getAll', payloadCreator(classListApi.getAll));

export const getListClass = createAsyncThunk('classList/getListClass', payloadCreator(classListApi.getListClass));

export const updateClass = createAsyncThunk('classList/editClass', payloadCreator(classListApi.editClass));

export const addClass = createAsyncThunk('classList/addClass', payloadCreator(classListApi.addClass));

export const getAllTrainer = createAsyncThunk('users/getAllTrainer', payloadCreator(usersApi.getAllTrainer));

const classList = createSlice({
  name: 'classList',
  initialState: { listClass: [], listClassCode: [], listTrainer: [] },
  reducers: {},
  extraReducers: {
    [getAll.fulfilled]: (state, action) => {
      state.listClass = action.payload.data.data;
    },
    [getListClass.fulfilled]: (state, action) => {
      state.listClassCode = action.payload.data.data;
    },
    [getAllTrainer.fulfilled]: (state, action) => {
      state.listTrainer = action.payload.data.data;
    }
  }
});

const classListReducer = classList.reducer;

export default classListReducer;
