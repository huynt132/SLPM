import React, { useEffect, useState } from 'react';
import { Button, Space, Table, Row, Col, Input, Tag, Spin } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import { path } from 'src/constants/path';
import { AiOutlinePlus } from 'react-icons/ai';
import SelectStatus from 'src/components/Select_DA/StatusSelect/SelectStatus';
import { addClass, getAll, updateClass } from './classList.slice';
import ModalUpdate from './Components/ModalUpdate/ModalUpdate';
import SelectTrainer from 'src/components/Select_DA/TrainerSelect/SelectTrainer';
import ModalAdd from './Components/ModalAdd/ModalAdd';
import SelectSubject from 'src/components/Select_DA/SubjectSelect/SelectSubject';
import { useNavigate } from 'react-router-dom';
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from '../../../../utils/helper';

export default function ClassListDashboard() {
  const role = useSelector(state => state.auth.role);

  const dispatch = useDispatch();
  const [paramsSearch, setParamsSearch] = useState({
    code: null,
    trainerId: null,
    subjectId: null,
    subjectCode: null,
    year: null,
    term: null,
    status: null,
    limit: 8,
    page: 1,
    total: 0
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  const [errorType, setErrorType] = useState('');
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  // state handle Edit
  const [isEditing, setIsEditing] = useState(false);
  const [editRecord, setEditRecord] = useState(null);
  // state handle Add
  const [isAdding, setIsAdding] = useState(false);
  const [addRecord, setAddRecord] = useState({
    // trainerId: [],
    block5Class: 0,
    status: 'active'
  });
  // data source table
  const [dataSource, setDataSource] = useState([]);

  const navigate = useNavigate();
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    {
      title: 'Subject',
      dataIndex: 'subjectName',
      key: 'subjectName'
    },
    {
      title: 'Code',
      className: 'w-5',
      dataIndex: 'code',
      key: 'code'
    },
    {
      title: 'Trainer',
      dataIndex: 'trainerName',
      key: 'trainerName'
    },
    {
      title: 'Semester',
      dataIndex: 'term',
      className: 'w-10',
      key: 'term',
      render: (_, record, idx) => <span key={`term_${idx}`}>{`${record.year}-${record.term}`}</span>
    },
    // {
    //   title: 'Year',
    //   dataIndex: 'year',
    //   key: 'year'
    // },
    {
      title: 'Block 5',
      className: 'w-10',
      dataIndex: 'block5Class',
      key: 'block5Class'
    },
    {
      title: 'Status',
      key: 'status',
      className: 'w-10',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag
              className={`${
                status === 'active' ? 'btn-active' : status === 'cancelled' ? 'btn-cancel' : 'btn-inactive'
              } text-center br-10`}
              key={status}
            >
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Create By',
    //   dataIndex: 'createdByFullName',
    //   key: 'createdByFullName'
    // },
    {
      title: 'Action',
      className: 'w-20',
      key: 'action',
      render: (_, record) => {
        return (
          <Space size="middle" style={{ display: 'flex', alignItems: ' center' }}>
            {['ADMIN', 'AUTHOR', 'TRAINER'].includes(role.toUpperCase()) && (
              <Button
                style={{
                  borderRadius: '10px',
                  width: '30px',
                  height: '30px',
                  border: 'none',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                  color: '#fff',
                  background: '#6BBCB3'
                }}
                onClick={() => onClickBtnEdit(record)}
              >
                <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
              </Button>
            )}
            <Button
              style={{
                borderRadius: '10px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#1987b3'
              }}
              onClick={() => {
                navigate(path.classEvaluationDashboard, {
                  state: { classId: record.id }
                });
              }}
            >
              <span className="m-auto">Class Eval</span>
            </Button>
            <Button
              style={{
                borderRadius: '10px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#cdc9a5'
              }}
              onClick={() => {
                navigate(path.iterationEvaluationDashboard, {
                  state: { classId: record.id }
                });
              }}
            >
              <span className="m-auto">Iteration Eval</span>
            </Button>
          </Space>
        );
      }
    }
  ];

  // get api list all Class
  const getListClass = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getAll({ params }));
      const res = unwrapResult(data);
      const dataTable = res.data.data.map(item => {
        return {
          key: item.id,
          id: item.id,
          code: item.code,
          trainerId: item.trainerId,
          trainerName: item.trainerName,
          subjectId: item.subjectId,
          subjectName: item.subjectName,
          subjectCode: item.subjectCode,
          year: item.year,
          term: item.term,
          status: item.status,
          block5Class: item.block5Class === 0 ? 'No' : 'Yes',
          createdByFullName: item.createdByFullName,
          gitLabId: item.gitLabId,
          gitLabUrl: item.gitLabUrl
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total,
        subjectId: res.data.curSubject
      });
      setSelectedSubject(res.data.curSubject);
      setDataSource(dataTable);
      showMessage(res.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setIsShowModal(true);
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  // call api
  useEffect(() => {
    getListClass(paramsSearch);
  }, [dispatch]);

  // get api edit Setting
  const onUpdateClass = async body => {
    try {
      setButtonLoading(true);
      const res = await dispatch(updateClass(body));
      // handle when dispatch action have error
      unwrapResult(res);
      setDataSource(prevList =>
        prevList.map(item =>
          item.id === res.payload.data.data.id
            ? {
                ...item,
                code: res.payload.data.data.code,
                trainerId: res.payload.data.data.trainerId,
                trainerName: res.payload.data.data.trainerName,
                subjectId: res.payload.data.data.subjectId,
                subjectName: res.payload.data.data.subjectName,
                subjectCode: res.payload.data.data.subjectCode,
                year: res.payload.data.data.year,
                term: res.payload.data.data.term,
                status: res.payload.data.data.status,
                block5Class: res.payload.data.data.block5Class === 1 ? 'Yes' : 'No',
                createdByFullName: res.payload.data.data.createdByFullName
              }
            : item
        )
      );
      setIsEditing(false);
      showMessage(res.payload.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // =======================ADD============================
  const handleClickBtnAdd = () => {
    setIsAdding(true);
  };

  const handleAddClass = async () => {
    try {
      setButtonLoading(true);
      const res = await dispatch(addClass(addRecord));
      // handle when dispatch action have error
      unwrapResult(res);
      showMessage(res.payload.data.message);
      setIsAdding(false);
      setDataSource(prevList => {
        return [
          ...prevList,
          {
            id: res.payload.data.data.id,
            key: res.payload.data.data.id,
            code: res.payload.data.data.code,
            trainerId: res.payload.data.data.trainerId,
            trainerName: res.payload.data.data.trainerName,
            subjectId: res.payload.data.data.subjectId,
            subjectName: res.payload.data.data.subjectName,
            subjectCode: res.payload.data.data.subjectCode,
            year: res.payload.data.data.year,
            term: res.payload.data.data.term,
            status: res.payload.data.data.status,
            block5Class: res.payload.data.data.block5Class === 'No' ? 'No' : 'Yes',
            createdByFullName: res.payload.data.data.createdByFullName,
            gitLabId: res.payload.data.data.gitLabId
          }
        ];
      });
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  //handle btn active and deactivate - edit setting
  // const handleToggleActive = record => {
  //   if (record.status === 'active') {
  //     const body = {
  //       id: record.id,
  //       code: record.code,
  //       trainerId: record.trainerId,
  //       trainerName: record.trainerName,
  //       subjectId: record.subjectId,
  //       subjectName: record.subjectName,
  //       year: record.year,
  //       term: record.term,
  //       status: 'cancelled',
  //       block5Class: record.block5Class,
  //       createdByFullName: record.createdByFullName
  //     };
  //     onUpdateClass(body);
  //   } else {
  //     const body = {
  //       id: record.id,
  //       code: record.code,
  //       trainerId: record.trainerId,
  //       trainerName: record.trainerName,
  //       subjectId: record.subjectId,
  //       subjectName: record.subjectName,
  //       year: record.year,
  //       term: record.term,
  //       status: 'closed',
  //       block5Class: record.block5Class,
  //       createdByFullName: record.createdByFullName
  //     };
  //     onUpdateClass(body);
  //   }
  // };
  // =======================EDIT===========================

  const onClickBtnEdit = record => {
    setIsEditing(true);
    setEditRecord(prevState => {
      return { ...prevState, ...record };
    });
  };

  const handleEditRecord = () => {
    let bl5ClassFormat;
    if (typeof editRecord.block5Class === 'number') {
      bl5ClassFormat = editRecord.block5Class;
    } else {
      editRecord.block5Class === 'No ' ? (bl5ClassFormat = 1) : (bl5ClassFormat = 0);
    }

    const body = {
      key: editRecord.id,
      id: editRecord.id,
      code: editRecord.code,
      trainerId: editRecord.trainerId,
      trainerName: editRecord.trainerName,
      subjectId: editRecord.subjectId,
      subjectName: editRecord.subjectName,
      subjectCode: editRecord.subjectCode,
      year: editRecord.year,
      term: editRecord.term,
      status: editRecord.status,
      block5Class: bl5ClassFormat,
      createdByFullName: editRecord.createdByFullName,
      gitLabId: editRecord.gitLabId
    };
    onUpdateClass(body);
  };

  const handleSearch = () => {
    if (isEmpty(paramsSearch?.subjectId)) {
      setIsShowModal(true);
      setModalText('Please choose subject to search!');
      setModalIcon('fail');
      return;
    }
    getListClass({
      ...paramsSearch,
      page: 1
    });
  };

  // handle pagination as antd
  const handleTableChange = value => {
    getListClass({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  // handle reset filter
  const [selectedTrainer, setSelectedTrainer] = useState();
  const [selectedValuesStatus, setSelectedValuesStatus] = useState();
  const [selectedSubject, setSelectedSubject] = useState();

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        code: null,
        trainerId: null,
        subjectId: null,
        subjectCode: null,
        year: null,
        term: null,
        status: null,
        limit: 8,
        page: 1
      };
    });
    setSelectedTrainer([]);
    setSelectedValuesStatus([]);
    setSelectedSubject([]);
    getListClass({
      code: null,
      trainerId: null,
      subjectId: null,
      subjectCode: null,
      year: null,
      term: null,
      status: null,
      limit: 8,
      page: 1
    });
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absClass}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}> Class Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {['ADMIN', 'AUTHOR'].includes(role.toUpperCase()) && (
            <Button className="br-10 btn-action d-flex w-100 h-80" onClick={handleClickBtnAdd}>
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Class</span>
            </Button>
          )}
        </Col>
      </Row>

      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="px-1">
          <SelectSubject
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectSubject={selectedSubject}
            setSelectSubject={setSelectedSubject}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectTrainer
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectTrainer={selectedTrainer}
            setSelectTrainer={setSelectedTrainer}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectStatus
            numberStatus={3}
            setParamsSearch={setParamsSearch}
            selectedValuesStatus={selectedValuesStatus}
            setSelectedValuesStatus={setSelectedValuesStatus}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.code}
            placeholder="Input class code"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, code: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={2} offset={4} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={handleResetFilter}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>

      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total
          // showSizeChanger: true
        }}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />
      <ModalUpdate
        isEditing={isEditing}
        setIsEditing={setIsEditing}
        handleEditRecord={handleEditRecord}
        editRecord={editRecord}
        setEditRecord={setEditRecord}
        buttonLoading={buttonLoading}
      />
      <ModalAdd
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        handleAddClass={handleAddClass}
        setAddRecord={setAddRecord}
        addRecord={addRecord}
        buttonLoading={buttonLoading}
      />
    </Spin>
  );
}
