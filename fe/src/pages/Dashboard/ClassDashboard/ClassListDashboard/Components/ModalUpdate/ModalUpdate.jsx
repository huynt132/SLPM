import { Checkbox, Input, InputNumber, Modal, Radio, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './ModalUpdate.module.css';
import PropTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { getAllSubject } from 'src/pages/Dashboard/SubjectDashboard/SubjectManagement/subjectManagement.slice';

const { Option } = Select;

export default function ModalUpdate({
  isEditing,
  setIsEditing,
  handleEditRecord,
  setEditRecord,
  editRecord,
  buttonLoading
}) {
  const [listSubject, setListSubject] = useState([]);
  const dispatch = useDispatch();
  const listTrainer = useSelector(state => state.classListManagement.listTrainer);
  const getListSubjectWithCondition = async params => {
    const data = await dispatch(getAllSubject({ params }));
    const res = unwrapResult(data);
    setListSubject(res.data.data);
  };
  const role = useSelector(state => state.auth.role);

  useEffect(() => {
    getListSubjectWithCondition('');
  }, []);

  return (
    <Modal
      title="Update Information"
      visible={isEditing}
      onOk={handleEditRecord}
      okText={'Update'}
      onCancel={() => {
        setIsEditing(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.rowModal}>
        <h4 className={styles.rowText}>
          <div className={styles.rowText}>
            <h4>Subject</h4>
            <span className="importantInfor">*</span>
          </div>
        </h4>
        <div className={styles.rowSelect}>
          <Select
            disabled={role.toUpperCase() === 'TRAINER'}
            labelInValue
            value={{
              value: editRecord?.subjectName
            }}
            style={{
              width: '100%'
            }}
            onChange={value =>
              setEditRecord(record => {
                return { ...record, subjectId: value.value, subjectName: value.label };
              })
            }
          >
            {listSubject.map(item => {
              return (
                <Option key={item.id} value={item.id}>
                  {item.subjectName}
                </Option>
              );
            })}
          </Select>
        </div>
      </div>
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Class Code</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            disabled={role.toUpperCase() === 'TRAINER'}
            value={editRecord?.code}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, code: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Trainer</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <Select
              disabled={role.toUpperCase() === 'TRAINER'}
              labelInValue
              value={{
                value: editRecord?.trainerName
              }}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setEditRecord(record => {
                  return { ...record, trainerId: value.value, trainerName: value.label };
                })
              }
            >
              {listTrainer.map(item => {
                return (
                  <Option key={item.id} value={item.id} label={item.fullName}>
                    {item.fullName}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Gitlab Group ID:</h4>
          <Input
            placeholder="Edit Gitlab Group ID"
            value={editRecord?.gitLabId}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, gitLabId: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Term</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <Select
              disabled={role.toUpperCase() === 'TRAINER'}
              labelInValue
              value={{
                value: editRecord?.term
              }}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setEditRecord(record => {
                  return { ...record, term: value.value };
                })
              }
            >
              <Option key={1} value={'spring'}>
                Spring
              </Option>
              <Option key={2} value={'summer'}>
                Summer
              </Option>
              <Option key={3} value={'winter'}>
                Winter
              </Option>
            </Select>
          </div>
        </div>

        <div className={`${styles.rowModal} ${styles.rowModalFooter}`}>
          <div className={styles.rowModalFooterSide}>
            <div className={styles.rowText}>
              <h4>Year</h4>
              <span className="importantInfor">*</span>
            </div>
          </div>
          <div className={styles.rowYear}>
            <div className={'w-50'}>
              <InputNumber
                disabled={role.toUpperCase() === 'TRAINER'}
                min={2022}
                defaultValue={editRecord?.year}
                onChange={value => {
                  setEditRecord(record => {
                    return { ...record, year: value };
                  });
                }}
              />
            </div>
            <div className={styles.rowText}>
              <h4 style={{ fontSize: '12px' }}>Block 5 class</h4>
            </div>
            <Checkbox
              disabled={role.toUpperCase() === 'TRAINER'}
              checked={editRecord?.block5Class === 'Yes' ? true : false}
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, projectLeader: e.target.checked ? 1 : 0 };
                });
              }}
            ></Checkbox>
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Edit Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              disabled={role.toUpperCase() === 'TRAINER'}
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              value={editRecord?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'cancelled'}>Cancelled</Radio>
              <Radio value={'closed'}>Closed</Radio>
            </Radio.Group>
          </div>
        </div>
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func,
  handleEditRecord: PropTypes.func,
  setEditRecord: PropTypes.func,
  editRecord: PropTypes.object,
  buttonLoading: PropTypes.bool
};
