import { Checkbox, Input, InputNumber, Modal, Radio, Select, Spin } from 'antd';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styles from './ModalAdd.module.css';
import PropTypes from 'prop-types';
import { isEmpty } from '../../../../../../utils/helper';
import SelectCustomValue from '../../../../../../components/Select/SelectCustomValue';

const { Option } = Select;

export default function ModalAdd({ addRecord, isAdding, setIsAdding, handleAddClass, setAddRecord, buttonLoading }) {
  const listTrainer = useSelector(state => state.classListManagement.listTrainer);

  const listSubject = useSelector(state => state.subjectManagement.listSubjectCode);

  // useEffect(() => {
  //   if (isEmpty(addRecord?.status))
  //     setAddRecord(prev => {
  //       return { ...prev, status: 'active' };
  //     });
  // }, [addRecord?.status]);

  // useEffect(() => {
  //   if (isEmpty(addRecord?.projectLeader))
  //     setAddRecord(prev => {
  //       return { ...prev, projectLeader: 0 };
  //     });
  // }, [addRecord?.projectLeader]);

  return (
    <Modal
      title="Add Class"
      visible={isAdding}
      onOk={handleAddClass}
      okText={'Add'}
      onCancel={() => {
        setIsAdding(false);
        setAddRecord({
          block5Class: 0,
          status: 'active',
          trainerId: null,
          code: null,
          subjectId: null,
          term: null,
          year: null
        });
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.rowModal}>
        <div className={styles.rowText}>
          <h4>Subject</h4>
          <span className="importantInfor">*</span>
        </div>
        <div className={styles.rowSelect}>
          <Select
            placeholder="Select Subject"
            value={addRecord?.subjectId === null ? [] : addRecord?.subjectId}
            style={{
              width: '100%'
            }}
            onChange={value => {
              setAddRecord(record => {
                return { ...record, subjectId: value };
              });
            }}
          >
            {listSubject.map(item => {
              return (
                <Option key={item.id} value={item.id}>
                  {item.subjectName}
                </Option>
              );
            })}
          </Select>
        </div>
      </div>
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Class Code</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            value={addRecord?.code}
            placeholder="Input Class Code"
            onChange={e =>
              setAddRecord(record => {
                return { ...record, code: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Trainer</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <Select
              value={addRecord?.trainerId === null ? [] : addRecord?.trainerId}
              placeholder="Select Trainer"
              style={{
                width: '100%'
              }}
              onChange={value =>
                setAddRecord(record => {
                  return { ...record, trainerId: value };
                })
              }
            >
              {listTrainer.map(item => {
                return (
                  <Option key={item.id} value={item.id} label={item.fullName}>
                    {item.fullName}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Gitlab Group ID</h4>
          <Input
            placeholder="Input Gitlab Group ID"
            value={addRecord?.gitLabId}
            onChange={e =>
              setAddRecord(record => {
                return { ...record, gitLabId: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Term</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectCustomValue
              customClassName={'border-radius-select'}
              mode={'single'}
              onOptionSelected={value =>
                setAddRecord(record => {
                  return { ...record, term: value };
                })
              }
              query={{
                table: 'classes',
                field: 'term'
              }}
              values={isEmpty(addRecord?.term) ? [] : addRecord?.term}
              placeholder={'Select Term'}
            />
          </div>
        </div>
        <div className={`${styles.rowModal} ${styles.rowModalFooter}`}>
          <div className={styles.rowModalFooterSide}>
            <div className={styles.rowText}>
              <h4>Year</h4>
              <span className="importantInfor">*</span>
            </div>
          </div>
          <div className={styles.rowYear}>
            <div className={'w-50'}>
              <InputNumber
                className={'br-8'}
                placeholder="Input Year"
                value={addRecord?.year}
                min={2022}
                max={2099}
                onChange={value => {
                  setAddRecord(record => {
                    return { ...record, year: value };
                  });
                }}
              />
            </div>
            <div className={styles.rowText}>
              <h4 style={{ fontSize: '12px' }}>Block 5 class</h4>
              {/*<span className="importantInfor">*</span>*/}
            </div>
            <Checkbox
              checked={addRecord?.block5Class}
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, block5Class: e.target.checked ? 1 : 0 };
                });
              }}
            ></Checkbox>
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              value={addRecord?.status}
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'cancelled'}>Cancelled</Radio>
              <Radio value={'closed'}>Closed</Radio>
            </Radio.Group>
          </div>
        </div>
      </div>
    </Modal>
  );
}

ModalAdd.propTypes = {
  isAdding: PropTypes.bool,
  setIsAdding: PropTypes.func,
  handleAddClass: PropTypes.func,
  setAddRecord: PropTypes.func,
  buttonLoading: PropTypes.bool
};
