import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import classSettingApi from 'src/api/classSetting.api';
import { payloadCreator } from 'src/utils/helper';

export const getAll = createAsyncThunk('settingList/getAll', payloadCreator(classSettingApi.getListSettingClass));

export const getSettingClass = createAsyncThunk(
  'settingList/getSettingType',
  payloadCreator(classSettingApi.getListSettingType)
);

export const editClassSetting = createAsyncThunk(
  'settingList/editSetting',
  payloadCreator(classSettingApi.editClassSetting)
);

export const addClassSetting = createAsyncThunk(
  'settingList/addSetting',
  payloadCreator(classSettingApi.addClassSetting)
);

const classSettingList = createSlice({
  name: 'classSettingList',
  initialState: { listSetting: [], settingType: [] },
  reducers: {},
  extraReducers: {
    [getAll.fulfilled]: (state, action) => {
      state.listSetting = action.payload.data.data;
    },
    [getSettingClass.fulfilled]: (state, action) => {
      state.settingType = action.payload.data.data;
    }
    // [editSetting.fulfilled]: (state, action) => {
    //   state.listSetting.filter(item => item.id !== action.payload.data.data.id);
    //   state.listSetting.push(action.payload.data.data);
    // }
  }
});

const classSettingListReducer = classSettingList.reducer;

export default classSettingListReducer;
