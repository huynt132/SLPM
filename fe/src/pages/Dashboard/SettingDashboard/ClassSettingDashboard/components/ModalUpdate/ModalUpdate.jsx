import { Input, Modal, Radio, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './ModalUpdate.module.css';
import PropTypes from 'prop-types';
import { getSettingClass } from '../../classSetting.slice';
import { unwrapResult } from '@reduxjs/toolkit';

const { Option } = Select;
const { TextArea } = Input;

export default function ModalUpdate({
  isEditing,
  setIsEditing,
  handleEditRecord,
  setEditRecord,
  editRecord,
  buttonLoading
}) {
  const dispatch = useDispatch();
  const [listSettingClass, setListSettingClass] = useState([]);
  const getAllType = async params => {
    const data = await dispatch(getSettingClass({ params }));
    const res = unwrapResult(data);
    setListSettingClass(res.data.data);
  };

  useEffect(() => {
    getAllType('');
  }, []);

  return (
    <Modal
      title="Update Information"
      visible={isEditing}
      onOk={handleEditRecord}
      okText={'Update'}
      onCancel={() => {
        setIsEditing(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Class</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input disabled value={editRecord?.classCode} />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Edit Setting Type</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <Select
              labelInValue
              value={{
                value: editRecord?.type
              }}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setEditRecord(record => {
                  return { ...record, typeId: value.value, type: value.label };
                })
              }
            >
              {listSettingClass.map((item, key) => {
                return (
                  <Option key={key} value={item.value}>
                    {item.label}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Edit Setting Title</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            value={editRecord?.title}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, title: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>{editRecord && editRecord.typeId === 71 ? 'Select Color Label' : 'Setting value'}</h4>
            <span className="importantInfor">*</span>
          </div>

          {editRecord && editRecord.typeId === 71 ? (
            <input
              className={styles.inputColor}
              type="color"
              value={editRecord?.value}
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, value: e.target.value };
                });
              }}
            />
          ) : (
            <Input
              placeholder={'Input setting value'}
              value={setEditRecord?.value}
              onChange={e =>
                setEditRecord(record => {
                  return { ...record, value: e.target.value };
                })
              }
            />
          )}

          {/* <Input
            value={editRecord?.value}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, value: e.target.value };
              })
            }
          /> */}
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Edit Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              value={editRecord?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}> Description</h4>
          <TextArea
            className={'text-black br-8'}
            value={editRecord?.description}
            rows={4}
            placeholder="Description ... "
            onChange={e => {
              setEditRecord(record => {
                return { ...record, description: e.target.value };
              });
            }}
          />
        </div>
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func,
  handleEditRecord: PropTypes.func,
  setEditRecord: PropTypes.func,
  editRecord: PropTypes.object,
  buttonLoading: PropTypes.bool
};
