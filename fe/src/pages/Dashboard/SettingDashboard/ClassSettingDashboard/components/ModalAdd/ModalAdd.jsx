import { Input, Modal, Radio, Select } from 'antd';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styles from './ModalAdd.module.css';
import PropTypes from 'prop-types';
import SelectClass from 'src/components/Select/SelectClass';
import { isEmpty } from '../../../../../../utils/helper';

const { Option } = Select;
const { TextArea } = Input;

export default function ModalAdd({
  addRecord,
  isAdding,
  setIsAdding,
  handleAddingSetting,
  setAddRecord,
  buttonLoading
}) {
  const listSettingType = useSelector(state => state.classSetting.settingType);

  useEffect(() => {
    if (isEmpty(addRecord?.status))
      setAddRecord(prev => {
        return { ...prev, status: 'active' };
      });
  }, [addRecord?.status]);

  return (
    <Modal
      title="Add Class Setting"
      visible={isAdding}
      onOk={handleAddingSetting}
      okText={'Add'}
      onCancel={() => {
        setIsAdding(false);
        setAddRecord(null);
      }}
      width="650px"
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Class</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <SelectClass
              mode={'single'}
              // disabled={true}
              onOptionSelected={value => {
                setAddRecord(record => {
                  return { ...record, classId: value };
                });
              }}
              placeholder={'Select class'}
              classes={isEmpty(addRecord?.classId) ? [] : addRecord?.classId}
            />
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Setting Type</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <Select
              labelInValue
              value={
                isEmpty(addRecord?.typeId)
                  ? []
                  : {
                      value: addRecord?.typeId
                    }
              }
              placeholder={'Select setting type'}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setAddRecord(record => {
                  return { ...record, typeId: value.value, value: null };
                })
              }
            >
              {listSettingType.map((item, key) => {
                return (
                  <Option key={key} value={item.value}>
                    {item.label}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Setting Title</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            placeholder={'Input setting title'}
            value={addRecord?.title}
            onChange={e =>
              setAddRecord(record => {
                return { ...record, title: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>{addRecord && addRecord.typeId === 71 ? 'Select Color Label' : 'Setting value'}</h4>
            <span className="importantInfor">*</span>
          </div>

          {addRecord && addRecord.typeId === 71 ? (
            <input
              className={styles.inputColor}
              type="color"
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, value: e.target.value };
                });
              }}
            />
          ) : (
            <Input
              placeholder={'Input setting value'}
              value={addRecord?.value}
              onChange={e =>
                setAddRecord(record => {
                  return { ...record, value: e.target.value };
                })
              }
            />
          )}
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              value={addRecord?.status}
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}> Description</h4>
          <TextArea
            className={'text-black br-8'}
            value={addRecord?.description}
            rows={4}
            placeholder="Description ... "
            onChange={e =>
              setAddRecord(record => {
                return { ...record, description: e.target.value };
              })
            }
          />
        </div>
      </div>
    </Modal>
  );
}

ModalAdd.propTypes = {
  isAdding: PropTypes.bool,
  setIsAdding: PropTypes.func,
  handleAddingSetting: PropTypes.func,
  setAddRecord: PropTypes.func,
  buttonLoading: PropTypes.bool
};
