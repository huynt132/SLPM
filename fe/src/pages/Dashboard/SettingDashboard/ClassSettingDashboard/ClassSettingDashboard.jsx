import React, { useEffect, useState } from 'react';
import { Button, Space, Table, Row, Col, Input, Tag, Popconfirm, Spin, Switch } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { addClassSetting, editClassSetting, getAll } from './classSetting.slice';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import { path } from 'src/constants/path';
import { AiOutlinePlus } from 'react-icons/ai';
// import ModalUpdate from './components/ModalUpdate/ModalUpdate';
// import ModalAdd from './components/ModalAdd/ModalAdd';
import SelectStatus from 'src/components/Select_DA/StatusSelect/SelectStatus';
import SelectClass from 'src/components/Select_DA/ClassSelect/SelectClass';
import ModalUpdate from './components/ModalUpdate/ModalUpdate';
import ModalAdd from './components/ModalAdd/ModalAdd';
import { showMessage } from 'src/utils/commonHelper';

const ClassSettingDashboard = () => {
  const dispatch = useDispatch();
  const [paramsSearch, setParamsSearch] = useState({
    classId: null,
    status: null,
    titleOrValue: null,
    limit: 8,
    page: 1,
    total: 0
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  const [errorType, setErrorType] = useState(''); // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  // state handle Edit
  const [isEditing, setIsEditing] = useState(false);
  const [editRecord, setEditRecord] = useState(null);
  // state handle Add
  const [isAdding, setIsAdding] = useState(false);
  const [addRecord, setAddRecord] = useState(null);
  // data source table
  const [dataSource, setDataSource] = useState([]);

  const [currentData, setCurrentData] = useState({});

  const userRole = useSelector(state => state.auth.role);

  // get api list Setting
  const getListSetting = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getAll({ params }));
      const res = unwrapResult(data);
      const dataTable = res.data.data.map(item => {
        return {
          key: item.id,
          id: item.id,
          classId: item.classId,
          classCode: item.classCode,
          typeId: item.typeId,
          type: item.type,
          title: item.title,
          value: item.value,
          status: item.status,
          gitLabId: item?.gitLabId,
          description: item.description
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setDataSource(dataTable);
      setLoading(false);
      showMessage(res.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setIsShowModal(true);
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  // call api
  useEffect(() => {
    getListSetting(paramsSearch);
  }, [dispatch]);

  // =======================ADD============================
  const handleClickBtnAdd = () => {
    setIsAdding(true);
    // setAddRecord(prevState => {
    //   return { ...prevState, classId: paramsSearch.classId };
    // });
  };

  const handleAddingSetting = async () => {
    try {
      let res;
      setButtonLoading(true);
      if (addRecord.typeId === 71 && addRecord.value === null) {
        res = await dispatch(addClassSetting({ ...addRecord, value: '#000' }));
      } else {
        res = await dispatch(addClassSetting(addRecord));
      }
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setButtonLoading(false);
      showMessage(res.payload.data.message);
      setIsAdding(false);
      setDataSource(prevList => {
        return [
          ...prevList,
          {
            key: result.data.data.id,
            id: result.data.data.id,
            classId: result.data.data.classId,
            classCode: result.data.data.classCode,
            typeId: result.data.data.typeId,
            type: result.data.data.type,
            title: result.data.data.title,
            value: result.data.data.value,
            status: result.data.data.status,
            description: result.data.data.description
          }
        ];
      });
      setAddRecord(null);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // // =======================EDIT===========================

  const onClickBtnEdit = record => {
    setIsEditing(true);
    setEditRecord(prevState => {
      return { ...prevState, ...record };
    });
  };

  // get api edit Setting
  const onEditSetting = async body => {
    try {
      setLoading(true);
      const res = await dispatch(editClassSetting(body));
      // handle when dispatch action have error
      const result = unwrapResult(res);
      setLoading(false);
      setDataSource(prevList =>
        prevList.map(item =>
          item.id === result.data.data.id
            ? {
                ...item,
                key: item.id,
                id: result.data.data.id,
                classId: result.data.data.classId,
                classCode: result.data.data.classCode,
                typeId: result.data.data.typeId,
                type: result.data.data.type,
                title: result.data.data.title,
                value: result.data.data.value,
                status: result.data.data.status,
                gitLabId: result.data.data?.gitLabId,
                description: result.data.data.description
              }
            : item
        )
      );
      setIsEditing(false);
      showMessage(res.payload.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
        setLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  //handle btn active and deactivate - edit setting
  const handleToggleActive = record => {
    if (record.status === 'active') {
      const body = {
        id: record.id,
        classId: record.classId,
        typeId: record.typeId,
        title: record.title,
        value: record.value,
        status: 'Inactive',
        description: record.description,
        gitLabId: record?.gitLabId
      };
      onEditSetting(body);
    } else {
      const body = {
        id: record.id,
        classId: record.classId,
        typeId: record.typeId,
        title: record.title,
        value: record.value,
        status: 'Active',
        gitLabId: record?.gitLabId,
        description: record.description
      };
      onEditSetting(body);
    }
  };

  // handle accept edit
  const handleEditRecord = () => {
    const body = {
      id: editRecord.id,
      classId: editRecord.classId,
      typeId: editRecord.typeId,
      title: editRecord.title,
      value: editRecord.value,
      gitLabId: editRecord?.gitLabId,
      status: editRecord.status,
      description: editRecord.description
    };
    onEditSetting(body);
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    {
      title: 'Class',
      className: 'w-10',
      dataIndex: 'classCode',
      key: 'classCode'
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type'
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: 'Value',
      dataIndex: 'value',
      key: 'value'
    },

    {
      title: 'Status',
      key: 'status',
      className: 'w-10',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-10`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    {
      title: 'Action',
      className: 'w-15',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" style={{ display: 'flex', alignItems: ' center' }}>
          {userRole !== 'Student' && (
            <Popconfirm
              placement="top"
              title={`Are you sure you want to ${record.status === 'active' ? 'deactivate' : 'activate'} this record?`}
              onConfirm={() => handleToggleActive(record)}
              okText="Yes"
              cancelText="No"
            >
              <Switch
                className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
                checked={record.status === 'active' ? true : false}
              />
            </Popconfirm>
          )}
          {userRole !== 'Student' && (
            <Button
              style={{
                borderRadius: '10px',
                width: '30px',
                height: '30px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#6BBCB3'
              }}
              onClick={() => onClickBtnEdit(record)}
            >
              <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
            </Button>
          )}
        </Space>
      )
    }
  ];

  // handle pagination as antd
  const handleTableChange = value => {
    getListSetting({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const handleSearch = () => {
    getListSetting({
      ...paramsSearch,
      page: 1
    });
  };

  // handle reset filter
  const [selectedValuesClass, setSelectedValuesClass] = useState();
  const [selectedValuesStatus, setSelectedValuesStatus] = useState();

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        classId: null,
        status: null,
        titleOrValue: null,
        limit: 8,
        page: 1
      };
    });
    setSelectedValuesClass([]);
    setSelectedValuesStatus([]);
    getListSetting({
      classId: null,
      status: null,
      limit: 8,
      page: 1
    });
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absClassSetting}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>Class Setting Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          {userRole !== 'Student' && (
            <Button className="br-10 btn-action d-flex w-100 h-80" onClick={handleClickBtnAdd}>
              <AiOutlinePlus className="icon-plus m-auto" />
              <span className="m-auto">Create Class Setting</span>
            </Button>
          )}
        </Col>
      </Row>
      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="px-1">
          <SelectClass
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectedValuesClass={selectedValuesClass}
            setSelectedValuesClass={setSelectedValuesClass}
            currentClass={[]}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectStatus
            setParamsSearch={setParamsSearch}
            selectedValuesStatus={selectedValuesStatus}
            setSelectedValuesStatus={setSelectedValuesStatus}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.titleOrValue}
            placeholder="Input title or value"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, titleOrValue: e.target.value };
              });
            }}
          />
        </Col>
        <Col offset={8} span={2} className="px-1">
          <Button className="br-10 btn-grey w-100" onClick={handleResetFilter}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>

      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total
          // showSizeChanger: true
        }}
        // loading={loading}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />
      <ModalUpdate
        isEditing={isEditing}
        setIsEditing={setIsEditing}
        handleEditRecord={handleEditRecord}
        editRecord={editRecord}
        setEditRecord={setEditRecord}
        buttonLoading={loading}
      />
      <ModalAdd
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        handleAddingSetting={handleAddingSetting}
        setAddRecord={setAddRecord}
        buttonLoading={buttonLoading}
        addRecord={addRecord}
      />
    </Spin>
  );
};

export default ClassSettingDashboard;
