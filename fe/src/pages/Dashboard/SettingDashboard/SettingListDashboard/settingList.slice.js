import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import settingListApi from 'src/api/settingList.api';
// import LocalStorage from 'src/constants/localStorage';
import { payloadCreator } from 'src/utils/helper';

export const getAll = createAsyncThunk('settingList/getAll', payloadCreator(settingListApi.getAll));

export const getSettingType = createAsyncThunk(
  'settingList/getSettingType',
  payloadCreator(settingListApi.getSettingType)
);

export const editSetting = createAsyncThunk('settingList/editSetting', payloadCreator(settingListApi.editSetting));

export const addSetting = createAsyncThunk('settingList/addSetting', payloadCreator(settingListApi.addSetting));

const settingList = createSlice({
  name: 'settingList',
  initialState: { listSetting: [], settingType: [] },
  reducers: {},
  extraReducers: {
    [getAll.fulfilled]: (state, action) => {
      state.listSetting = action.payload.data.data;
    },
    [getSettingType.fulfilled]: (state, action) => {
      state.settingType = action.payload.data.data;
    }
    // [editSetting.fulfilled]: (state, action) => {
    //   state.listSetting.filter(item => item.id !== action.payload.data.data.id);
    //   state.listSetting.push(action.payload.data.data);
    // }
  }
});

const settingListReducer = settingList.reducer;

export default settingListReducer;
