import React, { useEffect, useState } from 'react';
import { Button, Space, Table, Row, Col, Input, Tag, Popconfirm, Spin, Switch } from 'antd';
import { useDispatch } from 'react-redux';
import { addSetting, editSetting, getAll } from './settingList.slice';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { VscEdit } from 'react-icons/vsc';
import { path } from 'src/constants/path';
import { AiOutlinePlus } from 'react-icons/ai';
import ModalUpdate from './components/ModalUpdate/ModalUpdate';
import ModalAdd from './components/ModalAdd/ModalAdd';
import SelectSettingType from 'src/components/Select_DA/SettingTypeSelect/SelectSettingType';
import SelectStatus from 'src/components/Select_DA/StatusSelect/SelectStatus';
import { showMessage } from 'src/utils/commonHelper';

const SettingListDashboard = () => {
  const dispatch = useDispatch();
  const [paramsSearch, setParamsSearch] = useState({
    settingTypeID: null,
    settingValue: null,
    settingTitle: null,
    status: null,
    limit: 8,
    page: 1,
    total: 0
  });

  // state loading
  const [loading, setLoading] = useState(false);
  // button loading
  const [buttonLoading, setButtonLoading] = useState(false);
  //modal message show state
  const [isShowModal, setIsShowModal] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  const [errorType, setErrorType] = useState('');
  // state handle Edit
  const [isEditing, setIsEditing] = useState(false);
  const [editRecord, setEditRecord] = useState(null);
  // state handle Add
  const [isAdding, setIsAdding] = useState(false);
  const [addRecord, setAddRecord] = useState(null);
  // data source table
  const [dataSource, setDataSource] = useState([]);

  // get api list Setting
  const getListSetting = async params => {
    try {
      setLoading(true);
      const data = await dispatch(getAll({ params }));
      const res = unwrapResult(data);
      const dataTable = res.data.data.map(item => {
        return {
          key: item.id,
          id: item.id,
          type: item.settingType,
          title: item.title,
          value: item.value,
          order: item.displayOrder,
          status: item.status,
          description: item.description
        };
      });
      setParamsSearch({
        ...params,
        total: res.data.total
      });
      setDataSource(dataTable);
      setLoading(false);
      showMessage(res.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setIsShowModal(true);
      } else if (error.status === 401) {
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
        setErrorType('tokenExpired');
      }
    } finally {
      setLoading(false);
    }
  };

  // call api
  useEffect(() => {
    getListSetting(paramsSearch);
  }, [dispatch]);

  // =======================ADD============================
  const handleClickBtnAdd = () => {
    setIsAdding(true);
  };

  const handleAddingSetting = async () => {
    try {
      setButtonLoading(true);
      const res = await dispatch(addSetting(addRecord));
      // handle when dispatch action have error
      unwrapResult(res);
      setButtonLoading(false);
      setIsAdding(false);
      showMessage(res.payload.data.message);
      setDataSource(prevList => {
        return [
          ...prevList,
          {
            id: res.payload.data.data.id,
            type:
              res.payload.data.data.typeId === 2
                ? 'Role'
                : res.payload.data.data.typeId === 3
                ? 'Issue type'
                : 'Issue status',
            title: res.payload.data.data.title,
            value: res.payload.data.data.value,
            order: res.payload.data.data.displayOrder,
            status: res.payload.data.data.status,
            description: res.payload.data.data.description
          }
        ];
      });
      setAddRecord(null);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  // =======================EDIT===========================

  const onClickBtnEdit = record => {
    setIsEditing(true);
    setEditRecord(prevState => {
      return { ...prevState, ...record };
    });
  };

  // get api edit Setting
  const onEditSetting = async body => {
    try {
      setLoading(true);
      const res = await dispatch(editSetting(body));
      // handle when dispatch action have error
      unwrapResult(res);
      setLoading(false);
      setDataSource(prevList =>
        prevList.map(item =>
          item.id === res.payload.data.data.id
            ? {
                ...item,
                title: res.payload.data.data.title,
                value: res.payload.data.data.value,
                order: res.payload.data.data.displayOrder,
                status: res.payload.data.data.status,
                description: res.payload.data.data.description
              }
            : item
        )
      );
      setIsEditing(false);
      showMessage(res.payload.data.message);
    } catch (error) {
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error.data.message || error.data.error);
        setIsShowModal(true);
        setButtonLoading(false);
        setLoading(false);
      }
    } finally {
      setButtonLoading(false);
      setLoading(false);
    }
  };

  //handle btn active and deactivate - edit setting
  const handleToggleActive = record => {
    if (record.status === 'active') {
      const body = {
        id: record.id,
        typeId: record.type === 'Role' ? 2 : record.type === 'Issue type' ? 3 : 4,
        title: record.title,
        value: record.value,
        displayOrder: record.order,
        status: 'inactive',
        description: record.description
      };
      onEditSetting(body);
    } else {
      const body = {
        id: record.id,
        typeId: record.type === 'Role' ? 2 : record.type === 'Issue type' ? 3 : 4,
        title: record.title,
        value: record.value,
        displayOrder: record.order,
        status: 'active',
        description: record.description
      };
      onEditSetting(body);
    }
  };

  // handle accept edit
  const handleEditRecord = () => {
    const body = {
      id: editRecord.id,
      typeId: editRecord.type === 'Role' ? 2 : editRecord.type === 'Issue type' ? 3 : 4,
      title: editRecord.title,
      value: editRecord.value,
      displayOrder: editRecord.order,
      status: editRecord.status,
      description: editRecord.description
    };
    onEditSetting(body);
  };

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type'
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: 'Value',
      dataIndex: 'value',
      key: 'value'
    },
    // {
    //   title: 'Display Order',
    //   dataIndex: 'order',
    //   key: 'order'
    // },
    {
      title: 'Status',
      key: 'status',
      className: 'w-10',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-10`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    // {
    //   title: 'Description',
    //   dataIndex: 'description',
    //   key: 'description'
    // },
    {
      title: 'Action',
      className: 'w-15',
      key: 'action',
      render: (_, record) => (
        <Space size="middle" style={{ display: 'flex', alignItems: ' center' }}>
          <Popconfirm
            placement="top"
            title={`Are you sure you want to ${record.status === 'active' ? 'deactivate' : 'activate'} this setting?`}
            onConfirm={() => handleToggleActive(record)}
            okText="Yes"
            cancelText="No"
          >
            {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
            <Switch
              className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
              checked={record.status === 'active' ? true : false}
            />
          </Popconfirm>
          <Button
            style={{
              borderRadius: '10px',
              width: '30px',
              height: '30px',
              border: 'none',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              color: '#fff',
              background: '#6BBCB3'
            }}
            onClick={() => onClickBtnEdit(record)}
          >
            <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
          </Button>
        </Space>
      )
    }
  ];

  // handle pagination as antd
  const handleTableChange = value => {
    getListSetting({
      ...paramsSearch,
      limit: value.pageSize,
      page: value.current
    });
  };

  const handleSearch = () => {
    getListSetting({
      ...paramsSearch,
      page: 1
    });
  };

  // handle reset filter
  const [selectedValuesSetting, setSelectedValuesSetting] = useState();
  const [selectedValuesStatus, setSelectedValuesStatus] = useState();

  const handleResetFilter = () => {
    setParamsSearch(preState => {
      return {
        ...preState,
        settingTypeID: null,
        settingValue: null,
        settingTitle: null,
        status: null,
        limit: 8,
        page: 1
      };
    });
    setSelectedValuesSetting([]);
    setSelectedValuesStatus([]);
    getListSetting({
      settingTypeID: null,
      settingValue: null,
      settingTitle: null,
      status: null,
      limit: 8,
      page: 1
    });
  };

  return (
    <Spin tip="Loading..." spinning={loading}>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        navigateTo={path.absListSetting}
        text={modalText}
        iconname={modalIcon}
        type={errorType}
      />
      <Row>
        <Col span={18}>
          <h1 className={'m-0'}>System Setting Management</h1>
        </Col>
        <Col span={4} offset={2} className="px-1">
          <Button className="br-10 btn-action d-flex w-100 h-80" onClick={handleClickBtnAdd}>
            <AiOutlinePlus className="icon-plus m-auto" />
            <span className="m-auto">Create Setting</span>
          </Button>
        </Col>
      </Row>
      <Row className={'mt-2 mb-4'}>
        <Col span={4} className="pr-1">
          <SelectSettingType
            setLoading={setLoading}
            setParamsSearch={setParamsSearch}
            selectedValuesSetting={selectedValuesSetting}
            setSelectedValuesSetting={setSelectedValuesSetting}
          />
        </Col>
        <Col span={4} className="px-1">
          <SelectStatus
            setParamsSearch={setParamsSearch}
            selectedValuesStatus={selectedValuesStatus}
            setSelectedValuesStatus={setSelectedValuesStatus}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.settingTitle}
            placeholder="Input setting title"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, settingTitle: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={4} className="px-1">
          <Input
            value={paramsSearch.settingValue}
            placeholder="Input setting value"
            className="br-8"
            onChange={e => {
              setParamsSearch(prevState => {
                return { ...prevState, settingValue: e.target.value };
              });
            }}
          />
        </Col>
        <Col span={2} className="px-1" style={{ marginLeft: '250px' }}>
          <Button className="br-10 btn-grey w-100" onClick={handleResetFilter}>
            Reset
          </Button>
        </Col>
        <Col span={2} className="px-1">
          <Button className="br-10 btn-action w-100" onClick={handleSearch}>
            Search
          </Button>
        </Col>
      </Row>
      <Table
        columns={columns}
        pagination={{
          className: 'border-radius-paging',
          current: paramsSearch.page,
          pageSize: paramsSearch.limit,
          total: paramsSearch.total
          // showSizeChanger: true
        }}
        // loading={loading}
        dataSource={dataSource}
        onChange={value => handleTableChange(value)}
      />
      <ModalUpdate
        isEditing={isEditing}
        setIsEditing={setIsEditing}
        handleEditRecord={handleEditRecord}
        editRecord={editRecord}
        setEditRecord={setEditRecord}
        buttonLoading={loading}
      />
      <ModalAdd
        isAdding={isAdding}
        setIsAdding={setIsAdding}
        handleAddingSetting={handleAddingSetting}
        setAddRecord={setAddRecord}
        buttonLoading={buttonLoading}
        addRecord={addRecord}
      />
    </Spin>
  );
};

export default SettingListDashboard;
