import { Input, Modal, Radio, Select } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import styles from './ModalUpdate.module.css';
import PropTypes from 'prop-types';

const { Option } = Select;
const { TextArea } = Input;

export default function ModalUpdate({
  isEditing,
  setIsEditing,
  handleEditRecord,
  setEditRecord,
  editRecord,
  buttonLoading
}) {
  const listSettingType = useSelector(state => state.settingList.settingType);
  return (
    <Modal
      title="Update Information"
      visible={isEditing}
      onOk={handleEditRecord}
      okText={'Update'}
      onCancel={() => {
        setIsEditing(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Type</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Type</h4> */}
          <div className={styles.rowSelect}>
            <Select
              labelInValue
              value={{
                value: editRecord?.type
              }}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setEditRecord(record => {
                  return { ...record, type: value.value };
                })
              }
            >
              {listSettingType.map((item, key) => {
                return (
                  <Option key={key} value={item.id}>
                    {item.value}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Title</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Title</h4> */}
          <Input
            value={editRecord?.title}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, title: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Value</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Value</h4> */}
          <Input
            value={editRecord?.value}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, value: e.target.value };
              })
            }
          />
        </div>
        {/* <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Edit Display order</h4>
          <Input
            value={editRecord?.order}
            onChange={e =>
              setEditRecord(record => {
                return { ...record, order: e.target.value };
              })
            }
          />
        </div> */}
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          {/* <h4 className={styles.rowText}>Status</h4> */}
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              onChange={e => {
                setEditRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              value={editRecord?.status}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}>Description</h4>
          <TextArea
            className={'text-black br-8'}
            value={editRecord?.description}
            rows={4}
            placeholder="Description ... "
            onChange={e => {
              setEditRecord(record => {
                return { ...record, description: e.target.value };
              });
            }}
          />
        </div>
      </div>
    </Modal>
  );
}

ModalUpdate.propTypes = {
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func,
  handleEditRecord: PropTypes.func,
  setEditRecord: PropTypes.func,
  editRecord: PropTypes.object,
  buttonLoading: PropTypes.bool
};
