import { Input, Modal, Radio, Select } from 'antd';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import styles from './ModalAdd.module.css';
import PropTypes from 'prop-types';
import { isEmpty } from 'src/utils/helper';

const { Option } = Select;
const { TextArea } = Input;

export default function ModalAdd({
  addRecord,
  isAdding,
  setIsAdding,
  handleAddingSetting,
  setAddRecord,
  buttonLoading
}) {
  const listSettingType = useSelector(state => state.settingList.settingType);
  useEffect(() => {
    if (isEmpty(addRecord?.status))
      setAddRecord(prev => {
        return { ...prev, status: 'active' };
      });
  }, [addRecord?.status]);
  return (
    <Modal
      title="Add Setting"
      visible={isAdding}
      onOk={handleAddingSetting}
      okText={'Add'}
      onCancel={() => {
        setIsAdding(false);
        setAddRecord(null);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.modal}>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Type</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect}>
            <Select
              value={addRecord?.typeId}
              placeholder={'Select Setting Type'}
              style={{
                width: '100%'
              }}
              onChange={value =>
                setAddRecord(record => {
                  return { ...record, typeId: value };
                })
              }
            >
              {listSettingType.map((item, key) => {
                return (
                  <Option key={key} value={item.id}>
                    {item.value}
                  </Option>
                );
              })}
            </Select>
          </div>
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Title</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            value={addRecord?.title}
            placeholder={'Input Setting Title'}
            onChange={e =>
              setAddRecord(record => {
                return { ...record, title: e.target.value };
              })
            }
          />
        </div>
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Value</h4>
            <span className="importantInfor">*</span>
          </div>
          <Input
            value={addRecord?.value}
            placeholder={'Input Setting Value'}
            onChange={e =>
              setAddRecord(record => {
                return { ...record, value: e.target.value };
              })
            }
          />
        </div>
        {/* <div className={styles.rowModal}>
          <h4 className={styles.rowText}> Display order</h4>
          <Input
            value={addRecord?.displayOrder}
            onChange={e =>
              setAddRecord(record => {
                return { ...record, displayOrder: parseInt(e.target.value) };
              })
            }
          />
        </div> */}
        <div className={styles.rowModal}>
          <div className={styles.rowText}>
            <h4>Status</h4>
            <span className="importantInfor">*</span>
          </div>
          <div className={styles.rowSelect} style={{ border: 'none' }}>
            <Radio.Group
              value={addRecord?.status}
              onChange={e => {
                setAddRecord(record => {
                  return { ...record, status: e.target.value };
                });
              }}
              defaultValue={'active'}
            >
              <Radio value={'active'}>Active</Radio>
              <Radio value={'inactive'}>Inactive</Radio>
            </Radio.Group>
          </div>
        </div>
        <div className={styles.rowModal}>
          <h4 className={styles.rowText}> Description</h4>
          <TextArea
            className={'text-black br-8'}
            value={addRecord?.description}
            rows={4}
            placeholder="Description ... "
            onChange={e =>
              setAddRecord(record => {
                return { ...record, description: e.target.value };
              })
            }
          />
        </div>
      </div>
    </Modal>
  );
}

ModalAdd.propTypes = {
  isAdding: PropTypes.bool,
  setIsAdding: PropTypes.func,
  handleAddingSetting: PropTypes.func,
  setAddRecord: PropTypes.func,
  buttonLoading: PropTypes.bool
};
