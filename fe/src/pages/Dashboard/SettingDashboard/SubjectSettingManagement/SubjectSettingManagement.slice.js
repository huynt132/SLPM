import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import subjectSettingManagementApi from 'src/api/subjectSettingManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const getSubjectSettingList = createAsyncThunk(
  'subjectSettings/getAll',
  payloadCreator(subjectSettingManagementApi.getSubjectSettings)
);

export const editSubjectSetting = createAsyncThunk(
  'subjectSettings/edit',
  payloadCreator(subjectSettingManagementApi.editSubjectSetting)
);

export const addSubjectSetting = createAsyncThunk(
  'subjectSettings/edit',
  payloadCreator(subjectSettingManagementApi.addSubjectSetting)
);

export const getSubjectSettingTypes = createAsyncThunk(
  'subjectSettings/getType',
  payloadCreator(subjectSettingManagementApi.getSubjectSettingTypes)
);

const subjectSettingManagement = createSlice({
  name: 'subjectManagement',
  initialState: {
    settingType: []
  },
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {
    [getSubjectSettingTypes.fulfilled]: (state, action) => {
      state.settingType = action.payload.data.data.map(subject => ({
        label: subject.value,
        value: subject.id
      }));
    }
  }
});

const subjectSettingManagementReducer = subjectSettingManagement.reducer;

export default subjectSettingManagementReducer;
