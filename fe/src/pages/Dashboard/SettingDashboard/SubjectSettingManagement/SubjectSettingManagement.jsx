// react
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Popconfirm, Button, Space, Table, Tag, Row, Col, Input, Spin, Switch } from 'antd';
// style
import { AiOutlinePlus } from 'react-icons/ai';
import { VscEdit } from 'react-icons/vsc';
// services
import { getSubjectSettingList, editSubjectSetting } from './SubjectSettingManagement.slice';
// custom component
import SelectSubject from 'src/components/Select/SelectSubject';
import SelectCustomValue from 'src/components/Select/SelectCustomValue';
import SelectSubjectSettingType from 'src/components/Select/SelectSubjectSettingType';
import { path } from 'src/constants/path';

//helper
import { showMessage } from 'src/utils/commonHelper';
import { isEmpty } from 'src/utils/helper';
import ModalEditSubjectSetting from '../SubjectSettingManagement/components/ModalEditSubjectSetting/ModalEditSubjectSetting';
import ModalMessage from '../../../../components/ModalMessage/ModalMessage';

const SubjectSettingManagement = () => {
  const dispatch = useDispatch();
  const [isEditing, setIsEditing] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [loading, setLoading] = useState(false);
  const curSubjectSetting = useRef({});
  const curSubjectSettingIdx = useRef(0);
  const editSubjectSettingType = useRef('add');

  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');
  const [errorType, setErrorType] = useState('');
  const userRole = useSelector(state => state.auth.role);

  const pagination = useRef({
    limit: 8,
    page: 1,
    total: 0
  });

  const [filter, setFilter] = useState({
    subjectId: null,
    settingTypeId: null,
    settingTitle: null,
    settingValue: null,
    status: null
  });

  useEffect(() => {
    getSubjectSettings(filter);
  }, [dispatch]);

  const getSubjectSettings = async filters => {
    try {
      setLoading(true);
      let query = {
        ...filters,
        ...pagination.current
      };
      // if (!isEmpty(query.subjectId)) query.subjectId = query.subjectId.join(',');
      // if (!isEmpty(query.settingTypeId)) query.settingTypeId = query.settingTypeId.join(',');
      const res = await dispatch(getSubjectSettingList(query));
      const result = unwrapResult(res);
      if (result.data.success) {
        setDataSource(result.data.data);
        if (!isEmpty(result?.data?.curSubject))
          setFilter(prev => {
            return { ...prev, subjectId: result.data.curSubject };
          });
        pagination.current.total = result.data.total;
        showMessage(result.data.message);
      }
    } catch (error) {
      if (error.status === 400) {
        setDataSource([]);
        setIsShowMessage(true);
        setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
        setMessageType('fail');
      } else if (error.status === 401) {
        setErrorType('tokenExpired');
        setMessageType('fail');
        setMessageText('Your login session has expired, please login again !!');
        setIsShowMessage(true);
      }
    } finally {
      setLoading(false);
    }
  };

  const clearFilters = () => {
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    setFilter(prev => {
      return {
        ...prev,
        subjectId: null,
        settingTypeId: null,
        status: null,
        settingTitle: null,
        settingValue: null
      };
    });
    getSubjectSettings({
      subjectId: null,
      settingTypeId: null,
      status: null,
      settingTitle: null,
      settingValue: null
    });
  };

  const search = () => {
    if (isEmpty(filter?.subjectId)) {
      setIsShowMessage(true);
      setMessageText('Please choose subject to search!');
      setMessageType('fail');
      return;
    }
    pagination.current = {
      limit: 8,
      page: 1,
      total: 0
    };
    getSubjectSettings(filter);
  };

  async function editSubjectSettingStatus() {
    if (isEmpty(curSubjectSetting.current)) return showMessage('Update data is invalid, please check again!', 'fail');
    try {
      setLoading(true);
      const res = await dispatch(
        editSubjectSetting({
          ...curSubjectSetting.current,
          status: curSubjectSetting.current.status === 'active' ? 'inactive' : 'active'
        })
      );
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        curSubjectSetting.current.status = curSubjectSetting.current.status === 'active' ? 'inactive' : 'active';
      }
    } catch (error) {
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while update data!');
      setMessageType('fail');
    } finally {
      setLoading(false);
    }
  }

  const action = (rowData, action) => {
    if (!isEmpty(rowData)) curSubjectSetting.current = rowData;
    if (action.action === 'isHideModalEditSubjectSetting') {
      setIsEditing(false);
    }
    // if (action.action === 'triggerSearch') {
    //   getSubjectSettings(filter)
    // }
    if (action.action === 'editSubjectSettingStatus') {
      editSubjectSettingStatus();
    }
    if (action.action === 'editSubjectSettingSuccess') {
      setDataSource(prevList => {
        let tmp = [...prevList];
        tmp[curSubjectSettingIdx.current] = rowData;
        return [...tmp];
      });
      setIsEditing(false);
    }
    if (action.action === 'editSubjectSetting') {
      editSubjectSettingType.current = 'Update';
      curSubjectSettingIdx.current = action.data;
      setIsEditing(true);
    }
    if (action.action === 'addSubjectSetting') {
      editSubjectSettingType.current = 'Add';
      setIsEditing(true);
    }
    if (action.action === 'showMessage') {
      setIsShowMessage(true);
      setMessageText(action.data.description);
      setMessageType(action.data.type);
    }
  };
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      className: 'w-5',
      key: 'id'
    },
    // {
    //   title: 'Subject Name',
    //   dataIndex: 'subjectName',
    //   key: 'subjectName'
    // },
    {
      title: 'Type',
      dataIndex: 'settingType',
      key: 'settingType'
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: 'Value',
      dataIndex: 'value',
      key: 'value'
    },
    {
      title: 'Status',
      className: 'w-10',
      key: 'status',
      dataIndex: 'status',
      render: status => (
        <span>
          {
            <Tag className={`${status === 'active' ? 'btn-active' : 'btn-inactive'} text-center br-10`} key={status}>
              {status}
            </Tag>
          }
        </span>
      )
    },
    {
      title: 'Action',
      key: 'action',
      className: 'w-15',
      render: (_, record, idx) => (
        <Space size="middle">
          {userRole !== 'Trainer' && (
            <Popconfirm
              disabled={userRole === 'Trainer' ? true : false}
              placement="top"
              title={`Are you sure you want to ${
                record.status === 'active' ? 'deactivate' : 'activate'
              } this subject setting?`}
              onConfirm={() => action(record, { action: 'editSubjectSettingStatus' })}
              okText="Yes"
              cancelText="No"
            >
              {/* <Button className={`br-10 d-flex ${record.status === 'active' ? 'btn-inactive' : 'btn-active'} `}>
              <span className="m-auto">{record.status === 'active' ? 'deactivate' : 'activate'}</span>
            </Button> */}
              <Switch
                className={`${record.status === 'active' ? 'switch-custom-inactive' : 'switch-custom-active'} `}
                checked={record.status === 'active' ? true : false}
              />
            </Popconfirm>
          )}
          {userRole !== 'Trainer' && (
            // <Button
            //   className="br-10 btn-edit d-flex"
            //   onClick={() => {
            //     action(record, { action: 'editSubjectSetting', data: idx });
            //   }}
            // >
            //   <VscEdit className="text-white m-auto" />
            // </Button>

            <Button
              style={{
                borderRadius: '10px',
                width: '30px',
                height: '30px',
                border: 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                color: '#fff',
                background: '#6BBCB3'
              }}
              onClick={() => action(record, { action: 'editSubjectSetting', data: idx })}
            >
              <VscEdit style={{ cursor: 'pointer', fontSize: '18px' }} />
            </Button>
          )}
        </Space>
      )
    }
  ];

  return (
    <>
      <Spin tip="Loading..." spinning={loading}>
        <Row>
          <Col span={18}>
            <h1 className={'m-0'}>Subject Setting Management</h1>
          </Col>
          <Col span={4} offset={2} className="px-1">
            {userRole !== 'Trainer' && (
              <Button
                className="br-10 btn-action d-flex w-100 h-80"
                onClick={() =>
                  action(
                    { displayOrder: null, subjectId: null, typeId: null, title: null, value: null },
                    { action: 'addSubjectSetting' }
                  )
                }
              >
                <AiOutlinePlus className="icon-plus m-auto" />
                <span className="m-auto">Create Subject Setting</span>
              </Button>
            )}
          </Col>
        </Row>
        {/*<hr className={'hr'} />*/}
        <Row className={'mt-2 mb-4'}>
          <Col span={4} className="px-1">
            <SelectSubject
              customClassName={'border-radius-select'}
              onOptionSelected={value => {
                setFilter(prev => {
                  return { ...prev, subjectId: value };
                });
              }}
              mode={'single'}
              subjects={isEmpty(filter.subjectId) ? [] : filter.subjectId}
            />
          </Col>
          <Col span={4} className="px-1">
            <SelectSubjectSettingType
              customClassName={'border-radius-select'}
              onOptionSelected={value => {
                setFilter(prev => {
                  return { ...prev, settingTypeId: value };
                });
              }}
              selects={isEmpty(filter.settingTypeId) ? [] : filter.settingTypeId}
            />
          </Col>
          <Col span={4} className="px-1">
            <SelectCustomValue
              customClassName={'border-radius-select'}
              mode={'single'}
              placeholder={'Select status'}
              onOptionSelected={value => {
                setFilter(prev => {
                  return { ...prev, status: value };
                });
              }}
              query={{
                table: 'subject_settings',
                field: 'status'
              }}
              values={isEmpty(filter.status) ? [] : filter.status}
            />
          </Col>
          <Col span={4} className="pr-1">
            <Input
              value={filter.title}
              placeholder="Input Setting Title"
              className="br-8"
              onChange={e => {
                setFilter(prev => {
                  return { ...prev, settingTitle: e.target.value };
                });
              }}
            />
          </Col>
          <Col span={4} className="px-1">
            <Input
              value={filter.value}
              placeholder="Input Setting Value"
              className="br-8"
              onChange={e => {
                setFilter(prev => {
                  return { ...prev, settingValue: e.target.value };
                });
              }}
            />
          </Col>
          <Col span={2} className="px-1">
            <Button className="br-10 btn-grey w-100" onClick={clearFilters}>
              Reset
            </Button>
          </Col>
          <Col span={2} className="px-1">
            <Button className="br-10 btn-action w-100" onClick={search}>
              Search
            </Button>
          </Col>
        </Row>
        <Table
          // loading={loading}
          columns={columns}
          rowKey={record => 'subject_setting' + record.id}
          pagination={{
            className: 'border-radius-paging',
            position: 'bottomLeft',
            pageSize: pagination.current.limit,
            total: pagination.current.total,
            pageSizeOptions: [5, 10, 25, 100],
            current: pagination.current.page,
            onChange: (page, pageSize) => {
              pagination.current.page = page;
              getSubjectSettings(filter);
            }
          }}
          dataSource={dataSource}
        />
        <ModalEditSubjectSetting
          isShowModal={isEditing}
          type={editSubjectSettingType.current}
          subjectSetting={curSubjectSetting.current}
          action={action}
          search={search}
        />
        <ModalMessage
          isShowModal={isShowMessage}
          isHideModal={() => setIsShowMessage(false)}
          navigateTo={path.absSubjectSetting}
          text={messageText}
          iconname={messageType}
          type={errorType}
        />
      </Spin>
    </>
  );
};
export default SubjectSettingManagement;
