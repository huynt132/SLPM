// React
import React, { useState, useEffect } from 'react';
import { Modal, Row, Col, Input, Radio } from 'antd';
import PropsTypes from 'prop-types';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

// style

// services
import { editSubjectSetting, addSubjectSetting } from '../../SubjectSettingManagement.slice';
import { showMessage } from 'src/utils/commonHelper';

// custom component
import SelectSubject from 'src/components/Select/SelectSubject';
import SelectSubjectSettingType from 'src/components/Select/SelectSubjectSettingType';

// helper
import { isEmpty } from 'src/utils/helper';

export default function ModalEditSubjectSetting({ isShowModal, type, subjectSetting, action, search }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [newSubjectSetting, setNewSubjectSetting] = useState({ ...subjectSetting });

  const [choiceSetting, setChoiceSetting] = useState(null);

  useEffect(() => {
    setNewSubjectSetting(subjectSetting);
  }, [subjectSetting]);

  useEffect(() => {
    if (isEmpty(newSubjectSetting?.status)) {
      setNewSubjectSetting(prev => {
        return {
          ...prev,
          status: 'active'
        };
      });
    }
  }, [newSubjectSetting?.status]);

  async function updateSubjectSetting() {
    try {
      setLoading(true);
      const res = await dispatch(editSubjectSetting({ ...newSubjectSetting }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        action(result.data.data, { action: 'editSubjectSettingSuccess' });
        resetModal();
      }
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while update data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  async function createSubjectSetting() {
    try {
      setLoading(true);
      const res = await dispatch(addSubjectSetting({ ...newSubjectSetting }));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        resetModal();
        action(null, { action: 'isHideModalEditSubjectSetting' });
        action(null, { action: 'triggerSearch' });
      }
      search();
    } catch (error) {
      if ([400, 401].includes(error.status))
        action(null, {
          action: 'showMessage',
          data: {
            description: isEmpty(error.data.message) ? 'An error occurred while create data!' : error.data.message,
            type: 'fail'
          }
        });
    } finally {
      setLoading(false);
    }
  }

  function handleSubmit() {
    if (type === 'Update') updateSubjectSetting();
    if (type === 'Add') createSubjectSetting();
  }

  function resetModal() {
    if (type === 'Add') setNewSubjectSetting({});
    if (type === 'Update') setNewSubjectSetting({ ...subjectSetting });
  }

  return (
    <Modal
      confirmLoading={loading}
      title={`${type} subject setting`}
      visible={isShowModal}
      onOk={() => {
        handleSubmit();
      }}
      afterClose={resetModal}
      width="600px"
      okText={type}
      onCancel={() => {
        resetModal();
        action(null, { action: 'isHideModalEditSubjectSetting' });
      }}
    >
      <div>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Subject</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <SelectSubject
              customClassName={'border-radius-select'}
              mode={'single'}
              onOptionSelected={value => {
                setNewSubjectSetting(prev => {
                  return { ...prev, subjectId: value };
                });
              }}
              subjects={isEmpty(newSubjectSetting.subjectId) ? [] : newSubjectSetting.subjectId}
              // disabled={type === 'Update'}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Setting Type</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <SelectSubjectSettingType
              customClassName={'border-radius-select'}
              onOptionSelected={value => {
                setNewSubjectSetting(prev => {
                  return { ...prev, typeId: value };
                });
                setChoiceSetting(value);
              }}
              selects={isEmpty(newSubjectSetting.typeId) ? [] : newSubjectSetting.typeId}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Title</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Input
              className={'br-8'}
              value={newSubjectSetting.title}
              placeholder="Setting Title"
              onChange={e => {
                setNewSubjectSetting(prev => {
                  return { ...prev, title: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Value</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Input
              className={'br-8'}
              value={newSubjectSetting.value}
              placeholder={choiceSetting === 12 ? 'Input Value By Percent (%)' : 'Input Value'}
              onChange={e => {
                setNewSubjectSetting(prev => {
                  return { ...prev, value: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6} className={``}>
            <div className="rowText">
              <h4>Status</h4>
              <span className="importantInfor">*</span>
            </div>
          </Col>
          <Col span={18}>
            <Radio.Group
              onChange={e => {
                setNewSubjectSetting(prev => {
                  return { ...prev, status: e.target.value };
                });
              }}
              value={newSubjectSetting.status}
            >
              <Radio value={'active'}>active</Radio>
              <Radio value={'inactive'}>inactive</Radio>
            </Radio.Group>
          </Col>
        </Row>
        <Row className={`py-1`}>
          <Col span={6}>
            <div className="rowText">
              <h4>Description</h4>
            </div>
          </Col>
          <Col span={18}>
            <Input.TextArea
              className={'text-black br-8'}
              value={newSubjectSetting.description}
              autoSize={{ minRows: 3, maxRows: 3 }}
              placeholder="Input Description"
              onChange={e => {
                setNewSubjectSetting(prev => {
                  return { ...prev, description: e.target.value };
                });
              }}
            />
          </Col>
        </Row>
      </div>
    </Modal>
  );
}

ModalEditSubjectSetting.prototype = {
  isShowModal: PropsTypes.bool,
  action: PropsTypes.func,
  type: PropsTypes.string,
  subjectSetting: PropsTypes.object,
  search: PropsTypes.func
};
ModalEditSubjectSetting.defaultProps = {
  isShowModal: false,
  type: 'Add',
  subjectSetting: {}
};
