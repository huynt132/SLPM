import { unwrapResult } from '@reduxjs/toolkit';
import { Button, Table, Row, Col } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';

import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from 'chart.js';
import { Bar } from 'react-chartjs-2';

import { getFunctionStatusCount } from './Dashboard.slice';
import { getClassStatistic } from './Dashboard.slice';
import { useLocation, useNavigate } from 'react-router-dom';
import SelectMilestone from '../../../components/Select/SelectMilestone';
import SelectClass from '../../../components/Select/SelectClass';
import { isEmpty } from '../../../utils/helper';
import { showMessage } from '../../../utils/commonHelper';
import { path } from '../../../constants/path';

export default function DashboardClass() {
  const { state } = useLocation();

  const dispatch = useDispatch();

  const [dataSource, setDataSource] = useState([]);
  const navigate = useNavigate();

  const [loadingStatistic, setLoadingStatistic] = useState(false);
  const [isShowMessage, setIsShowMessage] = useState(false);
  const [messageText, setMessageText] = useState('');
  const [messageType, setMessageType] = useState('');

  const [classStatistic, setClassStatistic] = useState({
    labels: [],
    datasets: []
  });
  let delayed;

  const chartRef = useRef();

  const [filter, setFilter] = useState({
    milestoneId: null,
    classId: null
  });

  useEffect(() => {
    // getRequirementsStatistic(filter);
    getClassStatistics(filter);
  }, [dispatch]);

  const columns = [
    {
      title: 'Team',
      dataIndex: 'projectCode',
      key: 'projectCode',
      render: (_, record) => (
        <div className={'d-flex justify-content-between'}>
          <span>{record.projectCode}</span>
          <Button
            className={'br-8'}
            onClick={() => {
              navigate(path.projectDash, {
                state: {
                  projectId: record?.projectId,
                  projectCode: record?.projectCode,
                  milestoneId: filter?.milestoneId
                }
              });
            }}
          >
            Dashboard
          </Button>
        </div>
      )
    },
    {
      title: 'Total',
      dataIndex: 'total',
      className: 'w-10',
      key: 'total',
      render: (_, record, idx) => <span key={`total_${idx}`}>{record?.total || 0}</span>
    },
    {
      title: 'Total Pending',
      dataIndex: 'totalPending',
      className: 'w-10',
      key: 'totalPending',
      render: (_, record, idx) => <span key={`total_pending_${idx}`}>{record?.totalPending || 0}</span>
    },
    {
      title: 'Total Planned',
      dataIndex: 'totalCommitted',
      className: 'w-10',
      key: 'totalCommitted',
      render: (_, record, idx) => <span key={`total_committed_${idx}`}>{record?.totalCommitted || 0}</span>
    },
    {
      title: 'Total Submitted',
      dataIndex: 'totalSubmitted',
      className: 'w-10',
      key: 'totalSubmitted',
      render: (_, record, idx) => <span key={`total_submitted_${idx}`}>{record?.totalSubmitted || 0}</span>
    },
    {
      title: 'Total Rejected',
      dataIndex: 'totalRejected',
      className: 'w-10',
      key: 'totalRejected',
      render: (_, record, idx) => <span key={`total_rejected_${idx}`}>{record?.totalRejected || 0}</span>
    },
    {
      title: 'Total Evaluated',
      dataIndex: 'totalEvaluated',
      className: 'w-10',
      key: 'totalEvaluated',
      render: (_, record, idx) => <span key={`total_evaluated_${idx}`}>{record?.totalEvaluated || 0}</span>
    }
  ];

  ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

  const options = {
    plugins: {
      title: {
        display: false,
        text: 'Grades by member: total, team-based, individual-based'
      }
    },
    maintainAspectRatio: false,
    animation: {
      onComplete: () => {
        delayed = true;
      },
      delay: context => {
        let delay = 0;
        if (context.type === 'data' && context.mode === 'default' && !delayed) {
          delay = context.dataIndex * 300 + context.datasetIndex * 100;
        }
        return delay;
      }
    },
    responsive: true,
    scales: {
      x: {
        min: 0,
        max: 10,
        title: {
          display: true,
          text: 'Team',
          family: 'Helvetica',
          font: {
            size: 16
          }
        }
      },
      y: {
        min: 0,
        max: 10,
        title: {
          display: true,
          text: 'Grade',
          family: 'Helvetica',
          font: {
            size: 16
          }
        }
      }
    }
  };

  const getRequirementsStatistic = async filters => {
    try {
      setLoadingStatistic(true);
      let res = await dispatch(getFunctionStatusCount({ ...filters }));
      res = unwrapResult(res);
      if (res.data.success) {
        setDataSource(res.data.data);
      }
    } catch (error) {
      setDataSource([]);
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
      setMessageType('fail');
    } finally {
      setLoadingStatistic(false);
    }
  };

  const getClassStatistics = async filters => {
    try {
      setLoadingStatistic(true);
      let res = await dispatch(getClassStatistic({ ...filters }));
      res = unwrapResult(res);
      if (res.data.success) {
        if (!isEmpty(res?.data?.data)) setClassStatistic(res.data.data);
        if (!isEmpty(res?.data?.curClassId)) {
          setFilter(prev => {
            return { ...prev, classId: res?.data?.curClassId };
          });
          getRequirementsStatistic({ ...filters, classId: res?.data?.curClassId });
        }
        showMessage(res.data.message);
      }
    } catch (error) {
      setDataSource([]);
      setIsShowMessage(true);
      setMessageText(error?.data?.message || 'An error occurred while retrieving data!');
      setMessageType('fail');
    } finally {
      setLoadingStatistic(false);
    }
  };

  return (
    <>
      <Row>
        <Col span={14}>
          <h1>{`Class dashboard`}</h1>
        </Col>
        <Col className={'pr-1'} span={4}>
          <SelectClass
            mode={'single'}
            customClassName={'border-radius-select'}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, classId: value };
              });
            }}
            placeholder={'Select class'}
            classes={isEmpty(filter?.classId) ? [] : filter?.classId}
          />
        </Col>
        <Col className={'px-1'} span={4}>
          <SelectMilestone
            customClassName={'border-radius-select'}
            filter={filter}
            mode={'single'}
            milestones={isEmpty(filter.milestoneId) ? [] : filter.milestoneId}
            onOptionSelected={value => {
              setFilter(prev => {
                return { ...prev, milestoneId: value };
              });
            }}
          />
        </Col>
        <Col span={2}>
          <Button
            className="br-10 btn-action w-100 mx-1"
            onClick={() => {
              // getRequirementsStatistic(filter);
              getClassStatistics(filter);
            }}
          >
            Search
          </Button>
        </Col>
      </Row>
      <Row>
        <h2 style={{ color: '#666' }} className={'m-auto'}>
          Grades by teams: total, team-based, individual-based
        </h2>
      </Row>
      <Row style={{ position: 'relative', margin: 'auto', height: '42vh' }}>
        <Bar ref={chartRef} options={options} data={classStatistic} />
      </Row>
      <Row>
        <h3>Requirement statistics by submitting status</h3>
      </Row>
      <Table
        loading={loadingStatistic}
        columns={columns}
        rowKey={record => 'milestone_' + record.projectId}
        dataSource={dataSource}
        pagination={false}
      />
      <ModalMessage
        isShowModal={isShowMessage}
        isHideModal={() => setIsShowMessage(false)}
        navigateTo={path.classDash}
        text={messageText}
        iconname={messageType}
      />
    </>
  );
}
