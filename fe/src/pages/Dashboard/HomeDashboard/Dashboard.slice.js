import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import dashboardApi from 'src/api/dashboard.api';
import { payloadCreator } from 'src/utils/helper';

export const getFunctionCount = createAsyncThunk(
  'submits/getFunctionCount',
  payloadCreator(dashboardApi.getFunctionCount)
);
export const getFunctionStatusCount = createAsyncThunk(
  'submits/getFunctionStatusCount',
  payloadCreator(dashboardApi.getFunctionStatusCount)
);
export const getClassStatistic = createAsyncThunk(
  'dashboard/getClassStatistic',
  payloadCreator(dashboardApi.getClassStatistic)
);
export const getTeamStatistic = createAsyncThunk(
  'dashboard/getTeamStatistic',
  payloadCreator(dashboardApi.getTeamStatistic)
);

const dashboard = createSlice({
  name: 'dashboard',
  initialState: {},
  reducers: {},
  extraReducers: {}
});

const dashboardReducer = dashboard.reducer;

export default dashboardReducer;
