import { Result } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import { path } from 'src/constants/path';

export default function NotFound() {
  return (
    <Result
      status="404"
      title="Oops!!"
      subTitle="Sorry, the page you visited does not exist."
      extra={<Link to={path.home}>Back Home</Link>}
    />
  );
}
