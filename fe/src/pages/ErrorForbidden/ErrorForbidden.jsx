import { Result } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import { path } from 'src/constants/path';

export default function ErrorForbidden() {
  return (
    <Result
      status="403"
      title="Oops!!"
      subTitle="Sorry, you are not authorized to access this page."
      extra={<Link to={path.classDash}>Back Home</Link>}
    />
  );
}
