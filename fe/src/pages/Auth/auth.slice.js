import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import authApi from 'src/api/auth.api';
import LocalStorage from 'src/constants/localStorage';
import { payloadCreator } from 'src/utils/helper';

export const register = createAsyncThunk('auth/register', payloadCreator(authApi.register));

export const login = createAsyncThunk('auth/authen', payloadCreator(authApi.login));

export const loginWithGoogle = createAsyncThunk('auth/loginGoogle', payloadCreator(authApi.loginWithGoogle));

export const verifyAccount = createAsyncThunk('auth/verifyAccount', payloadCreator(authApi.verifyAccount));

export const recoverPassword = createAsyncThunk('auth/recoverPassword', payloadCreator(authApi.recoverPassword));

const handleAuthFulfilled = (state, action) => {
  const { data } = action.payload.data;
  state.accessToken = data.jwtToken;
  state.role = data.role;
  state.user = data.user;
  localStorage.setItem(LocalStorage.accessToken, state.accessToken);
  localStorage.setItem(LocalStorage.role, JSON.stringify(state.role));
  localStorage.setItem(LocalStorage.user, JSON.stringify(state.user));
};

const auth = createSlice({
  name: 'auth',
  initialState: {
    accessToken: localStorage.getItem(LocalStorage.accessToken),
    role: JSON.parse(localStorage.getItem(LocalStorage.role)),
    user: JSON.parse(localStorage.getItem(LocalStorage.user)) || {}
  },
  reducers: {
    logout: state => {
      state = {};
      localStorage.removeItem(LocalStorage.accessToken);
      localStorage.removeItem(LocalStorage.role);
      localStorage.removeItem(LocalStorage.user);
    }
  },
  extraReducers: {
    // [register.fulfilled]: handleAuthFulfilled,
    [login.fulfilled]: handleAuthFulfilled,
    [loginWithGoogle.fulfilled]: handleAuthFulfilled
  }
});

const authReducer = auth.reducer;

export const logout = auth.actions.logout;

export default authReducer;
