import { unwrapResult } from '@reduxjs/toolkit';
import { Button } from 'antd';
import React, { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import ErrorMessage from 'src/components/ErrorMessage/ErrorMessage';
import InputText from 'src/components/InputText/InputText';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { path } from 'src/constants/path';
import { rules } from 'src/constants/rules';
import { recoverPassword } from '../auth.slice';
import styles from './ResetPassword.module.css';

export default function ForgotPassword() {
  // use react-hook-form to validate input
  const {
    control,
    handleSubmit,
    getValues,
    formState: { errors }
  } = useForm({
    email: ''
  });

  const dispatch = useDispatch();

  const [buttonLoading, setButtonLoading] = useState(false);

  const [isShowModal, setIsShowModal] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');

  const handleReset = async data => {
    try {
      setButtonLoading(true);
      const res = await dispatch(recoverPassword(data.email));
      unwrapResult(res);
      setButtonLoading(false);
      setModalIcon('success');
      setModalText('Please check your mail to get new password !!');
      setIsShowModal(true);
    } catch (error) {
      setIsShowModal(true);
      setModalIcon('fail');
      setModalText(error.data.message || error.data.error);
      setButtonLoading(false);
    }
  };

  return (
    <>
      <ModalMessage
        isShowModal={isShowModal}
        isHideModal={() => setIsShowModal(false)}
        text={modalText}
        iconname={modalIcon}
      />
      <form className={styles.form} onSubmit={handleSubmit(handleReset)} noValidate>
        <h3 className={styles.formTitle}>
          Please enter your email address. You will receive a link to create a new password via email.
        </h3>
        <div className={styles.inputEmail}>
          <label>
            Email address <span style={{ color: 'red' }}>*</span>
          </label>
          <ErrorMessage errors={errors} name="email" />
          <Controller
            name="email"
            control={control}
            rules={rules.email}
            render={({ field }) => (
              <InputText
                type="email"
                name="email"
                placeholder="Enter Your Email Address"
                onChange={e => field.onChange(e)}
                value={getValues('email')}
              />
            )}
          />
        </div>
        <div className={styles.formAction}>
          <Button
            type="primary"
            block
            size="large"
            htmlType="submit"
            loading={buttonLoading}
            // onClick={}
          >
            Send
          </Button>
        </div>
        <div className={styles.formFooter}>
          <h3>Remember your password ? </h3>
          <Link className={styles.linkStyle} to={path.login}>
            Sign in
          </Link>
        </div>
      </form>
    </>
  );
}
