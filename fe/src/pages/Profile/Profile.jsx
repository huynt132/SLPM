import React, { useEffect, useState } from 'react';
import styles from 'src/pages/Profile/Profile.module.scss';
import ModalEditUserProfile from 'src/pages/Profile/ProfileManagement/components/ModalEditUserProfile/ModalEditUserProfile';
import { Row, Col, Card, Avatar, Image, Button, Upload, Spin } from 'antd';
import { BsTelephoneInbound, BsFillPersonFill } from 'react-icons/bs';
import { MdOutlineMailOutline, MdOutlineAddCircleOutline } from 'react-icons/md';
import { FaBirthdayCake } from 'react-icons/fa';
import { IconContext } from 'react-icons';
import { useDispatch, useSelector } from 'react-redux';
import { getUserDetail, uploadAvatar } from '../Dashboard/ClassDashboard/UserDashboard/userList.slice';
import { unwrapResult } from '@reduxjs/toolkit';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';
import { path } from 'src/constants/path';
import ImgCrop from 'antd-img-crop';
import { AiOutlineFieldNumber } from 'react-icons/ai';
import { GrAdd } from 'react-icons/gr';
import { GiAstronautHelmet } from 'react-icons/gi';
import { VscLayersActive } from 'react-icons/vsc';
import ModalChangePassword from './ProfileManagement/components/ModalChangePassword/ModalChangePassword';
import { changePassword } from './ProfileManagement/Profile.slice';
import { showMessage } from 'src/utils/commonHelper';
import avatar from 'src/assets/images/avatardefault.png';
import { MdOutlineModeEdit } from 'react-icons/md';
import { RiAddLine } from 'react-icons/ri';

export default function Profile() {
  const userRedux = useSelector(state => state.auth.user);

  const [user, setUser] = useState(null);

  const [isShowModalEdit, setIsShowModalEdit] = useState(false);

  const [isShowModalChangePass, setIsShowModalChangePass] = useState(false);

  const [isShowModal, setIsShowModal] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  const [errorType, setErrorType] = useState('');

  const [loading, setLoading] = useState(false);

  const [buttonLoading, setButtonLoading] = useState(false);

  const dispatch = useDispatch();
  const updateAvatar = async file => {
    try {
      let body = new FormData();
      body.append('file', file);
      setLoading(true);
      const res = await dispatch(uploadAvatar(body));
      const result = unwrapResult(res);
      if (result.data.success) {
        setUser(prevState => {
          return {
            ...prevState,
            avatarLink: result.data.data
          };
        });
        //   setModalIcon('success');
        //   setModalText('Update successfully');
        //   setIsShowModal(true);
        showMessage(res.payload.data.message);
      }
    } catch (error) {
      setIsShowModal(true);
      setModalIcon('fail');
      setModalText(error?.data?.message || error?.data?.error || 'An error occurred while update data!');
    } finally {
      setLoading(false);
    }
  };

  const getUserProfile = async params => {
    try {
      const res = await dispatch(getUserDetail({ params }));
      const result = unwrapResult(res);
      setUser(result.data.data);
    } catch (error) {
      // setIsShowModal(true);
      // setModalIcon('fail');
      // setModalText(error.data.message || error.data.error);
      if (error.status === 400) {
        setModalIcon('fail');
        setModalText(error?.data?.message || 'An error occurred while retrieving data!');
        setIsShowModal(true);
      } else if (error.status === 401) {
        setErrorType('tokenExpired');
        setModalIcon('fail');
        setModalText('Your login session has expired, please login again !!');
        setIsShowModal(true);
      }
    }
  };

  useEffect(() => {
    getUserProfile({ id: userRedux.id });
  }, [dispatch]);

  // const handleChange = info => {
  //   if (info.file.status === 'uploading') {
  //     return;
  //   }

  //   if (info.file.status === 'done') {
  //     // Get this url from response in real world.
  //     getBase64(info.file.originFileObj, url => {
  //       setImageUrl(url);
  //     });
  //   }
  // };

  const [stateChangePass, setStateChangePass] = useState({});

  const handleChangePassword = async () => {
    try {
      setButtonLoading(true);
      const res = await dispatch(changePassword(stateChangePass));
      unwrapResult(res);
      setButtonLoading(false);
      showMessage(res.payload.data.message);
    } catch (error) {
      setIsShowModal(true);
      setModalIcon('fail');
      setModalText(error.data.message || error.data.error);
      setButtonLoading(false);
    }
  };

  return (
    <>
      <Spin spinning={loading}>
        <ModalMessage
          isShowModal={isShowModal}
          isHideModal={() => setIsShowModal(false)}
          navigateTo={path.profileDash}
          text={modalText}
          iconname={modalIcon}
          type={errorType}
        />
        <div className={styles.profileBanner}>
          <div className={styles.profileContainer}>
            <div className={styles.profileBannerTop}>
              <div className={styles.profileBannerTopLeft}>
                <Avatar
                  className={styles.profileBannerAvatar}
                  src={<Image src={user?.avatarLink ? user?.avatarLink : avatar} />}
                ></Avatar>
                <div className={styles.profileBannerAvatarInfo}>
                  <h2 className={styles.profileBannerName}>{user?.fullName}</h2>
                  <p className={styles.profileBannerAddress}>{user?.address}</p>
                </div>
                <div className={styles.avatarEdit}>
                  <ImgCrop rotate onModalOk={updateAvatar}>
                    <Upload listType="picture" showUploadList={false}>
                      <div className={styles.iconEdit}>{user?.avatarLink ? <MdOutlineModeEdit /> : <RiAddLine />}</div>
                    </Upload>
                  </ImgCrop>
                </div>
              </div>
              {/*<div className={styles.profileBannerTopRight}>*/}
              {/*  <ul className={styles.profileBannerStatistic}>*/}
              {/*    <li style={{ borderRight: '1px solid' }}>*/}
              {/*      <span className={styles.profileBannerStatisticValue}>13</span>*/}
              {/*      <span style={{ fontSize: '12px', lineHeight: '1.5' }}>Projects</span>*/}
              {/*    </li>*/}
              {/*    <li style={{ borderRight: '1px solid' }}>*/}
              {/*      <span className={styles.profileBannerStatisticValue}>25</span>*/}
              {/*      <span style={{ fontSize: '12px', lineHeight: '1.5' }}>Evaluation</span>*/}
              {/*    </li>*/}
              {/*    <li>*/}
              {/*      <span className={styles.profileBannerStatisticValue}>52</span>*/}
              {/*      <span style={{ fontSize: '12px', lineHeight: '1.5' }}>Features</span>*/}
              {/*    </li>*/}
              {/*  </ul>*/}
              {/*</div>*/}
            </div>
          </div>
        </div>
        <div className={styles.profileContent}>
          <Row>
            <Col span={16}>
              <Card
                title={'About'}
                extra={
                  <div>
                    <Button
                      className={'br-8'}
                      onClick={() => {
                        setIsShowModalEdit(!isShowModalEdit);
                      }}
                    >
                      Edit
                    </Button>
                    {user?.provider !== 'GOOGLE' && (
                      <Button
                        type="primary"
                        style={{ marginLeft: '2rem' }}
                        className={'br-8'}
                        onClick={() => {
                          setIsShowModalChangePass(true);
                        }}
                      >
                        Change Password
                      </Button>
                    )}
                  </div>
                }
                bordered={true}
                className={styles.cardWidget}
              >
                <Row className={styles.cardContentHolder}>
                  {/*<Col span={8}>*/}
                  {/*  <div className={styles.cardContentItem}>*/}
                  {/*    <IconContext.Provider value={{ color: '#fa8c16', size: '1rem', marginRight: '1rem' }}>*/}
                  {/*      <>*/}
                  {/*        <BsBuilding style={{ width: '4rem', height: '4rem', marginRight: '1rem' }} />*/}
                  {/*      </>*/}
                  {/*    </IconContext.Provider>*/}
                  {/*    <div style={{ flex: '1' }}>*/}
                  {/*      <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Works at</h5>*/}
                  {/*      <p style={{ marginBottom: '0 !important' }}>GHTK isc</p>*/}
                  {/*    </div>*/}
                  {/*  </div>*/}
                  {/*</Col>*/}
                  <Col span={8}>
                    <div className={styles.cardContentItem}>
                      <IconContext.Provider value={{ color: '#fa8c16', size: '1rem', marginRight: '1rem' }}>
                        <>
                          <FaBirthdayCake style={{ width: '4rem', height: '4rem', marginRight: '1rem' }} />
                        </>
                      </IconContext.Provider>
                      <div style={{ flex: '1' }}>
                        <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Birthday</h5>
                        <p style={{ marginBottom: '0 !important' }}>{user?.birthday}</p>
                      </div>
                    </div>
                  </Col>
                  <Col span={8}>
                    <div className={styles.cardContentItem}>
                      <IconContext.Provider value={{ color: '#fa8c16', size: '1rem', marginRight: '1rem' }}>
                        <>
                          <AiOutlineFieldNumber style={{ width: '4rem', height: '4rem', marginRight: '1rem' }} />
                        </>
                      </IconContext.Provider>
                      <div style={{ flex: '1' }}>
                        <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Roll Number</h5>
                        <p style={{ marginBottom: '0 !important' }}>{user?.rollNumber}</p>
                      </div>
                    </div>
                  </Col>
                  <Col span={8}>
                    <div className={styles.cardContentItem}>
                      <IconContext.Provider value={{ color: '#fa8c16', size: '1rem', marginRight: '1rem' }}>
                        <>
                          <GiAstronautHelmet style={{ width: '4rem', height: '4rem', marginRight: '1rem' }} />
                        </>
                      </IconContext.Provider>
                      <div style={{ flex: '1' }}>
                        <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Role</h5>
                        <p style={{ marginBottom: '0 !important' }}>{user?.role}</p>
                      </div>
                    </div>
                  </Col>
                  <Col span={8}>
                    <div className={styles.cardContentItem}>
                      <IconContext.Provider value={{ color: '#fa8c16', size: '1rem', marginRight: '1rem' }}>
                        <>
                          <VscLayersActive style={{ width: '4rem', height: '4rem', marginRight: '1rem' }} />
                        </>
                      </IconContext.Provider>
                      <div style={{ flex: '1' }}>
                        <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Status</h5>
                        <p style={{ marginBottom: '0 !important' }}>{user?.status}</p>
                      </div>
                    </div>
                  </Col>
                  <Col span={8}>
                    <div className={styles.cardContentItem}>
                      <IconContext.Provider value={{ color: '#fa8c16', size: '1rem', marginRight: '1rem' }}>
                        <>
                          <BsFillPersonFill style={{ width: '4rem', height: '4rem', marginRight: '1rem' }} />
                        </>
                      </IconContext.Provider>
                      <div style={{ flex: '1' }}>
                        <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Gender</h5>
                        <p style={{ marginBottom: '0 !important', fontSize: '12px' }}>{user?.gender}</p>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col span={8}>
              <Card title={'CONTACT'} bordered={true} className={styles.cardWidget}>
                <Row className={styles.cardContactHolder}>
                  <div>
                    <div className={styles.cardContentItem}>
                      <IconContext.Provider value={{ color: '#8C8C8C', size: '1rem', marginRight: '1rem' }}>
                        <>
                          <MdOutlineMailOutline style={{ width: '2.5rem', height: '2.5rem', marginRight: '1rem' }} />
                        </>
                      </IconContext.Provider>
                      <div style={{ flex: '1' }}>
                        <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Email</h5>
                        <p style={{ marginBottom: '0 !important' }}>
                          <span className={styles.link}>{user?.email}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div className={styles.cardContentItem}>
                      <IconContext.Provider value={{ color: '#8C8C8C', size: '1rem', marginRight: '1rem' }}>
                        <>
                          <BsTelephoneInbound style={{ width: '2.5rem', height: '2.5rem', marginRight: '1rem' }} />
                        </>
                      </IconContext.Provider>
                      <div style={{ flex: '1' }}>
                        <h5 style={{ marginBottom: '0.25rem', color: 'grey' }}>Phone</h5>
                        <p style={{ marginBottom: '0 !important' }}>
                          <span>{user?.tel}</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  {/*<div>*/}
                  {/*  <div className={styles.cardContentItem}>*/}
                  {/*    <IconContext.Provider value={{color: "#8C8C8C", size: "1rem", marginRight: "1rem"}}>*/}
                  {/*      <>*/}
                  {/*        <BsFacebook/>*/}
                  {/*      </>*/}
                  {/*    </IconContext.Provider>*/}
                  {/*    <div style={{flex: "1"}}>*/}
                  {/*      <h5 style={{marginBottom: "0.25rem", color: "grey"}}>Facebook link</h5>*/}
                  {/*      <p style={{marginBottom: "0 !important"}}>*/}
                  {/*        <span className={styles.link}>{user.facebookLink}</span>*/}
                  {/*      </p>*/}
                  {/*    </div>*/}
                  {/*  </div>*/}
                  {/*</div>*/}
                </Row>
              </Card>
            </Col>
            {/*<Col span={24} style={{ marginTop: '2rem' }}>*/}
            {/*  <Card title={'Projects'} bordered={true} className={styles.cardWidget}>*/}
            {/*    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae justo quis nibh dapibus*/}
            {/*    convallis ut sit amet enim. Aenean commodo maximus velit, ornare dignissim lorem fringilla id.*/}
            {/*    Suspendisse consequat, neque id aliquam lacinia, felis velit rhoncus ex, ac imperdiet neque leo sed*/}
            {/*    urna. Curabitur sagittis at augue eu vulputate. Cras interdum, lorem eget mollis rutrum, lectus dolor*/}
            {/*    pulvinar nisl, nec porta elit nulla eget eros. Cras vestibulum felis sed ante porttitor, ut finibus mi*/}
            {/*    congue. Proin pharetra blandit quam tristique tincidunt. Nullam sollicitudin sagittis rutrum. Nam a*/}
            {/*    fringilla sapien. Maecenas quis diam vel risus vestibulum faucibus rhoncus id mauris. Aenean euismod, mi*/}
            {/*    vitae facilisis semper, ante diam rhoncus metus, cursus ullamcorper urna dolor eu dolor. Proin nec nunc*/}
            {/*    pretium, porttitor magna et, vulputate augue. Orci varius natoque penatibus et magnis dis parturient*/}
            {/*    montes, nascetur ridiculus mus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per*/}
            {/*    inceptos himenaeos. Quisque et sem neque. Nunc lacinia tempus laoreet. Donec ligula metus, ultrices id*/}
            {/*    lectus nec, molestie laoreet nibh. Proin vitae quam aliquet, euismod diam nec, viverra enim. Phasellus*/}
            {/*    tristique vitae felis sit amet aliquam. Aliquam et arcu aliquam, feugiat ex id, commodo turpis. Etiam in*/}
            {/*    quam nec ipsum finibus aliquet in id lorem. Ut ac libero convallis massa tincidunt viverra quis at diam.*/}
            {/*    In eget viverra massa, eget lobortis nibh. Proin velit lacus, vehicula eu augue eu, sagittis maximus*/}
            {/*    felis. Pellentesque arcu elit, vulputate nec pretium at, faucibus at nisl. Suspendisse in nibh ac velit*/}
            {/*    venenatis vulputate ut sodales eros. Integer accumsan sed nulla at dapibus. Cras eget facilisis augue,*/}
            {/*    vitae dignissim purus. Nunc vitae suscipit eros. Integer ut mattis nisi. Pellentesque euismod ipsum in*/}
            {/*    turpis vehicula fermentum. Donec sagittis faucibus purus sed finibus. Lorem ipsum dolor sit amet,*/}
            {/*    consectetur adipiscing elit. Ut fringilla sed tellus id dignissim. Quisque vitae vehicula purus. Integer*/}
            {/*    elit enim, laoreet eu fermentum id, imperdiet sit amet felis. Maecenas eu dui viverra lectus semper*/}
            {/*    elementum quis quis mi. Mauris blandit tortor at sem ornare vestibulum. Donec ipsum metus, dapibus nec*/}
            {/*    odio in, finibus posuere turpis. Pellentesque facilisis lorem vitae vestibulum mollis. Mauris mattis*/}
            {/*    sollicitudin iaculis. Vestibulum ut leo ac diam consectetur eleifend. Morbi at dolor ac velit sagittis*/}
            {/*    pulvinar sit amet et justo. Nulla facilisi. Fusce sed libero ut nulla volutpat pellentesque sed in enim.*/}
            {/*    Duis malesuada vitae dolor vel venenatis. Vivamus porttitor dui sed lacus egestas, ut pretium ante*/}
            {/*    sagittis. Phasellus mattis pellentesque laoreet. Nullam vitae turpis nibh.*/}
            {/*  </Card>*/}
            {/*</Col>*/}
          </Row>
        </div>
        <ModalEditUserProfile
          getUserProfile={getUserProfile}
          isShowModal={isShowModalEdit}
          user={user}
          isHideModal={() => setIsShowModalEdit(false)}
        />
        <ModalChangePassword
          setIsShowModalChangePass={setIsShowModalChangePass}
          isShowModalChangePass={isShowModalChangePass}
          handleChangePassword={handleChangePassword}
          setStateChangePass={setStateChangePass}
          buttonLoading={buttonLoading}
          stateChangePass={stateChangePass}
        />
      </Spin>
    </>
  );
}
