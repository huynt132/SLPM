import { Modal, Row, Col, DatePicker, DatePickerProps, Input, InputNumber } from 'antd';
import React, { useEffect, useState } from 'react';
import { unwrapResult } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import PropsTypes from 'prop-types';
import moment from 'moment';
import { showMessage } from 'src/utils/commonHelper';
import { editUserProfile } from '../../Profile.slice';
import { isEmpty, formatDate } from 'src/utils/helper';
import ModalMessage from 'src/components/ModalMessage/ModalMessage';

export default function ModalEditUserProfile({ isShowModal, isHideModal, user, getUserProfile }) {
  const dispatch = useDispatch();

  const userRole = useSelector(state => state.auth.role);

  const [loading, setLoading] = useState(false);
  const [updateUser, setUpdateUser] = useState({});
  //modal message show state
  const [isShowModalMes, setIsShowModalMes] = useState(false);
  // modal message state
  const [modalText, setModalText] = useState('');
  // modal icon state
  const [modalIcon, setModalIcon] = useState('');
  useEffect(() => {
    setUpdateUser({
      id: user?.id,
      email: user?.email,
      fullName: user?.fullName,
      birthday: user?.birthday,
      tel: user?.tel,
      address: user?.address,
      rollNumber: user?.rollNumber,
      schoolName: user?.schoolName,
      status: user?.status,
      facebookLink: user?.facebookLink,
      gitLabToken: user?.gitLabToken,
      linkedinLink: user?.linkedinLink,
      roleId: user?.roleId
    });
  }, [isShowModal]);

  async function handleEditUser() {
    try {
      setLoading(true);
      // let newUserData = {...updateUser, birthday: formatDate(updateUser.birthday)};
      const res = await dispatch(editUserProfile(updateUser));
      const result = unwrapResult(res);
      if (result.data.success) {
        showMessage(result.data.message);
        setUpdateUser(result.data.data);
        getUserProfile({ id: user?.id });
      }
      isHideModal();
    } catch (error) {
      // showMessage(error.payload.data.message || 'An error occurred while update user information!', 'fail');
      setModalIcon('fail');
      setModalText(error.data.message || error.data.error);
      setIsShowModalMes(true);
    } finally {
      setLoading(false);
    }
  }

  return (
    <>
      <ModalMessage
        isShowModal={isShowModalMes}
        isHideModal={() => setIsShowModalMes(false)}
        text={modalText}
        iconname={modalIcon}
      />
      <Modal
        afterClose={() => {
          setUpdateUser({});
        }}
        confirmLoading={loading}
        visible={isShowModal}
        onOk={() => handleEditUser()}
        onCancel={isHideModal}
        okText="Update"
        cancelText="Cancel"
        title="Update user information"
        // centered
      >
        <Row className="mb-1" justify="space-between">
          <Col span={16}>
            <label>Full name:</label>
            <Input
              className={'br-8'}
              type="text"
              name="fullName"
              placeholder="Input Full Name"
              onChange={e =>
                setUpdateUser(prev => {
                  return { ...prev, fullName: e.target.value };
                })
              }
              value={updateUser?.fullName}
            />
          </Col>
          <Col span={7}>
            <label>Roll number:</label>
            <Input
              disabled={true}
              className={'br-8'}
              type="text"
              name="rollNumber"
              placeholder="Input Roll number"
              onChange={e =>
                setUpdateUser(prev => {
                  return { ...prev, rollNumber: e.target.value };
                })
              }
              value={updateUser?.rollNumber}
            />
          </Col>
        </Row>
        <Row className="mb-1">
          <Col span={24}>
            <label>Address:</label>
            <Input
              className={'br-8'}
              type="text"
              name="address"
              placeholder="Input Address"
              onChange={e =>
                setUpdateUser(prev => {
                  return { ...prev, address: e.target.value };
                })
              }
              value={updateUser?.address}
            />
          </Col>
        </Row>
        <Row justify="space-between" className="mb-1">
          <Col span={11}>
            <label>Phone number:</label>
            <Input
              className={'br-8'}
              style={{ display: 'block', width: '220px' }}
              name="tel"
              controls={false}
              placeholder="Input Phone number"
              onChange={e =>
                setUpdateUser(prev => {
                  return { ...prev, tel: e.target.value };
                })
              }
              value={updateUser?.tel}
            />
          </Col>
          <Col span={11}>
            <label>Birthday:</label>
            <DatePicker
              className={'br-8'}
              value={isEmpty(updateUser?.birthday) ? null : moment(updateUser.birthday, 'DD-MM-YYYY')}
              format="DD-MM-YYYY"
              placeholder="Select Birthday"
              locale="vi"
              onChange={(date, dateString) => {
                setUpdateUser(prev => {
                  return { ...prev, birthday: dateString };
                });
              }}
              disabledDate={current => {
                return current > moment();
              }}
            />
          </Col>
        </Row>
        <Row className="mb-1">
          <Col span={24}>
            <label>Email</label>
            <Input
              // name="gitlabId"
              className={'br-8'}
              placeholder="Input Email"
              value={updateUser?.email}
              disabled={true}
            />
          </Col>
        </Row>
        <Row className="mb-1">
          <Col span={11} style={{ marginRight: '3.8rem' }}>
            <label>Role</label>
            <Input
              // name="gitlabId"
              className={'br-8'}
              placeholder="Role"
              value={updateUser?.role}
              disabled={true}
            />
          </Col>
          <Col span={11}>
            <label>Status</label>
            <Input
              // name="gitlabId"
              className={'br-8'}
              placeholder="Status"
              value={updateUser?.status}
              disabled={true}
            />
          </Col>
        </Row>
        {/* {userRole === 'Trainer' && ( */}
        <Row className="mb-1">
          <Col span={24}>
            <label>Gitlab Token</label>
            <Input.Password
              className={'br-8'}
              name="gitlabId"
              placeholder="InputGitlab ID"
              onChange={e =>
                setUpdateUser(prev => {
                  return { ...prev, gitLabToken: e.target.value };
                })
              }
              value={updateUser?.gitLabToken}
            />
          </Col>
        </Row>
        {/* )} */}
      </Modal>
    </>
  );
}

ModalEditUserProfile.prototype = {
  isShowModal: PropsTypes.bool,
  isHideModal: PropsTypes.func,
  user: PropsTypes.object,
  getUserProfile: PropsTypes.func
};

ModalEditUserProfile.defaultProps = {
  isShowModal: false
};
