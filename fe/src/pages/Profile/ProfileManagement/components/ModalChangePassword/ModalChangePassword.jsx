import { Input, Modal } from 'antd';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './ModalAdd.module.css';

export default function ModalChangePassword({
  isShowModalChangePass,
  setIsShowModalChangePass,
  setStateChangePass,
  buttonLoading,
  handleChangePassword,
  stateChangePassword
}) {
  return (
    <Modal
      title="Change Password"
      visible={isShowModalChangePass}
      onOk={handleChangePassword}
      okText={'Change'}
      onCancel={() => {
        setIsShowModalChangePass(false);
      }}
      okButtonProps={{ loading: buttonLoading }}
    >
      <div className={styles.rowModal}>
        <h4 className={styles.rowText}>Old Password</h4>
        <Input
          type={'password'}
          value={stateChangePassword?.oldPassword}
          onChange={e =>
            setStateChangePass(prevState => {
              return {
                ...prevState,
                oldPassword: e.target.value
              };
            })
          }
        />
      </div>
      <div className={styles.rowModal}>
        <h4 className={styles.rowText}>New Password</h4>
        <Input
          type={'password'}
          value={stateChangePassword?.newPassword}
          onChange={e =>
            setStateChangePass(prevState => {
              return {
                ...prevState,
                newPassword: e.target.value
              };
            })
          }
        />
      </div>
      <div className={styles.rowModal}>
        <h4 className={styles.rowText}>Confirm New Password</h4>
        <Input
          type={'password'}
          value={stateChangePassword?.comfirmNewPassword}
          onChange={e =>
            setStateChangePass(prevState => {
              return {
                ...prevState,
                comfirmNewPassword: e.target.value
              };
            })
          }
        />
      </div>
    </Modal>
  );
}

ModalChangePassword.propTypes = {
  isShowModalChangePass: PropTypes.bool,
  buttonLoading: PropTypes.bool,
  setIsShowModalChangePass: PropTypes.func,
  setStateChangePass: PropTypes.func,
  handleChangePassword: PropTypes.func,
  stateChangePassword: PropTypes.object
};
