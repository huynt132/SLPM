import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import profileManagementApi from 'src/api/profileManagement.api';
import { payloadCreator } from 'src/utils/helper';

export const editUserProfile = createAsyncThunk('users/edit', payloadCreator(profileManagementApi.editProfile));

export const changePassword = createAsyncThunk(
  'users/changePassword',
  payloadCreator(profileManagementApi.changePassword)
);

const profileManagement = createSlice({
  name: 'profileManagement',
  initialState: {
    user: {}
  },
  reducers: {
    //dành cho các action k cần gọi api (ví dụ logout)
  },
  extraReducers: {
    //dành cho các action có gọi api
    [editUserProfile.fulfilled]: (state, action) => {
      state.user = action.payload.data.data;
    }
  }
});

const profileManagementReducer = profileManagement.reducer;

export default profileManagementReducer;
