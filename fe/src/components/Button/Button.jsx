import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Button.module.css';
import { AiOutlineRight } from 'react-icons/ai';
import PropsTypes from 'prop-types';

export default function Button({ text, link }) {
  return (
    <Link to={link} className={styles.settingButton}>
      <h3 className={styles.settingButtonText}>{text}</h3>
      <div className={styles.settingButtonIcon}>
        <AiOutlineRight />
      </div>
    </Link>
  );
}

Button.PropsTypes = {
  text: PropsTypes.string,
  link: PropsTypes.string
};
