import { Modal } from 'antd';
import React from 'react';
import { AiFillCheckCircle, AiFillCloseCircle } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import PropsTypes from 'prop-types';
import styles from './ModalMessage.module.css';
import { isEmpty } from '../../utils/helper';
import Button from '../Button/Button';
import { useDispatch } from 'react-redux';
import { logout } from 'src/pages/Auth/auth.slice';
import { path } from 'src/constants/path';

export default function ModalMessage({ isShowModal, isHideModal, navigateTo, text, iconname, type }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const handleOnOk = () => {
    isHideModal();
    switch (type) {
      case 'tokenExpired':
        dispatch(logout());
        navigate(path.login);
        window.location.reload();
        break;
      default:
        if (!isEmpty(navigateTo)) navigate(navigateTo);
    }
  };

  const getIconFromName = name => {
    switch (name) {
      case 'success':
        return (
          <div className={styles.modalCircle}>
            <AiFillCheckCircle className={styles.modalIcon} />
          </div>
        );
      case 'fail':
        return (
          <div className={styles.modalCircleFail}>
            <AiFillCloseCircle className={styles.modalIconFail} />
          </div>
        );
      default:
        return '';
    }
  };
  return (
    <Modal
      visible={isShowModal}
      onOk={() => handleOnOk()}
      onCancel={isHideModal}
      // okText="OK"
      // cancelText="Cancel"
      centered
      zIndex={9999}
      cancelButtonProps={{ style: { display: 'none' } }}
    >
      <div className={styles.modalContent}>
        {getIconFromName(iconname)}
        <h2 className={styles.modalText}>{text}</h2>
      </div>
    </Modal>
  );
}

ModalMessage.prototype = {
  isShowModal: PropsTypes.boolean,
  isHideModal: PropsTypes.func,
  navigateTo: PropsTypes.string,
  text: PropsTypes.string,
  iconname: PropsTypes.string,
  type: PropsTypes.string
};
