import React from 'react';
import * as mui from 'react-icons/md';
import { Link } from 'react-router-dom';
import { path } from 'src/constants/path';

// const getItem = React.memo(() => Ơ)

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type
  };
}

export const SidebarDataAdmin = [
  {
    key: 'system-administration',
    icon: React.createElement(mui.MdAdminPanelSettings),
    label: <span>System Administration</span>,
    children: [
      getItem(<Link to={path.settingList}>System Settings</Link>, '1'),
      getItem(<Link to={path.userManagement}>User Management</Link>, '2'),
      getItem(<Link to={path.subjectManagement}>Subject Management</Link>, '3')
    ]
  },
  {
    key: 'training-subjects',
    icon: React.createElement(mui.MdSubject),
    label: <span>Training Subjects</span>,
    children: [
      getItem(<Link to={path.subjectSetting}>Subject Settings</Link>, '4'),
      getItem(<Link to={path.iteration}>Iteration Management</Link>, '5'),
      getItem(<Link to={path.evaluationCriteriaManagement}>Evaluation Criteria</Link>, '6'),
      getItem(<Link to={path.classList}>Class Management</Link>, '7')
    ]
  },
  {
    key: 'class-configuration',
    icon: React.createElement(mui.MdOutlineClass),
    label: <span>Class Configuration</span>,
    children: [
      getItem(<Link to={path.classSetting}>Class Settings</Link>, '8'),
      getItem(<Link to={path.milestone}>Milestone Management</Link>, '14'),
      getItem(<Link to={path.projectManagement}>Team Management</Link>, '12'),
      getItem(<Link to={path.classUser}>Class Students</Link>, '11')
    ]
  },
  {
    key: 'project-management',
    icon: React.createElement(mui.MdCardTravel),
    label: <span>Project Management</span>,
    children: [
      getItem(<Link to={path.milestoneSubmit}>Milestone Submits</Link>, '13'),
      // getItem(<Link to={path.featureManagement}>Feature Management</Link>, '15'),
      getItem(<Link to={path.functionManagement}>Function Management</Link>, '16'),
      getItem(<Link to={path.trackingManagement}>Tracking Management</Link>, '17')
    ]
  }
];

export const SidebarDataAuthor = [
  {
    key: 'system-administration',
    icon: React.createElement(mui.MdAdminPanelSettings),
    label: <span>System Administration</span>,
    children: [getItem(<Link to={path.subjectManagement}>Subject Management</Link>, '3')]
  },
  {
    key: 'training-subjects',
    icon: React.createElement(mui.MdSubject),
    label: <span>Training Subjects</span>,
    children: [
      getItem(<Link to={path.subjectSetting}>Subject Settings</Link>, '4'),
      getItem(<Link to={path.iteration}>Iteration Management</Link>, '5'),
      getItem(<Link to={path.evaluationCriteriaManagement}>Evaluation Criteria</Link>, '6'),
      getItem(<Link to={path.classList}>Class Management</Link>, '7')
    ]
  },
  {
    key: 'class-configuration',
    icon: React.createElement(mui.MdOutlineClass),
    label: <span>Class Configuration</span>,
    children: [
      getItem(<Link to={path.classSetting}>Class Settings</Link>, '8'),
      getItem(<Link to={path.milestone}>Milestone Management</Link>, '14'),
      getItem(<Link to={path.projectManagement}>Team Management</Link>, '12'),
      getItem(<Link to={path.classUser}>Class Students</Link>, '11')
    ]
  },
  {
    key: 'project-management',
    icon: React.createElement(mui.MdCardTravel),
    label: <span>Project Management</span>,
    children: [
      getItem(<Link to={path.milestoneSubmit}>Milestone Submits</Link>, '13'),
      // getItem(<Link to={path.featureManagement}>Feature Management</Link>, '15'),
      getItem(<Link to={path.functionManagement}>Function Management</Link>, '16'),
      getItem(<Link to={path.trackingManagement}>Tracking Management</Link>, '17')
    ]
  }
];

export const SidebarDataTrainer = [
  {
    key: 'training-subjects',
    icon: React.createElement(mui.MdSubject),
    label: <span>Training Subjects</span>,
    children: [
      getItem(<Link to={path.subjectSetting}>Subject Settings</Link>, '4'),
      getItem(<Link to={path.iteration}>Iteration Management</Link>, '5'),
      getItem(<Link to={path.evaluationCriteriaManagement}>Evaluation Criteria</Link>, '6'),
      getItem(<Link to={path.classList}>Class Management</Link>, '7')
    ]
  },
  {
    key: 'class-configuration',
    icon: React.createElement(mui.MdOutlineClass),
    label: <span>Class Configuration</span>,
    children: [
      getItem(<Link to={path.classSetting}>Class Settings</Link>, '8'),
      getItem(<Link to={path.milestone}>Milestone Management</Link>, '14'),
      getItem(<Link to={path.projectManagement}>Team Management</Link>, '12'),
      getItem(<Link to={path.classUser}>Class Students</Link>, '11')
    ]
  },
  {
    key: 'project-management',
    icon: React.createElement(mui.MdCardTravel),
    label: <span>Project Management</span>,
    children: [
      getItem(<Link to={path.milestoneSubmit}>Milestone Submits</Link>, '13'),
      // getItem(<Link to={path.featureManagement}>Feature Management</Link>, '15'),
      getItem(<Link to={path.functionManagement}>Function Management</Link>, '16'),
      getItem(<Link to={path.trackingManagement}>Tracking Management</Link>, '17')
    ]
  }
];

export const SidebarDataStudent = [
  {
    key: 'training-subjects',
    icon: React.createElement(mui.MdSubject),
    label: <span>Training Subjects</span>,
    children: [getItem(<Link to={path.classList}>Class Management</Link>, '7')]
  },
  {
    key: 'class-configuration',
    icon: React.createElement(mui.MdOutlineClass),
    label: <span>Class Configuration</span>,
    children: [
      getItem(<Link to={path.classSetting}>Class Settings</Link>, '8'),
      getItem(<Link to={path.milestone}>Milestone Management</Link>, '14'),
      getItem(<Link to={path.projectManagement}>Team Management</Link>, '12'),
      getItem(<Link to={path.classUser}>Class Students</Link>, '11')
    ]
  },
  {
    key: 'project-management',
    icon: React.createElement(mui.MdCardTravel),
    label: <span>Project Management</span>,
    children: [
      getItem(<Link to={path.milestoneSubmit}>Milestone Submits</Link>, '13'),
      // getItem(<Link to={path.featureManagement}>Feature Management</Link>, '15'),
      getItem(<Link to={path.functionManagement}>Function Management</Link>, '16'),
      getItem(<Link to={path.trackingManagement}>Tracking Management</Link>, '17')
    ]
  }
];
