import styled from 'styled-components';
export const Logo = styled.div`
  width: 120px;
  height: 31px;
  cursor: pointer;
  margin-bottom: 80px;
  margin-left: 55px;
  & svg {
    width: 190px;
    height: 130px;
  }
`;
export const SidebarBottom = styled.div`
  margin: 40rem auto 0;
  background-color: #fff;
  width: 10rem;
  height: 4rem;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 2rem;
  cursor: pointer;
  transition: all 0.3s ease;
  &:hover {
    color: #fff !important;
    background-color: #0c9fb3;
  }
  & svg {
    height: 2rem;
    width: 5rem;
  }
`;
