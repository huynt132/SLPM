import React from 'react';
import { Navigate } from 'react-router-dom';
import { path } from 'src/constants/path';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

export default function Authorization({ children, authorType }) {
  const role = useSelector(state => state.auth.role);

  switch (authorType) {
    case 'admin':
      if (role.toUpperCase() !== 'ADMIN') {
        return <Navigate to={path.errorForbidden} />;
      }
      break;
    case 'adminAndAuthor':
      if (role.toUpperCase() !== 'ADMIN' && role.toUpperCase() !== 'AUTHOR') {
        return <Navigate to={path.errorForbidden} />;
      }
      break;
    case 'withoutStudent':
      if (role.toUpperCase() === 'STUDENT') {
        return <Navigate to={path.errorForbidden} />;
      }
      break;
    default:
      break;
  }

  return <>{children}</>;
}

Authorization.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};
