import React, { useEffect, useState } from 'react';
import { Avatar, Button, Image, Menu } from 'antd';
import {
  HeaderButton,
  HeaderItem,
  HeaderRight,
  HeaderText,
  HeaderSearch,
  HeaderChoice,
  HeaderSearchInput,
  HeaderSearchIcon,
  HeaderChoiceItem1,
  HeaderChoiceItem2,
  HeaderChoiceItem3,
  PopoverInfo,
  PopoverAvatar,
  PopoverText,
  PopoverName,
  PopoverRole,
  PopoverLink
} from './header.style';
import { Link, useNavigate } from 'react-router-dom';
import { path } from 'src/constants/path';
import { HeaderDataHome } from './utils/HeaderDataHome';
import useAuthenticated from 'src/hooks/useAuthenticated';
import usePopover from 'src/hooks/usePopover';
import { useDispatch, useSelector } from 'react-redux';
import { BiSupport, BiSearch } from 'react-icons/bi';
import { AiFillSetting, AiOutlineDashboard, AiOutlineLogout } from 'react-icons/ai';
import Popover from 'src/components/Popover/Popover';
import { logout } from 'src/pages/Auth/auth.slice';
import { getUserDetail } from 'src/pages/Dashboard/ClassDashboard/UserDashboard/userList.slice';
import { unwrapResult } from '@reduxjs/toolkit';
import avatar from 'src/assets/images/avatardefault.png';

export default function Headers() {
  const authenticated = useAuthenticated();
  const navigate = useNavigate();
  const userRole = useSelector(state => state.auth.role);
  const user = useSelector(state => state.auth.user);
  const userInfor = useSelector(state => state.userListManagement.userInfo);
  const [userInfo, setUserInfo] = useState(null);

  const { activePopover, showPopover, hidePopover } = usePopover();
  const dispatch = useDispatch();

  const handleLogout = () => {
    // logout();
    dispatch(logout());
    window.location.reload();
  };

  const getUserProfile = async params => {
    try {
      const res = await dispatch(getUserDetail({ params }));
      const result = unwrapResult(res);
      setUserInfo(result.data.data);
    } catch (error) {}
  };

  useEffect(() => {
    getUserProfile({ id: user.id });
  }, [userInfor?.avatarLink, userInfor?.fullName]);

  return (
    <>
      {authenticated && (
        <HeaderItem>
          <HeaderText>Welcome, {userInfo?.fullName.toUpperCase()}!</HeaderText>
          <HeaderRight>
            {/* <HeaderSearch>
              <HeaderSearchInput type="text" placeholder="Search any keywords" name="search" />
              <HeaderSearchIcon>
                <BiSearch />
              </HeaderSearchIcon>
            </HeaderSearch> */}
            <HeaderChoice>
              {/* <HeaderChoiceItem1>
                <BiSupport />
              </HeaderChoiceItem1>
              <HeaderChoiceItem2>
                <AiFillSetting />
              </HeaderChoiceItem2> */}
              <HeaderChoiceItem3 onMouseEnter={showPopover} onMouseLeave={hidePopover}>
                <Avatar
                  style={{
                    position: 'relative',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    color: '#f56a00',
                    backgroundColor: '#fde3cf',
                    width: '36px',
                    height: '36px',
                    cursor: 'pointer'
                  }}
                  src={
                    <Image
                      src={userInfo?.avatarLink ? userInfo.avatarLink : avatar}
                      style={{
                        width: 32
                      }}
                    />
                  }
                ></Avatar>
                <Popover active={activePopover}>
                  <PopoverInfo>
                    <PopoverAvatar>
                      <Avatar
                        style={{
                          position: 'relative',
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center',
                          color: '#f56a00',
                          backgroundColor: '#fde3cf',
                          width: '60px',
                          height: '60px',
                          cursor: 'pointer'
                        }}
                        src={
                          <Image
                            src={userInfo?.avatarLink ? userInfo.avatarLink : avatar}
                            style={{
                              width: 60
                            }}
                          />
                        }
                      ></Avatar>
                    </PopoverAvatar>
                    <PopoverText>
                      <PopoverName>{user.fullName.toUpperCase()}</PopoverName>
                      <PopoverRole>{userRole.toUpperCase()}</PopoverRole>
                    </PopoverText>
                  </PopoverInfo>
                  <Link to={path.classDash} style={{ color: '#000', fontWeight: 'bold' }}>
                    <PopoverLink>
                      <AiOutlineDashboard />
                      Dashboard
                    </PopoverLink>
                  </Link>
                  <Link to={path.profileDash} style={{ color: '#000', fontWeight: 'bold' }}>
                    <PopoverLink>
                      <AiFillSetting />
                      Profile
                    </PopoverLink>
                  </Link>
                  <PopoverLink onClick={handleLogout} style={{ color: '#000', fontWeight: 'bold' }}>
                    <AiOutlineLogout />
                    <span>Logout</span>
                  </PopoverLink>
                </Popover>
              </HeaderChoiceItem3>
            </HeaderChoice>
          </HeaderRight>
        </HeaderItem>
      )}
      {!authenticated && (
        <HeaderItem>
          <Menu
            style={{ height: '100%', backgroundColor: '#fff', border: 'none' }}
            theme="light"
            mode="horizontal"
            defaultSelectedKeys={['']}
            items={HeaderDataHome}
          />
          <HeaderButton>
            <Button type="primary" size="large" htmlType="submit" onClick={() => navigate(path.login)}>
              Login
            </Button>
            <Button type="primary" size="large" htmlType="submit" onClick={() => navigate(path.register)}>
              Register
            </Button>
          </HeaderButton>
        </HeaderItem>
      )}
    </>
  );
}
