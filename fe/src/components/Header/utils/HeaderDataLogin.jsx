import { Link } from 'react-router-dom';
import { path } from 'src/constants/path';

export const HeaderDataLogin = [
  {
    key: '1',
    label: <Link to={path.classDash}>Dashboard</Link>
  },
  {
    key: '2',
    label: <Link to={path.settingList}>Setting List</Link>
  }
];
