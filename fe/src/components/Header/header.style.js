import styled from 'styled-components';

export const HeaderItem = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-item: center;
  height: 100%;
  & ul {
    font-weight: 500;
    & li {
    }
  }
`;

export const HeaderButton = styled.div`
  & button {
    margin-left: 2rem;
    border-radius: 8px;
    box-shadow: 0 3px 5px 0 rgb(57 108 240 / 30%);
  }
  & button:first-child {
    background: ##1890ff;
    border-color: ##1890ff;
    &:hover {
      background: #fff;
      color: #000;
    }
  }
  & button:last-child {
    background: #03ac5a;
    border-color: #03ac5a;
    &:hover {
      background: #fff;
      color: #000;
    }
  }
`;

export const HeaderText = styled.h2`
  margin: auto 0;
`;

export const HeaderRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const HeaderSearch = styled.form`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const HeaderSearchInput = styled.input`
  height: 45px;
  border: 1px solid #eee;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  border-right: none;
  padding: 5px 25px;
`;

export const HeaderSearchIcon = styled.button`
  width: 45px;
  height: 45px;
  border: 1px solid #eee;
  border-left: none;
  border-bottom-right-radius: 20px;
  border-top-right-radius: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fff;
  & svg {
    width: 20px;
    height: 20px;
  }
`;

export const HeaderChoice = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
  margin-left: 5rem;
  width: 12rem;
`;

export const HeaderChoiceItem1 = styled.div`
  width: 36px;
  height: 36px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: rgba(57, 108, 240, 0.1);
  border: 1px solid rgba(57, 108, 240, 0.1);
  color: #396cf0;
  border-radius: 50%;
  box-shadow: 0 3px 5px 0 rgb(57 108 240 / 30%);
  cursor: pointer;
  transition: 0.5s all ease;
  & svg {
    width: 16px;
    height: 16px;
  }
  &:hover {
    background-color: #396cf0;
    color: #fff;
  }
`;
export const HeaderChoiceItem2 = styled.div`
  width: 36px;
  height: 36px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: rgba(57, 108, 240, 0.1);
  border: 1px solid rgba(57, 108, 240, 0.1);
  color: #396cf0;
  border-radius: 50%;
  box-shadow: 0 3px 5px 0 rgb(57 108 240 / 30%);
  cursor: pointer;
  transition: 0.5s all ease;
  & svg {
    width: 16px;
    height: 16px;
  }
  &:hover {
    background-color: #396cf0;
    color: #fff;
  }
`;
export const HeaderChoiceItem3 = styled.div`
  // width: 30%;
`;

// Popover styled

export const PopoverInfo = styled.div`
  line-height: initial !important;
  display: flex;
  align-items: start;
  justify-content: start;
  padding: 10px 10px;
`;

export const PopoverAvatar = styled.div`
  margin: auto 0;
`;

export const PopoverText = styled.div`
  margin-left: 2rem;
`;

export const PopoverName = styled.h2`
  margin: 0;
  margin-bottom: 5px;
`;

export const PopoverRole = styled.h3`
  margin: 0;
  color: #8492a6;
  font-size: 14px;
`;

export const PopoverLink = styled.div`
  line-height: initial !important;
  padding: 10px 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
  transition: all 0.2s ease;
  cursor: pointer;
  &:hover {
    background-color: #396cf0;
    color: #fff;
    & a {
      font-weight: 600;
    }
    & svg {
      color: #fff;
    }
  }
  & svg {
    width: 16px;
    height: 16px;
    color: #000;
    margin-right: 1.5rem;
  }
  & a {
    width: 100%;
    height: 100%;
    color: #8492a6;
    font-weight: 600;
  }
  &:nth-child(3) {
    border-bottom: 1px solid #eee;
  }
  &:last-child {
    transition: all 0.2s ease;
    & span {
      border: none;
      font-weight: 600;
      width: 100%;
    }
  }
`;
