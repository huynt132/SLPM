import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getAllProject } from 'src/pages/Dashboard/ProjectDashboard/FeatureManagement/featureManagement.slice';

export default function SelectProject({ selectProject, setLoading, setSelectProject, setParamsSearch }) {
  const dispatch = useDispatch();
  const [listProject, setListProject] = useState([]);

  const getAllProjects = async params => {
    setLoading(true);
    const data = await dispatch(getAllProject({ params }));
    const res = unwrapResult(data);
    setListProject(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getAllProjects({
      projectId: null,
      name: null,
      status: null,
      page: 1,
      limit: 8,
      total: 200
    });
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, projectId: value.toString() };
    });
    setSelectProject(value);
  };

  const { Option } = Select;
  const projects = listProject.map(item => {
    return (
      <Option key={item.id} value={item.id}>
        {item.name}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="multiple"
        allowClear
        style={{
          minWidth: '100%'
        }}
        placeholder="Select project"
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectProject}
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
      >
        {projects}
      </Select>
    </>
  );
}

SelectProject.propTypes = {
  selectProject: PropTypes.array,
  setLoading: PropTypes.func,
  setSelectProject: PropTypes.func,
  setParamsSearch: PropTypes.func
};
