import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getAllAuthor } from 'src/pages/Dashboard/SubjectDashboard/SubjectManagement/subjectManagement.slice';

export default function SelectAuthor({ selectAuthor, setLoading, setSelectAuthor, setParamsSearch }) {
  const dispatch = useDispatch();
  const [listAuthor, setListAuthor] = useState([]);

  const getAllAuthors = async params => {
    setLoading(true);
    const data = await dispatch(getAllAuthor({ params }));
    const res = unwrapResult(data);
    setListAuthor(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getAllAuthors('');
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, authorId: value.toString() };
    });
    setSelectAuthor(value);
  };

  const { Option } = Select;
  const authors = listAuthor.map(item => {
    return (
      <Option key={item.id} value={item.id}>
        {item.fullName}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="multiple"
        allowClear
        style={{
          minWidth: '100%'
        }}
        placeholder="Select Author"
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectAuthor}
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
      >
        {authors}
      </Select>
    </>
  );
}

SelectAuthor.propTypes = {
  selectAuthor: PropTypes.array,
  setLoading: PropTypes.func,
  setSelectAuthor: PropTypes.func,
  setParamsSearch: PropTypes.func
};
