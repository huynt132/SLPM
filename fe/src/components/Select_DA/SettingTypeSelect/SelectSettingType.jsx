import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { getSettingType } from 'src/pages/Dashboard/SettingDashboard/SettingListDashboard/settingList.slice';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';

export default function SelectSettingType({
  setLoading,
  setParamsSearch,
  selectedValuesSetting,
  setSelectedValuesSetting
}) {
  const dispatch = useDispatch();
  const [listType, setListType] = useState([]);
  const getAllType = async params => {
    setLoading(true);
    const data = await dispatch(getSettingType({ params }));
    const res = unwrapResult(data);
    setListType(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getAllType('');
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, settingTypeID: value.toString() };
    });
    setSelectedValuesSetting(value);
  };
  const { Option } = Select;
  const setting = listType.map(item => {
    return (
      <Option key={item.id} value={item.id}>
        {item.value}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="multiple"
        allowClear
        style={{
          minWidth: '100%',
          maxWidth: '100%'
        }}
        placeholder="Select setting type"
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectedValuesSetting}
      >
        {setting}
      </Select>
    </>
  );
}

SelectSettingType.propTypes = {
  setLoading: PropTypes.func,
  setParamsSearch: PropTypes.func,
  selectedValuesSetting: PropTypes.array,
  setSelectedValuesSetting: PropTypes.func
};
