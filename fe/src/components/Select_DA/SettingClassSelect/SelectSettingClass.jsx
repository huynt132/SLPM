import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getSettingClass } from 'src/pages/Dashboard/SettingDashboard/ClassSettingDashboard/classSetting.slice';

export default function SelectSettingClass({
  setLoading,
  setParamsSearch,
  selectedValuesSettingClass,
  setSelectedValuesSettingClass
}) {
  const dispatch = useDispatch();
  const [listSettingClass, setListSettingClass] = useState([]);
  const getAllType = async params => {
    setLoading(true);
    const data = await dispatch(getSettingClass({ params }));
    const res = unwrapResult(data);
    setListSettingClass(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getAllType('');
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, typeId: value.toString() };
    });
    setSelectedValuesSettingClass(value);
  };
  const { Option } = Select;
  const setting = listSettingClass.map(item => {
    return (
      <Option key={item.value} value={item.value}>
        {item.label}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="multiple"
        allowClear
        style={{
          minWidth: '100%'
        }}
        placeholder="Select setting class"
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectedValuesSettingClass}
      >
        {setting}
      </Select>
    </>
  );
}

SelectSettingClass.propTypes = {
  setLoading: PropTypes.func,
  setParamsSearch: PropTypes.func,
  selectedValuesSettingClass: PropTypes.array,
  setSelectedValuesSettingClass: PropTypes.func
};
