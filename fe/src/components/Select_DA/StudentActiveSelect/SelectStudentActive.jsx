import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getAllStudentActive } from 'src/pages/Dashboard/ClassDashboard/ClassUserListDashboard/classUser.slice';

export default function SelectStudentActive({
  selectStudentActive,
  setLoading,
  setSelectStudentActive,
  setParamsSearch
}) {
  const dispatch = useDispatch();
  const [listStudent, setListStudent] = useState([]);

  const getAllStudentsActive = async params => {
    setLoading(true);
    const data = await dispatch(getAllStudentActive({ params }));
    const res = unwrapResult(data);
    setListStudent(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    const body = {
      status: 'active',
      role: 'student'
    };
    getAllStudentsActive(body);
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, userId: value.toString() };
    });
    setSelectStudentActive(value);
  };

  const { Option } = Select;
  const students = listStudent.map((item, key) => {
    return (
      <Option key={key} value={item.value}>
        {item.label}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="multiple"
        allowClear
        showSearch
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        style={{
          minWidth: '100%'
        }}
        placeholder="Select Student Active"
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectStudentActive}
      >
        {students}
      </Select>
    </>
  );
}

SelectStudentActive.propTypes = {
  selectTrainer: PropTypes.array,
  setLoading: PropTypes.func,
  setSelectTrainer: PropTypes.func,
  setParamsSearch: PropTypes.func
};
