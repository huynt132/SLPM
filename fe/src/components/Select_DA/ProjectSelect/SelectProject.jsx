import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getProjectCodeAndName } from 'src/pages/Dashboard/ClassDashboard/ClassUserListDashboard/classUser.slice';

export default function SelectProject({
  setLoading,
  setParamsSearch,
  selectedValuesProject,
  setSelectedValuesProject,
  classId,
  selectedValuesClass
}) {
  const [buttonLoading, setButtonLoading] = useState(false);
  const dispatch = useDispatch();
  const [listProject, setListProject] = useState([]);
  const getProjects = async params => {
    setButtonLoading(true);
    const data = await dispatch(getProjectCodeAndName({ params }));
    const res = unwrapResult(data);
    setListProject(res.data.data);
    setButtonLoading(false);
  };

  useEffect(() => {
    let classIdParam;
    if (selectedValuesClass && typeof selectedValuesClass === 'object') {
      classIdParam = selectedValuesClass.join(',');
    } else {
      classIdParam = classId;
    }
    getProjects({ classId: classIdParam });
  }, [classId, selectedValuesClass]);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, projectId: value.toString() };
    });
    setSelectedValuesProject(value);
  };
  const { Option } = Select;
  const setting = listProject.map((item, key) => {
    return (
      <Option key={key} value={item.value}>
        {item.label}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="multiple"
        allowClear
        style={{
          minWidth: '100%',
          maxWidth: '100%'
        }}
        placeholder="Select Team"
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectedValuesProject}
        loading={buttonLoading}
      >
        {setting}
      </Select>
    </>
  );
}

SelectProject.propTypes = {
  setLoading: PropTypes.func,
  setParamsSearch: PropTypes.func,
  selectedValuesProject: PropTypes.array,
  setSelectedValuesProject: PropTypes.func
};
