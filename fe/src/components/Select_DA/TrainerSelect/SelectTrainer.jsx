import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getAllTrainer } from 'src/pages/Dashboard/ClassDashboard/ClassListDashboard/classList.slice';

export default function SelectTrainer({ selectTrainer, setLoading, setSelectTrainer, setParamsSearch }) {
  const dispatch = useDispatch();
  const [listTrainer, setListTrainer] = useState([]);

  const getAllAuthors = async params => {
    setLoading(true);
    const data = await dispatch(getAllTrainer({ params }));
    const res = unwrapResult(data);
    setListTrainer(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getAllAuthors('');
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, trainerId: value.toString() };
    });
    setSelectTrainer(value);
  };

  const { Option } = Select;
  const authors = listTrainer.map(item => {
    return (
      <Option key={item.id} value={item.id}>
        {item.fullName}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="multiple"
        allowClear
        showSearch
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        style={{
          minWidth: '100%'
        }}
        placeholder="Select Trainer"
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectTrainer}
      >
        {authors}
      </Select>
    </>
  );
}

SelectTrainer.propTypes = {
  selectTrainer: PropTypes.array,
  setLoading: PropTypes.func,
  setSelectTrainer: PropTypes.func,
  setParamsSearch: PropTypes.func
};
