import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getAllMilestones } from 'src/pages/Dashboard/SubjectDashboard/MilestoneManagement/milestoneManagement.slice';
import PropTypes from 'prop-types';

export default function SelectMilestone({
  setLoading,
  setParamsSearch,
  selectedValuesMilestone,
  setSelectedValuesMilestone,
  currentData
}) {
  const dispatch = useDispatch();
  const [listClass, setListClass] = useState([]);
  const getAllMilestone = async params => {
    setLoading(true);
    const data = await dispatch(getAllMilestones({ params }));
    const res = unwrapResult(data);
    setListClass(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getAllMilestone('');
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, milestoneId: value };
    });
    setSelectedValuesMilestone(value);
  };
  const { Option } = Select;
  const milestones = listClass.map((item, key) => {
    return (
      <Option key={key} value={item.value}>
        {item.label}
      </Option>
    );
  });

  return (
    <>
      <Select
        // mode="multiple"
        allowClear
        style={{
          minWidth: '100%'
        }}
        placeholder="Select Milestone"
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        onChange={handleSelect}
        className={'border-radius-select'}
        value={typeof selectedValuesMilestone === 'undefined' ? currentData.currentMilestone : selectedValuesMilestone}
      >
        {milestones}
      </Select>
    </>
  );
}

SelectMilestone.propTypes = {
  setLoading: PropTypes.func,
  setParamsSearch: PropTypes.func,
  selectedValuesMilestone: PropTypes.array,
  setSelectedValuesMilestone: PropTypes.func,
  currentData: PropTypes.object
};
