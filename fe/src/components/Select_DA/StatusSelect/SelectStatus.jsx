import React from 'react';
import { Select } from 'antd';
import PropTypes from 'prop-types';

export default function SelectStatus({ numberStatus, setParamsSearch, selectedValuesStatus, setSelectedValuesStatus }) {
  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, status: value?.toString() };
    });
    setSelectedValuesStatus(value);
  };
  const { Option } = Select;
  let listStatus = [];
  const handleNumberOption = () => {
    switch (numberStatus) {
      case 3: {
        listStatus = ['active', 'closed', 'cancelled'];
        return listStatus;
      }
      default: {
        listStatus = ['active', 'inactive'];
        return listStatus;
      }
    }
  };
  handleNumberOption();
  const listStatusOption = listStatus.map((item, key) => {
    return (
      <Option key={key} value={item}>
        {item.subjectName}
      </Option>
    );
  });

  return (
    <>
      <Select
        allowClear
        style={{
          minWidth: '100%'
        }}
        placeholder="Select Status"
        onChange={handleSelect}
        className={'border-radius-select'}
        filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
        value={selectedValuesStatus}
      >
        {listStatusOption}
        {/* <Option key={1} value={'active'}>
          Active
        </Option>
        <Option key={2} value={'inactive'}>
          Inactive
        </Option> */}
      </Select>
    </>
  );
}

SelectStatus.propTypes = {
  setParamsSearch: PropTypes.func,
  // selectedValuesStatus: PropTypes.string,
  setSelectedValuesStatus: PropTypes.func,
  numberStatus: PropTypes.number
};
