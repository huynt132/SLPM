import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getListClass } from 'src/pages/Dashboard/ClassDashboard/ClassListDashboard/classList.slice';

export default function SelectClass({
  setLoading,
  setParamsSearch,
  selectedValuesClass,
  setSelectedValuesClass,
  currentClass,
  mode
}) {
  const dispatch = useDispatch();
  const [listClass, setListClass] = useState([]);
  const getAllType = async params => {
    setLoading(true);
    const data = await dispatch(getListClass({ params }));
    const res = unwrapResult(data);
    setListClass(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getAllType('');
  }, []);

  const handleSelect = value => {
    if (value === undefined) {
      setSelectedValuesClass([]);
    } else {
      setParamsSearch(prevState => {
        return { ...prevState, classId: value?.toString() };
      });
      setSelectedValuesClass(value);
    }
  };

  const { Option } = Select;
  const setting = listClass.map((item, key) => {
    return (
      <Option key={key} value={item.value}>
        {item.label}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode={mode}
        allowClear
        style={{
          minWidth: '100%'
        }}
        placeholder="Select Class"
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectedValuesClass ? selectedValuesClass : currentClass}
      >
        {setting}
      </Select>
    </>
  );
}

SelectClass.propTypes = {
  setLoading: PropTypes.func,
  setParamsSearch: PropTypes.func,
  selectedValuesSetting: PropTypes.array,
  setSelectedValuesSetting: PropTypes.func,
  mode: PropTypes.string,
  currentClass: PropTypes.array
};

SelectClass.defaultProps = {
  mode: 'multiple'
};
