import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import PropTypes from 'prop-types';
import { getAllSubject } from 'src/pages/Dashboard/SubjectDashboard/SubjectManagement/subjectManagement.slice';

export default function SelectSubject({ selectSubject, setLoading, setSelectSubject, setParamsSearch, type }) {
  const dispatch = useDispatch();
  const [listSubject, setListSubject] = useState([]);

  const getListSubjectWithCondition = async params => {
    setLoading(true);
    const data = await dispatch(getAllSubject({ params }));
    const res = unwrapResult(data);
    setListSubject(res.data.data);
    setLoading(false);
  };

  useEffect(() => {
    getListSubjectWithCondition('');
  }, []);

  const handleSelect = value => {
    setParamsSearch(prevState => {
      return { ...prevState, subjectId: value };
    });
    setSelectSubject(value);
  };

  const { Option } = Select;
  const listSubjects = listSubject.map(item => {
    return (
      <Option key={item.id} value={item.id}>
        {item.subjectName}
      </Option>
    );
  });
  return (
    <>
      <Select
        mode="single"
        allowClear
        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        style={{
          minWidth: '100%',
          maxWidth: '100%'
        }}
        placeholder="Select Subject"
        onChange={handleSelect}
        className={'border-radius-select'}
        value={selectSubject}
      >
        {listSubjects}
      </Select>
    </>
  );
}

SelectSubject.propTypes = {
  setLoading: PropTypes.func,
  setSelectSubject: PropTypes.func,
  setParamsSearch: PropTypes.func,
  type: PropTypes.string
};
