// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getEvaluateOptions } from 'src/pages/Dashboard/ProjectDashboard/MilestoneSubmit/MilestoneSubmit.slice';
import { isEmpty } from '../../utils/helper';

// helper

export default function SelectLocEvaluation({
  placeholder,
  onOptionSelected,
  mode,
  customClassName,
  locEval,
  disabled,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...locEval] : locEval);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      if (isEmpty(filter.classId) || isEmpty(filter.type)) return 0;
      setLoading(true);
      const res = await dispatch(getEvaluateOptions(filter));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getOptions();
  }, [filter?.classId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...locEval] : locEval);
  }, [locEval]);

  const handleChange = (value, option) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(option);
  };

  return (
    <Select
      mode={mode}
      showSearch
      allowClear
      disabled={disabled || loading}
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectLocEvaluation.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  locEval: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectLocEvaluation.defaultProps = {
  placeholder: 'Select LocEval',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  locEval: []
};
