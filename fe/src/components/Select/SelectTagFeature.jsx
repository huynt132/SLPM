// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllFeatures } from 'src/pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';
import { isEmpty } from '../../utils/helper';

// helper

export default function SelectTagFeature({
  placeholder,
  onOptionSelected,
  disabled,
  features,
  mode,
  customClassName,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...features] : features);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      setOptions([]);
      const params = {
        projectId: filter?.projectId
      };
      const res = await dispatch(getAllFeatures(params));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (!isEmpty(filter?.projectId)) getOptions();
  }, [filter?.projectId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...features] : features);
  }, [features]);

  return (
    <Select
      disabled={disabled || loading}
      mode={mode}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={onOptionSelected}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectTagFeature.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.bool,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  features: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectTagFeature.defaultProps = {
  placeholder: 'Select Features',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  features: []
};
