// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getSubjectSettingTypes } from 'src/pages/Dashboard/SettingDashboard/SubjectSettingManagement/SubjectSettingManagement.slice';

// helper
import { isEmpty } from 'src/utils/helper';

export default function SelectSubjectSettingType({
  placeholder,
  disabled,
  onOptionSelected,
  selects,
  customClassName,
  mode
}) {
  const settingType = useSelector(state => state.subjectSettingManagement.settingType);

  const [selected, setSelected] = useState(mode === 'multiple' ? [...selects] : selects);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...selects] : selects);
  }, [selects]);

  async function getOptions() {
    try {
      setLoading(true);
      const res = await dispatch(getSubjectSettingTypes());
      setOptions(() => {
        return res.payload.data.data.map(subject => ({
          label: subject.value,
          value: subject.id
        }));
      });
      unwrapResult(res);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (!isEmpty(settingType)) setOptions(settingType);
    else getOptions();
  }, []);

  return (
    <Select
      mode={mode}
      disabled={disabled || loading}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={onOptionSelected}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectSubjectSettingType.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  selects: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectSubjectSettingType.defaultProps = {
  placeholder: 'Select Subject Setting Type',
  disabled: false,
  customClassName: '',
  mode: 'single',
  onOptionSelected: () => {},
  selects: []
};
