// React
import PropsTypes from 'prop-types';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select, Tag } from 'antd';
// service
import { getProjectLabel } from 'src/pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';
import { isEmpty } from '../../utils/helper';

// helper
const { Option } = Select;
export default function SelectLabel({
  placeholder,
  onOptionSelected,
  disabled,
  labels,
  mode,
  customClassName,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...labels] : labels);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const labelColor = useRef([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      onOptionSelected([]);
      setSelected([]);
      labelColor.current = [];
      const res = await dispatch(getProjectLabel(filter));
      const result = unwrapResult(res);
      setOptions(result.data.data);
      result.data.data.map(item => {
        labelColor.current[item.value] = item.color;
      });
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (!isEmpty(filter?.projectId)) getOptions();
  }, [filter?.projectId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...labels] : labels);
  }, [labels]);

  const tagRender = props => {
    const { label, value, closable, onClose } = props;
    const color = labelColor.current[value];

    const onPreventMouseDown = event => {
      event.preventDefault();
      event.stopPropagation();
    };

    return (
      <Tag
        color={color}
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{
          borderRadius: '2rem',
          marginRight: 3
        }}
      >
        {label}
      </Tag>
    );
  };

  return (
    <Select
      tagRender={tagRender}
      showArrow
      disabled={disabled || loading}
      mode={mode}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={onOptionSelected}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectLabel.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.bool,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  labels: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectLabel.defaultProps = {
  placeholder: 'Select Labels',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  labels: []
};
