// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllFunction } from 'src/pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';

// helper

export default function SelectFunction({ placeholder, onOptionSelected, disabled, mode, customClassName, functions }) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...functions] : functions);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      const params = {};
      const res = await dispatch(getAllFunction(params));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getOptions();
  }, [functions]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...functions] : functions);
  }, [functions]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      disabled={disabled || loading}
      mode={mode}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectFunction.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  functions: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectFunction.defaultProps = {
  placeholder: 'Select functions',
  disabled: false,
  mode: 'multiple',
  customClassName: '',
  onOptionSelected: () => {},
  functions: []
};
