// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getColumnType } from 'src/pages/Common/common.slice';
import { isEmpty } from '../../utils/helper';

export default function SelectCustomValue({
  placeholder,
  onOptionSelected,
  values,
  mode,
  customClassName,
  query,
  disabled
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...values] : values);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      const res = await dispatch(getColumnType(query));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getOptions();
  }, [dispatch]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...values] : values);
  }, [values]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      disabled={disabled || loading}
      mode={mode}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectCustomValue.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  values: PropsTypes.array,
  query: PropsTypes.object,
  disabled: PropsTypes.bool,
  customClassName: PropsTypes.string
};
SelectCustomValue.defaultProps = {
  placeholder: 'Select status',
  mode: 'multiple',
  customClassName: '',
  query: {},
  disabled: false,
  onOptionSelected: () => {},
  values: []
};
