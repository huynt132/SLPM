// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllUsers } from 'src/pages/Common/common.slice';
// helper

export default function SelectUser({ placeholder, onOptionSelected, disabled, mode, customClassName, users, filter }) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...users] : users);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      const res = await dispatch(getAllUsers(filter));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getOptions();
  }, [filter?.projectId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...users] : users);
  }, [users]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      disabled={disabled || loading}
      mode={mode}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectUser.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  users: PropsTypes.array,
  filter: PropsTypes.object,
  customClassName: PropsTypes.string
};
SelectUser.defaultProps = {
  placeholder: 'Select users',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  filter: {},
  onOptionSelected: () => {},
  users: []
};
