// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllFunctionType } from 'src/pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';
import { isEmpty } from '../../utils/helper';

// helper

export default function SelectFunctionType({
  placeholder,
  onOptionSelected,
  mode,
  customClassName,
  functionType,
  disabled,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...functionType] : functionType);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      setOptions([]);
      let params = {
        projectId: filter?.projectId
      };
      const res = await dispatch(getAllFunctionType({ ...params }));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getOptions();
  }, [filter?.projectId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...functionType] : functionType);
  }, [functionType]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      mode={mode}
      showSearch
      allowClear
      disabled={disabled || loading}
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectFunctionType.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  functionType: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectFunctionType.defaultProps = {
  placeholder: 'Select functionType',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  functionType: []
};
