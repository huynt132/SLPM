// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllSubject } from 'src/pages/Dashboard/SubjectDashboard/SubjectManagement/subjectManagement.slice';

// helper

export default function SelectSubject({ placeholder, disabled, onOptionSelected, subjects, mode, customClassName }) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...subjects] : subjects);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      const res = await dispatch(getAllSubject());
      const result = unwrapResult(res);
      setOptions(() => {
        return result.data.data.map(subject => ({
          label: subject.subjectName,
          value: subject.id
        }));
      });
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getOptions();
  }, []);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...subjects] : subjects);
  }, [subjects]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      mode={mode}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      disabled={disabled || loading}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectSubject.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  subjects: PropsTypes.array,
  customClassName: PropsTypes.string,
  currentSubject: PropsTypes.number
};
SelectSubject.defaultProps = {
  placeholder: 'Select Subjects',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  subjects: []
};
