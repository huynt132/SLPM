// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllProjects } from 'src/pages/Dashboard/ProjectDashboard/ProjectManagement/projectManagement.slice';
import { isEmpty } from '../../utils/helper';

// helper

export default function SelectProject({
  placeholder,
  onOptionSelected,
  mode,
  customClassName,
  projects,
  disabled,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...projects] : projects);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);
  const firstCall = useRef(true); //ref dùng để check lần đầu tiên render chỉ gọi api getOption 1 lần

  async function getOptions() {
    try {
      setLoading(true);
      setOptions([]);
      let params = {
        classId: filter?.classId,
        subjectId: filter?.subjectId
      };
      const res = await dispatch(getAllProjects({ ...params }));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (firstCall.current) {
      firstCall.current = false;
      return;
    }
    getOptions();
  }, [filter?.subjectId]);

  useEffect(() => {
    if (firstCall.current) return;
    getOptions();
  }, [filter?.classId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...projects] : projects);
  }, [projects]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      mode={mode}
      showSearch
      allowClear
      disabled={disabled || loading}
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectProject.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  projects: PropsTypes.array,
  customClassName: PropsTypes.string,
  currentProject: PropsTypes.number
};
SelectProject.defaultProps = {
  placeholder: 'Select projects',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  projects: []
};
