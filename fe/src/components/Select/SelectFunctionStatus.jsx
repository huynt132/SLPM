// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllFunctionStatus } from 'src/pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';
import { isEmpty } from '../../utils/helper';

// helper

export default function SelectFunctionStatus({
  placeholder,
  onOptionSelected,
  mode,
  customClassName,
  functionStatus,
  disabled,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...functionStatus] : functionStatus);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      setOptions([]);
      let params = {
        projectId: filter?.projectId
      };
      const res = await dispatch(getAllFunctionStatus({ ...params }));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (!isEmpty(filter?.projectId)) getOptions();
  }, [filter?.projectId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...functionStatus] : functionStatus);
  }, [functionStatus]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      mode={mode}
      showSearch
      allowClear
      disabled={disabled || loading}
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectFunctionStatus.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  functionStatus: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectFunctionStatus.defaultProps = {
  placeholder: 'Select functionStatus',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  functionStatus: []
};
