// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllIteration } from 'src/pages/Dashboard/SubjectDashboard/IterationManagement/iterationManagement.slice';

// helper

export default function SelectIteration({
  placeholder,
  disabled,
  onOptionSelected,
  iterations,
  mode,
  customClassName,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...iterations] : iterations);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);
  const firstCall = useRef(true); //ref dùng để check lần đầu tiên render chỉ gọi api getOption 1 lần

  async function getOptions() {
    try {
      setLoading(true);
      const params = {
        subjectId: filter?.subjectId,
        classId: filter?.classId,
        status: filter?.status
      };
      const res = await dispatch(getAllIteration(params));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (firstCall.current) return;
    getOptions();
  }, [filter?.status]);

  useEffect(() => {
    if (firstCall.current) {
      firstCall.current = false;
      return;
    }
    getOptions();
  }, [filter?.classId]);

  useEffect(() => {
    if (firstCall.current) return;
    getOptions();
  }, [filter?.subjectId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...iterations] : iterations);
  }, [iterations]);

  return (
    <Select
      disabled={disabled || loading}
      mode={mode}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={onOptionSelected}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectIteration.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.bool,
  onOptionSelected: PropsTypes.func,
  iterations: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectIteration.defaultProps = {
  placeholder: 'Select Iterations',
  mode: 'multiple',
  customClassName: '',
  disabled: false,
  onOptionSelected: () => {},
  iterations: []
};
