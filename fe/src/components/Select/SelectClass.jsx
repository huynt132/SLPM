// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllClasses } from 'src/pages/Dashboard/SubjectDashboard/MilestoneManagement/milestoneManagement.slice';

export default function SelectClass({
  placeholder,
  onOptionSelected,
  classes,
  mode,
  customClassName,
  filter,
  disabled
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...classes] : classes);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  const firstCall = useRef(true); //ref dùng để check lần đầu tiên render chỉ gọi api getOption 1 lần

  async function getOptions() {
    try {
      setLoading(true);
      const params = {
        subjectId: filter?.subjectId
      };
      const res = await dispatch(getAllClasses(params));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...classes] : classes);
  }, [classes]);

  useEffect(() => {
    getOptions();
  }, [filter?.subjectId]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      mode={mode}
      showSearch
      disabled={disabled || loading}
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectClass.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  classes: PropsTypes.array,
  filter: PropsTypes.object,
  customClassName: PropsTypes.string
};
SelectClass.defaultProps = {
  placeholder: 'Select Classes',
  mode: 'multiple',
  customClassName: '',
  disabled: false,
  onOptionSelected: () => {},
  classes: [],
  filter: []
};
