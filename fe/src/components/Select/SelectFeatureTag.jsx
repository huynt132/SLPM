// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getAllFeatures } from 'src/pages/Dashboard/ProjectDashboard/FunctionManagement/functionManagement.slice';
import { isEmpty } from '../../utils/helper';

// helper
const { Option } = Select;
export default function SelectFeatureTag({
  placeholder,
  onOptionSelected,
  disabled,
  features,
  customClassName,
  filter
}) {
  const [selected, setSelected] = useState(isEmpty(features) ? [] : Array.isArray(features) ? features : [features]);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      setOptions([]);
      const params = {
        projectId: filter?.projectId
      };
      const res = await dispatch(getAllFeatures(params));
      const result = unwrapResult(res);
      let opts = result.data.data.map(item => {
        return item.label;
      });
      setOptions(opts);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (!isEmpty(filter?.projectId)) getOptions();
  }, [filter?.projectId]);

  useEffect(() => {
    setSelected(isEmpty(features) ? [] : Array.isArray(features) ? features : [features]);
  }, [features]);

  return (
    <Select
      disabled={disabled || loading}
      mode={'tags'}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      // filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={onOptionSelected}
      loading={loading}
      className={customClassName}
    >
      {options.map(item => (
        <Option
          disabled={selected && selected.length > 0 ? (selected.includes(item) ? false : true) : false}
          key={item}
        >
          {item}
        </Option>
      ))}
    </Select>
  );
}

SelectFeatureTag.prototype = {
  placeholder: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  features: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectFeatureTag.defaultProps = {
  placeholder: 'Select Features',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  features: []
};
