// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
// service
import { getSubjectSetting } from 'src/pages/Dashboard/SubjectDashboard/SubjectManagement/subjectManagement.slice';
import { isEmpty } from '../../utils/helper';

// helper

export default function SelectEvaluate({
  placeholder,
  onOptionSelected,
  mode,
  customClassName,
  evaluates,
  disabled,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...evaluates] : evaluates);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getOptions() {
    try {
      setLoading(true);
      setOptions([]);
      let params = {
        classId: filter?.classId,
        type: filter.type
      };
      const res = await dispatch(getSubjectSetting({ ...params }));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (!isEmpty(filter?.classId)) getOptions();
  }, [filter?.classId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...evaluates] : evaluates);
  }, [evaluates]);

  const handleChange = (value, option) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(option);
  };

  return (
    <Select
      mode={mode}
      showSearch
      allowClear
      disabled={disabled || loading}
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectEvaluate.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  evaluates: PropsTypes.array,
  customClassName: PropsTypes.string,
  currentEvaluate: PropsTypes.number
};
SelectEvaluate.defaultProps = {
  placeholder: 'Select evaluates',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  evaluates: []
};
