// React
import PropsTypes from 'prop-types';
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import { Select } from 'antd';
import { getAllMilestones } from 'src/pages/Dashboard/SubjectDashboard/MilestoneManagement/milestoneManagement.slice';

// service
// helper

export default function SelectMilestone({
  placeholder,
  onOptionSelected,
  disabled,
  mode,
  customClassName,
  milestones,
  filter
}) {
  const [selected, setSelected] = useState(mode === 'multiple' ? [...milestones] : milestones);
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);
  const firstCall = useRef(true); //ref dùng để check lần đầu tiên render chỉ gọi api getOption 1 lần

  async function getOptions() {
    try {
      setLoading(true);
      const params = {
        projectId: filter?.projectId,
        classId: filter?.classId
      };
      const res = await dispatch(getAllMilestones(params));
      const result = unwrapResult(res);
      setOptions(result.data.data);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (firstCall.current) {
      firstCall.current = false;
      return;
    }
    getOptions();
  }, [filter?.classId]);

  useEffect(() => {
    if (firstCall.current) return;
    getOptions();
  }, [filter?.projectId]);

  useEffect(() => {
    setSelected(mode === 'multiple' ? [...milestones] : milestones);
  }, [milestones]);

  const handleChange = (value: { value: string, label: React.ReactNode }) => {
    setSelected(mode === 'multiple' ? [...value] : value);
    onOptionSelected(value);
  };

  return (
    <Select
      mode={mode}
      disabled={disabled || loading}
      showSearch
      allowClear
      style={{
        width: '100%'
      }}
      value={selected}
      filterOption={(input, option) => option.label.toLowerCase().includes(input.toLowerCase())}
      placeholder={placeholder}
      onChange={handleChange}
      options={options}
      loading={loading}
      className={customClassName}
    ></Select>
  );
}

SelectMilestone.prototype = {
  placeholder: PropsTypes.string,
  mode: PropsTypes.string,
  disabled: PropsTypes.string,
  onOptionSelected: PropsTypes.func,
  milestones: PropsTypes.array,
  customClassName: PropsTypes.string
};
SelectMilestone.defaultProps = {
  placeholder: 'Select milestones',
  mode: 'multiple',
  disabled: false,
  customClassName: '',
  onOptionSelected: () => {},
  milestones: []
};
