import styled from 'styled-components';

export const FormControl = styled.div`
  input {
    // outline: none;
    // border: 1px solid #e9ecef;
    // padding: 1.2rem;
    // flex-grow: 1;
    // width: 100%;
    // border-radius: 5px;
    // transition: ease 0.3s;
    width: 100%;
    padding: 1.6rem 4.8rem;
    font-size: 1.6rem;
    background-color: #e9ecef;
    border-radius: 0.8rem;
    border: 0.125rem solid var(--white);
    transition: all 0.2s ease-in-out;
  }
  input:focus {
    // outline: none;
    // border-color: #396cf0;
    // background-color: #fff;
    border: 0.125rem solid var(--primary-color);
  }
`;
