import React, { useState } from 'react';
import { BiMailSend, BiLockAlt, BiUser } from 'react-icons/bi';
import { AiOutlineFieldNumber } from 'react-icons/ai';
import { FaBirthdayCake } from 'react-icons/fa';
import { BsGenderMale, BsGenderFemale } from 'react-icons/bs';
import * as styles from './inputText.style';

export default function InputText({ value, disabled, iconname, ...props }) {
  const [focus, setFocus] = useState(false);

  const getIconFromName = name => {
    switch (name) {
      case 'mail':
        return <BiMailSend />;
      case 'lock':
        return <BiLockAlt />;
      case 'user':
        return <BiUser />;
      case 'rollNumber':
        return <AiOutlineFieldNumber />;
      case 'dateOfBirth':
        return <FaBirthdayCake />;
      case 'male':
        return <BsGenderMale />;
      case 'female':
        return <BsGenderFemale />;
      default:
        return '';
    }
  };

  return (
    <styles.FormControl focus={focus}>
      {getIconFromName(iconname)}
      <input
        value={value || ''}
        {...props}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        disabled={disabled}
      />
    </styles.FormControl>
  );
}
