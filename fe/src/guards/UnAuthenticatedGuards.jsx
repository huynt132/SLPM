import React from 'react';
import { Navigate } from 'react-router-dom';
import { path } from 'src/constants/path';
import useAuthenticated from 'src/hooks/useAuthenticated';
import PropTypes from 'prop-types';

export default function UnAuthenticatedGuards({ children }) {
  const authenticated = useAuthenticated();

  if (authenticated) {
    return <Navigate to={path.classDash} />;
  }

  return <>{children}</>;
}

UnAuthenticatedGuards.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)])
};
