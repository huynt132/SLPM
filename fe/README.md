# SWP490_G2

## How to run this project?

1. Requirement:
- Any computer run Ubuntu operating system.
- Connected to internet.
- Ram size greater than equal to than 4GB.
- Storage size greater than equal to 4GB.

2. Installation:
- Install docker engine on ubuntu.
- Run this following command.

```shell
docker-compose up
```